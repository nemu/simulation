Dateienverzeichnis

TrimSp3L.f : Fortran Code von TrimSp
             rechnet 3 Layer mit jeweils maximal 5 Elementen
             100 Stuetzstellen
TrimSp3L.exe : executable

Datmak3L   : Fortran Code zum Erstellen von input Dateien fuer TrimSp3L Rechnungen
             benutzt Stopping power nach ICRU (DateiL Stopicru.dat)
             wenn man andere Stopping powers nehmen moechte, muss man das im
             Fortran Code aendern

dichte.dat : Dichte der Elemente
masse.dat  : Masse der Elemente
elast.dat  : Festkoerperbindungsenergie der Elemente
stopicru   : Energieverlust von p in den Elementen, hier nach ICRU
stopping   : Energieverlust von p in den Elementen, hier nach AZ

generelle Beschreibung von TrimSp findet man in der Datei TRVMC95-3L.txt