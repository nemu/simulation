#!/usr/bin/perl

use strict;
use warnings;

use QtCore4;
use QtGui4;
use TrimSPGUI4;

sub main {
    my $app = Qt::Application(\@ARGV);
    my $trimsp = TrimSPGUI4();
    $trimsp->show();
    exit $app->exec();
}

main();
