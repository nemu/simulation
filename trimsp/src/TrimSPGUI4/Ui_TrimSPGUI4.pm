###############################################################################
# 
###############################################################################

#################################################################################
## Form generated from reading UI file 'TrimSPGUI4.ui'
##
## Created: Thu Aug 27 10:51:18 2015
##      by: Qt User Interface Compiler version 4.8.6
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

package Ui_TrimSPGUI4;

use strict;
use warnings;
use QtCore4;
use QtGui4;

sub fileSaveAction {
    return shift->{fileSaveAction};
}

sub fileSaveAsAction {
    return shift->{fileSaveAsAction};
}

sub fileChangeDirAction {
    return shift->{fileChangeDirAction};
}

sub fileQuitAction {
    return shift->{fileQuitAction};
}

sub plotProfilesAction {
    return shift->{plotProfilesAction};
}

sub helpContentsAction {
    return shift->{helpContentsAction};
}

sub helpAboutAction {
    return shift->{helpAboutAction};
}

sub plotFractionsAction {
    return shift->{plotFractionsAction};
}

sub fileStartAction {
    return shift->{fileStartAction};
}

sub fileOpenAction {
    return shift->{fileOpenAction};
}

sub plotMeanAction {
    return shift->{plotMeanAction};
}

sub widget {
    return shift->{widget};
}

sub gridLayout {
    return shift->{gridLayout};
}

sub progress {
    return shift->{progress};
}

sub tabs {
    return shift->{tabs};
}

sub layersTab {
    return shift->{layersTab};
}

sub groupLayer {
    return shift->{groupLayer};
}

sub layoutWidget {
    return shift->{layoutWidget};
}

sub layerLayout {
    return shift->{layerLayout};
}

sub textLabel1_4 {
    return shift->{textLabel1_4};
}

sub numLayer {
    return shift->{numLayer};
}

sub spacer2 {
    return shift->{spacer2};
}

sub layerTable {
    return shift->{layerTable};
}

sub projParam {
    return shift->{projParam};
}

sub z0Label {
    return shift->{z0Label};
}

sub numberLabel {
    return shift->{numberLabel};
}

sub projComboBox {
    return shift->{projComboBox};
}

sub dz {
    return shift->{dz};
}

sub energyLabel {
    return shift->{energyLabel};
}

sub numberProj {
    return shift->{numberProj};
}

sub textLabel1 {
    return shift->{textLabel1};
}

sub valAngle {
    return shift->{valAngle};
}

sub sigAngleLabel {
    return shift->{sigAngleLabel};
}

sub sigAngle {
    return shift->{sigAngle};
}

sub seedLabel {
    return shift->{seedLabel};
}

sub sigEnergy {
    return shift->{sigEnergy};
}

sub z0 {
    return shift->{z0};
}

sub valEnergy {
    return shift->{valEnergy};
}

sub ranSeed {
    return shift->{ranSeed};
}

sub albleLabel {
    return shift->{albleLabel};
}

sub sigELabel {
    return shift->{sigELabel};
}

sub dzLabel {
    return shift->{dzLabel};
}

sub groupBox15 {
    return shift->{groupBox15};
}

sub layout14 {
    return shift->{layout14};
}

sub fileNames {
    return shift->{fileNames};
}

sub textLabelFN {
    return shift->{textLabelFN};
}

sub fileNamePrefix {
    return shift->{fileNamePrefix};
}

sub textLabelPath {
    return shift->{textLabelPath};
}

sub outputDirectory {
    return shift->{outputDirectory};
}

sub workPath {
    return shift->{workPath};
}

sub browse {
    return shift->{browse};
}

sub splitter {
    return shift->{splitter};
}

sub start {
    return shift->{start};
}

sub quit {
    return shift->{quit};
}

sub addParTab {
    return shift->{addParTab};
}

sub addParam {
    return shift->{addParam};
}

sub textLabelEmpty {
    return shift->{textLabelEmpty};
}

sub textLabelEF {
    return shift->{textLabelEF};
}

sub parKK0R {
    return shift->{parKK0R};
}

sub parCA {
    return shift->{parCA};
}

sub textLabelIRL {
    return shift->{textLabelIRL};
}

sub parKK0 {
    return shift->{parKK0};
}

sub textLabelKK0 {
    return shift->{textLabelKK0};
}

sub textLabelRD {
    return shift->{textLabelRD};
}

sub parIRL {
    return shift->{parIRL};
}

sub textLabelIPOTR {
    return shift->{textLabelIPOTR};
}

sub parIPOTR {
    return shift->{parIPOTR};
}

sub textLabelESB {
    return shift->{textLabelESB};
}

sub parRD {
    return shift->{parRD};
}

sub textLabelCA {
    return shift->{textLabelCA};
}

sub parESB {
    return shift->{parESB};
}

sub textLabelSHEATH {
    return shift->{textLabelSHEATH};
}

sub textLabelKDEE2 {
    return shift->{textLabelKDEE2};
}

sub textLabelKDEE1 {
    return shift->{textLabelKDEE1};
}

sub parEF {
    return shift->{parEF};
}

sub lineEditEmpty {
    return shift->{lineEditEmpty};
}

sub parKDEE2 {
    return shift->{parKDEE2};
}

sub parERC {
    return shift->{parERC};
}

sub textLabelKK0R {
    return shift->{textLabelKK0R};
}

sub textLabelIPOT {
    return shift->{textLabelIPOT};
}

sub parIPOT {
    return shift->{parIPOT};
}

sub textLabelERC {
    return shift->{textLabelERC};
}

sub parSHEATH {
    return shift->{parSHEATH};
}

sub parKDEE1 {
    return shift->{parKDEE1};
}

sub scansTab {
    return shift->{scansTab};
}

sub scanSeq {
    return shift->{scanSeq};
}

sub layoutWidget1 {
    return shift->{layoutWidget1};
}

sub verticalLayout {
    return shift->{verticalLayout};
}

sub horizontalLayout_3 {
    return shift->{horizontalLayout_3};
}

sub label {
    return shift->{label};
}

sub comboScan {
    return shift->{comboScan};
}

sub scandL {
    return shift->{scandL};
}

sub radioList {
    return shift->{radioList};
}

sub scanList {
    return shift->{scanList};
}

sub dzListLabel {
    return shift->{dzListLabel};
}

sub scanListdz {
    return shift->{scanListdz};
}

sub radioLoop {
    return shift->{radioLoop};
}

sub scanLoop {
    return shift->{scanLoop};
}

sub textLabel2 {
    return shift->{textLabel2};
}

sub scanFrom {
    return shift->{scanFrom};
}

sub textLabel2_2 {
    return shift->{textLabel2_2};
}

sub scanTo {
    return shift->{scanTo};
}

sub textLabel2_2_2_2 {
    return shift->{textLabel2_2_2_2};
}

sub scanStep {
    return shift->{scanStep};
}

sub configTab {
    return shift->{configTab};
}

sub layoutWidget2 {
    return shift->{layoutWidget2};
}

sub verticalLayout_2 {
    return shift->{verticalLayout_2};
}

sub horizontalLayout {
    return shift->{horizontalLayout};
}

sub label_2 {
    return shift->{label_2};
}

sub trimbin {
    return shift->{trimbin};
}

sub trimBinBrowse {
    return shift->{trimBinBrowse};
}

sub horizontalLayout_2 {
    return shift->{horizontalLayout_2};
}

sub label_3 {
    return shift->{label_3};
}

sub pathTrimGUI {
    return shift->{pathTrimGUI};
}

sub guiPathBrowse {
    return shift->{guiPathBrowse};
}

sub menuBar {
    return shift->{menuBar};
}

sub file {
    return shift->{file};
}

sub plot {
    return shift->{plot};
}

sub helpMenu {
    return shift->{helpMenu};
}


sub setupUi {
    my ( $class, $trimSPGUI4 ) = @_;
    my $self = bless {}, $class;
    if ( !defined $trimSPGUI4->objectName() ) {
        $trimSPGUI4->setObjectName( "trimSPGUI4" );
    }
    $trimSPGUI4->resize( 712, 622 );
    my $sizePolicy = Qt::SizePolicy( Qt::SizePolicy::Fixed(), Qt::SizePolicy::Fixed() );
    $self->{$sizePolicy} = $sizePolicy;
    $sizePolicy->setHorizontalStretch( 0 );
    $sizePolicy->setVerticalStretch( 0 );
    $sizePolicy->setHeightForWidth( $trimSPGUI4->sizePolicy()->hasHeightForWidth() );
    $trimSPGUI4->setSizePolicy( $sizePolicy );
    my $icon = Qt::Icon();
    $icon->addPixmap(Qt::Pixmap("../icon.png"), Qt::Icon::Normal(), Qt::Icon::Off() );
    $trimSPGUI4->setWindowIcon( $icon );
    my $fileSaveAction = Qt::Action($trimSPGUI4);
    $self->{fileSaveAction} = $fileSaveAction;
    $fileSaveAction->setObjectName( "fileSaveAction" );
    $fileSaveAction->setIconVisibleInMenu( 1 );
    my $fileSaveAsAction = Qt::Action($trimSPGUI4);
    $self->{fileSaveAsAction} = $fileSaveAsAction;
    $fileSaveAsAction->setObjectName( "fileSaveAsAction" );
    my $fileChangeDirAction = Qt::Action($trimSPGUI4);
    $self->{fileChangeDirAction} = $fileChangeDirAction;
    $fileChangeDirAction->setObjectName( "fileChangeDirAction" );
    my $fileQuitAction = Qt::Action($trimSPGUI4);
    $self->{fileQuitAction} = $fileQuitAction;
    $fileQuitAction->setObjectName( "fileQuitAction" );
    my $plotProfilesAction = Qt::Action($trimSPGUI4);
    $self->{plotProfilesAction} = $plotProfilesAction;
    $plotProfilesAction->setObjectName( "plotProfilesAction" );
    my $icon1 = Qt::Icon();
    $icon1->addPixmap(Qt::Pixmap("../PlotProfiles.png"), Qt::Icon::Normal(), Qt::Icon::Off() );
    $plotProfilesAction->setIcon( $icon1 );
    my $helpContentsAction = Qt::Action($trimSPGUI4);
    $self->{helpContentsAction} = $helpContentsAction;
    $helpContentsAction->setObjectName( "helpContentsAction" );
    my $icon2 = Qt::Icon();
    $icon2->addPixmap(Qt::Pixmap("../../.designer/backup/image13"), Qt::Icon::Normal(), Qt::Icon::Off() );
    $helpContentsAction->setIcon( $icon2 );
    my $helpAboutAction = Qt::Action($trimSPGUI4);
    $self->{helpAboutAction} = $helpAboutAction;
    $helpAboutAction->setObjectName( "helpAboutAction" );
    my $icon3 = Qt::Icon();
    $icon3->addPixmap(Qt::Pixmap("../../.designer/backup/image0"), Qt::Icon::Normal(), Qt::Icon::Off() );
    $helpAboutAction->setIcon( $icon3 );
    my $plotFractionsAction = Qt::Action($trimSPGUI4);
    $self->{plotFractionsAction} = $plotFractionsAction;
    $plotFractionsAction->setObjectName( "plotFractionsAction" );
    $plotFractionsAction->setCheckable( 0 );
    $plotFractionsAction->setChecked( 0 );
    $plotFractionsAction->setEnabled( 1 );
    my $icon4 = Qt::Icon();
    $icon4->addPixmap(Qt::Pixmap("../PlotFraction.png"), Qt::Icon::Normal(), Qt::Icon::Off() );
    $plotFractionsAction->setIcon( $icon4 );
    my $fileStartAction = Qt::Action($trimSPGUI4);
    $self->{fileStartAction} = $fileStartAction;
    $fileStartAction->setObjectName( "fileStartAction" );
    my $fileOpenAction = Qt::Action($trimSPGUI4);
    $self->{fileOpenAction} = $fileOpenAction;
    $fileOpenAction->setObjectName( "fileOpenAction" );
    my $plotMeanAction = Qt::Action($trimSPGUI4);
    $self->{plotMeanAction} = $plotMeanAction;
    $plotMeanAction->setObjectName( "plotMeanAction" );
    my $icon5 = Qt::Icon();
    $icon5->addPixmap(Qt::Pixmap("../PlotMean.png"), Qt::Icon::Normal(), Qt::Icon::Off() );
    $plotMeanAction->setIcon( $icon5 );
    my $widget = Qt::Widget( $trimSPGUI4 );
    $self->{widget} = $widget;
    $widget->setObjectName( "widget" );
    my $gridLayout = Qt::GridLayout( $widget );
    $self->{gridLayout} = $gridLayout;
    $gridLayout->setSpacing( 6 );
    $gridLayout->setMargin( 11 );
    $gridLayout->setObjectName( "gridLayout" );
    my $progress = Qt::ProgressBar( $widget );
    $self->{progress} = $progress;
    $progress->setObjectName( "progress" );
    $progress->setValue( 0 );

    $gridLayout->addWidget( $progress, 1, 0, 1, 1 );

    my $tabs = Qt::TabWidget( $widget );
    $self->{tabs} = $tabs;
    $tabs->setObjectName( "tabs" );
    my $layersTab = Qt::Widget(  );
    $self->{layersTab} = $layersTab;
    $layersTab->setObjectName( "layersTab" );
    my $groupLayer = Qt::GroupBox( $layersTab );
    $self->{groupLayer} = $groupLayer;
    $groupLayer->setObjectName( "groupLayer" );
    $groupLayer->setGeometry( Qt::Rect(10, 10, 431, 511) );
    my $layoutWidget = Qt::Widget( $groupLayer );
    $self->{layoutWidget} = $layoutWidget;
    $layoutWidget->setObjectName( "layoutWidget" );
    $layoutWidget->setGeometry( Qt::Rect(10, 21, 411, 481) );
    my $layerLayout = Qt::GridLayout( $layoutWidget );
    $self->{layerLayout} = $layerLayout;
    $layerLayout->setSpacing( 6 );
    $layerLayout->setMargin( 11 );
    $layerLayout->setObjectName( "layerLayout" );
    $layerLayout->setContentsMargins(0, 0, 0, 0 );
    my $textLabel1_4 = Qt::Label( $layoutWidget );
    $self->{textLabel1_4} = $textLabel1_4;
    $textLabel1_4->setObjectName( "textLabel1_4" );
    $textLabel1_4->setWordWrap( 0 );

    $layerLayout->addWidget( $textLabel1_4, 0, 0, 1, 1 );

    my $numLayer = Qt::SpinBox( $layoutWidget );
    $self->{numLayer} = $numLayer;
    $numLayer->setObjectName( "numLayer" );
    $numLayer->setMinimum( 1 );
    $numLayer->setMaximum( 100 );

    $layerLayout->addWidget( $numLayer, 0, 1, 1, 1 );

    my $spacer2 = Qt::SpacerItem( 240, 20, Qt::SizePolicy::Expanding(), Qt::SizePolicy::Minimum() );

    $layerLayout->addItem( $spacer2, 0, 2, 1, 1 );

    my $layerTable = Qt::TableWidget( $layoutWidget );
    $self->{layerTable} = $layerTable;
    $layerTable->setObjectName( "layerTable" );
    my $sizePolicy1 = Qt::SizePolicy( Qt::SizePolicy::Expanding(), Qt::SizePolicy::Expanding() );
    $self->{$sizePolicy1} = $sizePolicy1;
    $sizePolicy1->setHorizontalStretch( 0 );
    $sizePolicy1->setVerticalStretch( 0 );
    $sizePolicy1->setHeightForWidth( $layerTable->sizePolicy()->hasHeightForWidth() );
    $layerTable->setSizePolicy( $sizePolicy1 );
    $layerTable->setMinimumSize( Qt::Size(409, 0) );
    $layerTable->setRowCount( 1 );
    $layerTable->setColumnCount( 3 );

    $layerLayout->addWidget( $layerTable, 1, 0, 1, 3 );

    my $projParam = Qt::GroupBox( $layersTab );
    $self->{projParam} = $projParam;
    $projParam->setObjectName( "projParam" );
    $projParam->setGeometry( Qt::Rect(450, 10, 240, 241) );
    $sizePolicy->setHeightForWidth( $projParam->sizePolicy()->hasHeightForWidth() );
    $projParam->setSizePolicy( $sizePolicy );
    my $z0Label = Qt::Label( $projParam );
    $self->{z0Label} = $z0Label;
    $z0Label->setObjectName( "z0Label" );
    $z0Label->setGeometry( Qt::Rect(11, 68, 113, 16) );
    $z0Label->setWordWrap( 0 );
    my $numberLabel = Qt::Label( $projParam );
    $self->{numberLabel} = $numberLabel;
    $numberLabel->setObjectName( "numberLabel" );
    $numberLabel->setGeometry( Qt::Rect(11, 45, 134, 16) );
    my $sizePolicy2 = Qt::SizePolicy( Qt::SizePolicy::Preferred(), Qt::SizePolicy::Preferred() );
    $self->{$sizePolicy2} = $sizePolicy2;
    $sizePolicy2->setHorizontalStretch( 0 );
    $sizePolicy2->setVerticalStretch( 0 );
    $sizePolicy2->setHeightForWidth( $numberLabel->sizePolicy()->hasHeightForWidth() );
    $numberLabel->setSizePolicy( $sizePolicy2 );
    $numberLabel->setWordWrap( 0 );
    my $projComboBox = Qt::ComboBox( $projParam );
    $self->{projComboBox} = $projComboBox;
    $projComboBox->setObjectName( "projComboBox" );
    $projComboBox->setGeometry( Qt::Rect(151, 21, 80, 20) );
    my $sizePolicy3 = Qt::SizePolicy( Qt::SizePolicy::Ignored(), Qt::SizePolicy::Preferred() );
    $self->{$sizePolicy3} = $sizePolicy3;
    $sizePolicy3->setHorizontalStretch( 0 );
    $sizePolicy3->setVerticalStretch( 0 );
    $sizePolicy3->setHeightForWidth( $projComboBox->sizePolicy()->hasHeightForWidth() );
    $projComboBox->setSizePolicy( $sizePolicy3 );
    my $dz = Qt::LineEdit( $projParam );
    $self->{dz} = $dz;
    $dz->setObjectName( "dz" );
    $dz->setGeometry( Qt::Rect(151, 92, 80, 20) );
    $sizePolicy3->setHeightForWidth( $dz->sizePolicy()->hasHeightForWidth() );
    $dz->setSizePolicy( $sizePolicy3 );
    my $energyLabel = Qt::Label( $projParam );
    $self->{energyLabel} = $energyLabel;
    $energyLabel->setObjectName( "energyLabel" );
    $energyLabel->setGeometry( Qt::Rect(11, 115, 73, 16) );
    $energyLabel->setWordWrap( 0 );
    my $numberProj = Qt::LineEdit( $projParam );
    $self->{numberProj} = $numberProj;
    $numberProj->setObjectName( "numberProj" );
    $numberProj->setGeometry( Qt::Rect(151, 45, 80, 20) );
    $sizePolicy3->setHeightForWidth( $numberProj->sizePolicy()->hasHeightForWidth() );
    $numberProj->setSizePolicy( $sizePolicy3 );
    $numberProj->setMaximumSize( Qt::Size(32767, 32767) );
    $numberProj->setWhatsThis( "" );
    $numberProj->setAccessibleName( "" );
    $numberProj->setInputMethodHints( Qt::ImhDigitsOnly()|Qt::ImhPreferNumbers() );
    my $textLabel1 = Qt::Label( $projParam );
    $self->{textLabel1} = $textLabel1;
    $textLabel1->setObjectName( "textLabel1" );
    $textLabel1->setGeometry( Qt::Rect(11, 21, 58, 16) );
    $textLabel1->setWordWrap( 0 );
    my $valAngle = Qt::LineEdit( $projParam );
    $self->{valAngle} = $valAngle;
    $valAngle->setObjectName( "valAngle" );
    $valAngle->setGeometry( Qt::Rect(151, 162, 80, 21) );
    $sizePolicy3->setHeightForWidth( $valAngle->sizePolicy()->hasHeightForWidth() );
    $valAngle->setSizePolicy( $sizePolicy3 );
    my $sigAngleLabel = Qt::Label( $projParam );
    $self->{sigAngleLabel} = $sigAngleLabel;
    $sigAngleLabel->setObjectName( "sigAngleLabel" );
    $sigAngleLabel->setGeometry( Qt::Rect(11, 186, 114, 16) );
    $sigAngleLabel->setWordWrap( 0 );
    my $sigAngle = Qt::LineEdit( $projParam );
    $self->{sigAngle} = $sigAngle;
    $sigAngle->setObjectName( "sigAngle" );
    $sigAngle->setGeometry( Qt::Rect(151, 186, 80, 20) );
    $sizePolicy3->setHeightForWidth( $sigAngle->sizePolicy()->hasHeightForWidth() );
    $sigAngle->setSizePolicy( $sizePolicy3 );
    my $seedLabel = Qt::Label( $projParam );
    $self->{seedLabel} = $seedLabel;
    $seedLabel->setObjectName( "seedLabel" );
    $seedLabel->setGeometry( Qt::Rect(11, 209, 86, 16) );
    $seedLabel->setWordWrap( 0 );
    my $sigEnergy = Qt::LineEdit( $projParam );
    $self->{sigEnergy} = $sigEnergy;
    $sigEnergy->setObjectName( "sigEnergy" );
    $sigEnergy->setGeometry( Qt::Rect(151, 139, 80, 20) );
    $sizePolicy3->setHeightForWidth( $sigEnergy->sizePolicy()->hasHeightForWidth() );
    $sigEnergy->setSizePolicy( $sizePolicy3 );
    my $z0 = Qt::LineEdit( $projParam );
    $self->{z0} = $z0;
    $z0->setObjectName( "z0" );
    $z0->setGeometry( Qt::Rect(151, 68, 80, 21) );
    $sizePolicy3->setHeightForWidth( $z0->sizePolicy()->hasHeightForWidth() );
    $z0->setSizePolicy( $sizePolicy3 );
    $z0->setMaximumSize( Qt::Size(32767, 32767) );
    my $valEnergy = Qt::LineEdit( $projParam );
    $self->{valEnergy} = $valEnergy;
    $valEnergy->setObjectName( "valEnergy" );
    $valEnergy->setGeometry( Qt::Rect(151, 115, 80, 21) );
    $sizePolicy3->setHeightForWidth( $valEnergy->sizePolicy()->hasHeightForWidth() );
    $valEnergy->setSizePolicy( $sizePolicy3 );
    my $ranSeed = Qt::LineEdit( $projParam );
    $self->{ranSeed} = $ranSeed;
    $ranSeed->setObjectName( "ranSeed" );
    $ranSeed->setGeometry( Qt::Rect(151, 209, 80, 21) );
    $sizePolicy3->setHeightForWidth( $ranSeed->sizePolicy()->hasHeightForWidth() );
    $ranSeed->setSizePolicy( $sizePolicy3 );
    my $albleLabel = Qt::Label( $projParam );
    $self->{albleLabel} = $albleLabel;
    $albleLabel->setObjectName( "albleLabel" );
    $albleLabel->setGeometry( Qt::Rect(11, 162, 73, 16) );
    $albleLabel->setWordWrap( 0 );
    my $sigELabel = Qt::Label( $projParam );
    $self->{sigELabel} = $sigELabel;
    $sigELabel->setObjectName( "sigELabel" );
    $sigELabel->setGeometry( Qt::Rect(11, 139, 114, 16) );
    $sigELabel->setWordWrap( 0 );
    my $dzLabel = Qt::Label( $projParam );
    $self->{dzLabel} = $dzLabel;
    $dzLabel->setObjectName( "dzLabel" );
    $dzLabel->setGeometry( Qt::Rect(11, 92, 127, 16) );
    $dzLabel->setWordWrap( 0 );
    my $groupBox15 = Qt::GroupBox( $layersTab );
    $self->{groupBox15} = $groupBox15;
    $groupBox15->setObjectName( "groupBox15" );
    $groupBox15->setGeometry( Qt::Rect(450, 260, 240, 261) );
    my $layout14 = Qt::Widget( $groupBox15 );
    $self->{layout14} = $layout14;
    $layout14->setObjectName( "layout14" );
    $layout14->setGeometry( Qt::Rect(10, 20, 221, 100) );
    my $fileNames = Qt::VBoxLayout( $layout14 );
    $self->{FileNames} = $fileNames;
    $fileNames->setSpacing( 0 );
    $fileNames->setMargin( 0 );
    $fileNames->setObjectName( "fileNames" );
    $fileNames->setContentsMargins(0, 0, 0, 0 );
    my $textLabelFN = Qt::Label( $layout14 );
    $self->{textLabelFN} = $textLabelFN;
    $textLabelFN->setObjectName( "textLabelFN" );
    $textLabelFN->setWordWrap( 0 );

    $fileNames->addWidget( $textLabelFN );

    my $fileNamePrefix = Qt::LineEdit( $layout14 );
    $self->{fileNamePrefix} = $fileNamePrefix;
    $fileNamePrefix->setObjectName( "fileNamePrefix" );

    $fileNames->addWidget( $fileNamePrefix );

    my $textLabelPath = Qt::Label( $layout14 );
    $self->{textLabelPath} = $textLabelPath;
    $textLabelPath->setObjectName( "textLabelPath" );
    $textLabelPath->setWordWrap( 0 );

    $fileNames->addWidget( $textLabelPath );

    my $outputDirectory = Qt::HBoxLayout(  );
    $self->{OutputDirectory} = $outputDirectory;
    $outputDirectory->setSpacing( 6 );
    $outputDirectory->setObjectName( "outputDirectory" );
    my $workPath = Qt::LineEdit( $layout14 );
    $self->{workPath} = $workPath;
    $workPath->setObjectName( "workPath" );

    $outputDirectory->addWidget( $workPath );

    my $browse = Qt::PushButton( $layout14 );
    $self->{browse} = $browse;
    $browse->setObjectName( "browse" );
    my $sizePolicy4 = Qt::SizePolicy( Qt::SizePolicy::Expanding(), Qt::SizePolicy::Fixed() );
    $self->{$sizePolicy4} = $sizePolicy4;
    $sizePolicy4->setHorizontalStretch( 0 );
    $sizePolicy4->setVerticalStretch( 0 );
    $sizePolicy4->setHeightForWidth( $browse->sizePolicy()->hasHeightForWidth() );
    $browse->setSizePolicy( $sizePolicy4 );

    $outputDirectory->addWidget( $browse );


    $fileNames->addLayout( $outputDirectory );

    my $splitter = Qt::Splitter( $groupBox15 );
    $self->{splitter} = $splitter;
    $splitter->setObjectName( "splitter" );
    $splitter->setGeometry( Qt::Rect(10, 220, 221, 24) );
    my $sizePolicy5 = Qt::SizePolicy( Qt::SizePolicy::Preferred(), Qt::SizePolicy::Minimum() );
    $self->{$sizePolicy5} = $sizePolicy5;
    $sizePolicy5->setHorizontalStretch( 0 );
    $sizePolicy5->setVerticalStretch( 0 );
    $sizePolicy5->setHeightForWidth( $splitter->sizePolicy()->hasHeightForWidth() );
    $splitter->setSizePolicy( $sizePolicy5 );
    $splitter->setOrientation( Qt::Horizontal() );
    my $start = Qt::PushButton( $splitter );
    $self->{start} = $start;
    $start->setObjectName( "start" );
    $start->setMinimumSize( Qt::Size(0, 25) );
    $splitter->addWidget( $start );
    my $quit = Qt::PushButton( $splitter );
    $self->{quit} = $quit;
    $quit->setObjectName( "quit" );
    $quit->setMinimumSize( Qt::Size(0, 25) );
    $splitter->addWidget( $quit );
    $tabs->addTab( $layersTab, Qt::Application::translate( 'TrimSPGUI4', "Layers", undef, Qt::Application::UnicodeUTF8() ) );
    my $addParTab = Qt::Widget(  );
    $self->{addParTab} = $addParTab;
    $addParTab->setObjectName( "addParTab" );
    my $addParam = Qt::GroupBox( $addParTab );
    $self->{addParam} = $addParam;
    $addParam->setObjectName( "addParam" );
    $addParam->setGeometry( Qt::Rect(10, 10, 251, 211) );
    my $textLabelEmpty = Qt::Label( $addParam );
    $self->{textLabelEmpty} = $textLabelEmpty;
    $textLabelEmpty->setObjectName( "textLabelEmpty" );
    $textLabelEmpty->setEnabled( 0 );
    $textLabelEmpty->setGeometry( Qt::Rect(10, 170, 63, 25) );
    $textLabelEmpty->setWordWrap( 0 );
    my $textLabelEF = Qt::Label( $addParam );
    $self->{textLabelEF} = $textLabelEF;
    $textLabelEF->setObjectName( "textLabelEF" );
    $textLabelEF->setGeometry( Qt::Rect(10, 21, 63, 25) );
    $textLabelEF->setWordWrap( 0 );
    my $parKK0R = Qt::SpinBox( $addParam );
    $self->{parKK0R} = $parKK0R;
    $parKK0R->setObjectName( "parKK0R" );
    $parKK0R->setGeometry( Qt::Rect(183, 45, 52, 26) );
    $parKK0R->setMinimum( 0 );
    $parKK0R->setMaximum( 4 );
    $parKK0R->setValue( 2 );
    my $parCA = Qt::LineEdit( $addParam );
    $self->{parCA} = $parCA;
    $parCA->setObjectName( "parCA" );
    $parCA->setGeometry( Qt::Rect(75, 147, 52, 25) );
    $sizePolicy1->setHeightForWidth( $parCA->sizePolicy()->hasHeightForWidth() );
    $parCA->setSizePolicy( $sizePolicy1 );
    my $textLabelIRL = Qt::Label( $addParam );
    $self->{textLabelIRL} = $textLabelIRL;
    $textLabelIRL->setObjectName( "textLabelIRL" );
    $textLabelIRL->setGeometry( Qt::Rect(129, 172, 53, 25) );
    $textLabelIRL->setWordWrap( 0 );
    my $parKK0 = Qt::SpinBox( $addParam );
    $self->{parKK0} = $parKK0;
    $parKK0->setObjectName( "parKK0" );
    $parKK0->setGeometry( Qt::Rect(183, 20, 52, 26) );
    $parKK0->setMinimum( 0 );
    $parKK0->setMaximum( 4 );
    $parKK0->setValue( 2 );
    my $textLabelKK0 = Qt::Label( $addParam );
    $self->{textLabelKK0} = $textLabelKK0;
    $textLabelKK0->setObjectName( "textLabelKK0" );
    $textLabelKK0->setGeometry( Qt::Rect(129, 20, 53, 25) );
    $textLabelKK0->setWordWrap( 0 );
    my $textLabelRD = Qt::Label( $addParam );
    $self->{textLabelRD} = $textLabelRD;
    $textLabelRD->setObjectName( "textLabelRD" );
    $textLabelRD->setGeometry( Qt::Rect(10, 122, 63, 25) );
    $textLabelRD->setWordWrap( 0 );
    my $parIRL = Qt::SpinBox( $addParam );
    $self->{parIRL} = $parIRL;
    $parIRL->setObjectName( "parIRL" );
    $parIRL->setGeometry( Qt::Rect(184, 171, 52, 26) );
    $parIRL->setMaximum( 2 );
    $parIRL->setValue( 0 );
    my $textLabelIPOTR = Qt::Label( $addParam );
    $self->{textLabelIPOTR} = $textLabelIPOTR;
    $textLabelIPOTR->setObjectName( "textLabelIPOTR" );
    $textLabelIPOTR->setGeometry( Qt::Rect(129, 147, 53, 25) );
    $textLabelIPOTR->setWordWrap( 0 );
    my $parIPOTR = Qt::SpinBox( $addParam );
    $self->{parIPOTR} = $parIPOTR;
    $parIPOTR->setObjectName( "parIPOTR" );
    $parIPOTR->setGeometry( Qt::Rect(183, 145, 52, 26) );
    $parIPOTR->setMinimum( 1 );
    $parIPOTR->setMaximum( 3 );
    my $textLabelESB = Qt::Label( $addParam );
    $self->{textLabelESB} = $textLabelESB;
    $textLabelESB->setObjectName( "textLabelESB" );
    $textLabelESB->setGeometry( Qt::Rect(10, 46, 63, 25) );
    $textLabelESB->setWordWrap( 0 );
    my $parRD = Qt::LineEdit( $addParam );
    $self->{parRD} = $parRD;
    $parRD->setObjectName( "parRD" );
    $parRD->setGeometry( Qt::Rect(75, 122, 52, 25) );
    $sizePolicy1->setHeightForWidth( $parRD->sizePolicy()->hasHeightForWidth() );
    $parRD->setSizePolicy( $sizePolicy1 );
    my $textLabelCA = Qt::Label( $addParam );
    $self->{textLabelCA} = $textLabelCA;
    $textLabelCA->setObjectName( "textLabelCA" );
    $textLabelCA->setGeometry( Qt::Rect(10, 147, 63, 25) );
    $textLabelCA->setWordWrap( 0 );
    my $parESB = Qt::LineEdit( $addParam );
    $self->{parESB} = $parESB;
    $parESB->setObjectName( "parESB" );
    $parESB->setGeometry( Qt::Rect(75, 46, 52, 25) );
    $sizePolicy1->setHeightForWidth( $parESB->sizePolicy()->hasHeightForWidth() );
    $parESB->setSizePolicy( $sizePolicy1 );
    my $textLabelSHEATH = Qt::Label( $addParam );
    $self->{textLabelSHEATH} = $textLabelSHEATH;
    $textLabelSHEATH->setObjectName( "textLabelSHEATH" );
    $textLabelSHEATH->setGeometry( Qt::Rect(10, 71, 63, 25) );
    $textLabelSHEATH->setWordWrap( 0 );
    my $textLabelKDEE2 = Qt::Label( $addParam );
    $self->{textLabelKDEE2} = $textLabelKDEE2;
    $textLabelKDEE2->setObjectName( "textLabelKDEE2" );
    $textLabelKDEE2->setGeometry( Qt::Rect(129, 96, 53, 25) );
    $textLabelKDEE2->setWordWrap( 0 );
    my $textLabelKDEE1 = Qt::Label( $addParam );
    $self->{textLabelKDEE1} = $textLabelKDEE1;
    $textLabelKDEE1->setObjectName( "textLabelKDEE1" );
    $textLabelKDEE1->setGeometry( Qt::Rect(129, 71, 53, 25) );
    $textLabelKDEE1->setWordWrap( 0 );
    my $parEF = Qt::LineEdit( $addParam );
    $self->{parEF} = $parEF;
    $parEF->setObjectName( "parEF" );
    $parEF->setGeometry( Qt::Rect(75, 21, 52, 25) );
    $sizePolicy1->setHeightForWidth( $parEF->sizePolicy()->hasHeightForWidth() );
    $parEF->setSizePolicy( $sizePolicy1 );
    my $lineEditEmpty = Qt::LineEdit( $addParam );
    $self->{lineEditEmpty} = $lineEditEmpty;
    $lineEditEmpty->setObjectName( "lineEditEmpty" );
    $lineEditEmpty->setEnabled( 0 );
    $lineEditEmpty->setGeometry( Qt::Rect(75, 170, 52, 25) );
    $sizePolicy1->setHeightForWidth( $lineEditEmpty->sizePolicy()->hasHeightForWidth() );
    $lineEditEmpty->setSizePolicy( $sizePolicy1 );
    my $parKDEE2 = Qt::SpinBox( $addParam );
    $self->{parKDEE2} = $parKDEE2;
    $parKDEE2->setObjectName( "parKDEE2" );
    $parKDEE2->setGeometry( Qt::Rect(183, 95, 52, 26) );
    $parKDEE2->setMinimum( 1 );
    $parKDEE2->setMaximum( 3 );
    $parKDEE2->setValue( 3 );
    my $parERC = Qt::LineEdit( $addParam );
    $self->{parERC} = $parERC;
    $parERC->setObjectName( "parERC" );
    $parERC->setGeometry( Qt::Rect(75, 96, 52, 26) );
    $sizePolicy1->setHeightForWidth( $parERC->sizePolicy()->hasHeightForWidth() );
    $parERC->setSizePolicy( $sizePolicy1 );
    my $textLabelKK0R = Qt::Label( $addParam );
    $self->{textLabelKK0R} = $textLabelKK0R;
    $textLabelKK0R->setObjectName( "textLabelKK0R" );
    $textLabelKK0R->setGeometry( Qt::Rect(129, 46, 53, 25) );
    $textLabelKK0R->setWordWrap( 0 );
    my $textLabelIPOT = Qt::Label( $addParam );
    $self->{textLabelIPOT} = $textLabelIPOT;
    $textLabelIPOT->setObjectName( "textLabelIPOT" );
    $textLabelIPOT->setGeometry( Qt::Rect(129, 121, 53, 26) );
    $textLabelIPOT->setWordWrap( 0 );
    my $parIPOT = Qt::SpinBox( $addParam );
    $self->{parIPOT} = $parIPOT;
    $parIPOT->setObjectName( "parIPOT" );
    $parIPOT->setGeometry( Qt::Rect(183, 120, 52, 26) );
    $parIPOT->setMinimum( 1 );
    $parIPOT->setMaximum( 3 );
    $parIPOT->setValue( 2 );
    my $textLabelERC = Qt::Label( $addParam );
    $self->{textLabelERC} = $textLabelERC;
    $textLabelERC->setObjectName( "textLabelERC" );
    $textLabelERC->setGeometry( Qt::Rect(10, 96, 63, 26) );
    $textLabelERC->setWordWrap( 0 );
    my $parSHEATH = Qt::LineEdit( $addParam );
    $self->{parSHEATH} = $parSHEATH;
    $parSHEATH->setObjectName( "parSHEATH" );
    $parSHEATH->setGeometry( Qt::Rect(75, 71, 52, 25) );
    $sizePolicy1->setHeightForWidth( $parSHEATH->sizePolicy()->hasHeightForWidth() );
    $parSHEATH->setSizePolicy( $sizePolicy1 );
    my $parKDEE1 = Qt::SpinBox( $addParam );
    $self->{parKDEE1} = $parKDEE1;
    $parKDEE1->setObjectName( "parKDEE1" );
    $parKDEE1->setGeometry( Qt::Rect(183, 70, 52, 26) );
    $parKDEE1->setMinimum( 1 );
    $parKDEE1->setMaximum( 5 );
    $parKDEE1->setValue( 4 );
    $tabs->addTab( $addParTab, Qt::Application::translate( 'TrimSPGUI4', "Additional Parameters", undef, Qt::Application::UnicodeUTF8() ) );
    my $scansTab = Qt::Widget(  );
    $self->{scansTab} = $scansTab;
    $scansTab->setObjectName( "scansTab" );
    my $scanSeq = Qt::GroupBox( $scansTab );
    $self->{scanSeq} = $scanSeq;
    $scanSeq->setObjectName( "scanSeq" );
    $scanSeq->setGeometry( Qt::Rect(10, 10, 421, 221) );
    $scanSeq->setCheckable( 1 );
    $scanSeq->setChecked( 0 );
    my $layoutWidget1 = Qt::Widget( $scanSeq );
    $self->{layoutWidget1} = $layoutWidget1;
    $layoutWidget1->setObjectName( "layoutWidget1" );
    $layoutWidget1->setGeometry( Qt::Rect(10, 20, 401, 191) );
    my $verticalLayout = Qt::VBoxLayout( $layoutWidget1 );
    $self->{verticalLayout} = $verticalLayout;
    $verticalLayout->setSpacing( 6 );
    $verticalLayout->setMargin( 11 );
    $verticalLayout->setObjectName( "verticalLayout" );
    $verticalLayout->setContentsMargins(0, 0, 0, 0 );
    my $horizontalLayout_3 = Qt::HBoxLayout(  );
    $self->{horizontalLayout_3} = $horizontalLayout_3;
    $horizontalLayout_3->setSpacing( 6 );
    $horizontalLayout_3->setObjectName( "horizontalLayout_3" );
    my $label = Qt::Label( $layoutWidget1 );
    $self->{label} = $label;
    $label->setObjectName( "label" );

    $horizontalLayout_3->addWidget( $label );

    my $comboScan = Qt::ComboBox( $layoutWidget1 );
    $self->{comboScan} = $comboScan;
    $comboScan->setObjectName( "comboScan" );
    my $sizePolicy6 = Qt::SizePolicy( Qt::SizePolicy::Preferred(), Qt::SizePolicy::Fixed() );
    $self->{$sizePolicy6} = $sizePolicy6;
    $sizePolicy6->setHorizontalStretch( 0 );
    $sizePolicy6->setVerticalStretch( 0 );
    $sizePolicy6->setHeightForWidth( $comboScan->sizePolicy()->hasHeightForWidth() );
    $comboScan->setSizePolicy( $sizePolicy6 );

    $horizontalLayout_3->addWidget( $comboScan );

    my $scandL = Qt::SpinBox( $layoutWidget1 );
    $self->{scandL} = $scandL;
    $scandL->setObjectName( "scandL" );
    $scandL->setEnabled( 0 );
    $scandL->setMinimum( 1 );
    $scandL->setMaximum( 100 );

    $horizontalLayout_3->addWidget( $scandL );


    $verticalLayout->addLayout( $horizontalLayout_3 );

    my $radioList = Qt::RadioButton( $layoutWidget1 );
    $self->{radioList} = $radioList;
    $radioList->setObjectName( "radioList" );

    $verticalLayout->addWidget( $radioList );

    my $scanList = Qt::LineEdit( $layoutWidget1 );
    $self->{scanList} = $scanList;
    $scanList->setObjectName( "scanList" );

    $verticalLayout->addWidget( $scanList );

    my $dzListLabel = Qt::Label( $layoutWidget1 );
    $self->{dzListLabel} = $dzListLabel;
    $dzListLabel->setObjectName( "dzListLabel" );
    $dzListLabel->setWordWrap( 0 );

    $verticalLayout->addWidget( $dzListLabel );

    my $scanListdz = Qt::LineEdit( $layoutWidget1 );
    $self->{scanListdz} = $scanListdz;
    $scanListdz->setObjectName( "scanListdz" );
    $scanListdz->setEnabled( 0 );

    $verticalLayout->addWidget( $scanListdz );

    my $radioLoop = Qt::RadioButton( $layoutWidget1 );
    $self->{radioLoop} = $radioLoop;
    $radioLoop->setObjectName( "radioLoop" );
    $radioLoop->setChecked( 1 );

    $verticalLayout->addWidget( $radioLoop );

    my $scanLoop = Qt::HBoxLayout(  );
    $self->{scanLoop} = $scanLoop;
    $scanLoop->setSpacing( 0 );
    $scanLoop->setMargin( 0 );
    $scanLoop->setObjectName( "scanLoop" );
    my $textLabel2 = Qt::Label( $layoutWidget1 );
    $self->{textLabel2} = $textLabel2;
    $textLabel2->setObjectName( "textLabel2" );
    $textLabel2->setWordWrap( 0 );

    $scanLoop->addWidget( $textLabel2 );

    my $scanFrom = Qt::LineEdit( $layoutWidget1 );
    $self->{scanFrom} = $scanFrom;
    $scanFrom->setObjectName( "scanFrom" );
    $sizePolicy6->setHeightForWidth( $scanFrom->sizePolicy()->hasHeightForWidth() );
    $scanFrom->setSizePolicy( $sizePolicy6 );

    $scanLoop->addWidget( $scanFrom );

    my $textLabel2_2 = Qt::Label( $layoutWidget1 );
    $self->{textLabel2_2} = $textLabel2_2;
    $textLabel2_2->setObjectName( "textLabel2_2" );
    $textLabel2_2->setWordWrap( 0 );

    $scanLoop->addWidget( $textLabel2_2 );

    my $scanTo = Qt::LineEdit( $layoutWidget1 );
    $self->{scanTo} = $scanTo;
    $scanTo->setObjectName( "scanTo" );
    $sizePolicy6->setHeightForWidth( $scanTo->sizePolicy()->hasHeightForWidth() );
    $scanTo->setSizePolicy( $sizePolicy6 );

    $scanLoop->addWidget( $scanTo );

    my $textLabel2_2_2_2 = Qt::Label( $layoutWidget1 );
    $self->{textLabel2_2_2_2} = $textLabel2_2_2_2;
    $textLabel2_2_2_2->setObjectName( "textLabel2_2_2_2" );
    $textLabel2_2_2_2->setWordWrap( 0 );

    $scanLoop->addWidget( $textLabel2_2_2_2 );

    my $scanStep = Qt::LineEdit( $layoutWidget1 );
    $self->{scanStep} = $scanStep;
    $scanStep->setObjectName( "scanStep" );
    $sizePolicy6->setHeightForWidth( $scanStep->sizePolicy()->hasHeightForWidth() );
    $scanStep->setSizePolicy( $sizePolicy6 );

    $scanLoop->addWidget( $scanStep );


    $verticalLayout->addLayout( $scanLoop );

    $tabs->addTab( $scansTab, Qt::Application::translate( 'TrimSPGUI4', "Scans (Disbaled)", undef, Qt::Application::UnicodeUTF8() ) );
    my $configTab = Qt::Widget(  );
    $self->{configTab} = $configTab;
    $configTab->setObjectName( "configTab" );
    my $layoutWidget2 = Qt::Widget( $configTab );
    $self->{layoutWidget2} = $layoutWidget2;
    $layoutWidget2->setObjectName( "layoutWidget2" );
    $layoutWidget2->setGeometry( Qt::Rect(10, 31, 671, 101) );
    my $verticalLayout_2 = Qt::VBoxLayout( $layoutWidget2 );
    $self->{verticalLayout_2} = $verticalLayout_2;
    $verticalLayout_2->setSpacing( 6 );
    $verticalLayout_2->setMargin( 11 );
    $verticalLayout_2->setObjectName( "verticalLayout_2" );
    $verticalLayout_2->setContentsMargins(0, 0, 0, 0 );
    my $horizontalLayout = Qt::HBoxLayout(  );
    $self->{horizontalLayout} = $horizontalLayout;
    $horizontalLayout->setSpacing( 6 );
    $horizontalLayout->setObjectName( "horizontalLayout" );
    my $label_2 = Qt::Label( $layoutWidget2 );
    $self->{label_2} = $label_2;
    $label_2->setObjectName( "label_2" );
    my $sizePolicy7 = Qt::SizePolicy( Qt::SizePolicy::Fixed(), Qt::SizePolicy::Preferred() );
    $self->{$sizePolicy7} = $sizePolicy7;
    $sizePolicy7->setHorizontalStretch( 0 );
    $sizePolicy7->setVerticalStretch( 0 );
    $sizePolicy7->setHeightForWidth( $label_2->sizePolicy()->hasHeightForWidth() );
    $label_2->setSizePolicy( $sizePolicy7 );
    $label_2->setMinimumSize( Qt::Size(110, 0) );
    $label_2->setMaximumSize( Qt::Size(140, 16777215) );

    $horizontalLayout->addWidget( $label_2 );

    my $trimbin = Qt::LineEdit( $layoutWidget2 );
    $self->{trimbin} = $trimbin;
    $trimbin->setObjectName( "trimbin" );

    $horizontalLayout->addWidget( $trimbin );

    my $trimBinBrowse = Qt::PushButton( $layoutWidget2 );
    $self->{trimBinBrowse} = $trimBinBrowse;
    $trimBinBrowse->setObjectName( "trimBinBrowse" );

    $horizontalLayout->addWidget( $trimBinBrowse );


    $verticalLayout_2->addLayout( $horizontalLayout );

    my $horizontalLayout_2 = Qt::HBoxLayout(  );
    $self->{horizontalLayout_2} = $horizontalLayout_2;
    $horizontalLayout_2->setSpacing( 6 );
    $horizontalLayout_2->setObjectName( "horizontalLayout_2" );
    my $label_3 = Qt::Label( $layoutWidget2 );
    $self->{label_3} = $label_3;
    $label_3->setObjectName( "label_3" );
    $sizePolicy7->setHeightForWidth( $label_3->sizePolicy()->hasHeightForWidth() );
    $label_3->setSizePolicy( $sizePolicy7 );
    $label_3->setMinimumSize( Qt::Size(110, 0) );
    $label_3->setMaximumSize( Qt::Size(140, 16777215) );

    $horizontalLayout_2->addWidget( $label_3 );

    my $pathTrimGUI = Qt::LineEdit( $layoutWidget2 );
    $self->{pathTrimGUI} = $pathTrimGUI;
    $pathTrimGUI->setObjectName( "pathTrimGUI" );

    $horizontalLayout_2->addWidget( $pathTrimGUI );

    my $guiPathBrowse = Qt::PushButton( $layoutWidget2 );
    $self->{guiPathBrowse} = $guiPathBrowse;
    $guiPathBrowse->setObjectName( "guiPathBrowse" );

    $horizontalLayout_2->addWidget( $guiPathBrowse );


    $verticalLayout_2->addLayout( $horizontalLayout_2 );

    $tabs->addTab( $configTab, Qt::Application::translate( 'TrimSPGUI4', "Configure", undef, Qt::Application::UnicodeUTF8() ) );

    $gridLayout->addWidget( $tabs, 0, 0, 1, 1 );

    $trimSPGUI4->setCentralWidget( $widget );
    my $menuBar = Qt::MenuBar( $trimSPGUI4 );
    $self->{menuBar} = $menuBar;
    $menuBar->setObjectName( "menuBar" );
    $menuBar->setGeometry( Qt::Rect(0, 0, 712, 20) );
    my $file = Qt::Menu( $menuBar );
    $self->{file} = $file;
    $file->setObjectName( "file" );
    my $plot = Qt::Menu( $menuBar );
    $self->{plot} = $plot;
    $plot->setObjectName( "plot" );
    my $helpMenu = Qt::Menu( $menuBar );
    $self->{helpMenu} = $helpMenu;
    $helpMenu->setObjectName( "helpMenu" );
    $trimSPGUI4->setMenuBar( $menuBar );
    $textLabel1_4->setBuddy( $numLayer );
    $z0Label->setBuddy( $z0 );
    $numberLabel->setBuddy( $numberProj );
    $energyLabel->setBuddy( $valEnergy );
    $textLabel1->setBuddy( $projComboBox );
    $sigAngleLabel->setBuddy( $sigAngle );
    $seedLabel->setBuddy( $ranSeed );
    $albleLabel->setBuddy( $valAngle );
    $sigELabel->setBuddy( $sigEnergy );
    $dzLabel->setBuddy( $dz );
    $textLabelFN->setBuddy( $fileNamePrefix );
    $textLabelPath->setBuddy( $workPath );
    $textLabelEmpty->setBuddy( $lineEditEmpty );
    $textLabelEF->setBuddy( $parEF );
    $textLabelIRL->setBuddy( $parIRL );
    $textLabelKK0->setBuddy( $parKK0 );
    $textLabelRD->setBuddy( $parRD );
    $textLabelIPOTR->setBuddy( $parIPOTR );
    $textLabelESB->setBuddy( $parESB );
    $textLabelCA->setBuddy( $parCA );
    $textLabelSHEATH->setBuddy( $parSHEATH );
    $textLabelKDEE2->setBuddy( $parKDEE2 );
    $textLabelKDEE1->setBuddy( $parKDEE1 );
    $textLabelKK0R->setBuddy( $parKK0R );
    $textLabelIPOT->setBuddy( $parIPOT );
    $textLabelERC->setBuddy( $parERC );
    $label->setBuddy( $comboScan );
    $dzListLabel->setBuddy( $scanListdz );
    $textLabel2->setBuddy( $scanFrom );
    $textLabel2_2->setBuddy( $scanTo );
    $textLabel2_2_2_2->setBuddy( $scanStep );
    $label_2->setBuddy( $trimbin );
    $label_3->setBuddy( $pathTrimGUI );
    Qt::Widget::setTabOrder( $tabs, $numLayer );
    Qt::Widget::setTabOrder( $numLayer, $layerTable );
    Qt::Widget::setTabOrder( $layerTable, $projComboBox );
    Qt::Widget::setTabOrder( $projComboBox, $numberProj );
    Qt::Widget::setTabOrder( $numberProj, $z0 );
    Qt::Widget::setTabOrder( $z0, $dz );
    Qt::Widget::setTabOrder( $dz, $valEnergy );
    Qt::Widget::setTabOrder( $valEnergy, $sigEnergy );
    Qt::Widget::setTabOrder( $sigEnergy, $valAngle );
    Qt::Widget::setTabOrder( $valAngle, $sigAngle );
    Qt::Widget::setTabOrder( $sigAngle, $ranSeed );
    Qt::Widget::setTabOrder( $ranSeed, $fileNamePrefix );
    Qt::Widget::setTabOrder( $fileNamePrefix, $workPath );
    Qt::Widget::setTabOrder( $workPath, $browse );
    Qt::Widget::setTabOrder( $browse, $start );
    Qt::Widget::setTabOrder( $start, $quit );
    Qt::Widget::setTabOrder( $quit, $parEF );
    Qt::Widget::setTabOrder( $parEF, $parESB );
    Qt::Widget::setTabOrder( $parESB, $parSHEATH );
    Qt::Widget::setTabOrder( $parSHEATH, $parERC );
    Qt::Widget::setTabOrder( $parERC, $parRD );
    Qt::Widget::setTabOrder( $parRD, $parCA );
    Qt::Widget::setTabOrder( $parCA, $lineEditEmpty );
    Qt::Widget::setTabOrder( $lineEditEmpty, $parKK0 );
    Qt::Widget::setTabOrder( $parKK0, $parKK0R );
    Qt::Widget::setTabOrder( $parKK0R, $parKDEE1 );
    Qt::Widget::setTabOrder( $parKDEE1, $parKDEE2 );
    Qt::Widget::setTabOrder( $parKDEE2, $parIPOT );
    Qt::Widget::setTabOrder( $parIPOT, $parIPOTR );
    Qt::Widget::setTabOrder( $parIPOTR, $parIRL );
    Qt::Widget::setTabOrder( $parIRL, $scanSeq );
    Qt::Widget::setTabOrder( $scanSeq, $comboScan );
    Qt::Widget::setTabOrder( $comboScan, $scandL );
    Qt::Widget::setTabOrder( $scandL, $radioList );
    Qt::Widget::setTabOrder( $radioList, $scanList );
    Qt::Widget::setTabOrder( $scanList, $scanListdz );
    Qt::Widget::setTabOrder( $scanListdz, $radioLoop );
    Qt::Widget::setTabOrder( $radioLoop, $scanFrom );
    Qt::Widget::setTabOrder( $scanFrom, $scanTo );
    Qt::Widget::setTabOrder( $scanTo, $scanStep );
    Qt::Widget::setTabOrder( $scanStep, $trimbin );
    Qt::Widget::setTabOrder( $trimbin, $trimBinBrowse );
    Qt::Widget::setTabOrder( $trimBinBrowse, $pathTrimGUI );
    Qt::Widget::setTabOrder( $pathTrimGUI, $guiPathBrowse );

    $menuBar->addAction( $file->menuAction() );
    $menuBar->addAction( $plot->menuAction() );
    $menuBar->addAction( $helpMenu->menuAction() );
    $file->addAction( $fileStartAction );
    $file->addSeparator();
    $file->addAction( $fileOpenAction );
    $file->addAction( $fileSaveAction );
    $file->addAction( $fileSaveAsAction );
    $file->addSeparator();
    $file->addAction( $fileChangeDirAction );
    $file->addAction( $fileQuitAction );
    $plot->addAction( $plotProfilesAction );
    $plot->addAction( $plotFractionsAction );
    $plot->addAction( $plotMeanAction );
    $helpMenu->addAction( $helpContentsAction );
    $helpMenu->addAction( $helpAboutAction );

    $self->retranslateUi( $trimSPGUI4 );
    Qt::Object::connect($browse, SIGNAL 'clicked()' , $trimSPGUI4, SLOT 'DirectoryBrowse()' );
    Qt::Object::connect($quit, SIGNAL 'clicked()' , $trimSPGUI4, SLOT 'ConfirmQuit()' );
    Qt::Object::connect($fileSaveAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'SaveFile()' );
    Qt::Object::connect($fileQuitAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'close()' );
    Qt::Object::connect($fileSaveAsAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'SaveFileAs()' );
    Qt::Object::connect($fileChangeDirAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'DirectoryBrowse()' );
    Qt::Object::connect($plotFractionsAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'PlotFraction()' );
    Qt::Object::connect($plotProfilesAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'PlotProfiles()' );
    Qt::Object::connect($start, SIGNAL 'clicked()' , $trimSPGUI4, SLOT 'StartSequenceOne()' );
    Qt::Object::connect($fileOpenAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'OpenFile()' );
    Qt::Object::connect($helpAboutAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'helpAboutAction()' );
    Qt::Object::connect($helpContentsAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'helpContentsAction()' );
    Qt::Object::connect($numberProj, SIGNAL 'editingFinished()' , $trimSPGUI4, SLOT 'test()' );
    Qt::Object::connect($scanSeq, SIGNAL 'toggled(bool)' , $trimSPGUI4, SLOT 'ToggleScanSingle()' );
    Qt::Object::connect($comboScan, SIGNAL 'currentIndexChanged(int)' , $trimSPGUI4, SLOT 'ToggleScanSingle()' );
    Qt::Object::connect($radioList, SIGNAL 'toggled(bool)' , $trimSPGUI4, SLOT 'ToggleScanSingle()' );
    Qt::Object::connect($radioLoop, SIGNAL 'toggled(bool)' , $trimSPGUI4, SLOT 'ToggleScanSingle()' );
    Qt::Object::connect($scandL, SIGNAL 'valueChanged(int)' , $trimSPGUI4, SLOT 'ToggleScanSingle()' );
    Qt::Object::connect($projComboBox, SIGNAL 'currentIndexChanged(int)' , $trimSPGUI4, SLOT 'ProjSmartDefaults()' );
    Qt::Object::connect($trimBinBrowse, SIGNAL 'clicked()' , $trimSPGUI4, SLOT 'TrimBin()' );
    Qt::Object::connect($guiPathBrowse, SIGNAL 'clicked()' , $trimSPGUI4, SLOT 'GUIPath()' );
    Qt::Object::connect($numLayer, SIGNAL 'valueChanged(int)' , $trimSPGUI4, SLOT 'PrepLayers()' );
    Qt::Object::connect($layerTable, SIGNAL 'cellChanged(int,int)' , $trimSPGUI4, SLOT 'findDensity()' );
    Qt::Object::connect($start, SIGNAL 'clicked()' , $trimSPGUI4, SLOT 'CollectValues()' );
    Qt::Object::connect($plotMeanAction, SIGNAL 'activated()' , $trimSPGUI4, SLOT 'PlotMean()' );

    $tabs->setCurrentIndex( 0 );


    Qt::MetaObject::connectSlotsByName( $trimSPGUI4 );
    return $self;
} # setupUi

sub setup_ui {
    my ( $trimSPGUI4 ) = @_;
    return setupUi( $trimSPGUI4 );
}

sub retranslateUi {
    my ( $self, $trimSPGUI4 ) = @_;
    $trimSPGUI4->setWindowTitle( Qt::Application::translate( 'TrimSPGUI4', "TrimSP GUI", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileSaveAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&Save", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileSaveAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Save", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileSaveAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+S", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{fileSaveAsAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "Save &As...", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileSaveAsAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Save As", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileSaveAsAction}->setShortcut( Qt::KeySequence( '' ) );
    $self->{fileChangeDirAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&Change Folder", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileChangeDirAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Change Folder", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileChangeDirAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+C", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{fileQuitAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&Quit", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileQuitAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Quit", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileQuitAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+Q", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{plotProfilesAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&Plot Profiles", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plotProfilesAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "&Plot Profiles", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plotProfilesAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+P, Ctrl+R", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{helpContentsAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&Contents...", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{helpContentsAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Contents", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{helpContentsAction}->setShortcut( Qt::KeySequence( '' ) );
    $self->{helpAboutAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&About", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{helpAboutAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "About", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{helpAboutAction}->setShortcut( Qt::KeySequence( '' ) );
    $self->{plotFractionsAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "Plot Fractions", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plotFractionsAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Plot Fractions", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plotFractionsAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+P, Ctrl+F", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{fileStartAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "Start", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileStartAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "&Start", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileStartAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Alt+S", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{fileOpenAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "&Open...", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileOpenAction}->setIconText( Qt::Application::translate( 'TrimSPGUI4', "Open", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileOpenAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+O", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{plotMeanAction}->setText( Qt::Application::translate( 'TrimSPGUI4', "Plot Mean", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plotMeanAction}->setToolTip( Qt::Application::translate( 'TrimSPGUI4', "Plot Mean/Straggling", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plotMeanAction}->setShortcut( Qt::KeySequence( Qt::Application::translate( 'TrimSPGUI4', "Ctrl+P, Ctrl+M", undef, Qt::Application::UnicodeUTF8() ) ) );
    $self->{groupLayer}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "Layers", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabel1_4}->setText( Qt::Application::translate( 'TrimSPGUI4', "Number of Layers", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{numLayer}->setWhatsThis( Qt::Application::translate( 'TrimSPGUI4', "Select the number of the layers of your structure (maximum 100 layers).", undef, Qt::Application::UnicodeUTF8() ) );
    my $layerTable = $self->{layerTable};
    if ( $layerTable->columnCount < 3 ) {
        $layerTable->setColumnCount(3);
    }

    my $__colItem = Qt::TableWidgetItem();
    $__colItem->setText( Qt::Application::translate( 'TrimSPGUI4', "Composition", undef, Qt::Application::UnicodeUTF8() ) );
    $layerTable->setHorizontalHeaderItem( 0, $__colItem );

    my $__colItem1 = Qt::TableWidgetItem();
    $__colItem1->setText( Qt::Application::translate( 'TrimSPGUI4', "Density [g/cm^3]", undef, Qt::Application::UnicodeUTF8() ) );
    $layerTable->setHorizontalHeaderItem( 1, $__colItem1 );

    my $__colItem2 = Qt::TableWidgetItem();
    $__colItem2->setText( Qt::Application::translate( 'TrimSPGUI4', "Thickness [A]", undef, Qt::Application::UnicodeUTF8() ) );
    $layerTable->setHorizontalHeaderItem( 2, $__colItem2 );
    if ( $layerTable->rowCount() < 1 ) {
        $layerTable->setRowCount(1);
    }

    my $__item = Qt::TableWidgetItem();
    $__item->setText( Qt::Application::translate( 'TrimSPGUI4', "SrTiO3", undef, Qt::Application::UnicodeUTF8() ) );
    $layerTable->setItem( 0, 0, $__item );

    my $__item1 = Qt::TableWidgetItem();
    $__item1->setText( Qt::Application::translate( 'TrimSPGUI4', "5.12", undef, Qt::Application::UnicodeUTF8() ) );
    $layerTable->setItem( 0, 1, $__item1 );

    my $__item2 = Qt::TableWidgetItem();
    $__item2->setText( Qt::Application::translate( 'TrimSPGUI4', "1000", undef, Qt::Application::UnicodeUTF8() ) );
    $layerTable->setItem( 0, 2, $__item2 );
    $self->{projParam}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "Projectile parameters", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{z0Label}->setText( Qt::Application::translate( 'TrimSPGUI4', "Starting depth [\303\205]", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{numberLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Number of projectiles", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{projComboBox}->insertItems(0, [Qt::Application::translate( 'TrimSPGUI4', "Muon", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "Li8", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "B12", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "H", undef, Qt::Application::UnicodeUTF8() )]);
    $self->{dz}->setText( Qt::Application::translate( 'TrimSPGUI4', "10.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{energyLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Energy [eV]", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{numberProj}->setText( Qt::Application::translate( 'TrimSPGUI4', "10000", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabel1}->setText( Qt::Application::translate( 'TrimSPGUI4', "Projectile", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{valAngle}->setText( Qt::Application::translate( 'TrimSPGUI4', "0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{sigAngleLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Angle sigma [deg]", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{sigAngle}->setText( Qt::Application::translate( 'TrimSPGUI4', "15", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{seedLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Random seed", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{sigEnergy}->setText( Qt::Application::translate( 'TrimSPGUI4', "450", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{z0}->setText( Qt::Application::translate( 'TrimSPGUI4', "0.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{valEnergy}->setText( Qt::Application::translate( 'TrimSPGUI4', "2000", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{ranSeed}->setText( Qt::Application::translate( 'TrimSPGUI4', "78741", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{albleLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Angle [deg]", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{sigELabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Energy sigma [eV]", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{dzLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Depth increment [\303\205]", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{groupBox15}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "File Names", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelFN}->setText( Qt::Application::translate( 'TrimSPGUI4', "File names prefix", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{fileNamePrefix}->setText( Qt::Application::translate( 'TrimSPGUI4', "SrTiO3", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelPath}->setText( Qt::Application::translate( 'TrimSPGUI4', "Save in subdirectory", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{workPath}->setText( Qt::Application::translate( 'TrimSPGUI4', "./", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{browse}->setText( Qt::Application::translate( 'TrimSPGUI4', "Browse", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{start}->setToolTip( Qt::Application::translate( 'TrimSPGUI4', "Start", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{start}->setText( Qt::Application::translate( 'TrimSPGUI4', "Start", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{quit}->setToolTip( Qt::Application::translate( 'TrimSPGUI4', "Quit", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{quit}->setText( Qt::Application::translate( 'TrimSPGUI4', "Quit", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{tabs}->setTabText( $self->{tabs}->indexOf( $self->{layersTab}), Qt::Application::translate( 'TrimSPGUI4', "Layers", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{addParam}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "Additional parameters", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelEmpty}->setText( '' );
    $self->{textLabelEF}->setText( Qt::Application::translate( 'TrimSPGUI4', "EF", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{parCA}->setText( Qt::Application::translate( 'TrimSPGUI4', "1.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelIRL}->setText( Qt::Application::translate( 'TrimSPGUI4', "IRL", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelKK0}->setText( Qt::Application::translate( 'TrimSPGUI4', "KK0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelRD}->setText( Qt::Application::translate( 'TrimSPGUI4', "RD", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelIPOTR}->setText( Qt::Application::translate( 'TrimSPGUI4', "IPOTR", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelESB}->setText( Qt::Application::translate( 'TrimSPGUI4', "ESB", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{parRD}->setText( Qt::Application::translate( 'TrimSPGUI4', "50.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelCA}->setText( Qt::Application::translate( 'TrimSPGUI4', "CA", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{parESB}->setText( Qt::Application::translate( 'TrimSPGUI4', "0.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelSHEATH}->setText( Qt::Application::translate( 'TrimSPGUI4', "SHEATH", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelKDEE2}->setText( Qt::Application::translate( 'TrimSPGUI4', "KDEE2", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelKDEE1}->setText( Qt::Application::translate( 'TrimSPGUI4', "KDEE1", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{parEF}->setText( Qt::Application::translate( 'TrimSPGUI4', "0.5", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{lineEditEmpty}->setText( '' );
    $self->{parERC}->setText( Qt::Application::translate( 'TrimSPGUI4', "0.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelKK0R}->setText( Qt::Application::translate( 'TrimSPGUI4', "KK0R", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelIPOT}->setText( Qt::Application::translate( 'TrimSPGUI4', "IPOT", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabelERC}->setText( Qt::Application::translate( 'TrimSPGUI4', "ERC", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{parSHEATH}->setText( Qt::Application::translate( 'TrimSPGUI4', "0.0", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{tabs}->setTabText( $self->{tabs}->indexOf( $self->{addParTab}), Qt::Application::translate( 'TrimSPGUI4', "Additional Parameters", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{scanSeq}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "Scan sequence", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{label}->setText( Qt::Application::translate( 'TrimSPGUI4', "Scan", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{comboScan}->insertItems(0, [Qt::Application::translate( 'TrimSPGUI4', "Energy", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "Energy Sigma", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "Angle", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "Angle Sigma", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "Number of Projectiles", undef, Qt::Application::UnicodeUTF8() ),
        Qt::Application::translate( 'TrimSPGUI4', "Thickness of layer", undef, Qt::Application::UnicodeUTF8() )]);
    $self->{radioList}->setText( Qt::Application::translate( 'TrimSPGUI4', "List of values", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{scanList}->setText( Qt::Application::translate( 'TrimSPGUI4', "1000,6000,10000", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{dzListLabel}->setText( Qt::Application::translate( 'TrimSPGUI4', "Corresponding depth increment (optional)", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{scanListdz}->setText( '' );
    $self->{radioLoop}->setText( Qt::Application::translate( 'TrimSPGUI4', "Loop", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabel2}->setText( Qt::Application::translate( 'TrimSPGUI4', "From", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{scanFrom}->setText( Qt::Application::translate( 'TrimSPGUI4', "1000", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabel2_2}->setText( Qt::Application::translate( 'TrimSPGUI4', "To", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{scanTo}->setText( Qt::Application::translate( 'TrimSPGUI4', "28000", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{textLabel2_2_2_2}->setText( Qt::Application::translate( 'TrimSPGUI4', "Step", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{scanStep}->setText( Qt::Application::translate( 'TrimSPGUI4', "1000", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{tabs}->setTabText( $self->{tabs}->indexOf( $self->{scansTab}), Qt::Application::translate( 'TrimSPGUI4', "Scans (Disbaled)", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{label_2}->setText( Qt::Application::translate( 'TrimSPGUI4', "Trim.SP binary", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{trimBinBrowse}->setText( Qt::Application::translate( 'TrimSPGUI4', "Browse", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{label_3}->setText( Qt::Application::translate( 'TrimSPGUI4', "Trim.SP GUI path", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{guiPathBrowse}->setText( Qt::Application::translate( 'TrimSPGUI4', "Browse", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{tabs}->setTabText( $self->{tabs}->indexOf( $self->{configTab}), Qt::Application::translate( 'TrimSPGUI4', "Configure", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{file}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "&File", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{plot}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "&Plot", undef, Qt::Application::UnicodeUTF8() ) );
    $self->{helpMenu}->setTitle( Qt::Application::translate( 'TrimSPGUI4', "&Help", undef, Qt::Application::UnicodeUTF8() ) );
} # retranslateUi

sub retranslate_ui {
    my ( $trimSPGUI4 ) = @_;
    retranslateUi( $trimSPGUI4 );
}

1;
