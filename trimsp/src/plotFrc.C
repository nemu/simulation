// File:        plotFrc.C
// Author:      Zaher Salman
// Date:        11/01/2012
// Purpose:     ROOT macro to read and plot implantation fractions in layers from trimsp calculation
//              Assume following file Format:
//
//  $Id$
//
//      Energy    SigmaE     Alpha  SigAlpha    ntot     imp  backsc   trans   tried    negE   impL1   impL2   impL3   impL4   impL5   impL6  impL7   range      straggeling  Eback       sigEback    Etrans      SigEtrans   red. E      PRC
//       15.00      0.45      0.00     15.00  100000   98362    1638       0  100000       0   98362       0       0       0       0       0     0   0.6664E+03  0.1799E+03  0.4980E+04  0.4250E+04  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00
//  plotFrc(file_name)

void plotFrc(char *FileName)
{
  TObjString *ostr;
  TString str,xlab;

  TString  line, label, treeLabel;
  TString  rootFileName;
  TObjArray  *token,*labels; 
  TObjString *strtoken;
  Int_t nPars = 0;
  Int_t ntokens = 0;
  Int_t i = 0, iloop;
  Int_t nline=0;
  Int_t intNorm=0;
  Ssiz_t pos;
  Double_t x[100],yl1[100],yl2[100],yl3[100],yl4[100],yl5[100],yl6[100],yl7[100],bck[100],yl[100][100];
  Int_t Flag[7]=0;
  
  FILE *fp = fopen(FileName,"r");
  if ( fp == NULL ){
    printf("File %s does not exist!\n", FileName);
    return;
  }

  while (line.Gets(fp)){
    if ( nline==0 ){
      // First line, get data labels 
      nline++;
      labels = line.Tokenize(" ");
      ntokens = labels->GetEntries();
      nPars=ntokens;
      ostr = dynamic_cast<TObjString*>(labels->At(0));
      xlab = ostr->GetString();
    } else {
      token = line.Tokenize(" ");
      ntokens = token->GetEntries();

      // This is the energy
      // Plots for other scanned parameters need to be treated differently
      strtoken = (TObjString*) token->At(0);
      label = strtoken->GetName();
      x[i] = label.Atof();

      // Plot all layers for now. Maybe stop when sum is zero in the future
      // Layers start from column # 10 up to 16.
      //      for (col=10;col<17;col++) {

      // This is the backscattered part
      strtoken = (TObjString*) token->At(6);
      label = strtoken->GetName();
      bck[i] = label.Atof();
      if (bck[i]>0) { 
	Flag[7]=1;
	intNorm = intNorm++;
      }

      for (iloop=0; iloop<=6; iloop++) {
      	strtoken = (TObjString*) token->At(iloop+10);
      	label = strtoken->GetName();
      	yl[iloop][i] = label.Atof();
      	if (yl[iloop][i]>0) { 
      	  Flag[iloop]=1;
      	  intNorm = intNorm++;
      	}
      }
      nline++;
      i++;
    }
  }
  

  TCanvas *c = new TCanvas("c",str);
  c->Show();
  TLegend *legend = new TLegend(0.8, 0.8, 0.95, 0.95);
  TMultiGraph *mg = new TMultiGraph();
  TGraph *gr[100];

  for ( Int_t iloop=0; iloop<=6; iloop++) {
    if (Flag[iloop]==1){
      gr[iloop] = new TGraph(i,x,yl[iloop]);
      //  gr1->Draw("APC");
      gr[iloop]->SetMarkerStyle(20+iloop);
      gr[iloop]->SetMarkerColor(TColor::GetColorBright(iloop+1));
      gr[iloop]->SetLineColor(TColor::GetColorBright(iloop+1));
      gr[iloop]->SetFillColor(TColor::GetColor(255,255,255)); // white
      // Preferably get the chemical formula from header
      ostr = dynamic_cast<TObjString*>(labels->At(iloop+10));
      xlab = ostr->GetString();
      legend->AddEntry(gr[iloop],xlab);
      legend->SetFillColor(TColor::GetColor(255,255,255)); // white
      mg->Add(gr[iloop]);
    }
  }

  if (Flag[7]==1){
    TGraph *gr[7] = new TGraph(i,x,bck);
    gr[7]->SetMarkerStyle(20);
    gr[7]->SetMarkerColor(TColor::kAzure+7);
    gr[7]->SetLineColor(TColor::kAzure+7);
    gr[7]->SetFillColor(TColor::GetColor(255,255,255)); // white
    legend->AddEntry(gr[7],"Back Scat.");
    mg->Add(gr[7]);
  }
  
  mg->Draw("APC");
  mg->GetXaxis()->SetTitle("Energy [keV]");
  mg->GetYaxis()->SetTitle("Implanted Particles");
  legend->Draw();

  // I am not sure what this does, but it waits until canvas is closed
  c->WaitPrimitive(" ");
  cout << endl << "Canvas Closed" << endl ;

  // Then quit root cleanly
  gApplication->Terminate();
}

