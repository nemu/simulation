Dateienverzeichnis

TrimSp7L.f : Fortran Code von TrimSp
             rechnet 7 Layer mit jeweils maximal 5 Elementen
             100 Stuetzstellen
TrimSp7L.exe : executable
TrimSp7L-test.f : 
             rechnet 7 Layer mit jeweils maximal 5 Elementen
             1000 Stuetzstellen
TrimSp7L-test.exe : executable

Datmak7L   : Fortran Code zum Erstellen von input Dateien fuer TrimSp3L Rechnungen
             benutzt Stopping power nach ICRU (DateiL Stopicru.dat)
             wenn man andere Stopping powers nehmen moechte, muss man das im
             Fortran Code aendern

will man mit TrimSp7L-test rechnen, muss man in der Batch Datei TrimSp7L durch
TrimSp7L-test ersetzen

dichte.dat : Dichte der Elemente
masse.dat  : Masse der Elemente
elast.dat  : Festkoerperbindungsenergie der Elemente
stopicru   : Energieverlust von p in den Elementen, hier nach ICRU
stopping   : Energieverlust von p in den Elementen, hier nach AZ

generelle Beschreibung von TrimSp findet man in der Datei TRVMC95-3L.txt