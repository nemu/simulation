c===============================================================================
c                       GEO_KAMMER.INPUT
c===============================================================================

 $kammer_geo


 !==============================================================================
 !                      'HORIZONTALER' KAMMERTEIL
 !
 !      hier wird x relativ zum Zentrum der Moderationskammer (=Kryoachse)
 !      gemessen! (bis auf xgrid1 und xgrid2, die relativ zur Rueckseite
 !      der Moderatorfolie, also 'xTarget' gemessen werden)
 !      mit Schleussenkammer
 !==============================================================================


 ! Vakuumrohr:

        radius_Rohr       =  75.


 ! Target:

        xtarget           =   2.0   ! Position RELATIV ZUM KRYO
        dytarget          =  30.0               ! Breite
        dztarget          =  30.0               ! Hoehe


 ! erstes Beschleunigungsgitter:

        xgrid1            =  12.0       !  ->   ! Position RELATIV ZUM MODERATOR
        dygrid1           =  48.0               ! Breite
        dzgrid1           =  48.0               ! Hoehe
        dist_wires_G1     =   1.0               ! Drahtabstand
        dWires_G1         =   0.025             ! Drahtdurchmesser


 ! zweites Beschleunigungsgitter:

        xgrid2            =  22.0       !  ->   ! Position RELATIV ZUM MODERATOR
        dygrid2           =  48.0               ! Breite
        dzgrid2           =  48.0               ! Hoehe
        dist_wires_G2     =   1.0               ! Drahtabstand
        dWires_G2         =   0.025             ! Drahtdurchmesser


 ! He-Schild:

        rHeShield         =  47.25              ! Radius
        dyHeShield        =  40.0               ! Breite des Fensters
        dzHeShield        =  36.0               ! Hoehe des Fensters


 ! LN-Schild:

        rLNShield         = 52.75               ! Radius
        dyLNShield        = 40.0                ! Breite des Fensters
        dzLNShield        = 36.0                ! Hoehe des Fensters


 ! Linse 1:

        xCenterOfLense_L1 = 250.0               ! Position der Linsenmitte
        MappenName_L1     = 'L1_1mm_mesh'       ! Name der Potentialmappe


 ! Spiegel:

        xSpiegel          = 499.0               ! Position (=477.0 + 22.0)
        MappenName_Sp     = 'mirror'            ! Name der Potentialmappe

        DreharmLaenge     =   11.0               ! horizont. Abstand zwischen
                                                !   Aufhaengung und Spiegelmitte
        BSpiegel          = 114.0               ! Breite
        hSpiegel          = 100.0               ! Hoehe
        DSpiegel          =  22.5               ! Tiefe
        dist_wires_Sp     =   1.0               ! Drahtabstand
        dWires_Sp         =   0.050             ! Drahtdurchmesser


 !==============================================================================
 !                      'VERTIKALER' KAMMERTEIL
 !
 !      hier wird x relativ zum Zentrum des Doppelkreuzes der Spiegel-
 !      aufhaengung gemessen!
 !==============================================================================

 ! Linse 2:

        xCenterOfLense_L2 = 267.0         ! Position der Linsenmitte der neuen L2
                                                          ! Elektroden 50.0 (0V) 60.0 (HV) 100.0 (0V)
                                                          ! Elektrodenabstaende 12.0
        MappenName_L2andFo= 'L2andFo'           ! es existiert keine Potentialmappe 

 ! TriggerDetektor: alt: 372.0 Stutzen:+150 alte TK: 250 neue TK: 296

        xTD               = 586.0                       ! Aufhaengung des TDs == Mitte
                                                        ! Abstand bis auf Folie 534.0
        mappenName_Fo     = 'FO_1'              !            des Doppelkreuzes


 ! Linse 3

        xCenterOfLense_L3 = 1111.0                 ! Linsenmitte
        MappenName_L3     = 'LENSE-3'      ! Name der Potentialmappe


 ! alter MCP2:

        xMCP2                 = 1688.0          ! Position MCP
        radius_MCP2active =   20.0              ! Radius der aktiven Flaeche
        MappenName_M2   =  'MCP_RUN9'   ! Name der Potentialmappe
 
 ! neuer MCP2:

        xMCP2                   = 1698.0                ! Position MCP
        radius_MCP2active =   23.0      ?????   ! Radius der aktiven Flaeche
                                                        ! es existiert keine Mappe
 

 !
 ! fuer nachfolgende Teile existieren keine Feldmappen
 ! 
 ! Ringanode ungesplittet
 ! vordere Position bei 1623.0
 ! HV Elektrode
 ! vorderer Durchmesser:  66.8
 ! hinterer Durchmesser: 116.0
 ! gerade Laenge       :  90.0
 ! Neigungswinkel      :  16 grad
 ! 0V Elektrode
 ! Abstand zu HV Elektrode: 12.0
 ! Durchmesser            : 116.0
 ! Laenge                 : 116.0

 ! Probenplatte Kryostat:
 ! nachfolgende Masse beziehen sich auf die Montage des Saphirs mit D = 6.0mm
 ! wird der Saphir nicht verwendet, muessen die Positionen entsprechend korrigiert
 ! werden
 ! 1696.0 
 ! Probenoberflaeche je nach Probendicke bei 1694.0 bis 1696.0
 ! Durchmesser Probenplatte: 70.0 mm
 ! Befestigungsring Probe  : 1694 bis 1692 (hat eine Dicke von 2.0, Abstand haengt
 !                                          von Probendicke ab)                                                 
 ! Oeffnungsdurchmesser    : 40.0 oder 45.0 mm
 ! Aussendurchmesser       : 70.0 mm
 ! Guardringe 
 ! Innendurchmesser        : 58.0 mm
 ! Aussendurchmesser       : 70.0 mm
 ! Dicke                   :  3.0 mm
 ! Abstand Guard1 zu 
 ! Probenbefestigungsring            : 13.0 mm !oder zu Probenplatte bei s-RG Messungen
 ! Abstand Guard2 zu 
 ! Probenbefestigungsring            : 29.0 mm
 ! Abstand Erdungsgitter Kuehlschild 
 ! zu Probenbefestigungsring         : 48.0 mm !Anstand von Probenplatte aus gemessen
 ! Anstand Erdungsgitter Kuehlschild - Guardringen bzw. Probenoberflaechen haengt von
 !         Probendicke ab.
 ! Position Kuehlplatte (0V)   : 1706.0
 ! Durchmesser                 : 70.0 mm
 ! Position Kuehlfinger        : 1711.0
 ! Durchmesser                 : 55.0 mm  
 

 $END

c===============================================================================

die Drahtabstaende und -durchmesser der Gitter des Triggerdetektors (in mm):
============================================================================

 $trigger_grids

        dist_Wires_V1   = 1.500
        dWires_V1       = 0.025

        dist_Wires_V2   = 1.500
        dWires_V2       = 0.025

        dist_Wires_V3   = 1.500
        dWires_V3       = 0.025

        dist_Wires_H1   = 1.500
        dWires_H1       = 0.025

        dist_Wires_H2   = 1.500
        dWires_H2       = 0.025

        dist_Wires_G    = 1.500
        dWires_G        = 0.025

 $END

