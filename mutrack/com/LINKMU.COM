$ set noverify
$ set noon
$!==============================================================================
$ prog= "mutrack"
$ ext = "_MU"
$!==============================================================================
$ sourceDir =  "''prog'$SOURCEdirectory"
$ objectDir =  "''prog'$OBJdirectory"
$ executeDir = "''prog'$EXEdirectory"
$!==============================================================================
$ archi = F$GETSYI("ARCH_NAME")    ! Host OS either "VAX" or "Alpha"
$ ext = "''ext'_''archi'"
$ set verify
$!==============================================================================
$ link -
	'objectDir':MUTRACK'ext', -
	'objectDir':SUB_ARTLIST'ext', - 
	'objectDir':SUB_INTEGR_L1'ext', -
	'objectDir':SUB_INTEGR_SP'ext', -
	'objectDir':SUB_INTEGR_L2andFo'ext', -
	'objectDir':SUB_INTEGR_FO'ext', -
	'objectDir':SUB_INTEGR_L3'ext', -
	'objectDir':SUB_INTEGR_M2'ext', -
	'objectDir':SUB_INPUT'ext', -
	'objectDir':SUB_PICTURE'ext', -
	'objectDir':SUB_OUTPUT'ext',- 
	'objectDir':SUB_TRIGGER'ext', -
	'objectDir':SUB_ELOSS'ext', -
	'cernlibs' /exe='executeDir':MUTRACK_'archi'
$ purge 'executeDir':*.EXE
$ set on
$ set noverify
$!==============================================================================
