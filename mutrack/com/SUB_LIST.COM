$!==============================================================================
$! KOMMANDOPROZEDUR FUER DAS SUBMITTEN VON 'INPUTFILE-LISTEN' VON MUTRACK UND
$! ACCEL MITTELS 'SUBMULISTn' BZW. 'SUBACLISTn'.
$!==============================================================================
$! 1. LOESCHE GEGEBENENFALLS DAS .MESSAGE FILE
$! 2. IN DER SCHLEIFE: VERSUCHE DAS .MESSAGE-FILE ZU OEFFNEN
$! 3. GELINGT DIES NICHT, SO WURDE DIE KOMMANDOPROZEDUR GERADE ERST GESTARTET.
$!    EIN PROGRAMMDURCHLAUF WIRD DANN AUF JEDEN FALL PORBIERT.
$! 4. LIES BEI NEUEM VERSUCH ERST DIE NUMMER DES ZULETZT ABGEARBEITETEN INPUT-
$!    FILES. IST DIESE = 1, SO LOESCHE DAS .MESSAGE FILE UND BEENDE PROZEDUR
$!==============================================================================
$! P1 = KUERZEL DES PROGRAMMES  = "MU" bzw. "AC"
$! P2 = PROGRAMMAUFRUF		= "MUTRACK" bzw. "ACCEL"
$! P3 = QUEUE
$! P4 = NUMMER DER INPUTLISTE (WAHRSCHEINLICH AUF 1-9 EINGESCHRAENKT)
$! P5 = QUEUE-ENTRY, AUF DEN SYNCHRONISIERT WERDEN SOLL
$!==============================================================================
$! Definition der Filenamen:
$ INPUTLISTNAME = "LIST''P4'"
$ COMFILENAME   = "SYS$SCRATCH:'P1'_LIST'P4'.COM"
$ MESSFILENAME  = "SYS$SCRATCH:''P1'_LIST''P4'.MESSAGE"
$!
$! Oeffnen des files fuer die zu submittende Kommandoprozedur:
$ FILE = F$SEARCH("''COMFILENAME'")
$ IF FILE .NES. "" THEN DELETE 'COMFILENAME'.* /NOCON
$ OPEN /WRITE comfile 'COMFILENAME'
$ OUT := WRITE COMFILE
$!
$! Erstellen der zu submittenden Kommandoprozedur:
$ OUT "$! DIESE KOMMANDOPROZEDUR WURDE DURCH 'SUB_LIST.COM' ERSTELLT UND WIRD
$ OUT "$! NUR TEMPORAER BENOETIGT, BIS DER DURCH ""$ SUB''P1'LIST ''P4'"" GESTARTETE
$ OUT "$! BATCHJOB ABGEARBEITET IST. BEI NORMALEM ENDE DES BATCHJOBS WIRD DIESES
$ OUT "$! FILE ZUM SCHLUSS GELOESCHT!"
$ OUT "$!=============================================================================
$ OUT "$ SET NOON
$ OUT "$! 
$ IF P5 .NES. ""
$ THEN
$	OUT "$ SYNCHRONIZE /ENTRY = ''P5' 
$	OUT "$!
$ ENDIF
$ OUT "$ DEFINE/NOLOG INPUTLISTNAME ''INPUTLISTNAME'"
$ OUT "$! in case a privious submitted batchjob didn't end properly:
$ OUT "$ FILE = F$SEARCH(""''MESSFILENAME'"")
$ OUT "$ IF FILE .NES. """" THEN DELETE ''MESSFILENAME'.* /NOCON
$ OUT "$!
$ OUT "$ FIRSTTIME = ""TRUE""
$ OUT "$ LOOP_START:
$ OUT "$ OPEN /SHARE /READ /ERROR=OPEN_ERROR messagefile ''MESSFILENAME'
$ OUT "$ READ  messagefile FILENR
$ OUT "$ CLOSE messagefile
$ OUT "$ IF (FILENR.EQ.1) THEN GOTO FINISH
$ OUT "$!
$ OUT "$ PROG_CALL:
$ OUT "$ ''P2'
$ OUT "$ GOTO LOOP_START
$ OUT "$!
$ OUT "$ FINISH:
$ OUT "$ FILE = F$SEARCH(""''MESSFILENAME'"")
$ OUT "$ IF FILE .NES. """" THEN DELETE ''MESSFILENAME'.* /NOCON
$ OUT "$ WRITE SYS$OUTPUT ""''P1'_LIST''P4' FINISHED PROPERLY""
$ OUT "$ EXIT
$ OUT "$!
$ OUT "$!------------------------------------------------------------------------------
$ OUT "$ OPEN_ERROR:
$ OUT "$ IF (FIRSTTIME.EQS.""TRUE"")
$ OUT "$ THEN
$ OUT "$	FIRSTTIME = ""FALSE""
$ OUT "$	GOTO PROG_CALL
$ OUT "$ ELSE
$ OUT "$!	in spite of the program has already been run there is no .MESSAGE file
$ OUT "$!	=> all input files have already been done => exit
$ OUT "$	GOTO FINISH
$ OUT "$ ENDIF
$ OUT "$!------------------------------------------------------------------------------
$ CLOSE COMFILE
$!
$! Submitten der Kommandoprozedur:
$ SUBMIT/DELETE/NOTIFY/NOPRINT/QUEUE='P3'/NAME='P1'LIST'P4' -
   /LOG_FILE='P2'$OUTdirectory 'COMFILENAME'
$!
$ EXIT
