c-------------------------------------------------------------------------------
c		Die verwendeten Directories
c-------------------------------------------------------------------------------

	character*(*)	MappenDir,TP_Dir,ACCEL_Dir,readDir,outDir,NrDir
	character*(*)	TMPDIR,geoDIR

	parameter (MappenDir = 'mutrack$mappenDirectory'  )
	parameter (TP_Dir    = 'mutrack$TPdirectory'      )
	parameter (ACCEL_Dir = 'mutrack$AHdirectory'      )
	parameter (readDir   = 'mutrack$READdirectory'    )
	parameter (geoDir    = 'mutrack$GEOdirectory'     )
	parameter (outDir    = 'mutrack$OUTdirectory'     )
	parameter (NrDir     = 'mutrack$NrDirectory'      )
	parameter (TMPDir    = 'SYS$SCRATCH'              )

