
c===============================================================================
c                          MAP_DEF_FO.INC
c===============================================================================

c in diesem File sind die notwendigen Groessen und Speicher fuer die Integration
c der Teilchenbahnen im Bereich vor der TD-Folie mittels des Programms 'MUTRACK'
c niedergelegt:

c general items:
c (also known in some other parts of the program via include file
c 'COM_KAMMER.INC')

	real Beschl_Faktor
	COMMON /Beschl_Faktor_Fo/ Beschl_Faktor

	character*40 MappenName
	real dl_max		! naeherungsweise Obergrenze fuer die einzelnen
				! Schrittlaengen:
	real MappenLaenge
	real xEnterMap

	COMMON /integration_Fo/ MappenName,dl_max,MappenLaenge,xEnterMap


c the grid characteristics:

	real Dx,Dr
	integer imax,jmax
	real xmax,rmax

	COMMON /integration_Fo/ Dx,Dr,imax,jmax,xmax,rmax


c der Uebergabebereich am MappenEnde:

	real xStartUeber
	real xEndUeber

	COMMON /integration_FO/ xStartUeber,xEndUeber


c the map:

        integer maxmem
        parameter (maxmem = 20595)
        real    map(0:maxmem)

        COMMON /integration_Fo/ map

