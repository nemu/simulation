	function fit_prec(t)

	real t, t0
	vector tzero(1)
	common/pawpar/par(6)
	

c 	t:	    time		    [ns]

c	par(1):	    Background		    [1]
c	par(2):	    normalization	    [1]
c	par(3):	    asymmetry		    [1]
c	par(4):	    frequency		    [MHz]
c	par(5):	    phase		    [degree]
c	par(6):	    muon lifetime	    [microsec]
c	par(7):	    relaxation		    [1/microsec]

c um richtige Fitergebnisse zu erhalten, muss der Fitroutine der Zeitnullpunkt
c des Spektrums mitgeteilt werden:

	t0 = tzero(1)
	t = t-t0


	twopi = 6.283185  
	fit_prec = par(1)+				  ! Background
     +	  par(2)*					  ! normalization
     +	  exp(-t/(1000.*par(6)))*			  ! decay
     +	  ( 1.+par(3)*					  ! asymmetry
c     +	    exp(-t*(par(7)/1000.))*			  ! relaxation
     +	    cos( twopi*(par(4)/1000. *t + par(5)/360.))   ! oscillation
     +    )

	end

