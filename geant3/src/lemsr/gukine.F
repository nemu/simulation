*
#include "geant321/pilot.h"
*
c------------------------------------------------------------------------------
c
      subroutine gukine
c
c       T. Prokscha,    15-Sep-2000,    PSI
c
c       Unix version
c
c       TP,     05-Apr-2001     PSI: Unix, NT, Linux
c
c------------------------------------------------------------------------------
c
      implicit none
c
#define CERNLIB_TYPE
#include  "geant321/gckine.inc"
#include  "geant321/gclist.inc"       
#include  "geant321/gctrak.inc"
c
#include  "cwn.inc"
#include  "common.inc"
c
c-----------------------------------------------------------------------------
c
c     local variables
c
      real*4       decay_rot(3) ! rotation of muon spin, 
c                               ! rotated e+-momenta
      real*4       x0, y0, z0, px0, py0, pz0, p0, pos_tot
      real*4       radius, phi0, cosphi, sinphi, costheta, sintheta
      real*4       theta2d, theta_beam, sintheta_beam, costheta_beam
      real*4       proj, phi_beam, sinphi_beam, cosphi_beam, sigma_beam
      integer*4    ntbeam,nttarg,nubuf,i
      parameter    (nubuf = 1)
      real*4       ubuf(nubuf)
      real*4       random_1(3), random_2(7)
      real*4       sigma_p
      real*4       off
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
        ipart = lsets(1)            
c
c-----------------------------------------------------------------------------
c
c       throw decay point at MCP, isotropic distribution and MICHEL-spectrum
c
        if ( lsets(2) .eq. 0 .and. lsets(7) .ne. 1) then
c
#if defined (OS_UNIX)
        call ranlux(random_1,3)
#else
        do i = 1, 3
           random_1(i) = ran(ix1)
        enddo
#endif
c
c------------------
c
c           the decay point at MCP or sample
c
c           z0 = +1.6      ! distance of MCP center to center of MARS
c           z0 = +1.48002 ! the Hamamatsu MCP's end is at z=1.48cm
c
c           set MCP2 to 1.52cm; doing this we have the same initial z-position
c           for the e+ with MCP mounting and sample mounting:
c
c           Cu ring of sample (z/2 = 0.2cm) at z = 1.6 cm
c           MCP Hamamatsu Z-stack (z/2 = 0.12cm) at z = 1.52 cm
c
            off = 0.
            z0  = +1.40002+off ! the Hamamatsu, Galileo MCP's end and sample end is at z=1.40cm now
c                       ! z=1.40002 corresponds to an implatation depth of the
c                       ! mu+ of 200 nm.
c
            radius = beam_parameter(3) * R_MCP * sqrt(random_1(1)) ! uniform distribution on MCP
c           radius = 4.7
            phi0   = TWO_PI * random_1(2)
            x0     = radius * cos(phi0) + beam_parameter(1)
            y0     = radius * sin(phi0) + beam_parameter(2)
c
c---------------------------
c
c           the decay time
c
            tofg = -TAU_MUON*alog(1.-random_1(3))
c
c           the michel spectrum
c
            call michel(pos_tot, p0)
c
c           anisotropic angular distribution, P=100%
c
            call get_direction(costheta, sintheta, cosphi, sinphi, 
     1                         pos_tot)
c
c           get the single momentum components in the laboratory system
c           (the muon spin is directed along the x-axis which points to the
c           LEFT detector.)
c
            if ( bfield(1) .ne. 0. or. bfield(2) .ne. 0. or. bfield(3)
     1          .ne. 0.) then
c
c               rotate when there is a B-field present; the subroutine
c               ROTATE(...) will return without any action if bfield(4)=pol.=0.
c
                decay_rot(1) =  p0 * costheta
                decay_rot(2) =  p0 * sintheta * cosphi
                decay_rot(3) =  p0 * sintheta * sinphi
c
                call rotate(decay_rot, tofg)
c
                px0 = decay_rot(1)
                py0 = decay_rot(2)
                pz0 = decay_rot(3)
c
            else
c
                px0 = p0 * costheta
                py0 = p0 * sintheta * cosphi
                pz0 = p0 * sintheta * sinphi
c
c               quick and dirty, introduce long. polarization in -z
c               rotate spin in z direction by 180 degree around y axis; have to keep
c               right-handed cartesian system. I could also rotate around the x-axis
c
c                pz0 = -p0 * costheta
c                px0 = -p0 * sintheta * cosphi
c                py0 =  p0 * sintheta * sinphi
c
c               quick and dirty, introduce long. polarization in +z
c
c                pz0 = p0 * costheta
c                px0 = p0 * sintheta * cosphi
c                py0 = p0 * sintheta * sinphi

            endif
c
c-------------------------
c
c       read input data from file
c
        else if ( lsets(7) .eq. 1 ) then
c
            read(70) x0, y0, z0, px0, py0, pz0
            z0 = z0-0.3
            x0 = x0 + beam_parameter(1)
            y0 = y0 + beam_parameter(2)
c
c-------------------------
c
c           for beam particles hitting MCP with momentum defined by
c           lsets(3), lsets(4), lsets(5)
c
        else
#if defined (OS_UNIX)
            call ranlux(random_2,7)
#else
            do i = 1, 5
             random_2(i) = ran(ix1)
            enddo
#endif
c           z0  = -74.              ! start in vacuum upstream from MCP
            z0  = float(lsets(2))   ! start in vacuum at z=z0;
            radius = beam_parameter(3) * R_MCP * sqrt(random_2(1)) ! uniform distribution on MCP
            phi0   = TWO_PI * random_2(2)
            x0     = radius * cos(phi0) + beam_parameter(1)
            y0     = radius * sin(phi0) + beam_parameter(2)
c
c           if sets(8) > 0 throw gaussian momentum distribution
c 
            p0 = pkine(1)
            if ( lsets(8) .gt. 0 ) then
               sigma_p = p0 * (lsets(8)/100. + lsets(9)/1000.)
               theta2d = sigma_p * 
     1                        sqrt( -2. * alog(1.-random_2(6)))
               proj    = sin( TWO_PI * random_2(7))
               p0 = p0 + theta2d*proj
            endif
c
c           throw THETA gaussian distributed to get some divergence of
c           the beam
c
            sigma_beam = float(lsets(6))    ! in degree
c
            if ( sigma_beam .eq. 0. ) then
                px0 = 0.
                py0 = 0.
                pz0 = p0
            else
              sigma_beam   = sigma_beam / 57.29577951  ! in rad
              theta2d      = sigma_beam * 
     1           sqrt( -2. * alog(1.-random_2(3)))
              proj         = sin( TWO_PI * random_2(4))
              theta_beam   = proj * theta2d
              sintheta_beam= sin(theta_beam)
              costheta_beam= cos(theta_beam)
c
              phi_beam     = TWO_PI * random_2(5)
              sinphi_beam  = sin(phi_beam)
              cosphi_beam  = cos(phi_beam)
c
              px0 = p0 * sintheta_beam * cosphi_beam
              py0 = p0 * sintheta_beam * sinphi_beam
              pz0 = p0 * costheta_beam
c
            endif
c               
        endif
c       
c-----------------------------------------------------------------------------
c
c       type*,' ivert = ',ivert
c
        vert(1)  = x0
        vert(2)  = y0
        vert(3)  = z0
        pvert(1) = px0/1000.    ! GEANT uses GeV/c
        pvert(2) = py0/1000.
        pvert(3) = pz0/1000.
        nt_x0    = x0
        nt_y0    = y0
        nt_z0    = z0
        nt_px0   = px0
        nt_py0   = py0
        nt_pz0   = pz0
        nt_p0    = p0
        nt_ipart = ipart
c
c       write(6,*) x0,y0,z0,px0,py0,pz0
c
c       store "vertex" (this means in this case the initial position and
c       momentum of the paricle) data to the GEANT data structures
c       JVERTX and JKINE. It has to be done !
c
        call gsvert(vert,ntbeam,nttarg,ubuf,nubuf,ivert)
        call gskine(pvert,ipart,ivert,ubuf,nubuf,itra)
c       call gpvert(ivert)
c       call gpkine(itra)
c
        return
        end
