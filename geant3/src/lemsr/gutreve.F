c#include "geant321/pilot.h"
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine gutreve
c
c     T. Prokscha,      15-Sep-2000,    PSI
c
c     Unix version, part of the geant_lemsr program
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      implicit none
c
c     include again some GEANT variables
c 
c     INTEGER       IKINE,ITRA,ISTAK,IVERT,IPART,ITRTYP,NAPART,IPAOLD
c     REAL          PKINE,AMASS,CHARGE,TLIFE,VERT,PVERT
c
c#include "geant321/gtkine.inc"
c#include "geant321/gckine.inc"
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     type*,' routine GUTREVE'
      call gtreve
c
      return
      end
