c
c       cwn.inc
c
c       include variable declaration for cwn Ntuples
c
c       TP,     15-Sep-2000,    PSI
c
c-----------------------------------------------------------------------------
c
c     CWN variables
c       
      real*4      nt_x0, nt_y0, nt_z0, nt_px0, nt_py0, nt_pz0, nt_p0
      integer*4   nt_ipart
      real*4      nt_xe, nt_ye, nt_ze, nt_eposend
      real*4      nt_xm, nt_ym, nt_zm, nt_emend
      real*4      nt_tmcp, nt_tsci    ! times are integer, use 1ns bins
      real*4      nt_xsci, nt_ysci, nt_zsci
      real*4      nt_desci, nt_descipos, nt_descigam, nt_desciele
      real*4      nt_xsco, nt_ysco, nt_zsco
      real*4      nt_desco, nt_descopos, nt_descogam, nt_descoele
      integer*4   nt_volno, nt_partcode
      integer*4   nt_mcpnhits, nt_scinhits, nt_sconhits
      real*4      nt_pxpos, nt_pypos, nt_pzpos
      real*4      nt_demcp
      real*4      nt_descil, nt_descit, nt_descir, nt_descib,
     1              nt_descol, nt_descot, nt_descor, nt_descob
c
c    the common blocks
c
      common  /de_ltrb/  nt_descil, nt_descit, nt_descir, nt_descib,
     1                     nt_descol, nt_descot, nt_descor, nt_descob
      common  /init_par/ nt_x0, nt_y0, nt_z0, nt_px0, nt_py0, nt_pz0, 
     1                     nt_p0
      common /partcode/ nt_ipart
      common /end_pos/ nt_xe, nt_ye, nt_ze, nt_eposend
      common /end_muon/ nt_xm, nt_ym, nt_zm, nt_emend
      common /times/ nt_tmcp, nt_tsci
      common /sci_pos/ nt_xsci, nt_ysci, nt_zsci
      common /sci_elos/ nt_desci, nt_descipos, nt_descigam, nt_desciele
      common /sco_pos/ nt_xsco, nt_ysco, nt_zsco
      common /sco_elos/ nt_desco , nt_descopos, nt_descogam, nt_descoele
      common /flags/ nt_volno, nt_partcode
      common /nhits/ nt_mcpnhits, nt_scinhits, nt_sconhits
      common /pos_mom/ nt_pxpos, nt_pypos, nt_pzpos
      common /de_mcp/ nt_demcp
c
c-----------------------------------------------------------------------------
