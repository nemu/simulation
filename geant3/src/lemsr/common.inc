c
c       ~/mc/geant/lemsr/src/common.inc
c
c       GEANT_LEMSR program,
c       include common blocks other than NTuple variables
c
c       TP,     05-Apr-2001,    PSI
c
c	17-Dec-2002	TP	extended bfield array from 4 to 5,
c				bfield(5)=polarisation in sample
c
c-----------------------------------------------------------------------
c
c       check OS
c
#if defined( _WIN32 )
#define OS_WIN
#else
#define OS_UNIX
#endif
c
c         parameter
c
      character*(*) IN_DIR, OUT_DIR
#if defined(OS_WIN)
      parameter IN_DIR  = 'c:\users\thomas\mc\geant\inp\'
      parameter OUT_DIR = 'c:\users\thomas\mc\geant\data\'
#else
      parameter (IN_DIR  = 
     1 '/afs/psi.ch/user/p/prokscha/simulation/geant3/inp/')
c      parameter (OUT_DIR = '/afs/psi.ch/user/p/prokscha/mc/geant/data/')
      parameter (OUT_DIR = '/scratch/prokscha/geant/')
#endif
c
      character*(*) INFILE, PARFILE    
      parameter     (INFILE  = IN_DIR//'geant_lemsr_input.dat')
      parameter     (PARFILE = IN_DIR//'geant_lemsr.input')
c
c
c      INFILE:  read input data from file instead of throwing them
c              in the program
c
c      PARFILE: read input parameter from this file
c
      real*4    MU_MASS, TWO_PI, POS_MASS, SQR_POS_MASS, 
     1          MAX_TOTAL_ENERGY, TAU_MUON, MU_GYRO, R_MCP, C_LIGHT
      parameter (MU_MASS   = .105658389 )       !muon mass, GeV/c^2
      parameter (POS_MASS  =  0.51099906 )      ! e+ mass, MeV/c^2
      parameter (SQR_POS_MASS =  0.26112 )      ! squared positron mass
      parameter (MAX_TOTAL_ENERGY = 52.8304302) ! MeV
c 
      parameter (TAU_MUON     = 2.19703E-6 )    ! muon life time in s
      parameter (MU_GYRO      = 13554.)         ! Hz/G
c 
      parameter (R_MCP        = 2.0 )          ! radius of MCP in cm
      parameter (TWO_PI = 6.283185307)
      parameter (C_LIGHT = 2.99792458E10)      ! speed of light in cm/s 
c
c-------------------------------------------------------------------
c
      real*4      bfield(5)         ! new data card with B-field components
c                                   ! bfield(4) = polarization (%)
c			            ! bfield(5) = polarization (%) in sample
      real*4      beam_parameter(4) ! new data card with beam parameters
                                    ! (1): x0
                                    ! (2): y0
                                    ! (3): r-fraction  r*R_MCP2 defines size
                                    ! (4): exponential relaxation rate 1/ns
      integer*4 ix1                 !random generator seed
c
c         count successfull and rejected MICHEL-energy throws
c
      integer*4    cou, rejcou
c
c------------------------------------------------------------------------------
c
c     flags for geometrical setup; it is possible to mount or remove the
c
c     'mcp2'  MCP2 plates
c     'mcpa'  MCP2 anode
c     'mcps'  MCP2 stainless steel vacuum tube
c     'samp'  sample  (this is at the moment the sample holder)
c     'cryo'  material of the cryostat without shielding
c     'crsh'  cryo shield
c     'efla'  the 100 CF flange at the end of the sample/MCP2 tube
c
c     the volumes are activated when printing
c
c     geom 'mcp2' 'efla'  for example when data cards are requested   
c
      logical*1   l_mcp2        
      logical*1   l_mcpa
      logical*1   l_mcps
      logical*1   l_samp
      logical*1   l_cryo
      logical*1   l_crsh
      logical*1   l_efla    
      logical*1   l_run11   
      character*4 samp_mat  
c
c------------------------------------------------------------------------
c
c       flags for secondary particles
c
      integer*4   lspar(10)   ! new data card defined by CALL FFKEY to
c                             ! select seoncdary particles
c                             ! '  e+', '  e-' and ' gam' possible.
c
      logical*1   l_pos 
      logical*1   l_ele 
      logical*1   l_gam 
c
c-------------------------------------------------------------------------
c
c     flags for output of percentage of processed events
c
      integer*4   percent(9) 
c
c-------------------------------------------------------------------------
c
c---  the common blocks
c 
      common /beam_offset/ beam_parameter
      common /magnetic_field/ bfield
      common /seeds/ ix1
      common /counter/ cou, rejcou
      common /geo_flags/ l_mcp2, l_mcpa, l_mcps, l_samp, l_cryo, 
     1                   l_crsh, l_efla, l_run11, samp_mat
      common /part_flag/ l_pos, l_ele, l_gam
      common /proc_event/ percent

c-----------------------------------------------------------------------
