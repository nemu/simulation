#include "geant321/pilot.h"
c
c-----------------------------------------------------------------------
c
      subroutine ugeom
c
c     this subroutine defines the geometrical set-up.
c     The z-axis is defines as the low-energy mu+ beam axis.
c     This means x-axis --> horizontal, pointing to the left
c                y-axis --> vertical, upwards.
c
c     T. Prokscha,      15-Sep-2000,    PSI
c
c     Unix version, part of the geant_lemsr program
c
c       TP,     05-Apr-2001,    PSI             Unix, NT, Linux
c
c------------------------------------------------------------------------
c
      implicit none
c
#include "common.inc" 
c
#define CERNLIB_TYPE
#include "geant321/gctmed.inc"     
#include "geant321/gcsets.inc"
#include "geant321/gclist.inc"      
#include "geant321/gcflag.inc"     
#include "geant321/gcphys.inc"       
#include "geant321/gccuts.inc"       
#include "geant321/gckine.inc"                                       
c
c-----------------------------------------------------------------------------
c
      real*4       par_box(3)
      real*4       par_tube(3)   
      real*4       par_tubs(5)    ! segment of a tube
      real*4           par_cone(5)
c
      real*4       theta1,theta2,theta3,phi1,phi2,phi3    !rotation angles
      integer*4    ivol                                   !for the side arm
c
      character*4  namesv(4)                              !for detector descr.
      character*4  namesh(7)                              ! to define hit
      integer*4    nbitsh(7)                              ! paramters on MCP
      real*4       orig(7),fact(7) 
c
      real*4       atomw(9),zatom(9),wmat(9),dens ! to define MCP material

c
      integer*4    mat_type   ! material type for sample plate
      real*4       off
      real*4       he_shield_thickness
      integer*4    Air, Vacuum, Al, Fe, Cu, Steel, Brass, Sapphire,
     1             Macor, MCP, NaI, Scint ! medium numbers
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      data namesh /'x   ','y   ','z   ','t   ','e   ','elos', 'part'/
      data nbitsh /7*32/
      data orig   /3*100.,0.,0.,0.,0./
      data fact   /3*100.,1.e+9,1000000.,1000000.,1./
c
c     the values for x,y,z,t,e and eloss will be internally converted
c     by GEANT to integers; 
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     MCP material data are taken from J.L.WIZA,Microchannel Plate Detectors,
c     Nucl.Instr.Meth. 162 (1979), 587-601
c
c     data         dens  /4./ ! density of MCP glass
      data         dens  /2./ ! about half of the MCP is "empty" due to
c                               ! the MCP channels  
      data         zatom /82.,8.,14.,19.,37.,56.,33.,55.,11./
      data         wmat  /.479,.258,.182,.042,.018,.013,.004,.002,.001/
      data         atomw /207.19,15.999,28.086,39.10,85.47,137.33,74.92,
     1                      132.91,22.99/
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     define materials
c
      call gmate    ! standard GEANT materials
c
c     some additional materials
c
c     define LEAD GLASS (MCP), NaI
c
      call gsmixt(17,'MCPGLASS',atomw,zatom,dens,9,wmat)
c
c     define NaI
c
      zatom(1) = 11.
      atomw(1) = 22.99
      zatom(2) = 53
      atomw(2) = 126.9
      wmat(1)  = 1.
      wmat(2)  = 1.
      dens     = 3.67
      call gsmixt(18,'NaICALOR',atomw,zatom,dens,-2,wmat)
c
c     define Scintillator NE102A
c
      zatom(1) = 1.
      atomw(1) = 1.0079
      zatom(2) = 6.
      atomw(2) = 12.011
      wmat(1)  = 1.104
      wmat(2)  = 1.
      dens     = 1.032
      call gsmixt(19,'SCINT',atomw,zatom,dens,-2,wmat)
c
c     stainless steel
c
      dens     = 7.930
      zatom(1) = 26.
      zatom(2) = 24.
      zatom(3) = 28.
      atomw(1) = 55.85
      atomw(2) = 52.00
      atomw(3) = 58.70
      wmat(1)  = 0.71
      wmat(2)  = 0.18
      wmat(3)  = 0.11
      call gsmixt(20,'STEEL',atomw,zatom,dens,3,wmat)
c
c     brass
c
      dens     = 8.67
      zatom(1) = 29.
      zatom(2) = 30.
      atomw(1) = 63.55
      atomw(2) = 65.38
      wmat(1)  = .80
      wmat(2)  = .19
c
      call gsmixt(21,'BRASS',atomw,zatom,dens,2,wmat)
c
c     define Sapphire == AL2O3
c
      zatom(1) = 13.
      atomw(1) = 26.98
      zatom(2) =  8.
      atomw(2) = 16.00
      wmat(1)  = 2.
      wmat(2)  = 3.
      dens     = 3.985
c
      call gsmixt(22,'SAPPHIRE',atomw,zatom,dens,-2,wmat)
c
c     define MACOR as SiO2 with higher density; the correct compound is
c 
c     (SiO2)46 (Al2O3)16 (MgO)17 (K2O)10 (B2O3)7 
c
      zatom(1) = 14.
      atomw(1) = 28.0855
      zatom(2) =  8.
      atomw(2) = 16.00
      wmat(1)  = 1.
      wmat(2)  = 2.
      dens     = 2.52
c
      call gsmixt(23,'MACOR',atomw,zatom,dens,-2,wmat)
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     define tracking medium numbers corresponding to the material numbers
c     stored in JMATE by calling GMATE
c
c      9 = Aluminum
c     10 = Iron
c     11 = Copper
c     15 = Air
c     16 = Vacuum
c     17 = MCP
c     18 = NaI  <--- not used for present setup
c     19 = Scintillator
c     20 = Stainless steel
c     21 = brass <--- not used
c     22 = sapphire   
c     23 = macor
c
      isvol = 0
      ifield = 0
c     epsil = 0.0001      ! boundary crossing precision set to 1 micron
      epsil = 0.001      ! boundary crossing precision set to 10 micron
c
      call gstmed( 1,'Air        $', 15, isvol, ifield, fieldm, 
     1               tmaxfd, STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 2,'Vacuum     $', 16, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 3,'Aluminum   $',  9, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 4,'Iron       $', 10, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 5,'Copper     $', 11, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 6,'Steel      $', 20, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 7,'Brass      $', 21, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 8,'Sapphire   $', 22, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed( 9,'Macor      $', 23, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
c
      isvol = 1   ! sensitive volume; this means this volume is a detector
      call gstmed(10,'MCPDetector$', 17, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed(11,'NaIDetector$', 18, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
      call gstmed(12,'Scintillato$', 19, isvol, ifield, fieldm, tmaxfd,
     1                STEMAX, deemax, epsil, stmin, 0, 0)
c
      Air      = 1
      Vacuum   = 2
      Al       = 3
      Fe       = 4
      Cu       = 5
      Steel    = 6
      Brass    = 7
      Sapphire = 8
      Macor    = 9
      MCP      = 10
      NaI      = 11
      Scint    = 12
c
c     redefine kinetic cut energies for muons in all media from its 
c     default value = 10 MeV to 10keV =0.00001 GeV
c
c     --->  define cut energies via CUTS input cards
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     define MOTHER VOLUME MARS
c
        par_box(1) = 100.
        par_box(2) = 100.
        par_box(3) = 100.
c
        call gsvolu('MARS','BOX ', Air, par_box, 3, ivol)
        if (ivol.lt.0) goto 900                   !this means an error during
c                                                                                         !call to GSVOLU
c------------------------------------
c
c       vacuum tube of sample or MCP2, data from Balzers offer
c
        par_tube(1) = 0.
        par_tube(2) = 7.65
        par_tube(3) = 16.2 
c
        call gsvolu('MCPV','TUBE', Vacuum, par_tube, 3, ivol) 
        if (ivol.lt.0) goto 900
c
c       now stainless steel part of tube
c
        par_tube(1) = 7.65
        par_tube(2) = 7.95
        par_tube(3) = 16.2 
c
        call gsvolu('MCPS','TUBE', Steel, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
c
c       160 CF flange upstream of MCP2 tube
c
        par_tube(1) = 7.95
        par_tube(2) = 10.125
        par_tube(3) = 1.1 
c
        call gsvolu('F160','TUBE', Steel, par_tube, 3, ivol) 
        if (ivol.lt.0) goto 900
c
c       100 CF flange at the end
c
        par_tube(1) = 0.
        par_tube(2) = 7.7
        par_tube(3) = 1.0 
c       
        call gsvolu('F100','TUBE', Steel, par_tube, 3, ivol) 
        if (ivol.lt.0) goto 900
c
c       200 CF flange upstream of MCP2 tube to connect to gate valve chamber
c
        par_tube(1) = 7.65
        par_tube(2) = 10.325
        par_tube(3) = 1.2 
c
        call gsvolu('F200','TUBE', Steel, par_tube, 3, ivol)
        if (ivol.lt.0) goto 900
c
c       par_tube(1) = 0.
c       par_tube(2) = 7.95
c       par_tube(3) = 1.2 
c
c       call gsvolu('T200','TUBE', Vacuum, par_tube, 3, ivol) 
c       if (ivol.lt.0) goto 900
c
c       the gate valve chamber
c
c
        par_tube(1) = 0.
        par_tube(2) = 10.325
        par_tube(3) = 9.25 
c
        call gsvolu('GATV','TUBE', Vacuum, par_tube, 3, ivol) 
        if (ivol.lt.0) goto 900
c
c       for simplicity choose outer diameter of the Gate valve chamber equal
c       to the outer diameter of 200 CF flange
c
        par_tube(1) = 10.325
        par_tube(2) = 12.65
        par_tube(3) = 9.25
c
        call gsvolu('GATS','TUBE', Steel, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
c
c       the L3 tube, vacuum
c
        par_tube(1) = 0.
        par_tube(2) = 10.0
        par_tube(3) = 23.2 
c
        call gsvolu('L3VA','TUBE', Vacuum, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
c
c       the L3 tube, stainless steel
c
        par_tube(1) = 10.0
        par_tube(2) = 10.3
        par_tube(3) = 23.2 
c
        call gsvolu('L3ST','TUBE', Steel, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
c
c       conical anode
c
c       the downstream part of HV piece
c
        par_cone(1) = 2.5  ! half length dz
        par_cone(2) = 4.8  ! inside radius at -dz
        par_cone(3) = 6.2  ! outside radius at -dz
        par_cone(4) = 3.3  ! inside radius at dz
        par_cone(5) = 3.6  ! outside radius at dz
c
        call gsvolu('RA-E','CONE', Steel, par_cone, 5, ivol)  
        if (ivol.lt.0) goto 900
c
c       the middle part, still on HV
c
        par_cone(1) = 2.0  ! half length dz
        par_cone(2) = 5.8  ! inside radius at -dz
        par_cone(3) = 6.2  ! outside radius at -dz
        par_cone(4) = 4.8  ! inside radius at dz
        par_cone(5) = 6.2  ! outside radius at dz
c
        call gsvolu('RA-M','CONE', Steel, par_cone, 5, ivol)  
        if (ivol.lt.0) goto 900
c
c       ground cylinder, stainless steel
c
        par_tube(1) =  5.8
        par_tube(2) =  6.2
        par_tube(3) =  5.8 
c
        call gsvolu('RA-G','TUBE', Steel, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
c
c------------------------------------------------------
c
c       sample cryo things 
c
czzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
c
c       sample holder
c
c       1. Cu plate (sample holder) on Cold finger, 0.5cm
c       
        par_tube(1) = 0.
        par_tube(2) = 3.5
c        par_tube(2) = 2.1   ! small sample plate
        par_tube(3) = 0.25
c
        if ( samp_mat .eq. 'saal' ) then
            mat_type = Al    ! Al
        else
            mat_type = Cu    ! CU
        endif
c
        call gsvolu('SAH1','TUBE',mat_type,par_tube,3,ivol) 
c        print*,'SAH1 ivol = ',ivol
        if (ivol.lt.0) goto 900
c
c       2. Cu plate (0.4cm), sample is mounted on this plate
c       
        par_tube(1) = 0.
        par_tube(2) = 3.5
c        par_tube(2) = 2.1  ! small sample plate
        par_tube(3) = 0.2
c
        call gsvolu('SAH2','TUBE',mat_type,par_tube,3,ivol) 
c        print*,'SAH2 ivol = ',ivol
        if (ivol.lt.0) goto 900
c
c       now select new color if sample plates are Al
c
        if ( mat_type .eq. Al ) then
            call gsatt('SAH1', 'COLO', 4)
            call gsatt('SAH2', 'COLO', 4)
        endif
c
c       Sample mounting ring (0.1cm), 4cm inner diameter
c       
c        par_tube(1) = 1.5
c        par_tube(2) = 2.1
        par_tube(1) = 2.
        par_tube(2) = 3.5
        par_tube(3) = 0.05
c
        call gsvolu('SAH3','TUBE',mat_type,par_tube,3,ivol)     
c        print*,'SAH3 ivol = ',ivol
        if ( mat_type .eq. Al ) then
            call gsatt('SAH3', 'COLO', 4)       ! set color for aluminum
        endif
        if (ivol.lt.0) goto 900
        if ( l_samp) then
            write(6,*) ' '
            if ( mat_type .eq. Cu ) then
                write(6,*) ' Sample holder ring: Cu.'
            else if ( mat_type .eq. Al ) then 
                write(6,*) ' Sample holder ring: Al.'
            else
                write(6,*) ' Sample holder ring: ', mat_type
            endif
            write(6,*) ' '
        endif
c
c
c       Sapphire mounted between 1. and 2. Cu plate, 0.6cm thick, 6cm diameter
c       
        par_tube(1) = 0.
c        par_tube(2) = 1.6      ! small sample plate
        par_tube(2) = 3.0
        par_tube(3) = 0.3
c
        call gsvolu('SAPH','TUBE', Sapphire, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
        call gsatt('SAPH', 'COLO', 3) ! green color for saphhire
c
czzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
c
c       cryo
c
c       1 cm plate, "cold finger"
c
        par_tube(1) = 0.
        par_tube(2) = 2.75
        par_tube(3) = 0.5
c
        call gsvolu('COFI','TUBE', Cu, par_tube, 3, ivol) 
        if (ivol.lt.0) goto 900
c
c       end plate of cryo (3cm diameter, 0.7cm thick)
c
        par_tube(1) = 0.
        par_tube(2) = 1.5
        par_tube(3) = 0.35
c
        call gsvolu('CRY1','TUBE', Cu, par_tube, 3, ivol) 
        if (ivol.lt.0) goto 900
c
c       "Waermetauscher", assume that it has 1cm open cylinder ?
c       5 cm long
c
        par_tube(1) = 0.5
        par_tube(2) = 1.5
        par_tube(3) = 2.5
c
        call gsvolu('CRY2','TUBE', Cu, par_tube, 3, ivol)
        if (ivol.lt.0) goto 900
c
c       the mounting ring for He-shield
c
        par_tube(1) = 3.8
        par_tube(2) = 4.7
        par_tube(3) = 0.55
c
        call gsvolu('CRY3','TUBE', Cu, par_tube, 3, ivol)  
        if (ivol.lt.0) goto 900
c
c       add 2mm thick plate for mounting ring
c       this is just to close the downstream side, I don't know if
c       dimensions and position are right.
c
        par_tube(1) = 1.5
        par_tube(2) = 3.8
        par_tube(3) = 0.1
c
        call gsvolu('CRY4','TUBE', Cu, par_tube, 3, ivol)
        if (ivol.lt.0) goto 900
c
c      call gsatt('CRY4', 'COLO', 3)
czzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
c
c       He shield
c       
        par_tube(1) = 4.7
        par_tube(2) = 4.8
        par_tube(3) = 4.5
c
        he_shield_thickness = par_tube(2) - par_tube(1)
        if ( l_crsh ) then
          write(6,*) ' '
          write(6,*) 'Thickness of Cu-He-shield: ',he_shield_thickness,
     1               ' cm.'
          write(6,*) ' '
        endif
c
        call gsvolu('CRSH','TUBE', Cu, par_tube, 3, ivol)
        if (ivol.lt.0) goto 900
c       
c------------------------------------------------------
c
c       MCP detector
c
        par_tube(1) = 0.
        par_tube(2) = 2.5
c
        if ( l_run11 ) then
            par_tube(3) = 0.15  ! Galileo Z-stack, 2.4mm totallength
                                ! Galileo would have 3.0mm totallength
        else
            par_tube(3) = 0.12 ! hamamatsu Z-stack, 2.4mm totallength
        endif
c
        if ( l_mcp2 ) then
            call gsvolu('DMCP','TUBE', MCP, par_tube, 3, ivol)  
                if (ivol.lt.0) goto 900
        endif
c
c       MCP2 mounting and anode up to Run11
c
        if ( .not. l_run11) then
c
c           macor ring
c
            par_tube(1) = 2.5
            par_tube(2) = 3.5
            par_tube(3) = 0.55
            call gsvolu('MCPM', 'TUBE', Macor, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           anode, use MACOR
c
            par_tube(1) = 0.
            par_tube(2) = 3.5
            par_tube(3) = 0.05
c
            call gsvolu('MCPA', 'TUBE', Macor, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           stainless steel ring
c
            par_tube(1) = 2.5
            par_tube(2) = 3.5
            par_tube(3) = 0.05
            call gsvolu('MCSR', 'TUBE', Steel, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           stainless steel support ring
c
            par_tube(1) = 2.75
            par_tube(2) = 4.25
            par_tube(3) = 0.2
            call gsvolu('MCSS', 'TUBE', Steel, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c--------------------------------------------
c
        else
c
c           Run 11 MCP detector
c
c           ceramic rings, use MACOR as material
c
            par_tube(1) = 2.4
            par_tube(2) = 3.25
            par_tube(3) = 0.075
            call gsvolu('MCPM', 'TUBE', Macor, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           anode in Run11, 
c
            par_box(1) = 3.65
            par_box(2) = 3.65
            par_box(3) = 0.4
            call gsvolu('MCPA','BOX ', Steel, par_box, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           now the anode is not completely made of steel but there
c           the are "vacuum cylinders".
c
            par_tube(1) = 0.
            par_tube(2) = 2.75
            par_tube(3) = 0.15
c
            call gsvolu('ANVA','TUBE', Vacuum, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           stainless steel ring for MCP2 mounting
c
            par_box(1) = 3.65
            par_box(2) = 3.65
            par_box(3) = 0.1
            call gsvolu('MCSR','BOX ', Steel, par_box, 3, ivol) 
            if (ivol.lt.0) goto 900
c       
            par_tube(1) = 0
            par_tube(2) = 2.75
            par_tube(3) = 0.1
            call gsvolu('MCVR','TUBE', Vacuum, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
c           stainless steel support ring
c       
            par_tube(1) = 4.0
            par_tube(2) = 4.8
            par_tube(3) = 0.25
            call gsvolu('MCSS','TUBE', Steel, par_tube, 3, ivol)
            if (ivol.lt.0) goto 900
c
        endif  ! if .not. l_run11
c
c------------------------------------------------------
c
c       inner scintillator
c
        par_tubs(1) = 9.0   ! inner radius
        par_tubs(2) = 9.5   ! outer radius
        par_tubs(3) = 13.0  ! half length in z
        par_tubs(4) = -45.  ! starting angle
        par_tubs(5) = +45.  ! ending angle      
        call gsvolu('SCIS','TUBS', Scint, par_tubs, 5, ivol)
        if (ivol.lt.0) goto 900
c
c       outer scintillator
c
        par_tubs(1) = 9.6   ! inner radius
        par_tubs(2) = 10.1  ! outer radius
        par_tubs(3) = 13.0  ! half length in z
        par_tubs(4) = -45.  ! starting angle
        par_tubs(5) = +45.  ! ending angle      
        call gsvolu('SCOS','TUBS', Scint, par_tubs, 5, ivol)
        if (ivol.lt.0) goto 900
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       define now the positions of vacuum chambers and detectors
c       within the MARS
c
c       z defines the beam direction
c
        if ( l_mcps) then
            call gspos('MCPS',1,'MARS',0.,0.,0.,0,'ONLY')
        endif
        if ( l_efla ) then
            call gspos('F100',1,'MARS',0.,0.,17.2,0,'ONLY')
        endif
c
        call gspos('MCPV',1,'MARS',0.,0.,0.,0,'ONLY')
        call gspos('F160',1,'MARS',0.,0.,-15.10,0,'ONLY')
c       call gspos('T200',1,'MARS',0.,0.,-17.40,0,'ONLY')
        call gspos('GATV',1,'MARS',0.,0.,-25.45,0,'ONLY')
        call gspos('GATS',1,'MARS',0.,0.,-25.45,0,'ONLY')
        call gspos('F200',1,'GATV',0.,0.,  8.05,0,'ONLY')
        call gspos('L3VA',1,'MARS',0.,0.,-57.90,0,'ONLY')
        call gspos('L3ST',1,'MARS',0.,0.,-57.90,0,'ONLY')
c
c       Ring anode
c
        if ( l_run11) then
                call gspos('RA-E',1,'MCPV',0.,0., -8.6,0,'ONLY')
                call gspos('RA-M',1,'MCPV',0.,0.,-13.1,0,'ONLY')
                call gspos('RA-G',1,'GATV',0.,0.,+3.35,0,'ONLY')
        endif
c
c       position of sample cryo things
c
c       off = 0.25
        off = 0.
        if ( l_samp ) then
c            call gspos('SAH3',1,'MCPV',0.,0.,+1.35+off,0,'ONLY')
            call gspos('SAH2',1,'MCPV',0.,0.,+1.6+off,0,'ONLY')
            call gspos('SAPH',1,'MCPV',0.,0.,+2.1+off,0,'ONLY')
            call gspos('SAH1',1,'MCPV',0.,0.,+2.65+off,0,'ONLY')
        endif
c
        if ( l_cryo ) then      ! parts of the CryoVac cryostat
            call gspos('COFI',1,'MCPV',0.,0.,+3.40+off,0,'ONLY')
            call gspos('CRY1',1,'MCPV',0.,0.,+4.25+off,0,'ONLY')
            call gspos('CRY2',1,'MCPV',0.,0.,+6.75+off,0,'ONLY')
            call gspos('CRY3',1,'MCPV',0.,0.,+5.10+off,0,'ONLY')
                call gspos('CRY4',1,'MCPV',0.,0.,+5.10+off,0,'ONLY')
        endif
c
        if ( l_crsh ) then 
            call gspos('CRSH',1,'MCPV',0.,0.,+1.6+off,0,'ONLY')
        endif
c
c       position of MCP
c
        if ( l_mcp2 ) then
            if ( l_run11) then
                call gspos('DMCP',1,'MCPV',0.,0.,+1.55,0,'ONLY') ! Galileo MCP
            else
                call gspos('DMCP',1,'MCPV',0.,0.,+1.52,0,'ONLY') ! Hamamatsu MCP
            endif
        endif
c
c       anode and stainless steel ring
c
        if ( l_mcpa ) then
c
            if ( .not. l_run11 ) then
c
c               Run 10
c       
                call gspos('MCPM', 1, 'MCPV', 0., 0., +1.85, 0, 'ONLY') ! MACOR ring
                call gspos('MCPA', 1, 'MCPV', 0., 0., +3.45, 0, 'ONLY') ! MACOR anodee
                call gspos('MCSS', 1, 'MCPV', 0., 0., +3.90, 0, 'ONLY') ! steel support ring
                call gspos('MCSR', 1, 'MCPV', 0., 0., +2.45, 0, 'ONLY') ! guard ring
                call gspos('MCSR', 2, 'MCPV', 0., 0., +2.95, 0, 'ONLY') ! guard ring
            else
c
c               Run 11
c
                call gspos('MCPM',1,'MCPV',0.,0.,+1.325,0,'ONLY') ! ceramic ring
                call gspos('MCPM',2,'MCPV',0.,0.,+1.775,0,'ONLY') ! ceramic ring
                call gspos('MCSR',1,'MCPV',0.,0.,+2.00,0,'ONLY') ! steel plate
                call gspos('MCVR',1,'MCSR',0.,0.,+0.00,0,'ONLY') ! hole in plate
                call gspos('MCPA',1,'MCPV',0.,0.,+3.10,0,'ONLY') ! the anode
                call gspos('ANVA',1,'MCPA',0.,0.,-0.25,0,'ONLY') ! part of anode
                call gspos('ANVA',2,'MCPA',0.,0.,+0.25,0,'ONLY') ! part of anode
                call gspos('MCSS',1,'MCPV',0.,0.,+5.75,0,'ONLY') ! support ring
c
            endif
c
        endif
c
c       position of the four scintillators
c
c--------------------------
c
c     the LEFT detector
c
      off = 0.
      call gspos('SCIS',1,'MARS',0.,0.,off,0,'ONLY')
      call gspos('SCOS',1,'MARS',0.,0.,off,0,'ONLY')
c
c--------------------------
c
c     the TOP detector; for comments in rotation matrix, see RIGHT detector
c     below
c
      theta1 = 90.
      phi1   = 90.
      theta2 = 90.
      phi2   = 180.
      theta3 = 0.
      phi3   = 0.
c
c     GSROTM(97...) defines rotation matrix 97
c
      call gsrotm(97,theta1,phi1,theta2,phi2,theta3,phi3)
c
      call gspos('SCIS',2,'MARS',0.,0., off,97,'ONLY')
      call gspos('SCOS',2,'MARS',0.,0., off, 97,'ONLY')
c
c--------------------------------
c
c     the RIGHT detector
c
c     Now, I can get 3 copies of the SCIS and SCOS by defining a rotation
c     matrix to place the other detectors in the MARS
c
c     THETA1 = polar angle of new x axis
c     PHI1   = azimuth of new x axis
c     THETA2 = polar angle of new y axis
c     PHI2   = azimuth of new y axis
c     THETA3 = polar angle of new z axis
c     PHI3   = azimuth of new z axis
c
c     SCIS and SCOS above correspond to the LEFT detector of the LEM
c     setup; now I make a copy to get the RIGHT detector
c       
c     z remains unchanged ==> THETA3 = 0, PHI3 = 0
c     the x axis becomes -x ==> THETA1 = 90, PHI1 = 180
c     the y axis becomes -y ==> THETA2 = 90, PHI2 = 270
c
c     the unity matrix is
c
c     theta1 = 90,    phi1 = 0
c     theta2 = 90,    phi2 = 90
c     theta3 = 0 ,    phi3 = 0 
c
      theta1 = 90.
      phi1   = 180.
      theta2 = 90.
      phi2   = 270.
      theta3 = 0.
      phi3   = 0.

c     GSROTM(98...) defines rotation matrix 98
c
      call gsrotm(98,theta1,phi1,theta2,phi2,theta3,phi3)
c
      call gspos('SCIS',3,'MARS',0.,0., off, 98,'ONLY')
      call gspos('SCOS',3,'MARS',0.,0., off, 98,'ONLY')
c
c--------------------------
c
c     the BOTTOM detector; for comments in rotation matrix, see RIGHT detector
c     above
c
      theta1 = 90.
      phi1   = 270.
      theta2 = 90.
      phi2   = 0. ! == 360
      theta3 = 0.
      phi3   = 0.
c
c     GSROTM(99...) defines rotation matrix 99
c
      call gsrotm(99,theta1,phi1,theta2,phi2,theta3,phi3)
c
      call gspos('SCIS',4,'MARS',0.,0., off, 99,'ONLY')
      call gspos('SCOS',4,'MARS',0.,0., off, 99,'ONLY')
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     define basic detector parameter and allocation of the detector
c     volume to a SET using GSDET; a SET becomes useful in large
c     detectors where it is possible to group together several detetectors
c     like the different components of a calorimeter;
c     in this simple case it isn't necessary, but GEANT aquires it
c
      namesv(1) = 'DMCP'
c
c     the following variables have been "included"
c
      nvname = 1                          
      numbv(1)  = 1
      idtype = 1      ! user defined detector ID
      if ( l_mcp2 ) then
          call gsdet('MDET','DMCP',nvname,namesv,numbv,idtype,100,
     1              100,iset,idet)
c
c     define now the hit parameters using GSDETH
c     MCP : the interesting variables I want to store are
c
c     - position    x,y,z
c     - time        t
c     - energy      e
c     - energyloss  eloss
c     - particle type ipart
c  
       call gsdeth('MDET','DMCP',7,namesh,nbitsh,orig,fact) 
      endif
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     define Scintillator detector
c
      nvname    = 1
      namesv(1) = 'SCIS'
      numbv(1)  = 3
      idtype = 2      ! user defined detector ID
      call gsdet('ISSC','SCIS',nvname,namesv,numbv,idtype,100,100,
     1              iset,idet)   
      call gsdeth('ISSC','SCIS',7,namesh,nbitsh,orig,fact)
      nvname    = 1
      namesv(1) = 'SCOS'
      numbv(1)  = 3
      idtype = 3      ! user defined detector ID
c
      call gsdet('OSSC','SCOS',nvname,namesv,numbv,idtype,100,100,
     1             iset, idet)   
      call gsdeth('OSSC','SCOS',7,namesh,nbitsh,orig,fact)
c
c     call gpsets('MDET','DMCP')
c     call gpsets('ISSC','SCIS')
c     call gpsets('OSSC','SCOS')
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      return
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     error handling
c
900   write(6,'(''0GEANT_LEMSR-E- Error in routine GSVOLU...'',/)')
      call exit
c
      end
