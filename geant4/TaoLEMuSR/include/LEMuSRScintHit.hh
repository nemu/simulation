//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRScintHit.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-12 16:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           INNER SCINT HIT
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRScintHit_h
#define LEMuSRScintHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

class LEMuSRScintHit : public G4VHit
{
public:
  // contructor and destructor
  LEMuSRScintHit();
  ~LEMuSRScintHit();
  
  void Draw();
  void Print();
  void print(G4String name);

  // operator for hit collector definition
  LEMuSRScintHit(const LEMuSRScintHit &right);
  const LEMuSRScintHit& operator=(const LEMuSRScintHit &right);
  G4int operator==(const LEMuSRScintHit &right) const;

  inline void *operator new(size_t);
  inline void operator delete(void *aHit);

  // private variables

private:
  G4double energy_deposition, time_of_flight, spin, kenergy, tenergy;
  G4ThreeVector position, momentum;
  G4LogicalVolume* lv_Volume;
  G4VPhysicalVolume* pv_Volume;
  G4String particle_name;

  // inline functions
public:
  inline void SetEnergyDeposition(G4double ed){energy_deposition = ed;}
  inline void AddEnergyDeposition(G4double ed){energy_deposition += ed;}
  inline G4double GetEnergyDeposition(){return energy_deposition;}

  inline void SetTimeOfFlight(G4double tf){ time_of_flight=tf;}
  inline void AddTimeOfFlight(G4double tf){ time_of_flight+=tf;}
  inline G4double GetTimeOfFlight(){return  time_of_flight;}

  inline void SetSpin(G4double sp){ spin=sp;}
  inline G4double GetSpin(){return  spin;}

  inline void SetPosition(G4ThreeVector pos){position=pos;}
  inline G4ThreeVector GetPosition(){return position;}

  inline void SetMomentum(G4ThreeVector mom){momentum=mom;}
  inline G4ThreeVector GetMomentum(){return momentum;}

  inline void SetParticleName(G4String name){particle_name=name;}
  inline G4String GetParticleName(){return particle_name;}

  inline G4LogicalVolume* GetLogicalVolume(){return lv_Volume;}
  inline G4VPhysicalVolume* GetPhysicalVolume(){return  pv_Volume;}

};

// define the collection class according to template G4THitsCollection

typedef G4THitsCollection<LEMuSRScintHit> LEMuSRScintHitsCollection;

extern G4Allocator<LEMuSRScintHit> LEMuSRScintHitAllocator;

inline void* LEMuSRScintHit :: operator new(size_t)
{
  void *aHit;
  aHit = (void*) LEMuSRScintHitAllocator.MallocSingle();
  return aHit;
}

inline void LEMuSRScintHit :: operator delete(void *aHit)
{
 LEMuSRScintHitAllocator.FreeSingle((LEMuSRScintHit*) aHit);
}


#endif
