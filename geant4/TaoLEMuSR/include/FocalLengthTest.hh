//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : FocalLengthTest.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2005-03-02 09:37
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               FocalLengthTest   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
#ifndef FocalLengthTest_h
#define FocalLengthTest_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include <fstream>
#include "LEMuSRVisManager.hh"
#include "LEMuSRDetectorConstruction.hh"

// root histogram classes
#include <stdlib.h>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"



class FocalLengthTest : public G4UserSteppingAction
{
public:
  
  static FocalLengthTest* GetInstance();
  
  FocalLengthTest()   ;
  ~FocalLengthTest()   ;
  
  void UserSteppingAction(const G4Step *theStep);

  void SetParticleVolumeNames(const G4Step *theStep);

  void SetPositionMomentum(const G4Step *theStep);
  
  void SetTimeEnergy(const G4Step *theStep);
  
  void SetAngles(const G4Step* aStep);

  void PrintDatas(const G4Step *theStep);

  void SetField(const G4Step *theStep);
 
  void SetSpinDirection(const G4Step* aStep);

  void SetPositronAngles(const G4Step *theStep);


  void LoopKiller(const G4Step*aStep);
   G4int loop;

  G4bool CheckCondition(const G4Step* aStep);

  G4String p_name, v_name, pv_name;  

  G4ThreeVector position,momentum,momentum_direction, polarization, m, parpol, p;

  G4double kenergy, tenergy, localtime, globaltime, proptime, fx, fy, fz, init_kenergy;

  G4double theta,sintheta, costheta, phi, sinphi, cosphi;

  

typedef struct    {
 
  Float_t kenergy;
 
  Float_t focal, ratio;
 
 Float_t  positionx, positiony,positionz,momdirx,momdiry,momdirz;
 Int_t index, event;
  } LEMuSRparticle ;

  LEMuSRparticle muon;

  void Update();

  void BookRoot();
  void FillRoot();
  void WriteRoot();

  void PrintField(const G4Step *theStep);
  
  //  TROOT eventTree;
  Int_t nevent , comp , split , write , hfill , read ,arg4, arg5  ;
  TFile *myFile;
  TTree *myTree;
  TBranch *b,*b2;
  TH1D *hEnergy, *hTime, *hAngle, *hAngle2;
  
  TTree *tree;

 TFile* F;
 TTree *dt;

  Float_t time;


  G4double L,R,A; 

  
private:
  
  static FocalLengthTest* pointer;
  

};

#endif
