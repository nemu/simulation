//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRElMagField.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               ElectroMagnetic Field   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMUSRELECTRICMAGFIELD_H
#define LEMUSRELECTRICMAGFIELD_H 1
 
#include"G4ElectricField.hh"
#include"G4MagneticField.hh"
#include"G4ElectroMagneticField.hh"
#include "globals.hh"
#include "G4ios.hh"

#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Matrix/Matrix.h"


#include "LEMuSRElectricField.hh"



/**
 * In order to build complex electromagnetic fields, the following class has been built. 
 * It allows the superposition of an electric field and a magnetic field. 
 *
 * The main reason for this is that the field manager attached to a given volume do not handle such a 
 * superposition. In the case of the sample chamber, this is a big problem because we have to handle 
 * the electric fields of the ring anode and of the sample cryostat as well as the magnetic field over the sample. 
 *
 * The electromagnetic field in the sample region (gate valve chamber + mcp2 chamber) is then the 
 * superposition of the following fields:
 * -# electric field for left side of the ring anode
 * -# electric field for right side of the ring anode
 * -# electric field for sample cryostat
 * -# magnetic field for sample region
 * .
 * On the same principle was built the class LEMuSRElFieldMix
 */

class LEMuSRElMagField : public G4MagneticField
{
public:
 

  LEMuSRElMagField( G4ElectricField* E,  G4MagneticField* B,  G4double field1_val, G4double field2_val);
  
  ~LEMuSRElMagField();
  
  void GetFieldValue(const G4double point[4], G4double *Bfield) const;
  
  void Test();
  
  G4bool   DoesFieldChangeEnergy() const { return true; }// important to state that field changes energy (electric part!!!)
  
public:
  G4double coef1, coef2;
  G4ElectricField* field1;
  G4MagneticField* field2;
  
};

#endif
