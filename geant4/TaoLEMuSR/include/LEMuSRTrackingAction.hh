//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRTrackingAction.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           TRACKING ACTION.CC
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//


#ifndef LEMuSRTrackingAction_h
#define LEMuSRTrackingAction_h 1
#include "G4DynamicParticle.hh"
#include "G4UserTrackingAction.hh"
#include "G4Track.hh"
#include "globals.hh"

// root histogram classes
#include <stdlib.h>

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "LEMuSRCryoHit.hh"
#include "G4SteppingManager.hh"        // Include from 'tracking'
#include "G4TrackingManager.hh"        // Include from 'tracking'


/*!
 * The role of the tracking action is to take special actions 
 * before or after a track. 
 * Two importants methods can be implemented at will by the user, 
 * the PreUserTrackingAction and the PostUserTrackingAction. Those two methods are
 * virtual methods and their names must not be changed.
 * One can use the tracking action for histogramming, but also to act on the tracked
 * particles, to get/print information, or even to delete useless tracks etc.( cf. \ref Useraction).
 */

class LEMuSRTrackingAction : public G4UserTrackingAction {

 public:
  LEMuSRTrackingAction();
 ~ LEMuSRTrackingAction();
  void PreUserTrackingAction(const G4Track*);
  void PostUserTrackingAction(const G4Track*);

  void Collect_datas(const G4Track*);
  static LEMuSRTrackingAction* GetInstance();
  static LEMuSRTrackingAction* pointer;

 //  TROOT eventTree;
  Int_t nevent , comp , split , write , hfill , read ,arg4, arg5  ;
  TFile *myFile;
  TTree *myTree;
  TBranch *b;
  TH1D *hEnergy, *hTime, *hAngle, *hAngle2;

  G4TrackingManager* tMgr;

  G4double GyroMagRatio;
  G4double GyroMagFactor;

private:
 


};
#endif


