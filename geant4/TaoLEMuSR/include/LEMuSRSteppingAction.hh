//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRSteppingAction.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           STEPPING ACTION.HH
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//




#ifndef LEMuSRSteppingAction_h
#define LEMuSRSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include <fstream>
#include "LEMuSRVisManager.hh"
#include "LEMuSRDetectorConstruction.hh"

/*!
 * The role of the stepping action is to take special actions 
 * before or after a step. 
 * Two importants methods can be implemented at will by the user, 
 * the PreUserTrackingAction and the PostUserTrackingAction. Those two methods are
 * virtual methods and their names must not be changed.
 * One can use the tracking action for histogramming, but also to act on the tracked
 * particles, to get/print information, or even to delete useless tracks etc.( cf. \ref Useraction).
 */

class LEMuSRSteppingAction : public G4UserSteppingAction
{
public:
  
  static LEMuSRSteppingAction* GetInstance();
  
  LEMuSRSteppingAction()   ;
  ~LEMuSRSteppingAction()   ;
  
  
  void UserSteppingAction(const G4Step *theStep);
  void FieldInfo(const G4Step *theStep);  
  void ParticleInfo(const G4Step *theStep);  
  void LoopKiller(const G4Step *theStep);  
  
private:
  
  static LEMuSRSteppingAction* pointer;
  G4int loop;


};

#endif
