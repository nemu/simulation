//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSREventAction.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           EVENT ACTION.HH
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSREventAction_h
#define LEMuSREventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

// root histogram classes
#include <stdlib.h>

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "LEMuSRCryoHit.hh"

class G4EventManager;
class G4Event;

/*!
 * The role of the event action is to take special actions at the beginning or at the end of each event. It can be used for histogramming, plotting trajectories, or any other useful actions.
 *
 * It can be used to initialized the hits collections for the sensitive detection. In \lemu simulation, we finally did not really use the event action.
*/
class LEMuSREventAction : public G4UserEventAction
{
public:
  //! Constructor.
  LEMuSREventAction();
  //! Destructor.
  ~LEMuSREventAction();
public: // with description
  //! Actions to take at the beginnig of the event
  void BeginOfEventAction(const G4Event* anEvent);
  //! Actions to take at the end of the event
  void EndOfEventAction(const G4Event* anEvent);
  
  void SetDrawFlag   (G4String val)  {drawFlag = val;};
  // Two virtual method the user can override.

void  SDCollManagement(const G4Event* evt);


private:
  G4String drawFlag;

  G4int iScintCollID, oScintCollID, CryoCollID, McpCollID;





  // root variables
public:

  //  TROOT eventTree;
  Int_t nevent , comp , split , write , hfill , read ,arg4, arg5  ;
  TFile *myFile;
  TTree *myTree;
  TBranch *b;
  TH1D *hTest ,*hTest2,*hTest3, *hTest4;


  LEMuSRCryoHit* aHit;
    G4double en_dep;



 typedef struct {
      G4double lifetime, energy, angle;
   } PRIMDATAS;


  PRIMDATAS PMdatas;

};

#endif
