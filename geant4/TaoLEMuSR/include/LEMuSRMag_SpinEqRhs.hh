//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
// $Id$
// GEANT4 tag $Name:  $
//
//
// class G4Mag_SpinEqRhs
//
// Class description:
//
// This is the equation of motion for a particle with spin in a pure
// magnetic field. The three components of the particle's spin are
// treated utilising BMT equation.

// History:
// - Created: J.Apostolakis, P.Gumplinger - February 8th, 1999.
// --------------------------------------------------------------------

#ifndef LEMuSRMAG_SPIN_EQRHS
#define LEMuSRMAG_SPIN_EQRHS

#include "G4Types.hh"
#include "G4Mag_EqRhs.hh"
#include "G4SteppingManager.hh"        // Include from 'tracking'
#include "G4EventManager.hh"
#include "G4TrackingManager.hh"        // Include from 'tracking'

class G4MagneticField;

/**
 * This class defines the motion equation to use to calculate the evolution of a
 * particle in a magnetic field. The derivatives vector is calculated for
 * the magnetic field, taking into account the spin precession
 * and the time evolution.
 */
class LEMuSRMag_SpinEqRhs : public G4Mag_EqRhs
{
   public:  // with description

     LEMuSRMag_SpinEqRhs( G4MagneticField* MagField );
    ~LEMuSRMag_SpinEqRhs();
       // Constructor and destructor. No actions.

     void SetChargeMomentumMass(G4double particleCharge, // in e+ units
                                G4double MomentumXc,
                                G4double mass); 
  
  //! Calculation of the derivatives
  /*! Given the value of the magnetic field B, this function 
   * calculates the value of the derivative dydx.
   */
  void EvaluateRhsGivenB( const  G4double y[],
                             const  G4double B[3],
                                    G4double dydx[] ) const;

 //! Return the gyromagnetic ratio
  /*!
   * This method call the event manager to get the tracking manager and then the current track in order to get the particle name. According to the name of the particle it returns the gyromagnetic ratio.
   */
  G4double GetGyromagneticRatio();
   private:

     G4double omegac;
     G4double anomaly;
     G4double ParticleCharge;

     G4double E;
     G4double gamma;
     G4double beta; 
     G4double m_mass;

  G4double oldomegac; //to remove
};

#endif /* LEMuSRMAG_SPIN_EQRHS */
