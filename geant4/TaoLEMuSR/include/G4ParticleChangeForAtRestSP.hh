//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
// ------------------------------------------------------------
//      GEANT 4 class header file
//
//      History:
//      17 August 2004  P. Gumplinger
// ------------------------------------------------------------
//
#ifndef G4ParticleChangeForAtRestSP_hh 
#define G4ParticleChangeForAtRestSP_hh 1

#include "G4ParticleChange.hh"

class G4ParticleChangeForAtRestSP : public G4ParticleChange
{
public:
  G4ParticleChangeForAtRestSP();
  virtual ~G4ParticleChangeForAtRestSP();
  
  virtual G4Step* UpdateStepForAtRest(G4Step* Step);

};

#endif
