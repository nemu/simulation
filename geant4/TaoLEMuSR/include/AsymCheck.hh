//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : AsymCheck.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               ASYMCHECK   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//


#ifndef AsymCheck_h
#define AsymCheck_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include <fstream>
#include "LEMuSRVisManager.hh"
#include "LEMuSRDetectorConstruction.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"


// root histogram classes
#include <stdlib.h>

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"



class AsymCheck : public G4UserSteppingAction
{
public:
  
  static AsymCheck* GetInstance();
  
  AsymCheck()   ;
  ~AsymCheck()   ;


  void UserSteppingAction(const G4Step *theStep);

  void SetParticleVolumeNames(const G4Step *theStep);

  void SetPositionMomentum(const G4Step *theStep);
  
  void SetTimeEnergy(const G4Step *theStep);
  
  void SetAngles(const G4Step* aStep);

  void PrintDatas(const G4Step *theStep);

  void SetField(const G4Step *theStep);
 
  void SetSpinDirection(const G4Step* aStep);

  void SetPositronAngles(const G4Step *theStep);
 
  G4bool CheckCondition(const G4Step* aStep);

  G4String p_name, v_name, pv_name;  

  G4ThreeVector position,momentum,momentum_direction, polarization, m, parpol, p;

  G4double kenergy, tenergy, localtime, globaltime, proptime;

  G4double theta,sintheta, costheta, phi, sinphi, cosphi;

  

typedef struct    {
 
  Float_t kenergy, tenergy, localtime,  globaltime, proptime;
  
  Float_t theta,phi;
  

 
  Float_t positionx, positiony,positionz,momdirx,momdiry,momdirz;
  
  Int_t ID;

  } LEMuSRparticle ;

typedef struct    {
 
   Float_t asym,L,R,S;
  

  } Asym ;

typedef struct    {
 
   Float_t omegat,time;
 
  } Spin ;

  
  LEMuSRparticle positron;
  Asym asym;
  Spin spin;


  void Update();

  void BookRoot();
  void FillRoot();
  void WriteRoot();

  void PrintAsym();
  
  //  TROOT eventTree;
  Int_t nevent , comp , split , write , hfill , read ,arg4, arg5  ;
  TFile *myFile;
  TTree *myTree;
  TBranch *b,*b2;
  TH1D *hEnergy, *hTime, *hAngle, *hAngle2;
  
  TTree *tree;

 TFile* F;
 TTree *dt;

  Float_t time;


  G4double L,R,A; 
  G4double BR1, BR2, dm1,dm2;// decay modes and branching ratios
  
private:
  
  static AsymCheck* pointer;
  

};

#endif
