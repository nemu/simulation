//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRMuonDecayChannel.hh , v 1.0
//  AUTHOR: Taofiq PARAISO based on G4MuonDecayChannel $Id$
//  DATE  : 2004-07-13 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          MUON DECAY CHANNEL.HH
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

/**
 * The LEMuSRMuonDecayChannel class contains the implementation of the asymmetric muon decay. The process is applicable to muon (positive or negative) and muonium.
 *
 * It was inspired from the G4MuonDecayChannel method, which did not take into account the spin polarization of the muon.
 * 
 * One should notice that recent versions of Geant4 feature a G4MuonDecayChannelWithSpin class, whose role is identical to the class we are describing.
 * 
 * The two caracteristics of this process are 
 * - The Michel spectrum for e+ energy distribution
 *   \image html michel.gif Michel's Spectrum.
 * - The cardioidal angular distribution as a function of the positron energy
 *   \image html kardio.gif Cardioid.
 * .
 * The angular direction of the positron emission is in relation with the energy of 
 * the positron. 
 * The more energy the positron has, the smaller the angle of emission with respect 
 * to the muon spin is. The V-A theory predicts the positron rate to be
 * \f[ \mbox{d}\Gamma^2(w,\theta)= \frac{1}{\tau n(w)\left[1+ D(w)cos\theta \right]}\mbox{d}w \mbox{d}(\cos \theta),\f]
 * \f$w\f$ being the ratio between the energy of the emitted positron and the 
 * maximal energy, and \f$\theta\f$ the angle between the muon spin and the 
 * positron momentum.
 * 
 * The distribution \f$n(w)\f$ along energy is given by the Michel's spectrum 
 * \f[ n(w)= w^2(3-2w)\f] and the asymmetric factor is given by 
 * \f[D(w) = \frac{2w-1}{3-2w}.\f] We assume, here, that the muons are fully 
 * polarized. 
 *
 * The distribution along energies becomes, for \f$\Theta=0\f$, 
 * \f[n(w)+n(w)D(w)= 2w^2\f]
 * and for \f$\Theta = \pi\f$, 
 * \f[n(w)-n(w)D(w)= 4(w^2-w^3)\f]
 *
 * The asymmetry can be derived integrating the rate \f$d\Gamma^2\f$, 
 * and one should get
 * \f[A(w_{min}, \Theta_0) = \frac{1+\cos\Theta_0}{6}\frac{1+2w_{min}^3-3w_{min}^4}{1-2w_{min}^3+w_{min}^4}\f]
 * where \f$\Theta_0\f$ is the opening angle of the solid angle.
 * This means that if one select all positron energies, \f$w_{min}\simeq 0\f$
 * - \f$A \simeq\frac{1}{3}\f$ for small solid angles
 * - \f$A \simeq\frac{1}{6}\f$ for large solid angles

 */

#ifndef LEMuSRMuonDecayChannel_h
#define LEMuSRMuonDecayChannel_h 1


#include "G4ios.hh"
#include "globals.hh"
#include "G4VDecayChannel.hh"
#include "G4DynamicParticle.hh"
#include "Randomize.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"

class LEMuSRMuonDecayChannel : public G4VDecayChannel
{

  // Class Decription

public:  
  //!Constructor. 
  LEMuSRMuonDecayChannel(const G4String& theParentName,
		     G4double        theBR);
  //! Destructor.
  ~LEMuSRMuonDecayChannel();
  
 static LEMuSRMuonDecayChannel* pointer;
 static LEMuSRMuonDecayChannel* GetInstance();


void   finalize();

public:  // With Description
  //! \mm
  virtual G4DecayProducts *DecayIt(G4double);     
  HepRandomEngine* theEngine;
  G4ThreeVector emomdir;  

  //! Angles.
  G4double alpha,sinalpha, cosalpha, delta, sindelta, cosdelta;
  //! Sines and cosines.
  G4double costheta, sintheta, phi, sinphi, cosphi, theta; 

  inline G4double GetTheta(){return theta;};
  inline G4double GetPhi(){return phi;};
 

  //! Polarized decay
  /*!
   * Gets the muon polarization and launch the Decay it method. 
  */
  G4DecayProducts *DecayItPolarized(G4double,G4ThreeVector polar);


private:

 
};

#endif
