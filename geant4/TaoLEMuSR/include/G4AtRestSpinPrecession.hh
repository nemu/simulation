//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
// ------------------------------------------------------------
//      GEANT 4 class header file
//
//      History:
//      17 August 2004  P. Gumplinger
// ------------------------------------------------------------
//
#ifndef G4AtRestSpinPrecession_hh
#define G4AtRestSpinPrecession_hh 1

#include "G4VRestProcess.hh"
#include "G4ParticleChangeForAtRestSP.hh"

class G4AtRestSpinPrecession : public G4VRestProcess
{

public:
  G4AtRestSpinPrecession();
  ~G4AtRestSpinPrecession();

  G4bool IsApplicable(const G4ParticleDefinition&);
  G4VParticleChange* AtRestDoIt(const G4Track& aTrack,const G4Step& aStep);

  virtual G4double GetMeanLifeTime(const G4Track&,
				   G4ForceCondition*);
private:

  G4ParticleChangeForAtRestSP fParticleChange;

  G4ThreeVector NewSpin(const G4Step& aStep,
			G4ThreeVector B, G4double deltatime );

};

#endif
