//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRCryoField.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               CRYOFIELD   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "G4ElectricField.hh"
#include "G4ThreeVector.hh"
#include "G4ios.hh"
/*!
 * This class is defines homogenous electric field in the cryostat region.
 * It is obsolete since a field map has been introduces for this region.
 * Should be removed in the future.
 */

class LEMuSRCryoField : public G4ElectricField
{
public: 

  /*! This constructor initializes the field for all the sample chamber
   */
  LEMuSRCryoField(G4ThreeVector FieldVector);
  /*! The constructor initializes the field within the boundary cylinder centered in mcpv_z, radius and length= zmax-zmin
   */

  LEMuSRCryoField(G4ThreeVector FieldVector,G4double radius,G4double zmin, G4double zmax);
  ~LEMuSRCryoField();

  G4bool   DoesFieldChangeEnergy() const { return true; }
  // Since an electric field can change track energy
  
  void GetFieldValue(const G4double pos[4], G4double *field) const;
  
  G4bool uniform;


  G4double R;

  
private:
  G4double flength, fpotential, fradius, fzmin, fzmax;


  G4ThreeVector position;
  G4ThreeVector EField;

};
