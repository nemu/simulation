//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRElMag_SpinEqRhs.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               ElectroMagnetic Spin Equation   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//


#ifndef LEMuSRELMAG_SPIN_EQRHS
#define LEMuSRELMAG_SPIN_EQRHS 1

#include "G4Types.hh"
#include "G4Mag_EqRhs.hh"
#include "G4SteppingManager.hh"        // Include from 'tracking'
#include "G4TrackingManager.hh"        // Include from 'tracking'
#include "LEMuSRTrackingAction.hh"
#include "LEMuSRSteppingAction.hh"
#include "G4EventManager.hh"
#include "G4TrackingManager.hh"
#include "G4Track.hh"
#include "G4ParticleDefinition.hh"        // Include from 'tracking'



class G4MagneticField;


/**
 * This class defines the motion equation to use to calculate the evolution of a
 * particle in an electromagnetic field. The derivatives vector is calculated for
 * the electric field, and for the magnetic field, taking into account the spin precession
 * and the time evolution.
 */
class LEMuSRElMag_SpinEqRhs : public G4Mag_EqRhs
{
   public:  // with description
  //! Constructor.
     LEMuSRElMag_SpinEqRhs( G4MagneticField* MagField );
  //! Destructor.
    ~LEMuSRElMag_SpinEqRhs();
       // Constructor and destructor. No actions.

  //! Set constant particle parameters
     void SetChargeMomentumMass(G4double particleCharge, // in e+ units
                                G4double MomentumXc,
                                G4double mass); 
  //! Calculate derivatives.
     void EvaluateRhsGivenB( const  G4double y[],
                             const  G4double B[3],
                                    G4double dydx[] ) const;
       // Given the value of the electromagnetic field B, this function 
       // calculates the value of the derivative dydx.
 

  //! Return the gyromagnetic ratio
  /*!
   * This method call the event manager to get the tracking manager and then the current track in order to get the particle name. According to the name of the particle it returns the gyromagnetic ratio.
   */

  G4double GetGyromagneticRatio();

   private:

     G4double omegac;
     G4double anomaly;
     G4double ParticleCharge;

     G4double E;
     G4double gamma, m_mass;
     G4double beta; 
     G4double cst;

     G4double oldomegac; //for debugging :: to remove

};

#endif
