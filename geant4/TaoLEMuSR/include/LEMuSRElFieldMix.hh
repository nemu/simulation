//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRElFieldMix.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2005-02-14 15:15
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               Electric Field  MIX
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMUSRELECTRICFIELDMIX_H
#define LEMUSRELECTRICFIELDMIX_H 1
 
#include"G4ElectricField.hh"
#include"G4ElectroMagneticField.hh"
#include "globals.hh"
#include "G4ios.hh"

#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Matrix/Matrix.h"


#include "LEMuSRElectricField.hh"


/**
 * In order to build complex electric fields, the following class has been built. 
 * It allows the superposition of many electric fields recurrently. 
 *
 * The main reason for this is that the field manager attached to a given volume do not handle such a 
 * superposition. In the case of the sample chamber, this is a big problem because we have to handle 
 * the fields of the ring anode and of the sample cryostat. 
 *
 * The electric field in the sample region (gate valve chamber + mcp2 chamber) is then the 
 * superposition of the following electric fields:
 * -# left side of the ring anode
 * -# right side of the ring anode
 * -# sample cryostat
 * .
 * On the same principle was built the class LEMuSRElMagField.
 */
class LEMuSRElFieldMix : public G4ElectricField
{
public:
 
  //! Contructor for two field map superposition.
  LEMuSRElFieldMix( LEMuSRElectricField* E1,  LEMuSRElectricField* E2,  G4double field1_val, G4double field2_val);

  //! Constructor for two general fields superposition.
  LEMuSRElFieldMix( G4ElectricField* E1,  G4ElectricField* E2,  G4double field1_val, G4double field2_val);
  

  //! Destructor. 
  ~LEMuSRElFieldMix();
  
  //! Method to return the field at position-time point[4].
  /*!
   * As an electric field superposition is linear, the GetFieldValue method simply ask each input field
   * for its value at the given position-time and returns as a field value the weighted sum of the 
   * two answers.
   */
  void GetFieldValue(const G4double point[4], G4double*) const;
  
  void Test();
  
public:
  
  //! Input field 1.
  G4ElectricField* field1;
  //! Input field 2.
  G4ElectricField* field2;

  //! Weight for input field 1.
  G4double coef1;
  //! Weight for input field 2.
  G4double coef2;

  
};

#endif


