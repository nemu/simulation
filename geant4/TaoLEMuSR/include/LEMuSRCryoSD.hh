//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRCryoSD.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               CRYOSD   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//




#ifndef LEMuSRCryoSD_h
#define LEMuSRCryoSD_h 1


#include "G4VSensitiveDetector.hh"
#include "LEMuSRCryoHit.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"

//! Root histogramming classes
#include <stdlib.h>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "LEMuSRCryoHit.hh"



class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;


/**
 *The sensitive detector classes are the most important to simulate the data acquisition.
 *The sensitive detector object is attached to the detector in LEMuSRDetectorConstruction.
 *
 *During the particle transportation, geant4 will check if the volume where the particle
 *travels is a sensitive detector (cf. G4Transportation). If this is the case, the method
 *ProcessHits will be executed.
 *
 *In the geant4 standard, the ProcessHit method would create a hit object, fill it with
 *desired parameters and send it to the corresponding hits collection for histogramming.
 *
 *This implementationof the \lemu simulation uses Root for histogramming, and therefore the hit object are not used.
 *Instead of filling a collection, we directly fill a Root tree with all the data we want.
 *A new hit structutre has been implemented according to Root format.
*/

class LEMuSRCryoSD : public G4VSensitiveDetector
{
public:
  
//! The constructor specifies the name.
  LEMuSRCryoSD(G4String name);
  ~LEMuSRCryoSD();
  
  //!Initialization of the sensitive detector
  void Initialize (G4HCofThisEvent* HCE);

  //! The main method for sensitive detection. 
 /*!
   * This method contains all the operations to perform when a particle is detected in the volume. Note that it is called at each step, which is very important because a single particle can make many hits. For some detectors it is important to detect a particle only once (to avoid multiple counts in histograms).
   * The CheckCondition method (see later) can be used to segregate unwanted hits. For any good event, the following methods are executed:
   * -# GetDatas() : to store the parameters.
   * -# getHit()   :   to fill the Root hit object with the previous parameters.
   * -# FillRoot() : to fill the Root tree with the Root hit object.
   */
  G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*ROhist);
  void EndOfEvent (G4HCofThisEvent* HCE);
  void clear();
  void DrawAll();
  void PrintAll();
 
  //! Root method to book a tree.
  void BookRoot();

  //! Root method to fill a tree.
  void FillRoot();

  //! Root method to write a tree in a file.
  void WriteRoot();

  TFile *myFile;
  TTree *myTree;
  TTree *tree;

  // HIT datas

  G4String p_name, vname;
  G4double spin, edep, toten, kinen, tof, globaltime, proptime;
  G4ThreeVector hitpos, hitmom;
  G4int ID;

  //! Boolean variable set by the CheckCondition method. If condition=true, the hit will be stored in the Root tree.
  G4bool condition;

  //! Events segregation.
  /*!
   * The CheckCondition method is the first called by the ProcessHit method. It will check if the step information should be stored or not. Here, the step is registered when the following conditions are fulfilled:
   * - The particle is a muon or a muonium
   * - The particle is in the Sample Holder2 (lv_SAH2, cf. LEMuSRDetectorConstruction.cc) 
   */
  G4bool CheckCondition(const G4Step* aStep);


  //! Obtaining data on the particle.
  /*!
   * The GetDatas() method take useful data from the step:
   * - Volume, name, spin
   * - Position, momentum
   * - Time
   * - Energy
   * .
   * Those data are taken in G4 format i.e. with units.
   */
  void GetDatas(const G4Step* aStep);

  //! The Root Hit Structure.
typedef struct    
{
  Float_t kenergy, tenergy, edeposit;
  Float_t localtime,  globaltime, proptime;
  Float_t positionx, positiony,positionz;
  Float_t momdirx,momdiry,momdirz, foil;
  Int_t muon,positron,gamma, runid;
} cryoHit ;


typedef struct
{

  Int_t muon,positron,gamma, runid;

} particleHit ;


typedef struct
{
  Int_t grid, guards, mcp, cryo;
  Float_t RAL, RAR, L3, TDet1, TDet2, TDet3;
  Float_t Bmag;
} detectorData ;

  //! Root hit object.
  cryoHit theHit;
  particleHit theParticle;
  detectorData detector;


  //! Filling the Root hit object.
  /*!
   * The getHit() method fills the Root hit object theHit with the data taken in the GetDatas method. As root do not handle units, the user must specify them explicitely. Otherwise, the default \gf  units are used.  
   */
  void getHit();
 
private:
  LEMuSRCryoHitsCollection *CryoCollection;

  G4double positionResolution;
  G4int mu,e,g;
  
};


#endif
