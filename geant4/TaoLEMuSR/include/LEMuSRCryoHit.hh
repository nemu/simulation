//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRCryoHit.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               CRYOHIT   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//




#ifndef LEMuSRCryoHit_h
#define LEMuSRCryoHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"


/**
 *The LEMuSRCryoHit class defines the Hit object for the Sample Plate. A hit is an elementary
 *component for the sensitive detection. As soon as particle enters in the specified 
 *volume, a hit object is created and will store any parameter the user wants to
 *consider.
 *The hit can then be plotted, printed or registered in a hit collection for further histogramming.
 *
 *However, it is not necessary to use hits objects since histograms can be filled
 *directly from the sensitive detector classes (cf. LEMuSRCryoSD.cc).
 *
*/
class LEMuSRCryoHit : public G4VHit
{
public:
  // contructor and destructor
  LEMuSRCryoHit();
  ~LEMuSRCryoHit();
  
  void Draw();
  void Print();
  void print(G4String name);

  // operator for hit collector definition
  LEMuSRCryoHit(const LEMuSRCryoHit &right);
  const LEMuSRCryoHit& operator=(const LEMuSRCryoHit &right);
  G4int operator==(const LEMuSRCryoHit &right) const;

  inline void *operator new(size_t);
  inline void operator delete(void *aHit);

  // private variables

private:
  G4double energy_deposition, time_of_flight, spin;
  G4ThreeVector position, momentum;
  G4LogicalVolume* lv_Volume;
  G4VPhysicalVolume* pv_Volume;
  G4String particle_name;

  // inline functions
public:
  inline void SetEnergyDeposition(G4double ed){energy_deposition = ed;}
  inline void AddEnergyDeposition(G4double ed){energy_deposition += ed;}
  inline G4double GetEnergyDeposition(){return energy_deposition;}

  inline void SetTimeOfFlight(G4double tf){ time_of_flight=tf;}
  inline void AddTimeOfFlight(G4double tf){ time_of_flight+=tf;}
  inline G4double GetTimeOfFlight(){return  time_of_flight;}

  inline void SetSpin(G4double sp){ spin=sp;}
  inline G4double GetSpin(){return  spin;}


  inline void SetPosition(G4ThreeVector pos){position =pos;}
  inline G4ThreeVector GetPosition(){return position;}

  inline void SetMomentum(G4ThreeVector mom){momentum =mom;}
  inline G4ThreeVector GetMomentum(){return momentum;}

  inline void SetParticleName(G4String name){particle_name=name;}
  inline G4String GetParticleName(){return particle_name;}

  inline G4LogicalVolume* GetLogicalVolume(){return lv_Volume;}
  inline G4VPhysicalVolume* GetPhysicalVolume(){return  pv_Volume;}

};

// define the collection class according to template G4THitsCollection

typedef G4THitsCollection<LEMuSRCryoHit> LEMuSRCryoHitsCollection;

extern G4Allocator<LEMuSRCryoHit> LEMuSRCryoHitAllocator;

inline void* LEMuSRCryoHit :: operator new(size_t)
{
  void *aHit;
  aHit = (void*) LEMuSRCryoHitAllocator.MallocSingle();
  return aHit;
}

inline void LEMuSRCryoHit :: operator delete(void *aHit)
{
 LEMuSRCryoHitAllocator.FreeSingle((LEMuSRCryoHit*) aHit);
}

#endif
