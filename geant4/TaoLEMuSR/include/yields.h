//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : YIELDS.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2005-04
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                                 YIELDS
/*
  fIRST IMPLEMENTATION BY ANLSEM,H. IN FORTRAN
  C++ CONVERSION T.K.PARAISO 04-2005
*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$// 

#ifndef Yields_h
#define Yield_h 1

#include "globals.hh"

class Yields
{
  
public:
  //constructor
  Yields();
  //destructor
  ~Yields();
  
  void GetYields(double E, double masse, double yvector[]);
  

private:
  // VARIABLES
  double Q_zero,Q_minus,D;
  double Yield_minus,Yield_zero,Yield_plus;
  
  double help1,help2,help3;
  
};


#endif
