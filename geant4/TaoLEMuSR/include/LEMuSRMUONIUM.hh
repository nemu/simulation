//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRMUONIUM.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 16:15
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               AT REST SPIN ROTATION   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#ifndef LEMuSRMUONIUM_h
#define LEMuSRMUONIUM_h 1
#include "G4VDiscreteProcess.hh"
#include "G4EnergyLossTables.hh"
#include "G4GPILSelection.hh"
#include "G4PhysicsLogVector.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VParticleChange.hh"
#include "G4UnitsTable.hh"
#include "G4MaterialCutsCouple.hh"
#include "G4ForceCondition.hh"

#include "G4ParticleTable.hh"
#include "G4DynamicParticle.hh"
#include "LEMuSRPrimaryGeneratorAction.hh"

#include "yields.h"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*!
 * LEMuSRMUONIUM class defines the muonium formation process in the carbon foil according 
 * to yields from Gonin's paper Sci. Rev. Instrum. 65 (3), 648-652 (1994).
 * 
 * \image html yields3.gif The muonium formation yields.
 *
 * The main parameters are the thickness of the foil and the energy of the muon. For a given
 * energy, a corresponding proportion of the muons will be converted into muonium. 
 * Concretely, the muon will be killed and replace by a muonium with same properties of time,
 * energy, momentum, position etc.
 *
 * This process is executed at the end of a step, i.e. the muon is converted into muonium after 
 * flying through the carbon foil.
 *cf. yields.h, gonin.cc.
 */
class LEMuSRMUONIUM : public G4VDiscreteProcess

{
 public:    // with description
  //! \ct
  LEMuSRMUONIUM(const G4String& name="Muformation" ,
		    G4ProcessType   aType = fElectromagnetic );
  
  //! \dt
  ~LEMuSRMUONIUM();

  //! Main method.
  /*!
   *The muonium formation process is executed at the end of a step. 
   */
  G4VParticleChange* PostStepDoIt(
			     const G4Track& ,
			     const G4Step& 
			    );
  G4double GetMeanFreePath(const G4Track& aTrack,
			   G4double   previousStepSize,
			   G4ForceCondition* condition
                                                               );



  //! Condition for process application (step object).
  G4bool CheckCondition( const G4Step& aStep);
  //! Condition for process application (step pointer).
  G4bool CheckCondition( const G4Step* aStep);


  G4String p_name, vname;
  G4double spin, edep, toten, kinen, tof, globaltime, proptime;
  G4ThreeVector hitpos, hitmom;
  G4int ID;

  //! \cbv
  G4bool condition;


  void GetDatas( const G4Step* aStep);
   // model parameters
  G4ParticleTable* particleTable; 
  G4ParticleDefinition* particle;
  Yields Gonin;
  G4double yvector[3];
  G4double rnd;
  G4DynamicParticle *DP;
  
  //! The particle change object.
  G4VParticleChange fParticleChange; 
 




  void  PrepareSecondary(const G4Track&);
  G4Track* aSecondary;

   void InitializeSecondaries(const G4Track&);
 

 
};


#endif
 

 
