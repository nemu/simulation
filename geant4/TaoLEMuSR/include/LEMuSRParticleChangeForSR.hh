//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRParticleChangeForSR.hh , v 1.2b
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:36
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                         PARTICLE CHANGE FOR SPIN ROTATION   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRParticleChangeForSR_h 
#define LEMuSRParticleChangeForSR_h 1

#include "G4ParticleChangeForTransport.hh"

/*!
 * This class defines the particle change object for the spin rotation process.
 * cf. LEMuSRAtRestSpinRotation
*/
class  LEMuSRParticleChangeForSR : public G4ParticleChangeForTransport
{

  G4Step* UpdateStepForAtRest(G4Step* Step);
  
public: 
  G4double theDecayTime;




  inline void SetDecayTime(G4double fDecayTime) { theDecayTime =   fDecayTime; }
  
  inline G4double GetDecayTime() { return theDecayTime; }
  
};


#endif
