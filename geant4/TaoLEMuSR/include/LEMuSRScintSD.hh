//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRScintSD.hh  , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-12 16:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           INNER SCINT SD
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRScintSD_h
#define LEMuSRScintSD_h 1



#include "G4VSensitiveDetector.hh"

#include "LEMuSRScintHit.hh"

// root histogram classes
#include <stdlib.h>
 
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "LEMuSRCryoHit.hh"
#include "G4PrimaryParticle.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class LEMuSRScintSD : public G4VSensitiveDetector
{
public:
  
  LEMuSRScintSD(G4String name);
  ~LEMuSRScintSD();
  
  void Initialize (G4HCofThisEvent* HCE);
  G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*ROhist);
  void EndOfEvent (G4HCofThisEvent* HCE);
  void clear();
  void DrawAll();
  void PrintAll();
 
  void BookRoot();
  void FillRoot();
  void WriteRoot();

  TFile *myFile;
  TTree *myTree;
  TTree *tree;

  // HIT datas

  G4String p_name, vname;
  G4double spin, edep, toten, kinen, tof, globaltime, proptime;
  G4ThreeVector hitpos, hitmom,hitvertex;
  G4int scintID, runID;
  G4double parentCharge;

  G4bool condition;

  G4bool CheckCondition(const G4Step* aStep);

  void GetDatas(const G4Step* aStep);


typedef struct    
{
  Float_t kenergy, tenergy, edeposit;
  Float_t localtime,  globaltime, proptime;
  Float_t positionx, positiony,positionz;
  Float_t momdirx,momdiry,momdirz;
  Float_t ipositionx, ipositiony,ipositionz;
  Float_t scLeft, scRight, scBottom, scTop  ;
  Int_t runID;
  Float_t motherCharge;
} scintHit ;

  scintHit theHit;
  void getHit();
 
private:
  LEMuSRScintHitsCollection *ScintCollection;

  G4double positionResolution;
  
};

#endif
