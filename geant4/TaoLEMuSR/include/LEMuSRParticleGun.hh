//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRParticleGun.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:48
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          PARTICLE GUN
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRParticleGun_h
#define  LEMuSRParticleGun_h 1

#include "G4ParticleGun.hh"
#include "G4Event.hh"
#include "G4PrimaryParticle.hh"

class LEMuSRParticleGunMessenger;
#include"G4ParticleGunMessenger.hh"

/*!
 * The LEMuSRParticleGun is the class which stores the initial settings of the particles.  
 * Its interplay with the LEMuSRPrimaryGeneratorAction (PGA) is very strong. 
 * Indeed, the particle gun object is instanciated  and initialized in the PGA. 
 * 
 * After this initialization, the PGA calls the particle gun's GeneratePrimaryVertex method
 * to create an event with the defined initial conditions.
 *
 * As an image one can think that the PGA loads the gun with some defined particles, 
 * set the shooting energy, aims and fires. Hence the name.
 */
class  LEMuSRParticleGun: public G4ParticleGun
{
public:
  //!\ct
  LEMuSRParticleGun();
  //!\dt
  ~LEMuSRParticleGun();

  //! Creates the first event.
 void GeneratePrimaryVertex(G4Event* evt);

  //! Assign a decay time
  inline void SetDecayTime(G4double d) {decaytime = d;} 

  G4double decaytime;


protected:
  //! Default initialization of the gun.
  void SetInitialValues();

 private:
  //! Messenger
     LEMuSRParticleGunMessenger* theMessenger;

};

#endif
