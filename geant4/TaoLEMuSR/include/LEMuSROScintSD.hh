//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSROScintSD.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               Outer SCINT SD   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSROScintSD_h
#define LEMuSROScintSD_h 1



#include "G4VSensitiveDetector.hh"

#include "LEMuSROScintHit.hh"


// root histogram classes
#include <stdlib.h>

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "LEMuSRCryoHit.hh"



class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class LEMuSROScintSD : public G4VSensitiveDetector
{
public:
  
  LEMuSROScintSD(G4String name);
  ~LEMuSROScintSD();
  
  void Initialize (G4HCofThisEvent* HCE);
  G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*ROhist);
  void EndOfEvent (G4HCofThisEvent* HCE);
  void clear();
  void DrawAll();
  void PrintAll();
 
  void BookRoot();
  void FillRoot();
  void WriteRoot();

  TFile *myFile;
  TTree *myTree;
  TTree *tree;

  // HIT datas

  G4String p_name, vname;
  G4double spin, edep, toten, kinen, tof, globaltime, proptime;
  G4ThreeVector hitpos, hitmom;
  G4int ID;

  G4bool condition;

  G4bool CheckCondition(const G4Step* aStep);

  void GetDatas(const G4Step* aStep);


typedef struct    
{
  Float_t kenergy, tenergy, edeposit;
  Float_t localtime,  globaltime, proptime;
  Float_t positionx, positiony,positionz;
  Float_t momdirx,momdiry,momdirz;
  Float_t scLeft, scRight, scBottom, scTop  ;
  Int_t runID;
} scintHit ;

  scintHit theHit;
  void getHit();
 
private:
  LEMuSROScintHitsCollection *ScintCollection;

  G4double positionResolution;
  
};

#endif
