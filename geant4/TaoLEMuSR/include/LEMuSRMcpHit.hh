//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRMcpHit.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               MCP HITS   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRMcpHit_h
#define LEMuSRMcpHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

class LEMuSRMcpHit : public G4VHit
{
public:
  // contructor and destructor
  LEMuSRMcpHit();
  ~LEMuSRMcpHit();
  
  void Draw();
  void Print();
  void print(G4String name);

  // operator for hit collector definition
  LEMuSRMcpHit(const LEMuSRMcpHit &right);
  const LEMuSRMcpHit& operator=(const LEMuSRMcpHit &right);
  G4int operator==(const LEMuSRMcpHit &right) const;

  inline void *operator new(size_t);
  inline void operator delete(void *aHit);

  // private variables

private:
  G4double energy_deposition, time_of_flight,spin;
  G4ThreeVector position, momentum;
  G4LogicalVolume* lv_Volume;
  G4VPhysicalVolume* pv_Volume;
  G4String particle_name;

  // inline functions
public:
  inline void SetEnergyDeposition(G4double ed){energy_deposition = ed;}
  inline void AddEnergyDeposition(G4double ed){energy_deposition += ed;}
  inline G4double GetEnergyDeposition(){return energy_deposition;}

  inline void SetTimeOfFlight(G4double tf){ time_of_flight=tf;}
  inline void AddTimeOfFlight(G4double tf){ time_of_flight+=tf;}
  inline G4double GetTimeOfFlight(){return  time_of_flight;}

  inline void SetSpin(G4double sp){ spin=sp;}
  inline G4double GetSpin(){return  spin;}

  inline void SetPosition(G4ThreeVector pos){position =pos;}
  inline G4ThreeVector GetPosition(){return position;}

  inline void SetMomentum(G4ThreeVector mom){momentum =mom;}
  inline G4ThreeVector GetMomentum(){return momentum;}

  inline void SetParticleName(G4String name){particle_name=name;}
  inline G4String GetParticleName(){return particle_name;}


  inline G4LogicalVolume* GetLogicalVolume(){return lv_Volume;}
  inline G4VPhysicalVolume* GetPhysicalVolume(){return  pv_Volume;}

};

// define the collection class according to template G4THitsCollection

typedef G4THitsCollection<LEMuSRMcpHit> LEMuSRMcpHitsCollection;

extern G4Allocator<LEMuSRMcpHit> LEMuSRMcpHitAllocator;

inline void* LEMuSRMcpHit :: operator new(size_t)
{
  void *aHit;
  aHit = (void*) LEMuSRMcpHitAllocator.MallocSingle();
  return aHit;
}

inline void LEMuSRMcpHit :: operator delete(void *aHit)
{
 LEMuSRMcpHitAllocator.FreeSingle((LEMuSRMcpHit*) aHit);
}


#endif
