//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRDepolarize.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               LEMUSRDEPOLARIZE   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

/*!
This class was built to depolarize the muon in some materials. It is not used in the simulation and shall probably be removed.
*/

#ifndef LEMuSRDepolarize_h
#define LEMuSRDepolarize_h 1
#include "G4VDiscreteProcess.hh"
#include "G4EnergyLossTables.hh"
#include "G4GPILSelection.hh"
#include "G4PhysicsLogVector.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"
#include "G4UnitsTable.hh"
#include "G4MaterialCutsCouple.hh"
#include "G4ForceCondition.hh"

#include "G4ParticleTable.hh"
#include "G4DynamicParticle.hh"
#include "LEMuSRPrimaryGeneratorAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class LEMuSRDepolarize : public G4VDiscreteProcess

{
 public:    // with description
  LEMuSRDepolarize(const G4String& name="mudep" ,
		    G4ProcessType   aType = fElectromagnetic );
  

  ~LEMuSRDepolarize();


  G4VParticleChange* AtRestDoIt(
				  const G4Track& ,
				  const G4Step& 
				  );
  
  G4VParticleChange* PreStepDoIt(
				  const G4Track& ,
				  const G4Step& 
				  );
  
  G4VParticleChange* PostStepDoIt(
				  const G4Track& ,
				  const G4Step& 
				  );
 
  
  G4double GetMeanFreePath(const G4Track& aTrack,
			   G4double   previousStepSize,
			   G4ForceCondition* condition
			   );
  
  
  
  G4bool CheckCondition( const G4Step& aStep);
  G4bool CheckCondition( const G4Step* aStep);

  
  G4String p_name, vname;
  G4double spin, edep, toten, kinen, tof, globaltime, proptime, itime, ftime,gtime, deltatime;
  G4ThreeVector hitpos, hitmom;
  G4int ID;
  
  G4bool condition;

  void GetDatas( const G4Step* aStep);
   // model parameters
  G4ParticleTable* particleTable; 
  G4ParticleDefinition* particle;
  G4double rnd;
  G4DynamicParticle *DP;
  
   G4ParticleChange fParticleChange; 
 

  void  PrepareSecondary(const G4Track&);
  G4Track* aSecondary;

   void InitializeSecondaries(const G4Track&);
 

 
};


#endif
 

 
