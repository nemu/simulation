//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRDetectorMessenger.hh, v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                        DETECTOR MESSENGER   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



#ifndef LEMuSRDetectorMessenger_h
#define LEMuSRDetectorMessenger_h 1


#include "G4ios.hh"
#include "globals.hh"
#include "G4UImessenger.hh"
#include "LEMuSRDetectorConstruction.hh"
#include "LEMuSRElectricField.hh"
#include "G4RunManager.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"

#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcommand.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

class G4UIcommand;
class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWith3Vector;
class G4UIcmdWithAnInteger;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class LEMuSRDetectorConstruction; //modif

/**
 * The LEMuSRDetectorMessenger class defines the terminal's commands to operate changes in the detector geometry. The following settings can be personalized:
 * -# MCP/Sample cryostat geometry
 * -# Electric Fields on/off
 * -# Magnetic Fields on/off
 * -# Sample cryostat grid
 * -# Sample cryostat guards
 * -# Electric potentials
 * -# Carbon foil thickness
 * -# Maximal step size in field regions
 * -# Sample holder material
 * -# Views of the detector
 * .
 * Many more can be implemented at will by the user.
 */
class LEMuSRDetectorMessenger : public G4UImessenger  {
public:
  //! Constructor.
  /*! 
   * It stores the pointer to the detector provided by LEMuSRDetectorConstruction.
   * For the command names, refer to the LEMuSRDetectorMessenger.cc file.
   */
  LEMuSRDetectorMessenger(LEMuSRDetectorConstruction*);

  //! Destructor.
  ~LEMuSRDetectorMessenger();
  
  
public:
  //! Applying the modifications.
  void SetNewValue(G4UIcommand* command, G4String newvalue);  
  
 
 //arguments
private: 
  //! Pointer to the detector.
  LEMuSRDetectorConstruction* theDetector;
  
  // commands	
private: 
  //! Directory name for the commands: /Detector/

  G4UIdirectory*  DetMode;
  
  //commands with STRING
  //! Detector with MCP/CRYO
  G4UIcmdWithAString* SetDetMode ;
  //! Enable/Disable electric fields
  G4UIcmdWithAString* SetElField ;
  //! Enable/Disable grids at sample cryostat
  G4UIcmdWithAString* SetGrid ;
  //! Enable/Disable guards at sample cryostat
  G4UIcmdWithAString* SetGuards ;
  //! Set the view of the detector (quarter, half, total).
  /*!
   * Only for visualization purpose. The total geometry must be loaded to run a simulation. 
  */
  G4UIcmdWithAString* SetDetVisualization ;  
  //! The material for the sample holder
  G4UIcmdWithAString* SetSAHmaterial ;  

  //! Would replace the environment variable in order to run any test without recompilation.
  G4UIcmdWithAString* SetAsymCheck ;
  G4UIcmdWithAString* SetFieldCheck ;

  //commands with DOUBLE
  //! Third lens voltage settings
  G4UIcmdWithADouble* SetThirdLensPotential, *SetCFthk ;
  //! Maximal step in field regions
  G4UIcmdWithADouble* SetFieldStepLim ;
  //! Magnetic field value (will be changed to current)
  G4UIcmdWithADouble* SetMagField ;
  //! Sample cryostat voltage
  G4UIcmdWithADouble* SetCryoPotential;


  //coomand with 3vector
  //! Ring anode potential
  G4UIcmdWith3Vector* SetRAPotential;


  //! The new detector to build
  G4VPhysicalVolume* newDetector;


};

#endif				
