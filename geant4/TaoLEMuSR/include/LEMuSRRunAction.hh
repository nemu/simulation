//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRRunAction.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           RUN ACTION.HH
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



#ifndef LEMuSRRunAction_h
#define LEMuSRRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4ParticleGun.hh"


// root histogram classes
#include <stdlib.h>

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"

#include "G4VPhysicalVolume.hh"
// LEMuSR
#include "LEMuSRDetectorConstruction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4Run;
/*!
 * This class registers the actions to take at the beginning of a run. 
 * It has been imlpemented to enable the user interface.
 * cf \ref runaction.
*/
class LEMuSRRunAction : public G4UserRunAction
{
  public:
    LEMuSRRunAction();
   ~LEMuSRRunAction();

  public:
  //! \ct
    void BeginOfRunAction(const G4Run*);
  //! \dt
    void EndOfRunAction(const G4Run*);

};

#endif
