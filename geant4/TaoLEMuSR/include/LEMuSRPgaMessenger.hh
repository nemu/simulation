//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRPgaMessenger.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-16 09:12
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          PRIMARY GENERATOR ACTION MESSENGER
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//


#ifndef LEMUSRPGAMESSENGER_H
#define LEMUSRPGAMESSENGER_H 1

class LEMuSRPrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;

#include "G4UImessenger.hh"
#include "globals.hh"
/**
 * The LEMuSRPgaMessenger class is the completion of the LEMuSRParticleGunMessenger
 * class which defines the terminal's commands to operate changes in the initial 
 * conditions of the muon beam. 
 *
 * The commands directory lemuGun/ is completed with commands
 * to shoot particles randomely and to specify the energy distribution etc.
 */
class LEMuSRPgaMessenger: public G4UImessenger
{
  public:
  //!\ct 
   LEMuSRPgaMessenger(LEMuSRPrimaryGeneratorAction* thPGA);
  //!\dt 
  ~LEMuSRPgaMessenger();
    
  public:
  //! \mm
    void SetNewValue(G4UIcommand * command,G4String newValues);

  private:
  //! Pointer to the LEMuSRPrimaryGeneratorAction object.
    LEMuSRPrimaryGeneratorAction * lemuPGA;
    
  private: //commands
  //! Directory of the commands.
  G4UIdirectory *             pgaDirectory;
  //! Set particle energy.
  G4UIcmdWithAString          *setRndEnergy;
  //! Set the particle type.
  G4UIcmdWithAString          *setMuonium;
  //! Unusedx
  G4UIcmdWithoutParameter *reset;
  G4UIcmdWith3Vector
  //! Set Gun Position (fixed).
    *posCmd, 
    //! Set Momentum Direction.  
    *momCmd, 
    //! Set Square Scan.
    *scaCmd, 
    //! Set Circular Scan.
    *scbCmd, 
    //! Set Gaussian Scan.
    *scgCmd, 
    //! Set Gaussian Energy Distribution.
    *setEGauss;    
  //! Set Energy Value.
  G4UIcmdWithADouble *setEnergy;
  // Set Energy Offset.
  G4UIcmdWithADouble *setKEoffset;
  // Set Particle's Charge (not implemented).
  G4UIcmdWithADouble *setCharge;




};

#endif
