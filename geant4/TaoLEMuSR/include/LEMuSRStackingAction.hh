//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRStackingAction.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          STACKING ACTION.HH
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRStackingAction_H
#define LEMuSRStackingAction_H 1

#include "globals.hh"
#include "G4UserStackingAction.hh"
#include "G4ThreeVector.hh"

class G4Track;

#include "LEMuSRScintHit.hh"
class LEMuSRStackingActionMessenger;

/*!
 * The role of the stacking action is to select the tracks to register or not. 
 * Because of its architecture the \lemu simulation does not need to use a stacking action( cf. \ref Useraction).
 */
class LEMuSRStackingAction : public G4UserStackingAction
{
public:
  LEMuSRStackingAction();
  virtual ~LEMuSRStackingAction();
  
public:
  virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
  virtual void NewStage();
  virtual void PrepareNewEvent();
  
private:
  //  G4bool InsideRoI(const G4Track * aTrack,G4double ang);
  G4VHitsCollection* GetCollection(G4String colName);
  
  LEMuSRScintHitsCollection* ScintHits;
   LEMuSRStackingActionMessenger* theMessenger;
  
  G4int stage;

  /*    G4int reqMuon;
	G4int reqIsoMuon;
	G4int reqIso;
	G4double angRoI;
  */

  
public:
  G4bool kill_e, kill_gamma, kill_nu_e, kill_nu_mu;

  void KillUnwanted();


 /*    inline void SetNRequestMuon(G4int val) { reqMuon = val; }
	inline G4int GetNRequestMuon() const { return reqMuon; }
	inline void SetNRequestIsoMuon(G4int val) { reqIsoMuon = val; }
	inline G4int GetNRequestIsoMuon() const { return reqIsoMuon; }
	inline void SetNIsolation(G4int val) { reqIso = val; }
	inline G4int GetNIsolation() const { return reqIso; }
	inline void SetRoIAngle(G4double val) { angRoI = val; }
	inline G4double GetRoIAngle() const { return angRoI; }
  */
};

#endif

