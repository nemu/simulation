
#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "globals.hh"
#include "G4VUserPhysicsList.hh"

#include "G4MuonPlus.hh"
#include "G4MuonMinus.hh"
#include "G4NeutrinoMu.hh"
#include "G4AntiNeutrinoMu.hh"

#include "G4MultipleScattering.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"
#include "G4MuIonisation.hh"

#include "G4DecayWithSpin.hh"

class PhysicsList: public G4VUserPhysicsList
{
public:
  PhysicsList();
  ~PhysicsList();
  
protected:
  // Construct particle and physics process
  void ConstructParticle();
  void ConstructProcess();

  void SetCuts();

private:
 
   G4DecayWithSpin*        theDecayProcess;

   G4MuIonisation*         theMuPlusIonisation;
   G4MultipleScattering*   theMuPlusMultipleScattering;
   G4MuBremsstrahlung*     theMuPlusBremsstrahlung ;
   G4MuPairProduction*     theMuPlusPairProduction;
                                                                                
   G4MuIonisation*         theMuMinusIonisation;
   G4MultipleScattering*   theMuMinusMultipleScattering;
   G4MuBremsstrahlung*     theMuMinusBremsstrahlung ;
   G4MuPairProduction*     theMuMinusPairProduction;

};

#endif
