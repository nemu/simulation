//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRPrimaryGeneratorAction.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-16 09:12
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          PRIMARY GENERATOR ACTION
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



#ifndef LEMuSRPrimaryGeneratorAction_h
#define LEMuSRPrimaryGeneratorAction_h 1

// G4 LIBRARIES
#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "LEMuSRParticleGun.hh"
#include "G4Event.hh"
#include "Randomize.hh"
#include "G4ParticleGun.hh"
#include "LEMuSRPgaMessenger.hh"



#include <iomanip>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <ios>

// G4 CLASSES
//class G4RandGauss;
class G4ParticleGun;
class LEMuSRParticleGun;
class G4Event;


// LEMuSRPrimaryGeneratorAction CLASS DEFINITION
/*!
 * The initial parameters of the particle(s) to simulate are given in the 
 * LEMuSRPrimaryGeneratorAction class. This mandatory class work in interplay
 * with the LEMuSRParticleGun to create each new event of the simulation.
 * 
 * An event is the track of one particle and all the particles created during its
 * evolution in the detector, by decay, scattering or any other kind of process.
 * cf. \ref pga.
*/
 
class LEMuSRPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  // Constructor & Destructor
  public:
  //!\ct
  LEMuSRPrimaryGeneratorAction();
  //!\dt
  ~LEMuSRPrimaryGeneratorAction();


  //! Pointer to the instance.
static  LEMuSRPrimaryGeneratorAction* thePGA;
  //! Returns the pointer to the instance.
  static LEMuSRPrimaryGeneratorAction*  GetPGA();


  // Event generation method
  //!\mm
  void GeneratePrimaries(G4Event* anEvent);
  //! Selection of the beam profile.
  void GetScanningMode(G4int scan_mode);

private:
  //! Particle gun pointer.
  LEMuSRParticleGun* lemuParticleGun;
public:
  double   rndenergy;
  G4double 
  //! Particle's decay time.
  decaytime, 
    //! Energy of the particle.
    energy;
  HepRandomEngine * theEngine;
  
  G4double 
  //! Position of the gun.
  X, Y, Z,
    //! Momentum of the initial particle.
    momX, momY, momZ, 
    //! Scanning parameters.
    m_xrange, m_yrange;
  
  //! Scaning parameters.
  G4int m_counterx, m_nbxsteps, m_countery, m_nbysteps, scan, gauss;
  //! Scanning parameters.
  G4double radius, angle, mean, stddev, sc_mean, sc_stddev;
  //! Scanning parameters
  G4double circ;
  //! Scanning parameters.
  G4bool rnd;
  //! Pointer to the messenger.
  LEMuSRPgaMessenger* messenger;

  G4double 
  //! Particle's charge.
  charge, 
    //! Energy offset (3.73 keV by default) to take into account the acceleration at the carbon foil.
    ke_offset;

  //! Particle's name.
  G4String pname;

  //===============================
  std::ofstream Yprint;  
  double table[1000];
  int i,j;
};

#endif



