//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRMUONIUMScatt.hh , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 16:15
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               MUONIUMScatt   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#ifndef LEMuSRMUONIUMScatt_h
#define LEMuSRMUONIUMScatt_h 1
#include "G4VDiscreteProcess.hh"
#include "G4EnergyLossTables.hh"
#include "G4GPILSelection.hh"
#include "G4PhysicsLogVector.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"
#include "G4UnitsTable.hh"
#include "G4MaterialCutsCouple.hh"
#include "G4ForceCondition.hh"

#include "G4ParticleTable.hh"
#include "G4DynamicParticle.hh"
#include "LEMuSRPrimaryGeneratorAction.hh"


   
#include "G4ThreeVector.hh"
#include "G4MagneticField.hh"
#include "G4Transform3D.hh"
#include "G4UnitsTable.hh"

#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"

#include "G4ProcessVector.hh"
#include "G4ProcessManager.hh"

#include "G4EventManager.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*!
 * LEMuSRMUONIUMScatt class defines the muonium scattering process. It is a very basic model
 * since we assume the muonium loose its electron as soon as it enters a material 
 * (other than vauum).
 *
 * Moreover, the spin precession of the muonium in flight is also implemented in this clss. 
 */
class LEMuSRMUONIUMScatt : public G4VDiscreteProcess

{
 public:    // with description
  //! \ct
  LEMuSRMUONIUMScatt(const G4String& name="Muscatt" ,
		    G4ProcessType   aType = fGeneral );
  
  //! \dt
  ~LEMuSRMUONIUMScatt();

  //! \mm The actions are taken at the end of the step.
  G4VParticleChange* PostStepDoIt(
			     const G4Track& ,
			     const G4Step& 
			    );

  G4double GetMeanFreePath(const G4Track& aTrack,
			   G4double   previousStepSize,
			   G4ForceCondition* condition
                                                               );


  //! The condition to apply the process.
  G4bool CheckCondition( const G4Step& aStep);


  G4String p_name, vname;
  G4double spin, edep, toten, kinen, tof, globaltime, proptime;
  G4ThreeVector hitpos, hitmom;
  G4int ID;

  G4bool condition;

  void GetDatas( const G4Step* aStep);
   // model parameters
  G4ParticleTable* particleTable; 
  G4ParticleDefinition* particle;
  G4double yvector[3];
  G4double rnd;
  G4DynamicParticle *DP;
  
   G4ParticleChange fParticleChange; 
 

  G4ThreeVector RotateSpinIfMag( const G4Track& , const G4Step&);
  G4double point[3], B[6];
  G4double itime, gtime, ftime,deltatime;
  G4ThreeVector polar;  
  G4ThreeVector RotateSpin(const G4Step&, G4ThreeVector, G4double);

  void  PrepareSecondary(const G4Track&);
  G4Track* aSecondary;

  void InitializeSecondaries(const G4Track&);
 

 
};


#endif
 

 
