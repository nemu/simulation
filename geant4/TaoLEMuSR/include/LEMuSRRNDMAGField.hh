//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRRNDMAGField.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:48
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          RND MAG FIELD
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "G4UniformMagField.hh"
#include"G4ThreeVector.hh"
#include"G4ios.hh"

class LEMuSRRNDMAGField : public G4UniformMagField
{
public: 


  LEMuSRRNDMAGField(const G4ThreeVector FieldVector,  G4double randomness);
  ~LEMuSRRNDMAGField();
 
  void GetFieldValue(const G4double pos[4], G4double *field) const;

  G4bool uniform;


  G4double R;

  G4double randomness;

  
private:
  G4double flength, fpotential, fradius, fzmin, fzmax;


  G4ThreeVector position;
  G4ThreeVector BField;

};
