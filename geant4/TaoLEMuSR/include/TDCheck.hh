//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : TDCheck.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2005-03-01 10:11
//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               TDCHECK   
//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§//
#ifndef TDCheck_h
#define TDCheck_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include <fstream>
#include "LEMuSRVisManager.hh"
#include "LEMuSRDetectorConstruction.hh"


#include "G4RunManager.hh"
#include "G4Run.hh"


// root histogram classes
#include <stdlib.h>

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"



class TDCheck : public G4UserSteppingAction
{
public:
  
  static TDCheck* GetInstance();
  
  TDCheck()   ;
  ~TDCheck()   ;
  
  void UserSteppingAction(const G4Step *theStep);

  void SetParticleVolumeNames(const G4Step *theStep);

  void SetPositionMomentum(const G4Step *theStep);
  
  void SetTimeEnergy(const G4Step *theStep);

  G4bool CheckCondition(const G4Step* aStep);

  G4String p_name, v_name, pv_name, previousv_name, nextv_name;  

  G4ThreeVector position,momentum,momentum_direction, polarization, m, parpol, p;

  G4double kenergy, tenergy, localtime, globaltime, proptime, fx, fy, fz, oldz, thk_old;
  G4int id, old_id, index;
  G4double theta,sintheta, costheta, phi, sinphi, cosphi;


  void LoopKiller(const G4Step*aStep);
   G4int loop;
  

typedef struct    {
 
  Float_t ienergy,tenergy, localtime, globaltime, proptime;
 
  Float_t positionx, positiony,positionz,momdirx,momdiry,momdirz;

  Float_t angle;
  Int_t index;
  Float_t thickness;
  Int_t id, charge;

  } LEMuSRparticle ;

  LEMuSRparticle muon;

  void Update();

  void BookRoot();
  void FillRoot();
  void WriteRoot();
  
  //  TROOT eventTree;
  Int_t nevent , comp , split , write , hfill , read ,arg4, arg5  ;
  TFile *myFile;
  TTree *myTree;
  TBranch *b,*b2;
  TH1D *hEnergy, *hTime, *hAngle, *hAngle2;
  
  TTree *tree;

 TFile* F;
 TTree *dt;

  Float_t time;


  G4double L,R,A; 

  
private:
  
  static TDCheck* pointer;
  

};

#endif
