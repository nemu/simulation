//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRDecay.hh , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               LEMUSRDECAY   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#ifndef LEMuSRDecay_h
#define LEMuSRDecay_h 1

#include "G4Decay.hh"


#include "G4ios.hh"
#include "globals.hh"
#include "G4VRestDiscreteProcess.hh"
#include "G4ParticleChangeForDecay.hh"
class G4VExtDecayer;

class G4Decay;

class LEMuSRDecay : public G4Decay
{

protected: // With Description
  
  //! The main method for the decay process.
  /*!
   * The DecayIt() method returns by pointer a particle-change object,
   * which has information of daughter particles.
   */
  
     virtual G4VParticleChange* DecayIt(
					const G4Track& aTrack,
					const G4Step&  aStep
					);
  
  //! The particle change object. 
  G4ParticleChangeForDecay pParticleChangeForDecay;
  //  const  G4VExtDecayer *m_ExtDecayer;
  G4double      m_RemainderLifeTime;
  
public:
G4int  testa, testb;  
};


#endif
