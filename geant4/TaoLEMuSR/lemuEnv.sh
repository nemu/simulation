export G4VRMLFILE_VIEWER=freewrl_sa

export G4UI_USE_TCSH=1

# ROOT, ROOTSYS usually defined in /etc/profile
#export ROOTSYS=/usr/local/root
export G4UI_BUILD_ROOT_SESSION=1
#export PATH=$ROOTSYS/bin:$PATH
#export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH




# VISUALIZATION
export G4VIS_BUILD_OPENGLX_DRIVER=1
export G4VIS_USE_OPENGLX=1


#export G4VIS_BUILD_DAWN_DRIVER=1
#export G4VIS_USE_DAWN=1
#export DAWN_PS_PREVIEWER='/usr/X11R6/bin/gv'
#export G4DAWN_NAMED_PIPE=1
#export G4DAWN_GUI_ALWAYS=1


# LEMuSR
export ASYM_USE_LEMU=1

export LEMuSR_FIELDMAPS_DIR=/scratch/FieldMaps


alias mu=$G4WORKDIR/bin/Linux-g++/LEMuSR
export wlem=$G4WORKDIR/LEMuSR

