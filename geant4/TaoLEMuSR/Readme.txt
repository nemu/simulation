Geant4 ElectricField update files:

Copy files in the following directories:

G4El_EqRhs         -----> $G4Install/source/geometry/magneticfield /src and /include
G4El_UsualEqRhs    -----> $G4Install/source/geometry/magneticfield /src and /include
G4ChordFinder      -----> $G4Install/source/geometry/magneticfield /src and /include
G4ParticleGun      -----> $G4Install/source/event                  /src and /include


track/ is the geant4.6.2 version of $G4Install/source/track directory and should be updated