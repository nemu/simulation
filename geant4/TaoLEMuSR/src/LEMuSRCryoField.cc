//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRCryoField.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               CRYOFIELD   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include"LEMuSRCryoField.hh"


LEMuSRCryoField::LEMuSRCryoField(G4ThreeVector FieldVector)
{
  EField=FieldVector;

  uniform=true;

}

LEMuSRCryoField::~LEMuSRCryoField()
{;}

LEMuSRCryoField::  LEMuSRCryoField(G4ThreeVector FieldVector,G4double radius,G4double zmin, G4double zmax)
{

  // initialize
  EField=FieldVector;

  fradius    = radius;
  fzmin      = zmin;
  fzmax      = zmax;
  G4cout<<"zmin= "<<fzmin/cm<<"[cm] zmax= "<<fzmax/cm<<"[cm] "<<G4endl;

  uniform=false;

}



void LEMuSRCryoField::GetFieldValue (const G4double pos[4],
                                            G4double *field ) const 
{



  field[0]= 0.0;
  field[1]= 0.0;
  field[2]= 0.0;
  
  G4double  X,Y,Z;
  G4bool it=false;

 X= pos[0];Y=pos[1];Z=pos[2];
  if(Z<fzmax&&Z>fzmin)
    {
      G4double R=sqrt(X*X+Y*Y);
      if(R<fradius)
      	{
	  it= true;
	}
    }
 
  if(it||uniform)
    {
      //      G4cout<<"true!"<<G4endl;getchar;
      field[0]= EField.x() ;//TAO
      field[1]= EField.y() ;
      field[2]= EField.z() ;
      //   G4cout<<"CRYO FIELD:  Z  "<<Z/mm<<"[mm], FIELD  "<<field[0]/volt*meter<<"V/m "<<field[1]/volt*meter<< "V/m "<<field[2]/volt*meter<< "V/m "<<G4endl;
    }

}
