//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRElFieldMix.cc , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               Electric Field   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
#include"LEMuSRElFieldMix.hh"
#include"G4UnitsTable.hh"


LEMuSRElFieldMix::LEMuSRElFieldMix( LEMuSRElectricField *E1,  LEMuSRElectricField *E2,  G4double field1_val, G4double field2_val)
{
  coef1  = field1_val;
  coef2  = field2_val;
  field1 = E1;
  field2 = E2;
  G4cout<<"FIELDMIX COEFS "<< coef1 <<"AND "<< coef2<<G4endl;
} 

LEMuSRElFieldMix::LEMuSRElFieldMix( G4ElectricField *E1,  G4ElectricField *E2,  G4double field1_val, G4double field2_val)
{
  coef1  = field1_val;
  coef2  = field2_val;
  field1 = E1;
  field2 = E2;
  G4cout<<"FIELDMIX COEFS "<< coef1 <<"AND "<< coef2<<G4endl;
} 


LEMuSRElFieldMix::~LEMuSRElFieldMix()
{
  ;
}



void LEMuSRElFieldMix::GetFieldValue(const G4double point[4], G4double *Bfield ) const
{
  G4double Bfield1[3], Bfield2[3];

  /*  Bfield1[0] = 0;
  Bfield1[1] = 0;
  Bfield1[2] = 0;

  Bfield2[0] = 0;
  Bfield2[1] = 0;
  Bfield2[2] = 0;
  */

  G4double position[4],position2[4];
  position[0]=point[0];
  position[1]=point[1];
  position[2]=point[2];
  position[3]=point[3];
  position2[0]=point[0];
  position2[1]=point[1];
  position2[2]=point[2];
  position2[3]=point[3];
  ///  G4cout<<"Field zb" <<field1->minz <<std::endl;
  //  G4cout<<"Field zb" <<field2->maxz <<std::endl;

  field1->GetFieldValue(position, Bfield1);
  //  G4cout<<"Field 1 Value " << G4BestUnit(Bfield1[0]*coef1*meter,"Electric potential") <<"/m " <<Bfield1[1]*coef1/volt*meter <<" V/m "<< Bfield1[2]*coef1/volt*meter <<" V/m " <<G4endl;

  field2->GetFieldValue(position2, Bfield2);
  //  G4cout<<"Field 2 Value " << G4BestUnit(Bfield2[0]*coef2*meter,"Electric potential") <<"/m " <<Bfield2[1]*coef2/volt*meter <<" V/m "<< Bfield2[2]*coef2/volt*meter <<" V/m " <<G4endl;

  // Bfield1=Bfield1*coef1;
  //  Bfield2=Bfield2*coef2;

  Bfield[0] = Bfield1[0]*coef1+ Bfield2[0]*coef2;
  Bfield[1] = Bfield1[1]*coef1+ Bfield2[1]*coef2;
  Bfield[2] = Bfield1[2]*coef1+ Bfield2[2]*coef2;

  //  G4cout<<"Field M  Value " << G4BestUnit(Bfield[0]*meter,"Electric potential") <<"/m " <<Bfield[1]/volt*meter <<" V/m "<< Bfield[2]/volt*meter <<" V/m " <<G4endl;


}


