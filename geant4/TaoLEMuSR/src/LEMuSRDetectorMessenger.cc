//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRDetectorMessenger.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                        DETECTOR MESSENGER   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
#include "LEMuSRDetectorMessenger.hh"
#include "LEMuSRDetectorConstruction.hh"



//
// process remove
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleChange.hh"
#include "G4ProcessVector.hh"
#include "G4ProcessManager.hh"
#include "G4VProcess.hh"



LEMuSRDetectorMessenger::LEMuSRDetectorMessenger(LEMuSRDetectorConstruction *lemuDetector)
{
  theDetector=lemuDetector;
  DetMode =  new G4UIdirectory("/Detector/");
  DetMode->SetGuidance("Set detector parameters");

  // commands 
  SetDetMode = new G4UIcmdWithAString("/Detector/Mode",this);
  SetDetMode->SetGuidance("\n mcp: multiple channel mode \n cryo: cryostat mode ");
  SetDetMode->SetParameterName("mode",false);
  SetDetMode->SetDefaultValue("mcp");
  SetDetMode->AvailableForStates(G4State_PreInit,G4State_Idle); 


  // commands 

  // El field on/off
  SetElField = new G4UIcmdWithAString("/Detector/ElectricField",this);
  SetElField->SetGuidance("\n on: electric field on \n off: electric field off ");
  SetElField->SetParameterName("field",false);
  SetElField->SetDefaultValue("on");
  SetElField->AvailableForStates(G4State_PreInit,G4State_Idle); 


  // Grid on/off
  SetGrid = new G4UIcmdWithAString("/Detector/Sample/Grid",this);
  SetGrid->SetGuidance("\n on: Sample Grid on \n off: Sample Grid off ");
  SetGrid->SetParameterName("grid",false);
  SetGrid->SetDefaultValue("on");
  SetGrid->AvailableForStates(G4State_PreInit,G4State_Idle); 


  // Guards on/off
  SetGuards = new G4UIcmdWithAString("/Detector/Sample/Guards",this);
  SetGuards->SetGuidance("\n on: Sample Guards Rings on \n off: Sample Guards off ");
  SetGuards->SetParameterName("guards",false);
  SetGuards->SetDefaultValue("on");
  SetGuards->AvailableForStates(G4State_PreInit,G4State_Idle); 


  // ASYM
  SetAsymCheck = new G4UIcmdWithAString("/Detector/AsymCheck",this);
  SetAsymCheck->SetGuidance("\n on: asym on \n off:asym off ");
  SetAsymCheck->SetParameterName("asym",false);
  SetAsymCheck->SetDefaultValue("on");
  SetAsymCheck->AvailableForStates(G4State_PreInit,G4State_Idle); 


  // SAH MATERIAL
  SetSAHmaterial = new G4UIcmdWithAString("/Detector/Sample/HolderMaterial",this);
  SetSAHmaterial->SetGuidance("\n Sample material");
  SetSAHmaterial->SetParameterName("material",false);
  SetSAHmaterial->SetDefaultValue("copper");
  SetSAHmaterial->AvailableForStates(G4State_PreInit,G4State_Idle); 



  // ELECTRIC POTENTIALS
  SetThirdLensPotential = new G4UIcmdWithADouble("/Detector/Voltage/ThirdLens",this);
  SetThirdLensPotential->SetGuidance("\n Third Lens Middle Cylinder Voltage Value >> IN KILOVOLT ");
  SetThirdLensPotential->SetParameterName("f",false);
  SetThirdLensPotential->SetDefaultValue(1);
  SetThirdLensPotential->AvailableForStates(G4State_PreInit,G4State_Idle); 

  SetMagField = new G4UIcmdWithADouble("/Detector/MagneticField",this);
  SetMagField->SetGuidance("\n Magnetic Field Max Value >>> in GAUSS ");
  SetMagField->SetParameterName("f",false);
  SetMagField->SetDefaultValue(100);
  SetMagField->AvailableForStates(G4State_PreInit,G4State_Idle); 




  SetRAPotential = new G4UIcmdWith3Vector("/Detector/Voltage/ConicalAnode",this);
  SetRAPotential->SetGuidance("\n Conical anode left and right side Voltage Values >> IN KILOVOLT; set build to !=0 to build the detector ");
  SetRAPotential->SetParameterName("RA-Left Voltage [kV]","RA-Right Voltage [kV]","Build",false,true);
  SetRAPotential->SetDefaultValue(Hep3Vector(8.1,8.1,1.0));
  SetRAPotential->AvailableForStates(G4State_PreInit,G4State_Idle); 
 
  SetCryoPotential = new G4UIcmdWithADouble("/Detector/Voltage/Cryo",this);
  SetCryoPotential->SetGuidance("\n Cryo voltage in kV ");
  SetCryoPotential->SetParameterName("Voltage [kV]",true,true);
  SetCryoPotential->SetDefaultValue(7.2);
  SetCryoPotential->AvailableForStates(G4State_PreInit,G4State_Idle); 



  SetDetVisualization  = new G4UIcmdWithAString("/Detector/View",this);
  SetDetVisualization->SetGuidance("\n quarter: quarter cut view of the detector \n half: half cut view of the detector \n total: view of the total detector ");
  SetDetVisualization->SetParameterName("view",false);
 SetDetVisualization->SetDefaultValue("total");
 SetDetVisualization->AvailableForStates(G4State_PreInit,G4State_Idle); 
 
 SetFieldStepLim = new G4UIcmdWithADouble("/Detector/MaxStepInField",this);
 SetFieldStepLim->SetGuidance("\n The user step limitation in third lens chamber and sample chamber [mm] ");
 SetFieldStepLim->SetParameterName("usl",false);
 SetFieldStepLim->SetDefaultValue(10.);
 SetFieldStepLim->AvailableForStates(G4State_PreInit,G4State_Idle); 


 // Carbon Foil 
  SetCFthk = new G4UIcmdWithADouble("/Detector/CfoilThickness",this);
  SetCFthk->SetGuidance("\n Carbon foil thickness; unit is mug/cm2 ");
  SetCFthk->SetParameterName("f",false);
  SetCFthk->SetDefaultValue(1.0);
  SetCFthk->AvailableForStates(G4State_PreInit,G4State_Idle); 


}

LEMuSRDetectorMessenger::~LEMuSRDetectorMessenger()
{
  delete theDetector;
  delete DetMode;
  delete SetMagField;
  delete SetThirdLensPotential;
  delete SetCFthk;
  delete SetCryoPotential;
  delete SetRAPotential;
  delete SetElField;
  delete SetGrid;
  delete SetGuards;
  delete SetFieldCheck;
  delete SetAsymCheck;
  delete SetDetMode;
  delete SetDetVisualization;
  delete  SetFieldStepLim;
  delete SetSAHmaterial;

}


void LEMuSRDetectorMessenger::SetNewValue(G4UIcommand* command, G4String newvalue)//MUST have the name SetNewValue:: inherited method from messenger class.
{
  //G4UImanager* UI = G4UImanager::GetUIpointer();

  if(command == SetDetMode)
    {
      if(newvalue=="mcp")
	{
	  theDetector->mcdetector=1;
	}
      else if(newvalue=="cryo")
	{
	  theDetector->mcdetector=0;
	}
      else
	{
	  G4cout << "Unknown command: please check value."<<G4endl;
	}

      	  G4cout << "About to build new detector."<<G4endl;
      newDetector = theDetector->Construct();
	  G4cout << "New detector built."<<G4endl;
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
      
      G4cout << " runmanager updated\n"	;
      
      // Actualisation du visualiseur
      //UI ->ApplyCommand("/vis/viewer/refresh") ;
      //UI ->ApplyCommand("/vis/viewer/update");
      
    }
  

  else if(command == SetThirdLensPotential)
    {
      if(SetThirdLensPotential->GetNewDoubleValue(newvalue)!=0.)
	{
	  theDetector->elfield=1;
	  theDetector->L3FieldVal=SetThirdLensPotential->GetNewDoubleValue(newvalue);
	}

      else if(SetThirdLensPotential->GetNewDoubleValue(newvalue)==0.)
	{
	  theDetector->elfield=0;

	}
       newDetector = theDetector->Construct();
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
    
    }
  
  else if(command == SetMagField)
    {
      if(SetMagField->GetNewDoubleValue(newvalue)!=0.)
	{
	  theDetector->magfield=1;
	  theDetector->B=SetMagField->GetNewDoubleValue(newvalue);
	}
      else if(SetMagField->GetNewDoubleValue(newvalue)==0.)
	{
	  theDetector->magfield=0;
	}
      newDetector = theDetector->Construct();
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
    }
  


  else if(command == SetSAHmaterial)
    {
      if(G4Material::GetMaterial(newvalue))
	{
	  theDetector->Material_SAH=newvalue;
	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
	}
      else
	{
	  G4cout<<":-? Material "<<newvalue<<" not found!!!"<<G4endl;
	  G4cout<<"Sample holder material is still "
		<<theDetector->Material_SAH<<"."<<G4endl;
	}
      
    }


  else if(command == SetCFthk)
    {
      theDetector->cfthk=SetCFthk->GetNewDoubleValue(newvalue);
      newDetector = theDetector->Construct();
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
      G4cout<<"Thickness: " << LEMuSRDetectorConstruction::GetInstance()->cfthk<<G4endl;
    }
  
  else if(command == SetRAPotential)
    {
      G4double ral, rar, build;
      ral    = SetRAPotential->GetNew3VectorValue(newvalue).x();
      rar    = SetRAPotential->GetNew3VectorValue(newvalue).y();
      build = SetRAPotential->GetNew3VectorValue(newvalue).z(); 
      
      theDetector->RALval=ral;
      theDetector->RARval=rar;
      G4cout<<"Conical anode Voltage:\n RA-L: "<< ral <<"[kV]    RA-R: "<<rar <<" [kV]"<<G4endl;    
      theDetector->anode_elfield=1;
      if(build!=0.0)
	{
	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
    	}
      
      else
	{
	  G4cout<<"\n ~~~~~~~~~~~~~ATTENTION:: DETECTOR NOT BUILT~~~~~~~~~~~~"<<G4endl;    
	}
      
    }
  else if(command == SetCryoPotential)
    {
      G4double V;
      V    = SetCryoPotential->GetNewDoubleValue(newvalue);
      
      theDetector->cryoVoltage=V;
      G4cout<<"Cryo Voltage:\n V: "<< V <<"[kV]"<<G4endl;    
      theDetector->anode_elfield=1;
      newDetector = theDetector->Construct();
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
    }


  
  else if(command == SetElField)
    {
      if(newvalue=="on")
	{
	  //	  theDetector->elfield=1;
	  //	  theDetector->anode_elfield=1;
	  theDetector->trigger_field=1;

	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
	  G4cout<<"THIS COMMAND ONLY ENABLES THE TRIGGER FIELD.\n"
		<<"TO SET THE ELECTROSTATIC LENSES PLEASE USE THE /Detector/Voltage COMMANDS."<<G4endl;
	}
      else if(newvalue=="off")
	{
	  theDetector->elfield=0;
	  theDetector->anode_elfield=0;
	  theDetector->trigger_field=0;

	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
	  G4cout<<"ALL ELECTRIC FIELDS ARE NOW DISABLED."<<G4endl;
	}
      else
	{
	  G4cout<<"UNRECOGNIZED COMMAND PLEASE  ENTER on OR off."<<G4endl;
	}
    
    }



  // SAMPLE GRID
  else if(command == SetGrid)
    {
      if(newvalue=="on")
	{
	  theDetector->Grid=1;
	  
	}

      else if(newvalue=="off")
	{

	  theDetector->Grid=0;

	}
      G4cout<<"SAMPLE CRYOSTAT:: GRID ON ==>> DON'T FORGET TO REBUILD DETECTOR"<<G4endl;   
    }
  
 
  // SAMPLE GUARDS
  else if(command == SetGuards)
    {
      if(newvalue=="on")
	{
	  theDetector->Guards=1;
	  
	}

      else if(newvalue=="off")
	{

	  theDetector->Guards=0;

	}
       G4cout<<"SAMPLE CRYOSTAT:: GUARDS ON ==>> DON'T FORGET TO REBUILD DETECTOR"<<G4endl;   
  
    }
  


  else if(command == SetFieldStepLim)
    {
      theDetector->FieldStepLim=SetFieldStepLim->GetNewDoubleValue(newvalue)*mm;
      newDetector = theDetector->Construct();
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
    }



  else if(command == SetAsymCheck)
    {
      if(newvalue=="on")
	{
	  theDetector->AsymCheck=1;
	  
	}

      else if(newvalue=="off")
	{
	  theDetector->AsymCheck=0;

	}
       newDetector = theDetector->Construct();
      G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
    
    }


  else if(command == SetDetVisualization)
    {
      if(newvalue=="total")
	{
	  theDetector->dSPhi=0.*deg;
	  theDetector->dEPhi=360.*deg;
	  theDetector->halfview=0;
 
	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
 	  G4cout << " \n  TOTAL GEOMETRY IS LOADED.  \n"	;
	}
      else if(newvalue=="half")
	{
	  theDetector->dSPhi=90.*deg;
	  theDetector->dEPhi=180.*deg;
	  theDetector->halfview=1;
	  
	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
	  G4cout << " \n   !!! BEFORE '/run/beamOn' :: PLEASE ENSURE TOTAL GEOMETRY IS LOADED !!!  \n"	;
	}
      else if(newvalue=="quarter")
	{
	  theDetector->dSPhi=0.*deg;
	  theDetector->dEPhi=270.*deg;
	  theDetector->halfview=1; 
      
	  newDetector = theDetector->Construct();
	  G4RunManager::GetRunManager()->DefineWorldVolume(newDetector);
	  G4cout << " \n   !!! BEFORE '/run/beamOn' :: PLEASE ENSURE TOTAL GEOMETRY IS LOADED !!!  \n"	;
	}
      else
	{
	  G4cout << "Unknown command: please check value is total/quarter/half ."<<G4endl;
	}
      
     
      // Update visualization
      //UI ->ApplyCommand("/vis/viewer/refresh") ;
      //UI ->ApplyCommand("/vis/viewer/update");
      
    }

  else
    {
      G4cout << "Unknown command: please check value."<<G4endl;
    }












}
