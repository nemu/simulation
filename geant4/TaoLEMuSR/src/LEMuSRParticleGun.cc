//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRParticleGun.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:48
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                          PARTICLE GUN
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "LEMuSRParticleGun.hh"
#include "Randomize.hh"
#include "G4ios.hh"
#include "LEMuSRParticleGunMessenger.hh"

#include "G4ProcessManager.hh"
#include "G4VProcess.hh"

#include "G4ProcessVector.hh"



LEMuSRParticleGun::LEMuSRParticleGun()
{
  SetInitialValues();
  theMessenger = new LEMuSRParticleGunMessenger(this);
}

LEMuSRParticleGun::~LEMuSRParticleGun()
{
  delete theMessenger;
}

void LEMuSRParticleGun::SetInitialValues()
{
  NumberOfParticlesToBeGenerated = 1;
  particle_definition = 0;
  G4ThreeVector zero;
  particle_momentum_direction = (G4ParticleMomentum)zero;
  particle_energy = 0.0;
  particle_position = zero;
  particle_time = 0.0;
  particle_polarization = zero;
  particle_charge = 0.0;

}


void LEMuSRParticleGun::GeneratePrimaryVertex(G4Event* evt)
{
  if(particle_definition==0) return;

  //! \b 1. Creates a new vertex.
  /*!
   * The initial vertex of the particle is defined with a position-time
   * and receives the information of the particle to simulate.
   */
  G4PrimaryVertex* vertex = 
    new G4PrimaryVertex(particle_position,particle_time);


  //! \b 2. Creates new primary particle.
  /*!
   * The primary particle is used in the initial vertex to 
   * produce the dynamic particles which will actually be tracked.
   */
  G4double mass =  particle_definition->GetPDGMass();
  G4double energy = particle_energy + mass;
  G4double pmom = sqrt(energy*energy-mass*mass);
  G4double px = pmom*particle_momentum_direction.x();
  G4double py = pmom*particle_momentum_direction.y();
  G4double pz = pmom*particle_momentum_direction.z();
  for( G4int i=0; i<NumberOfParticlesToBeGenerated; i++ )
    {
      
      G4PrimaryParticle* particle =
	new G4PrimaryParticle(particle_definition,px,py,pz);
      particle->SetMass( mass );

      particle->SetPolarization(particle_polarization.x(),
				particle_polarization.y(),
				particle_polarization.z());
      particle->SetProperTime(decaytime);
      //    G4cout<<"Muon decay time = " << decaytime/ns <<"ns. \n" <<G4endl;
  
      //! \b 3. Set the primary particle to the vertex.    
      vertex->SetPrimary( particle );
      
      
    }
  
  //! \b 4. Sets the vertex to the event.
  evt->AddPrimaryVertex( vertex );
}
