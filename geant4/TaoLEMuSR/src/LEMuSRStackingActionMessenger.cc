//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRStackingActionMessenger.cc , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           STACKING ACTION MESSENGER
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



#include "LEMuSRStackingActionMessenger.hh"
#include "LEMuSRStackingAction.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4ios.hh"

LEMuSRStackingActionMessenger::LEMuSRStackingActionMessenger(LEMuSRStackingAction * msa)
:myAction(msa)
{
  ;
}

LEMuSRStackingActionMessenger::~LEMuSRStackingActionMessenger()
{
  ;
}

void LEMuSRStackingActionMessenger::SetNewValue(G4UIcommand *,G4String)
{
  ;
}

G4String LEMuSRStackingActionMessenger::GetCurrentValue(G4UIcommand *)
{
  //to make the compiler satisfied with function declaration
  return "LEMuSRStackingActionMessenger::GetCurrentValue undefined";
}

