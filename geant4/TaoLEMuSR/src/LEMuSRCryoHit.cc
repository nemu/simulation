//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRCryoHit.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               CRYOHIT   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "LEMuSRCryoHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"
#include <fstream>
#include <iomanip>

#include "G4UnitsTable.hh"

G4Allocator<LEMuSRCryoHit> LEMuSRCryoHitAllocator;

LEMuSRCryoHit::LEMuSRCryoHit()
{;}

LEMuSRCryoHit::~LEMuSRCryoHit()
{;}

LEMuSRCryoHit::LEMuSRCryoHit(const LEMuSRCryoHit &right) : G4VHit()
{
  particle_name = right.particle_name;
  energy_deposition = right.energy_deposition;
  time_of_flight = right.time_of_flight;
  position = right.position;
  momentum = right.momentum;
}

const LEMuSRCryoHit& LEMuSRCryoHit::operator=(const LEMuSRCryoHit &right) 
{
  particle_name = right.particle_name;
  energy_deposition = right.energy_deposition;
  time_of_flight = right.time_of_flight;
  position = right.position;
  momentum = right.momentum;
  return *this;
}



G4int LEMuSRCryoHit::operator==(const LEMuSRCryoHit &right) const
{
  return (this==&right) ? 1 : 0;
}

void LEMuSRCryoHit::Draw()
{
  G4VVisManager* VisManager = G4VVisManager::GetConcreteInstance();
  if(VisManager)
    {
      G4Circle circle(position);
      circle.SetScreenSize(0.1);
      circle.SetFillStyle(G4Circle::filled);
      G4Colour colour(1.,1.,1.);
      G4VisAttributes attributes(colour);
      circle.SetVisAttributes(attributes);
      VisManager->Draw(circle);
    }
}

void LEMuSRCryoHit::Print()
{}

void LEMuSRCryoHit::print(G4String name)
{
  using namespace std;
  
   ofstream TestPrint(name,ios::app);
   if (!TestPrint.is_open()) exit(8);
   TestPrint  << "particle name     : " <<  particle_name   <<" ;\n "
	      << "energy_deposition : " <<  G4BestUnit(energy_deposition,"Energy") <<" ;\n "
	      << "time_of_flight    : " <<  G4BestUnit(time_of_flight,"Time")    <<" ;\n "
	      << "position          : " <<  position          <<" ;\n "
	      << "momentum          : " <<  momentum          <<" ;\n " 
	      <<G4endl;

}
