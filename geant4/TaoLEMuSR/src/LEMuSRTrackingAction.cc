//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRTrackingAction.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           TRACKING ACTION.CC
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include"LEMuSRTrackingAction.hh"
#include"G4VProcess.hh"
#include"G4PrimaryParticle.hh"
#include"G4UnitsTable.hh"
#include"LEMuSRMuonDecayChannel.hh"

//! \ct
/*
 * The tracking manager can be accessed by calling
the event manager GetTrackingManager() method.
One should not create here a pointer to the tracking manager, which is created
by the event manager  and must be a singleton.
http://www-geant4.kek.jp/lxr/source/event/src/G4EventManager.cc
The event mnager is accessed by G4EventManager::GetEventManager().
*
* BUG REPORT:
* A previous version created here a new pointer of type G4TrackingManager: 
* the BUG due to this operation was the disparition of the tracking verbose
* which is enabled during the simulation by the command <tt> /tracking/verbose x</tt>.
 */
LEMuSRTrackingAction ::LEMuSRTrackingAction()
{
  pointer=this  ;
}



LEMuSRTrackingAction*  LEMuSRTrackingAction::pointer=0;

//! Returns  pointer to the tracking action.
LEMuSRTrackingAction*  LEMuSRTrackingAction::GetInstance()
{ 
  return pointer;
}


//! \dt
LEMuSRTrackingAction:: ~LEMuSRTrackingAction()
{
  ;
}

//! Actions to take at the beginning of a track.
void LEMuSRTrackingAction::PreUserTrackingAction(const G4Track* theTrack)
{
  ;
}


void LEMuSRTrackingAction::Collect_datas(const G4Track* theTrack)
{

  ;
}



//! Actions to take at the end of a track.
void LEMuSRTrackingAction::PostUserTrackingAction(const G4Track*)
{
  ;
}

