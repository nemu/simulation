//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
// $Id: LEMuSRParticleChangeForMSC.cc,v 1.12 2004/12/02 06:38:05 kurasige Exp $
// GEANT4 tag $Name: geant4-07-01 $
//
// 
// --------------------------------------------------------------
//	GEANT 4 class implementation file 
//
//	
//	
// ------------------------------------------------------------
//   Implemented for the new scheme                 23 Mar. 1998  H.Kurahige
//   Update for model variant of msc                16 Jan  2004  V.Ivanchenko
// --------------------------------------------------------------

#include "LEMuSRParticleChangeForMSC.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4DynamicParticle.hh"
#include "G4ExceptionSeverity.hh"

LEMuSRParticleChangeForMSC::LEMuSRParticleChangeForMSC():G4VParticleChange()
{
  Post_Update=false;

#ifdef G4VERBOSE
  if (verboseLevel>2) {
    G4cout << "LEMuSRParticleChangeForMSC::LEMuSRParticleChangeForMSC() " << G4endl;
  }
#endif
}

LEMuSRParticleChangeForMSC::~LEMuSRParticleChangeForMSC()
{
#ifdef G4VERBOSE
  if (verboseLevel>2) {
    G4cout << "LEMuSRParticleChangeForMSC::~LEMuSRParticleChangeForMSC() " << G4endl;
  }
#endif
}

LEMuSRParticleChangeForMSC::LEMuSRParticleChangeForMSC(
             const LEMuSRParticleChangeForMSC &right): G4VParticleChange(right)
{
   if (verboseLevel>1) {
    G4cout << "LEMuSRParticleChangeForMSC::  copy constructor is called " << G4endl;
   }
      theMomentumDirection = right.theMomentumDirection;
      thePosition = right.thePosition;
}

// assignment operator
LEMuSRParticleChangeForMSC & LEMuSRParticleChangeForMSC::operator=(
                                   const LEMuSRParticleChangeForMSC &right)
{
   if (verboseLevel>1) {
    G4cout << "LEMuSRParticleChangeForMSC:: assignment operator is called " << G4endl;
   }
   if (this != &right)
   {
      theListOfSecondaries = right.theListOfSecondaries;
      theSizeOftheListOfSecondaries = right.theSizeOftheListOfSecondaries;
      theNumberOfSecondaries = right.theNumberOfSecondaries;
      theStatusChange = right.theStatusChange;
      theLocalEnergyDeposit = right.theLocalEnergyDeposit;
      theSteppingControlFlag = right.theSteppingControlFlag;
      theTrueStepLength = right.theTrueStepLength;

      theMomentumDirection = right.theMomentumDirection;
      thePosition = right.thePosition;
   }
   return *this;
}

//----------------------------------------------------------------
// methods for updating G4Step
//

G4Step* LEMuSRParticleChangeForMSC::UpdateStepForAlongStep(G4Step* pStep)
{
  //  Update the G4Step specific attributes
  pStep->SetStepLength(theTrueStepLength);
  theStatusChange = pStep->GetTrack()->GetTrackStatus();

//   DumpInfo();


  return pStep;
}

G4Step* LEMuSRParticleChangeForMSC::UpdateStepForPostStep(G4Step* pStep)
{

  G4StepPoint* pPostStepPoint = pStep->GetPostStepPoint();
  /*     G4cout << "POST STEP UPDATE DATA: "  ;
	 G4cout.precision(6);
	 G4cout << "        Energy [MeV]        : "
	 << std::setw(20) <<
	 G4cout << "        Position - x (mm)   : "
	 << std::setw(20) << thePosition.x()/mm
	 << G4endl;
	 G4cout << "        Position - y (mm)   : "
	 << std::setw(20) << thePosition.y()/mm
	 << G4endl;
	 G4cout << "        Position - z (mm)   : "
	 << std::setw(20) << thePosition.z()/mm
	 << G4endl;
	 G4cout << "        Momentum Direct - x : "
	 << std::setw(20) << theMomentumDirection.x()
	 << G4endl;
	 G4cout << "        Momentum Direct - y : "
	 << std::setw(20) << theMomentumDirection.y()
	 << G4endl;
	 G4cout << "        Momentum Direct - z : "
	 << std::setw(20) << theMomentumDirection.z()
	 << G4endl;
	 
  */     
  
  //     theStatusChange = pStep->GetTrack()->GetTrackStatus();
  // A physics process always calculates the final state of the particle
  
  // update position
  pPostStepPoint->SetPosition( thePosition );
  
  // update  momentum direction
  //  G4cout<<"Momentum to set: "<<theMomentumDirection<<G4endl;
  pPostStepPoint->SetMomentumDirection(theMomentumDirection);
  
 
  return pStep;
}


//----------------------------------------------------------------
// methods for printing messages
//

void LEMuSRParticleChangeForMSC::DumpInfo() const
{
// use base-class DumpInfo
  G4VParticleChange::DumpInfo();

  G4cout.precision(6);
  G4cout << "        Energy [MeV]        : "
	 << std::setw(20) <<
  G4cout << "        Position - x (mm)   : "
       << std::setw(20) << thePosition.x()/mm
       << G4endl;
  G4cout << "        Position - y (mm)   : "
       << std::setw(20) << thePosition.y()/mm
       << G4endl;
  G4cout << "        Position - z (mm)   : "
       << std::setw(20) << thePosition.z()/mm
       << G4endl;
  G4cout << "        Momentum Direct - x : "
       << std::setw(20) << theMomentumDirection.x()
       << G4endl;
  G4cout << "        Momentum Direct - y : "
       << std::setw(20) << theMomentumDirection.y()
       << G4endl;
  G4cout << "        Momentum Direct - z : "
       << std::setw(20) << theMomentumDirection.z()
       << G4endl;
}


G4bool LEMuSRParticleChangeForMSC::CheckIt(const G4Track& aTrack)
{
  G4bool    itsOK = true;
  G4bool    exitWithError = false;

  G4double  accuracy;

  // check

  // MomentumDirection should be unit vector
  accuracy = std::abs(theMomentumDirection.mag2()-1.0);
  if (accuracy > accuracyForWarning) {
#ifdef G4VERBOSE
    G4cout << "  LEMuSRParticleChangeForMSC::CheckIt  : ";
    G4cout << "the Momentum Change is not unit vector !!" << G4endl;
    G4cout << "  Difference:  " << accuracy << G4endl;
#endif
    itsOK = false;
    if (accuracy > accuracyForException) exitWithError = true;
  }

  // dump out information of this particle change
#ifdef G4VERBOSE
  if (!itsOK) {
    G4cout << " LEMuSRParticleChangeForMSC::CheckIt " <<G4endl;
    DumpInfo();
  }
#endif

  // Exit with error
  if (exitWithError) {
    G4Exception("LEMuSRParticleChangeForMSC::CheckIt",
		"300",
		EventMustBeAborted,
		"momentum direction was illegal");
  }
  //correction
  if (!itsOK) {
    G4double vmag = theMomentumDirection.mag();
    theMomentumDirection = (1./vmag)*theMomentumDirection;
  }

  itsOK = (itsOK) && G4VParticleChange::CheckIt(aTrack);
  return itsOK;
}




