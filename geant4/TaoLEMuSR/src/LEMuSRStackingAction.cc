//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRStackingAction.cc , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           STACKING ACTION.CC
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



#include "LEMuSRStackingAction.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4HCofThisEvent.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "LEMuSRStackingActionMessenger.hh"
#include "G4ios.hh"

LEMuSRStackingAction::LEMuSRStackingAction()
 :ScintHits(0), stage(0)
{
 theMessenger = new LEMuSRStackingActionMessenger(this);

 kill_e = true; kill_gamma = true; kill_nu_e = kill_nu_mu = true;
}

LEMuSRStackingAction::~LEMuSRStackingAction()
{
 delete theMessenger;
 }



G4ClassificationOfNewTrack
LEMuSRStackingAction::ClassifyNewTrack(const G4Track *track)
{
  G4ClassificationOfNewTrack classification;
  classification = fUrgent;

  G4String p_name;
  p_name = track->GetDefinition()->GetParticleName(); // particle name   



  if (p_name=="e-")
    {
      if (kill_e) classification=fKill;
    }

  else if (p_name=="gamma")
    {
      if (kill_gamma)  classification=fKill;
    }

  else if (p_name=="nu_e")
    {
      if (kill_nu_e)  classification=fKill;
    }

  else if (p_name=="anti_nu_e")
    {
      if (kill_nu_e)  classification=fKill;
    }

  else if (p_name=="nu_mu")
    {
      if (kill_nu_mu)  classification=fKill;
    }

  else if (p_name=="anti_nu_mu")
    {
      if (kill_nu_mu)  classification=fKill;
    }





  return classification;
}


G4VHitsCollection* LEMuSRStackingAction::GetCollection(G4String colName)
{
  G4SDManager* SDMan = G4SDManager::GetSDMpointer();
  G4RunManager* runMan = G4RunManager::GetRunManager();
  int colID = SDMan->GetCollectionID(colName);
  if(colID>=0)
  {
    const G4Event* currentEvent = runMan->GetCurrentEvent();
    G4HCofThisEvent* HCE = currentEvent->GetHCofThisEvent();
    return HCE->GetHC(colID);
  }
  return 0;
}



void LEMuSRStackingAction::NewStage()
{

}


    
void LEMuSRStackingAction::PrepareNewEvent()
{ 
  stage = 0; 
  ScintHits = 0;
}


