//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRMuonPhysics.cc , v 1.5
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-02-25 14:47
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               MuonPhysics   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// AFTER $Id$
// GEANT4 tag $Name: geant4-08-00-patch-01 $ exampleN07.cc
//

#include "LEMuSRPhysicsList.hh"

#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4ios.hh"              

// LEMuSR Muonium Particle
#include "LEMuSRMuoniumParticle.hh"









//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//                     CONSTRUCTOR AND DESTRUCTOR                         //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


LEMuSRPhysicsList::LEMuSRPhysicsList():  G4VUserPhysicsList()
{
 defaultCutValue = 5.0*mm; 
 SetVerboseLevel(2);
}

LEMuSRPhysicsList::~LEMuSRPhysicsList()
{}






/*! \anchor constructparticles*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//                     CONSTRUCTION OF THE DIFFERENT PARTICLES            //
//                     CALLS THE DIFFERENT CONSTRUCTION METHODS           //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


void LEMuSRPhysicsList::ConstructParticle()
{
  // In this method, static member functions should be called
  // for all particles which you want to use.
  // This ensures that objects of these particle types will be
  // created in the program. 
  SetCuts();
  ConstructBosons();
  ConstructLeptons();
  ConstructBaryons();
  //  ConstructMesons();


}




/*! \anchor constructparticlesmethods*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//              CONSTRUCTION METHODS FOR THE DIFFERENT PARTICLES          //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


void LEMuSRPhysicsList::ConstructBosons()
{
  // pseudo-particles
  G4Geantino::GeantinoDefinition();
  G4ChargedGeantino::ChargedGeantinoDefinition();

  // gamma
  G4Gamma::GammaDefinition();
}
 
void LEMuSRPhysicsList::ConstructLeptons()
{
  // leptons
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
  G4MuonPlus::MuonPlusDefinition();
  LEMuSRMuoniumParticle::MuoniumDefinition();
  G4MuonMinus::MuonMinusDefinition();

  G4NeutrinoE::NeutrinoEDefinition();
  G4AntiNeutrinoE::AntiNeutrinoEDefinition();
  G4NeutrinoMu::NeutrinoMuDefinition();
  G4AntiNeutrinoMu::AntiNeutrinoMuDefinition();
}

void LEMuSRPhysicsList::ConstructMesons()
{
 //  mesons
  G4PionPlus::PionPlusDefinition();
  G4PionMinus::PionMinusDefinition();
  G4PionZero::PionZeroDefinition();
  G4Eta::EtaDefinition();
  G4EtaPrime::EtaPrimeDefinition();
  G4KaonPlus::KaonPlusDefinition();
  G4KaonMinus::KaonMinusDefinition();
  G4KaonZero::KaonZeroDefinition();
  G4AntiKaonZero::AntiKaonZeroDefinition();
  G4KaonZeroLong::KaonZeroLongDefinition();
  G4KaonZeroShort::KaonZeroShortDefinition();
}

void LEMuSRPhysicsList::ConstructBaryons()
{
//  barions
  G4Proton::ProtonDefinition();
  G4AntiProton::AntiProtonDefinition();
  G4Neutron::NeutronDefinition();
  G4AntiNeutron::AntiNeutronDefinition();
}












//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//                              DECLARE PROCESSES                         //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4MultipleScattering.hh"
#include "G4MultipleScattering52.hh"

#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuIonisation.hh"
#include "G4MuIonisation52.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4hIonisation.hh"


// LEMuSR Processes
#include "LEMuSRAtRestSpinRotation.hh"
#include "LEMuSRDepolarize.hh"
#include "LEMuSRMSC.hh"
#include "LEMuSRMUONIUM.hh"
#include "LEMuSRMUONIUMScatt.hh"

/*! \anchor constructprocesses*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//                     CONSTRUCTION OF THE DIFFERENT PROCESSES            //
//                     CALLS THE DIFFERENT CONSTRUCTION METHODS           //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



void LEMuSRPhysicsList::ConstructProcess()
{
  /*! The transportation process is added to all particle. cf.
   * <a href="">G4VPhysicsList</a>    
   */
  AddTransportation();
  
  /*! Construction of Electromagnetic processes:
   *  those are the processes for the leptons and the bosons
   */
  ConstructEM();

  /*! Construction of the Decay process when applicable. 
   *
   * Construction of User Cuts and Step Limitation. */
  ConstructGeneral();
}




/*! \anchor constructprocessesmetods*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//                 THE DIFFERENT PROCESSES CONSTRUCTION METHODS           //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



void LEMuSRPhysicsList::ConstructEM()
{
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
     
    if (particleName == "gamma") {
    // gamma
      pmanager->AddDiscreteProcess(new G4GammaConversion());
      pmanager->AddDiscreteProcess(new G4ComptonScattering());      
      pmanager->AddDiscreteProcess(new G4PhotoElectricEffect());

    } else if (particleName == "e-") {
    //electron
      G4VProcess* theeminusMultipleScattering = new G4MultipleScattering();
      G4VProcess* theeminusIonisation         = new G4eIonisation();
      G4VProcess* theeminusBremsstrahlung     = new G4eBremsstrahlung();
      //
      // add processes
      pmanager->AddProcess(theeminusMultipleScattering);
      pmanager->AddProcess(theeminusIonisation);
      pmanager->AddProcess(theeminusBremsstrahlung);
      //      
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(theeminusMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(theeminusIonisation,         idxAlongStep,2);
      pmanager->SetProcessOrdering(theeminusBremsstrahlung,     idxAlongStep,3);      
      //
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(theeminusMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(theeminusIonisation,         idxPostStep,2);
      pmanager->SetProcessOrdering(theeminusBremsstrahlung,     idxPostStep,3);

    } else if (particleName == "e+") {
    //positron
      G4VProcess* theeplusMultipleScattering = new G4MultipleScattering();
      G4VProcess* theeplusIonisation         = new G4eIonisation();
      G4VProcess* theeplusBremsstrahlung     = new G4eBremsstrahlung();
      G4VProcess* theeplusAnnihilation       = new G4eplusAnnihilation();
      //
      // add processes
      pmanager->AddProcess(theeplusMultipleScattering);
      pmanager->AddProcess(theeplusIonisation);
      pmanager->AddProcess(theeplusBremsstrahlung);
      pmanager->AddProcess(theeplusAnnihilation);
      //
      // set ordering for AtRestDoIt
      pmanager->SetProcessOrderingToFirst(theeplusAnnihilation, idxAtRest);
      //
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(theeplusMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(theeplusIonisation,         idxAlongStep,2);
      pmanager->SetProcessOrdering(theeplusBremsstrahlung,     idxAlongStep,3);      
      //
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(theeplusMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(theeplusIonisation,         idxPostStep,2);
      pmanager->SetProcessOrdering(theeplusBremsstrahlung,     idxPostStep,3);
      pmanager->SetProcessOrdering(theeplusAnnihilation,       idxPostStep,4);
  
    }
    
    
     
    /*! \anchor muplusphysics*/
    //! Muon Plus Physics is implemented here.
    else if( particleName == "mu+" ) {
      
      // PROCESSES FOR MUON PLUS

      //! Multiple Scattering including Meyer'Algorithm: LEMuSRMSC
      G4VProcess* aMultipleScattering = new LEMuSRMSC();//new G4MultipleScattering52();

      //! Muonium formation: LEMuSRMUONIUM
      G4VProcess* aMuoniumFormation = new LEMuSRMUONIUM();

      //! Spin Precession Process At Rest: LEMuSRAtRestSpinRotation
      G4VProcess* aRestSpinRotation = new LEMuSRAtRestSpinRotation();



      //! Unchanged \gf processes
      G4VProcess* aBremsstrahlung     = new G4MuBremsstrahlung();
      G4VProcess* aPairProduction     = new G4MuPairProduction();
      G4VProcess* anIonisation        = new G4MuIonisation();
  


      // ADD WANTED PROCESSES TO PROCESS MANAGER

      // add lemu processes
      pmanager->AddProcess(aMultipleScattering);
      pmanager->AddProcess(aMuoniumFormation);
      pmanager->AddProcess(aRestSpinRotation);
     
      // add \gf processes
      pmanager->AddProcess(aBremsstrahlung);
      pmanager->AddProcess(anIonisation);
      pmanager->AddProcess(aPairProduction);
    

      // SET PROCESSES ORDERING (ALONG STEP. POST STEP. AT REST).
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(aMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(anIonisation,        idxAlongStep,3);
      pmanager->SetProcessOrdering(aBremsstrahlung,     idxAlongStep,4);
      pmanager->SetProcessOrdering(aPairProduction,     idxAlongStep,5);
      
      
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(aMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(aMuoniumFormation,   idxPostStep,2);
      pmanager->SetProcessOrdering(anIonisation, idxPostStep,3);
      pmanager->SetProcessOrdering(aBremsstrahlung,     idxPostStep,4);
      pmanager->SetProcessOrdering(aPairProduction,     idxPostStep,5);

      // set ordering for AtRestDoIt
      pmanager->SetProcessOrderingToFirst(aRestSpinRotation, idxAtRest);


     }   

    
    /*! \anchor muoniumphysics*/
    //! Muonium Physics is implemented here.
       else if( particleName == "Mu" ) {
	 
      // PROCESSES FOR MUONIUM

      //! Spin Precession Process At Rest: LEMuSRAtRestSpinRotation
      G4VProcess* aRestSpinRotation = new LEMuSRAtRestSpinRotation();
      G4VProcess* aMuScatt           = new LEMuSRMUONIUMScatt();

      // ADD WANTED PROCESSES TO PROCESS MANAGER

      // add lemu processes
      pmanager->AddProcess(aMuScatt);
      pmanager->AddProcess(aRestSpinRotation);
      
      
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(aMuScatt,            idxPostStep,1);

      // set ordering for AtRestDoIt
      pmanager->SetProcessOrderingToFirst(aRestSpinRotation, idxAtRest);
    
      
     }

    
    /*! \anchor muonminusphysics*/
    //! Muon Minus Physics is implemented here.
    else if( particleName == "mu-" ) {
      //muon minus
      G4VProcess* aMultipleScattering = new G4MultipleScattering52();
      G4VProcess* aBremsstrahlung     = new G4MuBremsstrahlung();
      G4VProcess* aPairProduction     = new G4MuPairProduction();
      G4VProcess* anIonisation        = new G4MuIonisation();
      //
      // add processes
      pmanager->AddProcess(anIonisation);
      pmanager->AddProcess(aMultipleScattering);
      pmanager->AddProcess(aBremsstrahlung);
      pmanager->AddProcess(aPairProduction);
      //
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(aMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(anIonisation,        idxAlongStep,2);
      pmanager->SetProcessOrdering(aBremsstrahlung,     idxAlongStep,3);
      pmanager->SetProcessOrdering(aPairProduction,     idxAlongStep,4);
      
      //
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(aMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(anIonisation,        idxPostStep,2);
      pmanager->SetProcessOrdering(aBremsstrahlung,     idxPostStep,3);
      pmanager->SetProcessOrdering(aPairProduction,     idxPostStep,4);




     } else if ((!particle->IsShortLived()) &&
	       (particle->GetPDGCharge() != 0.0) && 
	       (particle->GetParticleName() != "chargedgeantino")) {
     // all others charged particles except geantino     
     G4VProcess* aMultipleScattering = new G4MultipleScattering();
     G4VProcess* anIonisation        = new G4hIonisation();
     //
     // add processes
     pmanager->AddProcess(anIonisation);
     pmanager->AddProcess(aMultipleScattering);
     //
     // set ordering for AlongStepDoIt
     pmanager->SetProcessOrdering(aMultipleScattering, idxAlongStep,1);
     pmanager->SetProcessOrdering(anIonisation,        idxAlongStep,2);
     //
     // set ordering for PostStepDoIt
     pmanager->SetProcessOrdering(aMultipleScattering, idxPostStep,1);
     pmanager->SetProcessOrdering(anIonisation,        idxPostStep,2);
    }
  }
}





// LEMuSR decay process
#include "LEMuSRMuonDecayChannel.hh"
#include "LEMuSRDecay.hh"
// Geant decay processes
#include "G4MuonDecayChannel.hh"
#include "G4MuonDecayChannelWithSpin.hh"
#include "G4DecayWithSpin.hh"
#include "G4Decay.hh"
#include "G4UserSpecialCuts.hh"
#include "G4DecayTable.hh"
// Step Limiter Process
#include "G4StepLimiter.hh"

/*! \anchor constructdecayprocess*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//                                                                        //
//                 CONSTRUCTION METHOD FOR DECAY PROCESS                  //
//                                                                        //
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

//! This method constructs the decay process for all the particles.
void LEMuSRPhysicsList::ConstructGeneral()
{
  /*! Declare Decay Processes (from \lemu and \gf).*/
  //   G4Decay* theDecayProcess = new G4Decay();
   LEMuSRDecay* theLEMuSRDecayProcess = new LEMuSRDecay();

   /*! Declare  User Cut.*/ 
   G4StepLimiter* aStepLimiter = new G4StepLimiter();
   G4UserSpecialCuts* aUserCut = new G4UserSpecialCuts();
   /*! Initialization of the particle iterator.*/
  theParticleIterator->reset();


  while( (*theParticleIterator)() )
    {
      G4ParticleDefinition* particle = theParticleIterator->value();
      
      /*! If the particle is a muon plus or a muonium, 
       * add the decay process from \lemu.
      */
      if (particle->GetParticleName()=="mu+")
	{
	  /*! Initialize the decay table.*/
	  /*!
	   * Although the asymmetric decay process is a feature of 
	   *new Geant4 versions, 
	   * it is recommended to use the original \lemu implementation. 
	   */
	  G4DecayTable* MuonPlusDecayTable = new G4DecayTable();
	  MuonPlusDecayTable -> Insert(new LEMuSRMuonDecayChannel("mu+",1.00));
	  MuonPlusDecayTable ->DumpInfo();
	  G4MuonPlus::MuonPlusDefinition() -> SetDecayTable(MuonPlusDecayTable); 
	  
	  G4ProcessManager* pmanager = particle->GetProcessManager();
	  if (theLEMuSRDecayProcess->IsApplicable(*particle)) 
	    { 
	      pmanager ->AddProcess(theLEMuSRDecayProcess);
	      pmanager ->AddProcess(aStepLimiter);

	      // set ordering 
	      pmanager ->SetProcessOrderingToLast(aStepLimiter, idxPostStep);
	      pmanager ->SetProcessOrderingToLast(theLEMuSRDecayProcess, idxPostStep);
	      pmanager ->SetProcessOrderingToLast(theLEMuSRDecayProcess, idxAtRest);
	    }
	      pmanager->DumpInfo();
	}
      else if (particle->GetParticleName()=="Mu")
	{
	  /*! Initialize the decay table.*/
	  
	  /*!
	    Half of the muonium have a completely isotropic decay. 
	  */
	  G4DecayTable* MuoniumDecayTable = new G4DecayTable();
	  MuoniumDecayTable -> Insert(new LEMuSRMuonDecayChannel("Mu",0.5));
	  MuoniumDecayTable -> Insert(new G4MuonDecayChannel("Mu",0.5)); 
	  MuoniumDecayTable ->DumpInfo();
	  LEMuSRMuoniumParticle::MuoniumDefinition() -> SetDecayTable(MuoniumDecayTable);
	  
	  G4ProcessManager* pmanager = particle->GetProcessManager();
	  if (theLEMuSRDecayProcess->IsApplicable(*particle)) 
	    { 
	      pmanager ->AddProcess(theLEMuSRDecayProcess);
	      pmanager ->AddProcess(aStepLimiter);
	      
	      // set ordering 
	      //	      pmanager ->SetProcessOrdering(aStepLimiter, idxPostStep, 1);  // not needed for the muonium
	      pmanager ->SetProcessOrderingToLast(theLEMuSRDecayProcess, idxPostStep);
	      pmanager ->SetProcessOrderingToLast(theLEMuSRDecayProcess, idxAtRest);
	    }

	      pmanager->DumpInfo();


	}
      
      



      /*! For all other particles, 
       * add the decay process from \lemu also, it checks if particle is muon or not.
      */
      else
	{
	  G4ProcessManager* pmanager = particle->GetProcessManager();
	      pmanager ->AddProcess(aUserCut);
	      pmanager ->SetProcessOrdering(aUserCut, idxPostStep);
	  if (theLEMuSRDecayProcess->IsApplicable(*particle)) 
	    { 
	      pmanager ->AddProcess(theLEMuSRDecayProcess);
	      // set ordering for PostStepDoIt and AtRestDoIt
	      pmanager ->SetProcessOrdering(theLEMuSRDecayProcess, idxPostStep);
	      pmanager ->SetProcessOrdering(theLEMuSRDecayProcess, idxAtRest);
	    }
	}
      
    }

}

#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"

void LEMuSRPhysicsList::SetCuts()
{
  if (verboseLevel >0){
    G4cout << "LEMuSRPhysicsList::SetCuts: default cut length : "
         << G4BestUnit(defaultCutValue,"Length") << G4endl;
  }  

  // These values are used as the default production thresholds
  // for the world volume.
  SetCutsWithDefault();
    SetCutValue(1.*mm, "e+"); 
    SetCutValue(1.*mm, "e-"); 
  //  SetCutValue(1*mm, "proton"); 


}


