//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRParticleChangeForSR.cc , v 1.2b
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-08-20 10:36
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                         PARTICLE CHANGE FOR SPIN ROTATION   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "LEMuSRParticleChangeForSR.hh"



G4Step* LEMuSRParticleChangeForSR::UpdateStepForAtRest(G4Step* pStep)
{
  G4StepPoint* pPreStepPoint  = pStep->GetPreStepPoint();
  G4StepPoint* pPostStepPoint = pStep->GetPostStepPoint();
  //G4Track*     aTrack  = pStep->GetTrack();

  // update polarization

  pPostStepPoint->SetMomentumDirection(theMomentumDirectionChange);
  pPostStepPoint->SetKineticEnergy(theEnergyChange);
  pPostStepPoint->AddPolarization( thePolarizationChange
  				   - pPreStepPoint->GetPolarization());
  
  // update position and time
  pPostStepPoint->AddPosition( thePositionChange
			       - pPreStepPoint->GetPosition() );
 

 pStep->SetControlFlag( theSteppingControlFlag );
  
  // Update track
  pStep->UpdateTrack();

    
#ifdef G4SRVERBOSE
 G4cout<<"STEP UPDATED  " 
	  <<  aTrack->GetDynamicParticle()->GetPreAssignedDecayProperTime() 
	  <<"  " 
	  << aTrack->GetDynamicParticle()->GetProperTime() 
	<<G4endl;
 CheckIt(*aTrack);

#endif


  return pStep;
}

