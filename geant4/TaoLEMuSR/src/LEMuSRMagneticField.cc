//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRMagneticField.cc , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               Magnetic Field   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "LEMuSRMagneticField.hh"
#include "G4ios.hh"
#include <iomanip>


LEMuSRMagneticField::LEMuSRMagneticField(const G4ThreeVector FieldVector)
{
  BField=FieldVector;
  //   G4cout<<"MAGNETIC FIELD DEFINED AS "<<BField.x()/gauss<<"gauss"<<BField.y()/gauss<< "gauss"<<BField.z()/gauss<< "gauss"<<G4endl;
      //      getchar();
}


LEMuSRMagneticField::~LEMuSRMagneticField()
{;}


void LEMuSRMagneticField::GetFieldValue (const G4double pos[4],
                                            G4double *field ) const 
{

  field[0]= 0.0*gauss;
  field[1]= 0.0*gauss;
  field[2]= 0.0*gauss;
  
    G4double  X,Y,Z,factor;

  X= pos[0]*mm;Y=pos[1]*mm;Z=pos[2]*mm;
  
  //    G4cout<<pos[0]<<" "<<pos[1]<<" "<<pos[2]<<G4endl;
    /*  if(Z<20*cm&&Z>-20*cm)
	{ //G4cout<<"true!";*/
      factor=exp((-Z*Z)/(10*cm*10*cm));
      field[0]= BField.x()*factor*0 ;//TAO
      field[1]= BField.y()*factor*0 ;
      field[2]= BField.z()*factor*0 ;
      
      //      G4cout<<"true: "<<field[0]/gauss<<" gauss, "<<field[1]/gauss<< " gauss, "<<field[2]/gauss<< " gauss."<<G4endl;// getchar();
      //      getchar();
      /* 
    }
    */
}
