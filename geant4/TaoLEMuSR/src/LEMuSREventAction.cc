//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSREventAction.cc , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-07 11:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           EVENT ACTION.CC
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//

#include "LEMuSREventAction.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4RunManager.hh"
#include "LEMuSRPrimaryGeneratorAction.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"
#include "G4DynamicParticle.hh"
#include "G4DecayTable.hh"
#include "G4ParticleDefinition.hh"

#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "LEMuSRScintHit.hh"
#include "LEMuSROScintHit.hh"
#include "LEMuSRCryoHit.hh"
#include "LEMuSRMcpHit.hh"

#include "LEMuSRDetectorConstruction.hh"

#include "TROOT.h"
#include "TApplication.h"
#include "TSystem.h"
#include "TH1.h"
#include "TPad.h"
#include "TCanvas.h"


// sensitive detectors
#include "LEMuSRCryoSD.hh"

LEMuSREventAction::LEMuSREventAction()
 :drawFlag("all")
{
  iScintCollID = -1;
  oScintCollID = -1;
  McpCollID = -1;
  CryoCollID = -1;


  G4cout<<"EVENT ACTION INITIALIZED";

}



LEMuSREventAction::~LEMuSREventAction()
{

}

void LEMuSREventAction::BeginOfEventAction(const G4Event*)
{

  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  
  if(iScintCollID<0)
  {
   iScintCollID = SDman->GetCollectionID("InnerScintCollection");
   
  }

 if(oScintCollID<0)
  {
    oScintCollID = SDman->GetCollectionID("OuterScintCollection");
   
  }

  // check detectors mode (mcp or cryo => k)
  LEMuSRDetectorConstruction* lemuDet = LEMuSRDetectorConstruction::GetInstance();
  G4double k = lemuDet->GetParamMC();
  if(k==1){
 if(McpCollID<0)
  {
    McpCollID = SDman->GetCollectionID("McpCollection");
  }
  }
  else{
 if(CryoCollID<0)
  {
    CryoCollID = SDman->GetCollectionID("CryoCollection");
  }
  }


}



void LEMuSREventAction::EndOfEventAction(const G4Event* evt)
{
  // G4cout << ">>> Event " << evt->GetEventID() << G4endl;
  

  // extract the trajectories and draw them

  if (G4VVisManager::GetConcreteInstance())
    {
     G4TrajectoryContainer * trajectoryContainer = evt->GetTrajectoryContainer();
     G4int n_trajectories = 0;
     if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();

     for (G4int i=0; i<n_trajectories; i++)
        { 
	  G4Trajectory* trj = (G4Trajectory*)((*(evt->GetTrajectoryContainer()))[i]);
          if (drawFlag == "all") trj->DrawTrajectory(500);
          else if ((drawFlag == "charged")&&(trj->GetCharge() != 0.))
                                  trj->DrawTrajectory(50);
          else if ((drawFlag == "neutral")&&(trj->GetCharge() == 0.))
                                  trj->DrawTrajectory(50);				
        }
    }
  
  //  SDCollManagement(evt);

}


void LEMuSREventAction::SDCollManagement(const G4Event* evt)
{

  // informations about hits and draw them
  int nos_hit, nis_hit, nm_hit, nc_hit;


  
  if(iScintCollID<0||oScintCollID<0)
    {  
      G4cout<<"IDERROR";
      return;
    }
  G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
  LEMuSRScintHitsCollection* iScintHC = 0;
  LEMuSROScintHitsCollection* oScintHC = 0;
  LEMuSRMcpHitsCollection* McpHC = 0;
  LEMuSRCryoHitsCollection* CryoHC = 0;


  // check detectors mode (mcp or cryo => k)
  LEMuSRDetectorConstruction* lemuDet = LEMuSRDetectorConstruction::GetInstance();
  G4double k = lemuDet->GetParamMC();

 if(HCE)
  {
    iScintHC = (LEMuSRScintHitsCollection*)(HCE->GetHC(iScintCollID));
    oScintHC = (LEMuSROScintHitsCollection*)(HCE->GetHC(oScintCollID));
    if(k==0) {CryoHC = (LEMuSRCryoHitsCollection*)(HCE->GetHC(CryoCollID));}
    else{ McpHC = (LEMuSRMcpHitsCollection*)(HCE->GetHC(McpCollID));}
  }

 
  if(iScintHC)
  {
    nis_hit = iScintHC->entries();
    //  G4cout << "     " << nis_hit
    //   << " hits are stored in LEMuSRHitsCollection for SCIS." << G4endl;
  }

    if(oScintHC)
  {
      nos_hit = oScintHC->entries();
      //    G4cout << "     " << nos_hit
      //   << " hits are stored in LEMuSRHitsCollection for SCOS." << G4endl;

  }

 

  
      if(k==0)
      {
	if (CryoCollID <0) return ;
	
	if(CryoHC)
	  {
	    nc_hit = CryoHC->entries();
	    //	    G4cout << "     " << nc_hit
	    //		   << " hits are stored in LEMuSRHitsCollection for CRYO." << G4endl;
	    
	    

	    for(int i=0;i<nc_hit;i++)
	      {
		aHit = (*CryoHC)[i];
		
		en_dep = aHit->GetEnergyDeposition()/keV;

	      }
	  }
      }   
      
      else if(k==1)
	{
	  if(McpCollID <0) return ;
	  if(McpHC)
	    {
	      nm_hit = McpHC->entries();
	      //	    G4cout << "     " << nm_hit
	      //		   << " hits are stored in LEMuSRHitsCollection for MCP." << G4endl;
	      
	    }
	} 
      
  

}
