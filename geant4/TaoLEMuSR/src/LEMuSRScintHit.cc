//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : LEMuSRScintHit.cc , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-07-12 16:15
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                           INNER SCINT HIT
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "LEMuSRScintHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"
#include <fstream>
#include <iomanip>

#include "G4UnitsTable.hh"

G4Allocator<LEMuSRScintHit> LEMuSRScintHitAllocator;

LEMuSRScintHit::LEMuSRScintHit()
{;}

LEMuSRScintHit::~LEMuSRScintHit()
{;}

LEMuSRScintHit::LEMuSRScintHit(const LEMuSRScintHit &right) : G4VHit()
{
  particle_name = right.particle_name;
  energy_deposition = right.energy_deposition;
  time_of_flight = right.time_of_flight;
  position = right.position;
  momentum = right.momentum;
}

const LEMuSRScintHit& LEMuSRScintHit::operator=(const LEMuSRScintHit &right) 
{
  particle_name = right.particle_name;
  energy_deposition = right.energy_deposition;
  time_of_flight = right.time_of_flight;
  position = right.position;
  momentum = right.momentum;
  return *this;
}



G4int LEMuSRScintHit::operator==(const LEMuSRScintHit &right) const
{
  return (this==&right) ? 1 : 0;
}

void LEMuSRScintHit::Draw()
{
  G4VVisManager* VisManager = G4VVisManager::GetConcreteInstance();
  if(VisManager)
    {
      G4Circle circle(position);
      circle.SetScreenSize(0.1);
      circle.SetFillStyle(G4Circle::filled);
      G4Colour colour(1.,1.,1.);
      G4VisAttributes attributes(colour);
      circle.SetVisAttributes(attributes);
      VisManager->Draw(circle);
    }
}

void LEMuSRScintHit::Print()
{}

void LEMuSRScintHit::print(G4String name)
{
 using namespace std;

   ofstream TestPrint(name,ios::app);
   if (!TestPrint.is_open()) exit(8);
   TestPrint  << "particle_name     : " << particle_name <<" ;\n"
	      << "energy_deposition : " <<  G4BestUnit(energy_deposition,"Energy") <<" ;\n " 
	      << "time_of_flight    : " <<  G4BestUnit(time_of_flight,"Time")    <<" ;\n " 
	      << "position          : " << position          <<" ;\n " 
	      << "momentum          : " << momentum          <<" ;\n " 
	      <<G4endl;

}
