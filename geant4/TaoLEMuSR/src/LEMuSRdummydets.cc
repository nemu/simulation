//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//* 
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRDetectorConstruction.cc , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-06-24 16:33
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//   
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               DETECTOR CONSTRUCTION: DUMMY DETECTORS
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

     
// G4 GEOMETRIC FORMS CLASSES

#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Trap.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnitsTable.hh"

// G4 VOLUME DEFINITION CLASSES
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVReplica.hh"
#include "G4PVPlacement.hh"
#include "G4GeometryManager.hh"
#include "G4FieldManager.hh"
#include "G4PropagatorInField.hh"
#include "LEMuSRMag_SpinEqRhs.hh"
#include "G4Mag_SpinEqRhs.hh"
#include "G4ChordFinder.hh"
#include "G4El_UsualEqRhs.hh"
#include "G4ClassicalRK4.hh"
#include "G4SimpleHeum.hh"
#include "G4SimpleRunge.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4TransportationManager.hh"
#include "G4GeometryManager.hh"
#include "G4UnitsTable.hh"
//#include "G4RotationMatrix.hh"

// G4 CLASSES
#include "G4ios.hh"
#include <iomanip>


// HEADER
#include "LEMuSRDetectorConstruction.hh"
#include "LEMuSRDetectorMessenger.hh"


// process remove
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleChange.hh"
#include "G4ProcessVector.hh"
#include "G4ProcessManager.hh"
#include "G4VProcess.hh"

// electric fieldmap
#include "LEMuSRElectricField.hh"
#include "LEMuSRElFieldMix.hh"
#include "G4ElectricField.hh"
#include "G4ElectroMagneticField.hh"
#include "G4EqMagElectricField.hh"

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



///$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//                            DEFINE THE ASYMETRY DUMMY DETECTORS
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

void LEMuSRDetectorConstruction::lemuAsym()
{


  // solids
  Asym_tube  = new G4Tubs("sAsym",0.*cm,1*cm,0.0000001*cm, dSPhi, dEPhi);

  G4Sphere *Asym_sphl = new G4Sphere("sphAsymL",25*cm,25.000001*cm,0.*deg,360*deg,90*deg,90*deg);
  G4Sphere *Asym_sphr = new G4Sphere("sphAsymR",25*cm,25.000001*cm,0.*deg,360*deg,0*deg,90*deg);

  // logical volumes
  G4String mat = "vacuum";

  lv_Asym = new G4LogicalVolume(Asym_tube,G4Material::GetMaterial(mat),"lv_Asym",0,0,0);

  lv_AsymL = new G4LogicalVolume(Asym_sphl,G4Material::GetMaterial(mat),"lv_AsymL",0,0,0);
  lv_AsymR = new G4LogicalVolume(Asym_sphr,G4Material::GetMaterial(mat),"lv_AsymR",0,0,0); 
  
  //  G4Translate3D trl =G4ThreeVector(+50.*cm,0.*cm,0.*cm); //uncomment
  Asym_rotation = G4RotateY3D(90*deg);

  //  G4Transform3D asymt3d= trl;//*Asym_rotation;//uncomment
  // pv_Asym = new G4PVPlacement( asymt3d,lv_Asym,"pv_Asym1",lv_LABO, false, 0 );//uncomment

  //  G4Translate3D trl2 =G4ThreeVector(-50.*cm,0.*cm,0.*cm);//uncomment

  // G4Transform3D asymt3d2= trl2*Asym_rotation;//uncomment
  //pv_Asym2 = new G4PVPlacement( asymt3d2,lv_Asym,"pv_Asym2",lv_LABO, false, 0 );//uncomment
  
  pv_AsymL = new G4PVPlacement(Asym_rotation ,lv_AsymL,"pv_AsymL",lv_LABO, false, 0 );
  pv_AsymR = new G4PVPlacement(Asym_rotation ,lv_AsymR,"pv_AsymR",lv_LABO, false, 0 );

  //visual attibutes
  // lv_Asym->SetVisAttributes(VTBB_style);
 
   lv_AsymL->SetVisAttributes(Green_style);
   lv_AsymR->SetVisAttributes(dRed_style);


} 


//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//                            DEFINE THE ELECTRIC FIELD DUMMY DETECTORS
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
void LEMuSRDetectorConstruction::lemuFieldCheck()
{
 
  // solids
  fchk_tube = new G4Tubs("tube_fchk", 0., 6*cm, 0.001*mm, dSPhi, dEPhi);

  lv_fchk   = new G4LogicalVolume(fchk_tube,G4Material::GetMaterial("vacuum"),"lv_fchk",0 ,0 ,0 );
  lv_fchk2   = new G4LogicalVolume(fchk_tube,G4Material::GetMaterial("vacuum"),"lv_fchk",0 ,0 ,0 );
  lv_fchk3   = new G4LogicalVolume(fchk_tube,G4Material::GetMaterial("vacuum"),"lv_fchk",0 ,0 ,0 );


   pv_fchk  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,0*cm),lv_fchk,"pv_f0",lv_L3VA, false, 0 );
   pv_fchk1  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-22*cm),lv_fchk,"pv_f1",lv_L3VA, false, 0 );
   /*   pv_fchk2  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -21*cm),lv_fchk,"pv_f2",lv_L3VA, false, 0 );
   pv_fchk3  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -20*cm),lv_fchk,"pv_f3",lv_L3VA, false, 0 );
   pv_fchk4  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -19*cm),lv_fchk,"pv_f4",lv_L3VA, false, 0 );
   pv_fchk5  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -18*cm),lv_fchk,"pv_f5",lv_L3VA, false, 0 );
   pv_fchk6  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -17*cm),lv_fchk,"pv_f6",lv_L3VA, false, 0 );
   pv_fchk7  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -16*cm),lv_fchk,"pv_f7",lv_L3VA, false, 0 );
   pv_fchk8  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -15*cm),lv_fchk,"pv_f8",lv_L3VA, false, 0 );
   pv_fchk9  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -14*cm),lv_fchk,"pv_f9",lv_L3VA, false, 0 );
   pv_fchk10  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, -13*cm),lv_fchk,"pv_f10",lv_L3VA, false, 0 );
   pv_fchk11  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, -12*cm),lv_fchk,"pv_f11",lv_L3VA, false, 0 );
   pv_fchk12  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, -11*cm),lv_fchk,"pv_f12",lv_L3VA, false, 0 );
   pv_fchk13  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, -10*cm),lv_fchk,"pv_f13",lv_L3VA, false, 0 );
   pv_fchk14  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -9*cm),lv_fchk,"pv_f14",lv_L3VA, false, 0 );
   pv_fchk15  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -8*cm),lv_fchk,"pv_f15",lv_L3VA, false, 0 );
   pv_fchk16  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -7*cm),lv_fchk,"pv_f16",lv_L3VA, false, 0 );
   pv_fchk17  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -6*cm),lv_fchk,"pv_f17",lv_L3VA, false, 0 );
   pv_fchk18  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -5*cm),lv_fchk,"pv_f18",lv_L3VA, false, 0 );
   pv_fchk19  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -4*cm),lv_fchk,"pv_f19",lv_L3VA, false, 0 );
   pv_fchk20  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -3*cm),lv_fchk,"pv_f20",lv_L3VA, false, 0 );
   pv_fchk21  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -2*cm),lv_fchk,"pv_f21",lv_L3VA, false, 0 );
   pv_fchk22  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  -1*cm),lv_fchk,"pv_f22",lv_L3VA, false, 0 );
   pv_fchk23  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +1*cm),lv_fchk,"pv_f23",lv_L3VA, false, 0 );
   pv_fchk24  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +2*cm),lv_fchk,"pv_f24",lv_L3VA, false, 0 );
   pv_fchk25  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +3*cm),lv_fchk,"pv_f25",lv_L3VA, false, 0 );
   pv_fchk26  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +4*cm),lv_fchk,"pv_f26",lv_L3VA, false, 0 );
   pv_fchk27  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +5*cm),lv_fchk,"pv_f27",lv_L3VA, false, 0 );
   pv_fchk28  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +6*cm),lv_fchk,"pv_f28",lv_L3VA, false, 0 );
   pv_fchk29  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +7*cm),lv_fchk,"pv_f29",lv_L3VA, false, 0 );
   pv_fchk30  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +8*cm),lv_fchk,"pv_f30",lv_L3VA, false, 0 );
   pv_fchk31  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,  +9*cm),lv_fchk,"pv_f31",lv_L3VA, false, 0 );
   pv_fchk32  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +10*cm),lv_fchk,"pv_f32",lv_L3VA, false, 0 );
   pv_fchk33  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +11*cm),lv_fchk,"pv_f33",lv_L3VA, false, 0 );
   pv_fchk34  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +12*cm),lv_fchk,"pv_f34",lv_L3VA, false, 0 );
   pv_fchk35  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +13*cm),lv_fchk,"pv_f35",lv_L3VA, false, 0 );
   pv_fchk36  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +14*cm),lv_fchk,"pv_f36",lv_L3VA, false, 0 );
   pv_fchk37  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +15*cm),lv_fchk,"pv_f37",lv_L3VA, false, 0 );
   pv_fchk38  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +16*cm),lv_fchk,"pv_f38",lv_L3VA, false, 0 );
   pv_fchk39  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +17*cm),lv_fchk,"pv_f39",lv_L3VA, false, 0 );
   pv_fchk40  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +18*cm),lv_fchk,"pv_f40",lv_L3VA, false, 0 );
   pv_fchk41  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +19*cm),lv_fchk,"pv_f41",lv_L3VA, false, 0 );
   pv_fchk42  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +20*cm),lv_fchk,"pv_f42",lv_L3VA, false, 0 );
   pv_fchk43  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +21*cm),lv_fchk,"pv_f43",lv_L3VA, false, 0 ); */
   pv_fchk44  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, +21.5*cm),lv_fchk,"pv_f44",lv_L3VA, false, 0 );

#ifndef LEMU_TEST_FOCAL_LENGTH 
   // use another lv_fchk because belong to another mother volume
   pv_fchk45  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(-8-25.45)*cm),lv_fchk2,"pv_f45",lv_RAV, false, 0 );
   pv_fchk46  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(-7-25.45)*cm),lv_fchk2,"pv_f46",lv_RAV, false, 0 );
   pv_fchk47  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(-5-25.45)*cm),lv_fchk2,"pv_f47",lv_RAV, false, 0 );
   pv_fchk48  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(-2.5-25.45)*cm),lv_fchk2,"pv_f48",lv_RAV, false, 0 );
   pv_fchk49  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(0-25.45)*cm),lv_fchk2,"pv_f49",lv_RAV, false, 0 );
   pv_fchk50  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(2.5-25.45)*cm),lv_fchk2,"pv_f50",lv_RAV, false, 0 );
   pv_fchk51  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(5-25.45)*cm),lv_fchk2,"pv_f51",lv_RAV, false, 0 );
   pv_fchk52  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(7-25.45)*cm),lv_fchk2,"pv_f52",lv_RAV, false, 0 );
   pv_fchk53  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,(9-25.45)*cm),lv_fchk2,"pv_f53",lv_RAV, false, 0 );



   
   // use another lv_fchk because belong to another mother volume
     pv_fchk54  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-16*cm),lv_fchk3,"pv_f54",lv_MCPV, false, 0 );
   pv_fchk55  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-15*cm),lv_fchk3,"pv_f55",lv_MCPV, false, 0 );
   pv_fchk56  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-14*cm),lv_fchk3,"pv_f56",lv_MCPV, false, 0 );
   pv_fchk57  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-13*cm),lv_fchk3,"pv_f57",lv_MCPV, false, 0 );
   pv_fchk58  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-12*cm),lv_fchk3,"pv_f58",lv_MCPV, false, 0 );
   pv_fchk59  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-11*cm),lv_fchk3,"pv_f59",lv_MCPV, false, 0 );
   pv_fchk60  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-10*cm),lv_fchk3,"pv_f60",lv_MCPV, false, 0 );
   pv_fchk61  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-9*cm),lv_fchk3,"pv_f61",lv_MCPV, false, 0 );
   pv_fchk62  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm, -8*cm),lv_fchk3,"pv_f62",lv_MCPV, false, 0 );
   
   pv_fchk64  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-7*cm),lv_fchk3,"pv_f64",lv_MCPV, false, 0 );
   pv_fchk65  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-6*cm),lv_fchk3,"pv_f65",lv_MCPV, false, 0 );
   pv_fchk66  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-5*cm),lv_fchk3,"pv_f66",lv_MCPV, false, 0 );
   pv_fchk67  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-4*cm),lv_fchk3,"pv_f67",lv_MCPV, false, 0 );
   pv_fchk68  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-3*cm),lv_fchk3,"pv_f68",lv_MCPV, false, 0 );
   pv_fchk69  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,-2*cm),lv_fchk3,"pv_f69",lv_MCPV, false, 0 );
   pv_fchk70  = new G4PVPlacement( 0,G4ThreeVector(0.*cm,0.*cm,+1.75*cm),lv_fchk3,"pv_f70",lv_MCPV, false, 0 );
   
#endif


}
