//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRElMagField.cc , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-09-17 10:20
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               ElectroMagnetic Field   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
#include"LEMuSRElMagField.hh"
#include"G4UnitsTable.hh"
#include "G4ios.hh"
#include <iomanip>

LEMuSRElMagField::LEMuSRElMagField( G4ElectricField *E,  G4MagneticField *B,  G4double field1_val, G4double field2_val)
{
  coef1  = field1_val;
  coef2  = field2_val;
  field1 = E;
  field2 = B;
  G4cout<<"ELMAGFIELD COEFS "<< coef1 <<"AND "<< coef2<<G4endl;
  G4cout<<"nb: coefs disabled in code"<<G4endl;
} 


LEMuSRElMagField::~LEMuSRElMagField()
{
  ;
}



void LEMuSRElMagField::GetFieldValue(const G4double point[4], G4double *EMfield ) const
{
  G4double Efield[3], Bfield[3];

  G4double position[4],position2[4];
  position[0]=point[0];
  position[1]=point[1];
  position[2]=point[2];
  position[3]=point[3];
  position2[0]=point[0];
  position2[1]=point[1];
  position2[2]=point[2];
  position2[3]=point[3];
  ///  G4cout<<"Field zb" <<field1->minz <<std::endl;
  //  G4cout<<"Field zb" <<field2->maxz <<std::endl;

  field1->GetFieldValue(position, Efield);
  
  field2->GetFieldValue(position2, Bfield);
  /*
  G4cout<<"Field E Value " << G4BestUnit(Efield[0]*coef1*meter,"Electric potential") <<"/m " <<Efield[1]*coef1/volt*meter <<" V/m "<< Efield[2]*coef1/volt*meter <<" V/m " <<G4endl;
  G4cout<<"Field B Value " << Bfield[0]/gauss <<"gauss " <<Bfield[1]/gauss <<"gauss "<< Bfield[2]/gauss <<"gauss " <<G4endl;
  */

  EMfield[0] = Bfield[0];//* coef1
  EMfield[1] = Bfield[1];
  EMfield[2] = Bfield[2];
  EMfield[3] = Efield[0];// *coef2
  EMfield[4] = Efield[1];
  EMfield[5] = Efield[2];
  
  /*    G4cout<<"Field EM Field Value " << EMfield[0]/gauss <<"gauss " <<EMfield[1]/gauss <<"gauss "<< EMfield[2]/gauss <<"gauss \n" 
	<< EMfield[3]/volt*meter <<"V/m " <<EMfield[3]/volt*meter <<"V/m "<< EMfield[5]/volt*meter <<"V/m " <<G4endl;
  */
}


