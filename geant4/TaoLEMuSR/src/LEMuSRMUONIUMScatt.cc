//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRMUONIUMScatt.cc , v 1.3
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 16:15
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               MUONIUMScatt   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#include "LEMuSRMUONIUMScatt.hh"
#include "G4StepStatus.hh"
#include "G4Navigator.hh"
#include "G4TransportationManager.hh"
#include "Randomize.hh"
#include "G4ProductionCutsTable.hh"
#include "G4FieldManager.hh"
#include "G4Field.hh"



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    
using namespace std;

LEMuSRMUONIUMScatt::  LEMuSRMUONIUMScatt(const G4String& name ,
			       G4ProcessType   aType )
 : G4VDiscreteProcess(name, aType)
{}


LEMuSRMUONIUMScatt:: ~LEMuSRMUONIUMScatt()
{}


/*!
 * At the end of the step, the current volume is checked and if the muonium is in a solid material (except the carbon foil where it is generated) it is stopped immediately.
 *
 * The RotateSpinIfMag method is also called here, in order to perform the spin precession of a muonium flying in a magnetic field. Indeed the momentum and spin propagation of neutral particles is not taken into account in \gf. 
*/
G4VParticleChange* LEMuSRMUONIUMScatt::PostStepDoIt(
                                               const G4Track& trackData,
                                               const G4Step& aStep )
{

  fParticleChange.Initialize(trackData);
  
  // tao :: /*! *- Get Time information */
  itime = trackData.GetProperTime();
  gtime = trackData.GetGlobalTime();
  ftime = trackData.GetDynamicParticle()->GetPreAssignedDecayProperTime(); 
  
  deltatime = ftime - itime;
  fParticleChange.ProposeGlobalTime(deltatime + itime -gtime);
  
  /*! - Set position, momentum, energy and time of the particle change. */
  fParticleChange.ProposePosition(trackData.GetPosition());
  fParticleChange.ProposeMomentumDirection(trackData.GetMomentumDirection());
  fParticleChange.ProposeEnergy(trackData.GetKineticEnergy());
  fParticleChange.ProposeGlobalTime(gtime);
  fParticleChange.ProposeProperTime(itime);
  fParticleChange.ProposeTrackStatus(trackData.GetTrackStatus()) ;
  
  /*! * - Verify the condition of applying the process: if the Mu is in a material different than vacuum and carbon foil, then stop it directly.*/
  if( CheckCondition(aStep))
    {
         fParticleChange.ProposePosition(trackData.GetStep()->GetPreStepPoint()->GetPosition());
      fParticleChange.ProposeTrackStatus(fStopButAlive) ;
      
    }   
  else
    {;}

  /*! *-  If the Mu is in flight, then rotate the spin if there is a magnetic field.*/
  polar = RotateSpinIfMag(trackData, aStep);
  fParticleChange.ProposePolarization(polar);


  //   fParticleChange.DumpInfo();
  /*! * - Return the particle change object. */
  return &fParticleChange;



}




/*!
 * The muonium will be stopped as soon as it enters a material different as the carbon foil and vacuum.
*/
G4bool LEMuSRMUONIUMScatt::CheckCondition( const G4Step& aStep)
{
  G4bool condition=false;
  p_name = aStep.GetTrack()->GetDefinition()->GetParticleName(); // particle name  
  if(p_name == "Mu"&&aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName()!="lv_CFOIL"&&aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetMaterial()->GetName()!="vacuum")
    {
      condition=true;
    }
  
  return condition;
 
}  

 

G4double LEMuSRMUONIUMScatt:: GetMeanFreePath(const G4Track& ,
			   G4double  ,
			   G4ForceCondition* condition
                                                               )
{

  *condition = Forced;
  return DBL_MAX;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*!
 * Not used.
*/
void LEMuSRMUONIUMScatt::GetDatas( const G4Step* )
{;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



void LEMuSRMUONIUMScatt::PrepareSecondary(const G4Track& track)
{
  aSecondary = new G4Track(DP,track.GetDynamicParticle()->GetPreAssignedDecayProperTime(),track.GetPosition());


;}

/*!
 * If a magnetic field is present in the volume the muonium is going through the spin precession is performed.
*/
G4ThreeVector LEMuSRMUONIUMScatt::RotateSpinIfMag( const G4Track& theTrack, const G4Step& aStep)
{
  G4ThreeVector theSpin = theTrack.GetPolarization() ;

  if(theTrack.GetKineticEnergy()!=0&&aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetFieldManager())
    {
      
      
      G4FieldManager *fMgr = theTrack.GetVolume()->GetLogicalVolume()->GetFieldManager();
      
      
      //  if(!fMgr->DoesFieldChangeEnergy())//then we have a magnetic field
      if(fMgr->FieldHasMagComponent()==true)//then we have a magnetic field
	{
	  
#ifdef G4SRVERBOSE
	  G4cout<<"IN FLIGHT::: MAGNETIC FIELD HERE" <<G4endl;// getchar();
#endif
	  
	  // tao :: Get Field
	  point[0]=theTrack.GetPosition().x()/mm;
	  point[1]=theTrack.GetPosition().y()/mm;
	  point[2]=theTrack.GetPosition().z()/mm;
	 

	  const G4Field* mfield;
	  mfield = fMgr->GetDetectorField();       
	  
	  mfield->GetFieldValue(point,B);

#ifdef G4SRVERBOSE
          G4cout <<"IN FLIGHT::: POSITION ="<< point[0]/mm <<" mm, " << point[1]/mm <<" mm, " << point[2]/mm<<" mm" <<G4endl;

#endif


	  
#ifdef G4SRVERBOSE
	  G4cout <<"IN FLIGHT::: MAGNETIC FIELD B="<< B[0]/gauss <<" G, " << B[1]/gauss <<" G, " << B[2]/gauss<<" G" <<G4endl;
	  
#endif
	      
#ifdef G4SRVERBOSE
	  G4cout <<"IN FLIGHT::: TIME= proper: "<< itime <<" ;  global: " << gtime  <<" decay: " << ftime <<G4endl;
#endif
	  
	  
	  G4ThreeVector magField(B[0],B[1],B[2]);

	if(sqr(B[0])  + sqr(B[1]) +sqr(B[2]) !=0)
	     {
		theSpin= RotateSpin(aStep,magField,deltatime);
             }
 	  
#ifdef G4SRVERBOSE
	  G4cout<<"IN FLIGHT::: spin rotated";
#endif

	}
    }

	  return theSpin;
}


  



G4ThreeVector LEMuSRMUONIUMScatt::RotateSpin( const G4Step& aStep, G4ThreeVector B, G4double deltatime )
{
  
  
  G4Transform3D Spin_rotation;
 
  G4double Bnorm = sqrt(sqr(B[0])  + sqr(B[1]) +sqr(B[2]) );

#ifdef G4SRVERBOSE
  G4cout<< "IN FLIGHT::: PARAMETERS\n" 
	<< "Magnetic Field Norm  : " << G4BestUnit(Bnorm,"Magnetic flux density") <<"\n";
#endif
    
  G4double omega,q,a,fqz;
  G4double gamma;
  q= aStep.GetTrack()->GetDefinition()->GetPDGCharge();
  a= 1.165922e-3;
  // Muon's value.. not used. just for comparison in debugging.
  fqz = 8.5062e+7*rad/(s*kilogauss);
  
  gamma = 0.5*((1.*eplus)/(0.1056584*GeV/(c_light*c_light))-(1.*eplus)/(0.51099906*MeV/(c_light*c_light)));

  //  G4cout<<  fqz*(s*tesla)<<G4endl;
  //  G4cout<<  gamma*(s*tesla)<<G4endl;
 


#ifdef G4SRVERBOSE
  G4cout<< "IN FLIGHT::: PARAMETERS\n" 
	<< "Charge  : " << q <<"\n";
#endif
  
  //  omega= - (fqz)*(1.+a) * Bnorm;
  omega= - (gamma) * Bnorm;

#ifdef G4SRVERBOSE
  G4cout<< "IN FLIGHT::: PARAMETERS\n" 
	<< "Frequency: " << G4BestUnit(fqz*gauss/(2*M_PI*rad),"Frequency") <<"\n";
  G4cout<< "IN FLIGHT::: PARAMETERS\n" 
	<< "FrequencyG: " << G4BestUnit(gamma*gauss/(2*M_PI*rad),"Frequency") <<"\n";  
#endif
   
  
  G4double rotation_angle = deltatime*omega; 
  

  Spin_rotation= G4Rotate3D(rotation_angle,B/Bnorm);
  
  HepVector3D spin = aStep.GetTrack()->GetPolarization();
  HepVector3D newspin;
  newspin = Spin_rotation*spin;
  
  G4double x,y,z,alpha;
  x = sqrt(spin*spin);
  y = sqrt(newspin*newspin);
  z = spin*newspin/x/y;
  alpha = acos(z);
  
#ifdef G4SRVERBOSE
  G4cout<< "IN FLIGHT::: PARAMETERS\n" 
	<< "Initial spin  : " << spin <<"\n"
	<< "Delta time    : " << deltatime <<"\n"
	<< "Rotation angle: " << rotation_angle/(M_PI*rad) <<"\n"
	<< "New spin      : " << newspin <<"\n"
	<< "Checked norms : " << x <<" " <<y <<" \n"
	<< G4endl; 
#endif
  
  return newspin; 
}

