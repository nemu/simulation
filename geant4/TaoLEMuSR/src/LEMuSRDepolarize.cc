//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
// 
//  ID    :LEMuSRDepolarize.cc , v 1.2
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2006-01-19 15:17
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               LEMUSRDEPOLARIZE   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

#include "LEMuSRDepolarize.hh"
#include "G4StepStatus.hh"
#include "G4Navigator.hh"
#include "G4TransportationManager.hh"
#include "Randomize.hh"
#include "G4ProductionCutsTable.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    
using namespace std;

LEMuSRDepolarize::  LEMuSRDepolarize(const G4String& name ,
				     G4ProcessType   aType )
  : G4VDiscreteProcess(name, aType)
{}


LEMuSRDepolarize:: ~LEMuSRDepolarize()
{}


G4VParticleChange* LEMuSRDepolarize::AtRestDoIt(
						const G4Track& trackData,
						const G4Step& aStep )
{
  // Initialize ParticleChange  (by setting all its members equal
  //                             to corresponding members in G4Track)
  fParticleChange.Initialize(trackData);
  
  // tao :: Get Time
  itime = trackData.GetProperTime();
  gtime = trackData.GetGlobalTime();
  ftime = trackData.GetDynamicParticle()->GetPreAssignedDecayProperTime(); 
  
  deltatime = ftime - itime;
  fParticleChange.ProposeGlobalTime(deltatime + itime -gtime);
  
  // set position momentum energy
  fParticleChange.ProposePosition(trackData.GetPosition());
  fParticleChange.ProposeMomentumDirection(trackData.GetMomentumDirection());
  fParticleChange.ProposeEnergy(trackData.GetKineticEnergy());
  fParticleChange.ProposeGlobalTime(gtime);
  fParticleChange.ProposeProperTime(itime);
  fParticleChange.ProposeTrackStatus(trackData.GetTrackStatus()) ;
  
  if( CheckCondition(aStep))
    {
      G4double costheta, sintheta, cosphi, sinphi, theta, phi;
  
      phi  = 2.0*M_PI*G4UniformRand()*rad;
      sinphi = sin(phi);
      cosphi = cos(phi);
  
      theta  = M_PI*G4UniformRand()*rad;
      sintheta = sin(theta);
      costheta = cos(theta);
  
      // rotation angles
      G4double px = sintheta*sinphi;
      G4double py = sintheta*cosphi;
      G4double pz = costheta;
  
      G4ThreeVector direction0(px,py,pz);
  
      fParticleChange.ProposePolarization(px,py,pz);
      // G4cout<<"DEPOLARIZE at rest"<<G4endl;
  
    }  
  else
    {
      fParticleChange.ProposePolarization(trackData.GetPolarization());
    }
  //   fParticleChange.DumpInfo();
  return &fParticleChange;
}


G4VParticleChange* LEMuSRDepolarize::PostStepDoIt(
						  const G4Track& trackData,
						  const G4Step& aStep )
{
  // Initialize ParticleChange  (by setting all its members equal
  //                             to corresponding members in G4Track)
  fParticleChange.Initialize(trackData);
  
  // tao :: Get Time
  itime = trackData.GetProperTime();
  gtime = trackData.GetGlobalTime();
  ftime = trackData.GetDynamicParticle()->GetPreAssignedDecayProperTime(); 
  
  deltatime = ftime - itime;
  fParticleChange.ProposeGlobalTime(deltatime + itime -gtime);
  
  // set position momentum energy
  fParticleChange.ProposePosition(trackData.GetPosition());
  fParticleChange.ProposeMomentumDirection(trackData.GetMomentumDirection());
  fParticleChange.ProposeEnergy(trackData.GetKineticEnergy());
  fParticleChange.ProposeGlobalTime(gtime);
  fParticleChange.ProposeProperTime(itime);
  fParticleChange.ProposeTrackStatus(trackData.GetTrackStatus()) ;
  
  if( CheckCondition(aStep))
    {
      G4double costheta, sintheta, cosphi, sinphi, theta, phi;
  
      phi  = 2.0*M_PI*G4UniformRand()*rad;
      sinphi = sin(phi);
      cosphi = cos(phi);
  
      theta  = M_PI*G4UniformRand()*rad;
      sintheta = sin(theta);
      costheta = cos(theta);
  
      // rotation angles
      G4double px = sintheta*sinphi;
      G4double py = sintheta*cosphi;
      G4double pz = costheta;
  
      G4ThreeVector direction0(px,py,pz);
      fParticleChange.ProposePolarization(px,py,pz);
      //      G4cout<<"DEPOLARIZE post step"<<G4endl;
 
    }  
  else
    {
      fParticleChange.ProposePolarization(trackData.GetPolarization());
    }
  //   fParticleChange.DumpInfo();
  return &fParticleChange;

}


G4bool LEMuSRDepolarize::CheckCondition( const G4Step& aStep)
{
  G4bool condition=false;
  p_name = aStep.GetTrack()->GetDefinition()->GetParticleName(); // particle name  
  if((p_name == "mu+"||p_name=="Mu"))
    { 
      if(aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName()=="lv_SAH1"
	 ||aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName()=="lv_SAH2"
	 ||aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName()=="lv_SAH3"
	 ||aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName()=="lv_SAPH")
	{
	  condition=true;// ACTIVATE PROCESS IN VOLUME ONLY:: LOOK IN G4 DOCUMENTATION
	}
    }
  return condition;
  
}  


G4double LEMuSRDepolarize:: GetMeanFreePath(const G4Track& ,
					    G4double  ,
					    G4ForceCondition* condition
					    )
{
  
  *condition = Forced;
  return DBL_MAX;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LEMuSRDepolarize::GetDatas( const G4Step* )
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



void LEMuSRDepolarize::PrepareSecondary(const G4Track& track)
{
  aSecondary = new G4Track(DP,track.GetGlobalTime(),track.GetPosition());
}
