//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION 
//
//  ID    : LEMuSRDetectorConstruction.cc , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2004-06-24 16:33
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//   
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                               DETECTOR CONSTRUCTION: MATERIALS
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

     
// G4 GEOMETRIC FORMS CLASSES

#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Trap.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnitsTable.hh"

// G4 VOLUME DEFINITION CLASSES
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVReplica.hh"
#include "G4PVPlacement.hh"
#include "G4GeometryManager.hh"
#include "G4FieldManager.hh"
#include "G4PropagatorInField.hh"
#include "LEMuSRMag_SpinEqRhs.hh"
#include "G4Mag_SpinEqRhs.hh"
#include "G4ChordFinder.hh"
#include "G4El_UsualEqRhs.hh"
#include "G4ClassicalRK4.hh"
#include "G4SimpleHeum.hh"
#include "G4SimpleRunge.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4TransportationManager.hh"
#include "G4GeometryManager.hh"
#include "G4UnitsTable.hh"
//#include "G4RotationMatrix.hh"

// G4 CLASSES
#include "G4ios.hh"
#include <iomanip>


// HEADER
#include "LEMuSRDetectorConstruction.hh"
#include "LEMuSRDetectorMessenger.hh"


// process remove
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleChange.hh"
#include "G4ProcessVector.hh"
#include "G4ProcessManager.hh"
#include "G4VProcess.hh"

// electric fieldmap
#include "LEMuSRElectricField.hh"
#include "LEMuSRElFieldMix.hh"
#include "G4ElectricField.hh"
#include "G4ElectroMagneticField.hh"
#include "G4EqMagElectricField.hh"

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//



//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//                            DEFINE COLOR AND USER LIMIT ATTRIBUTES
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

void LEMuSRDetectorConstruction :: LoadAttributes()
{

  // visual attributes
  //G4VisAttributes InBlack = new G4VisAttributes(G4Colour(0.,0.,0.));// for special projection plot.
  Blue_style =  new G4VisAttributes(G4Colour(0.80,0.83,1.));// InBlack
  Blue_style->SetForceSolid(true);
  //Blue_style->SetForceWireframe(false);
  fBlue_style = new G4VisAttributes(G4Colour(0.85,.88,0.92));//InBlack
  fBlue_style->SetForceSolid(true);

  
  Red_style = new G4VisAttributes(G4Colour(1.0,0.,.0));
  Red_style->SetForceSolid(true);

  oxsteel = new G4VisAttributes(G4Colour(0.9,0.8,0.75));
  oxsteel->SetForceSolid(true);
  
  dRed_style = new G4VisAttributes(G4Colour(0.5,0.,.0));
  dRed_style->SetForceSolid(true);
  // dRed_style->SetForceWireframe(false);

  Green_style = new G4VisAttributes(G4Colour(0.,1.,.0));
  Green_style->SetForceSolid(true);
  // Green_style->SetForceWireframe(false);
  
  White_style = new G4VisAttributes(G4Colour(1.,1.,1.0));
  White_style->SetForceSolid(true);
  //  White_style->SetForceWireframe(false);
  
  lBlue_style = new G4VisAttributes(G4Colour(0.,.5,1.0));
  lBlue_style->SetForceSolid(true);


  dBlue_style = new G4VisAttributes(G4Colour(0.,.25,.5));
  dBlue_style->SetForceSolid(true);

  Purple_style = new G4VisAttributes(G4Colour(1.,0.,1.0));
  Purple_style->SetForceSolid(true);
  //  Purple_style->SetForceWireframe(false);
 
  MCP_style = new G4VisAttributes(G4Colour(0.5,0.2,.7));
  MCP_style->SetForceSolid(true);
  // MCP_style->SetForceWireframe(false);

  MACOR_style = new G4VisAttributes(G4Colour(0.9,0.9,.1));
  MACOR_style->SetForceSolid(true);
  // MACOR_style->SetForceWireframe(false);

  SCINT_style = new G4VisAttributes(G4Colour(0.5,0.5,.75));
  SCINT_style->SetForceSolid(true);
  
  dSCINT_style = new G4VisAttributes(G4Colour(0.3,0.3,.3));
  dSCINT_style->SetForceSolid(true);



  
  VTBB_style = new G4VisAttributes(G4Colour(0.9,0.9,.9));
  //VTBB_style->SetForceSolid(true);
  VTBB_style->SetForceWireframe(true);
  	
  // user limits
  VTBB_lim = new G4UserLimits();   
 

}




//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//                            MATERIALS DEFINITION
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//

void LEMuSRDetectorConstruction :: MaterialsDefinition ()
{
  
 
  
  G4double a; 		    //  atomic mass
  G4double z; 		    //  atomic number
  G4double density; 	    //
  G4String name, symbol;    // 
  G4int nbelements;         //  number of elements
  G4double fractionmass;    //  fractionnal mass - for mixtures
  
  
  
  /*** ELEMENTS***/
  
 
  
  // definition hydrogene
  a=1.01*g/mole;
  G4Element* H = new G4Element(name="hydrogen", symbol="H", z=1., a);

  // definition boron
  a=10.811*g/mole;
  G4Element* B = new G4Element(name="boron", symbol="B", z=5., a);

    
  // definition carbon
  a=12.0107*g/mole;
  G4Element* C= new G4Element(name="carbone", symbol="C", z=6., a);
  
 // definition nitrogen
  a=14.01*g/mole;
  G4Element* N = new G4Element(name="nitrogen",	// le nom
			       symbol="N",         	// le symbole
			       z=7.,     	// le numero atomique
			       a);		// la masse atomique
  
  
  // definition oxygene
  a=16.00*g/mole;
  G4Element* O = new G4Element(name="oxygen",symbol="O", z=8., a);
  
  // definition sodium						
  a=22.989770*g/mole;
  G4Element* Na= new G4Element(name="sodium", symbol="Na", z=11, a);
  

  // definition magnesium
  a=24.305*g/mole;
  G4Element* Mg= new G4Element ("magnesium","Mg", z=12,a) ;	 

  // definition aluminium				
  a=26.981538*g/mole;
  G4Element* Al= new G4Element(name="aluminium", symbol="Al", z=13, a);
  
  // definition silicon
  a=28.0855*g/mole;
  G4Element* Si= new G4Element(name="silicon", symbol="Si", z=14., a);
  
  // definition potassium
  a=39.0983*g/mole;
  G4Element* K= new G4Element(name="potassium", symbol="K", z=19., a);
  
  // definition chromium
  a=51.9961*g/mole;
  G4Element* Cr= new G4Element(name="chromium", symbol="Cr", z=24., a);
  
  // definition mangenese
  //  a=54.938049*g/mole;
  //  G4Element* Mn= new G4Element(name="manganese", symbol="Mn", z=25., a);
  
  // definition iron						
  a=55.847*g/mole;
  G4Element* Fe= new G4Element(name="fer", symbol="Fe", z=26., a);
  
   // definition nickel
  a=58.6934*g/mole;
  G4Element* Ni= new G4Element(name="nickel", symbol="Ni", z=28., a);
  
  // definition copper
  a=63.546*g/mole;
  G4Element* Cu= new G4Element(name="copper", symbol="Cu", z=29., a);
  
  // definition zinc
  a=65.409*g/mole;
  G4Element* Zn= new G4Element(name="zinc", symbol="Zn", z=30., a);

  // definition arsenic
  a=74.92160*g/mole;
  G4Element* As = new G4Element(name="arsenic", symbol="As", z=33., a);
  
  // definition rubidium
  a=85.4678*g/mole;
  G4Element* Rb= new G4Element(name="rubidium", symbol="Rb", z=37., a);
  
  //definition iodine
  a=126.90447*g/mole;
  G4Element* I = new G4Element(name="iodine", symbol="I", z=53., a);

  //definition caesium
  a=132.90545*g/mole;
  G4Element* Cs = new G4Element(name="caesium", symbol="Cs", z=55., a);

  //definition barium
  a=137.327*g/mole;
  G4Element* Ba= new G4Element(name="barium", symbol="Ba", z=56., a);

  // definition tantalum
  a=180.95*g/mole;
  G4Element* Ta= new G4Element(name="tantalum", symbol="Ta", z=73., a);

  // definition tungsten					
  a=183.85*g/mole;
  G4Element* W= new G4Element(name="tungsten", symbol="W", z=74., a);
  
  // definition gold
  a=196.97*g/mole;
  G4Element* Au= new G4Element(name="gold", symbol="Au", z=79., a);

  // definition lead
  a=207.2*g/mole;
  G4Element* Pb= new G4Element(name="lead", symbol="Pb", z=82., a);
    
  
  
    
	
  /***MATERIALS***/
  
  // definition : composition de Air
  density=1.290*mg/cm3;
  G4Material* Air= new G4Material(name="Air", density,nbelements=2);
  Air->AddElement (N, fractionmass=0.7);//fractionmass=G4double
  Air->AddElement (O, fractionmass=0.3);//fractionmass=G4double
 

  density=1.e-9*g/cm3;
  G4Material* vac= new G4Material(name="vac", density,nbelements=1);
  vac->AddMaterial (Air, fractionmass=1.);//fractionmass=G4double
      
 
  // vacuum
  density=1.e-15*g/cm3;
  /*  G4Material* vacuum = new G4Material("vacuum", z=1., a=1.01*g/mole, density, kStateGas, 3.e-10*pascal, 2.73*kelvin);
  G4Material* vacuum2;
  vacuum2= vacuum;
  */
   density= 2.376e-15*g/cm3; 
   G4double temperature= 300*kelvin;
   G4double pressure= 3.0e-9*pascal;
   G4Material* vacuum = new G4Material(name="vacuum", density, nbelements=1,
                                        kStateGas,temperature,pressure);
   vacuum-> AddMaterial(Air, fractionmass= 1.);
  
  //definition H2O=
  density = 1.000*g/cm3;
  G4Material* H2O = new G4Material(name="H2O", density, 2);
  H2O->AddElement (H, nbelements=2);//nbelements=G4int
  H2O->AddElement (O, nbelements=1);//nbelements=G4int
  


  //definition material 'tantale'
  density = 16.650*g/cm3;
  G4Material* tantale = new G4Material(name="tantale", density, 1);
  tantale->AddElement (Ta, nbelements=1);
  
  //definition material 'or'
  density = 19.300*g/cm3;
  G4Material* gold = new G4Material(name="gold", density, 1);
  gold->AddElement (Au, nbelements=1);
  
  // definition material 'iron'
  density = 7.874*g/cm3;
  G4Material* iron = new G4Material("iron",density,1);
  iron->AddElement (Fe,1);
  
  // definition material 'tungsten'
  density =19.250*g/cm3;
  G4Material*  tungsten = new G4Material("tungsten",density,1);			
  tungsten->AddElement(W,1);


  // definition : composition of Graphite
  density=2.*g/cm3;
  G4Material* graphite= new G4Material(name="graphite",density,1);
  graphite->AddElement (C,1);


  /***MATERIALS***/
  
 // definition composition of NaI
  density = 3.67*g/cm3;
  G4Material* NaI = new G4Material(name="NaI", density, 2);
  NaI->AddElement (Na, nbelements=1);//nbelements=G4int
  NaI->AddElement (I, nbelements=1);//nbelements=G4int

 // definition composition of MgO
  density = 3.60*g/cm3;
  G4Material* MgO = new G4Material(name="MgO", density, 2);
  MgO->AddElement (Mg, nbelements=1);//nbelements=G4int
  MgO->AddElement (O, nbelements=1);//nbelements=G4int

 // definition composition of K2O
  density = 2.350*g/cm3;
  G4Material* K2O = new G4Material(name="K2O", density, 2);
  K2O->AddElement (O, nbelements=1);
  K2O->AddElement (K, nbelements=2);

// definition composition of SiO2
  density = 2.533*g/cm3;
  G4Material* SiO2 = new G4Material(name="SiO2", density, 2);
  SiO2->AddElement (O, nbelements=2);
  SiO2->AddElement (Si, nbelements=1);


 // definition composition of B2O3
  density = 2.550*g/cm3;
  G4Material* B2O3 = new G4Material(name="B2O3", density, 2);
  B2O3->AddElement (B, nbelements=2);
  B2O3->AddElement (O, nbelements=3);

  
 // definition : composition of saphire //Al2-O3
  density=3.985*g/cm3;
  G4Material*saphire = new G4Material(name="saphire", density,2);
  saphire->AddElement (Al, nbelements=2);
  saphire->AddElement (O, nbelements=3);

  //definition material 'copper'
  density = 8.920*g/cm3;
  G4Material* copper = new G4Material(name="copper", density, 1);
  copper->AddElement (Cu, nbelements=1);
  
  // definition material 'aluminium'
  density = 2.700*g/cm3;
  G4Material* aluminium = new G4Material("aluminium",density,1);
  aluminium->AddElement (Al,1);
 
  // definition composition de Brass
  density=8.67*g/cm3;
  G4Material* brass= new G4Material(name="brass", density,4);
  brass->AddElement (Cu, fractionmass=63./100.);
  brass->AddElement (Pb, fractionmass=3./100.);
  brass->AddElement (Fe, fractionmass=0.1/100.);
  brass->AddElement (Zn, fractionmass=33.9/100.);
  

  // definition composition de mcpglass
  density = 2.*g/cm3;
  G4Material* mcpglass = new G4Material(name="mcpglass", density, 9);
  mcpglass->AddElement (Pb, fractionmass= 0.48 );
  mcpglass->AddElement (O, fractionmass= 0.258 );
  mcpglass->AddElement (Si, fractionmass= 0.182 );
  mcpglass->AddElement (K, fractionmass= 0.042 );
  mcpglass->AddElement (Rb, fractionmass= 0.018 );
  mcpglass->AddElement (Ba, fractionmass= 0.013 );
  mcpglass->AddElement (As, fractionmass= 0.004 );
  mcpglass->AddElement (Cs, fractionmass= 0.002 );
  mcpglass->AddElement (Na, fractionmass= 0.001 );
  
  // definition composition de scint
  density = 1.*g/cm3;
  G4Material* scint = new G4Material(name="scint", density, 2);
  scint->AddElement (C, fractionmass= 1/2.04 );
  scint->AddElement (H, fractionmass=  1.04/2.04);
  
  // definition : composition de stainless_steel
  density=7.930*g/cm3;
  G4Material* stainless_steel= new G4Material(name="stainless_steel", density,3);
  stainless_steel->AddElement (Ni, fractionmass= 0.11);
  stainless_steel->AddElement (Cr, fractionmass= 0.18);
  stainless_steel->AddElement (Fe, fractionmass= 0.71);

  
  // definition : composition de macor
  density=2.52*g/cm3;
  G4Material* macor= new G4Material(name="macor", density,5);
  macor->AddMaterial (SiO2, fractionmass=0.47);
  macor->AddMaterial (saphire, fractionmass=0.17);
  macor->AddMaterial (MgO, fractionmass=0.18);
  macor->AddMaterial (K2O, fractionmass=0.105);
  macor->AddMaterial (B2O3, fractionmass=0.075);

  //  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
  
}

