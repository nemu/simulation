#include<stdlib.h>
#include<iostream>
#include<sstream>
#include<ios>
#include<fstream>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <stdlib.h> 
#include<ios>

#include"meyer.h"

void GFunctions(double*,double*, const double tau);



  meyer GET;

int main()
{


  // DECLARATION OF MEYER's PARAMETERS
  
  /* Meyer's p255: "We consider a beam of initially parallel particles
     with mass m1 and atomic number Z1 which penetrates a material
     layer of thickness t with N atoms per unit volume of mass m2 and
     atomic number Z2. We assume that each scattering centre will be
     effective according to the scattering cross section
     dsigma/dŋ=¶a²f(ŋ)/ŋ² within a spherical volume of radius r0
  */
  
  
  double a, a0, N; // screnqing parameter a 
  double Z1, Z2, D; //  charges numbers Z
  double epsilon, b; // reduced energy epsilon
  double mass1, mass2; // masses of incident & target particles 
  double v; // velocity of incident particle 
  double eta, theta;   // eta =  epsilon*sin(theta/2), (theta, scatt. angle) 
                       // cross section variable by Lindhard, Nielsen and Scharff
  double eSquare = 1.44E-10; // squared electric charge of electron in keV*cm
  
  double tau,thetaSchlange, thick;
  double Energy;

  std::cout<< "thickness? in µm/cm²" << std::endl;
  std::cin>>thick;
  thick=thick*1.0e-6/2;// density= 2g/cm³, 
                       // we want the conversion of thick in centimeter!

  std::cout<<"Enter energy in keV:    ";
  std::cin>>Energy;



  // meyer's functions   
  double g1,g2;
  double f1,f2;


  
  // EXPRESSION OF MEYER's PARAMETERS
  
  // The screening parameter
  // (Z1 = 1,  Z2 = 6,  ScreeningPar = 2.5764E-9)
  Z1 = 1;  Z2 = 6;
  a0=0.529e-8;//unit centimeter
  D= exp(2/3*log(Z1))+exp(2/3*log(Z2));
  a=0.885*a0/sqrt(D);//the screening parameter
  
  // The reduced energy
    mass1=1/9;
    mass2=12;
  //  b= 2*Z1*Z2*eSquare*(mass1+mass2)/(mass1*mass2*v*v);  
  //b= Z1*Z2 * e²[keV*cm] * (m1+m2)/m2 * 1/Energy[keV]
  b= Z1*Z2*eSquare*(mass1+mass2)/(mass2*Energy); 
  epsilon = a/b;
  std::cout<<"\n€:    "<<epsilon <<std::endl;
  
  // The variable eta
  eta= epsilon*sin(theta/2);

  // Number of target per unit of volume
  // N= density of cfoil/atomicmassofcarbon
  // density= 2g/cm³
  // C_atomic_mass= 12 g/mole
  // N= 2/12*6.02e+23=1.0e+17
  N=1.0e+23;
  

  // The reduced thickness
  //a*a ~ 2.7e-17
  //thickness ~ 1.e-6
  //tau ~ e+23*e-17*e-6 ~ unit order
  tau = M_PI*a*a*N*thick;// whith the thickness in centimeter

  std::cout<<"a "<<a<<std::endl;
  std::cout<<"tau "<<tau<<std::endl;




  /*  std::cout<< "theta~? " << std::endl;
  std::cin>>thetaSchlange;


  GET.GFunctions(&g1,&g2,tau);

  std::cout<< "g1("<<tau<<")= "<< g1 << std::endl;
  std::cout<< "g2("<<tau<<")= "<< g2 << std::endl;

  //  thetaSchlange=0;
  GET.F_Functions_Meyer(  tau,thetaSchlange,&f1,&f2);
  std::cout<< "f1("<<tau<<","<<thetaSchlange<<")= "<< f1 << std::endl;
  std::cout<< "f2("<<tau<<","<<thetaSchlange<<")= "<< f2 << std::endl;
  */

  GET.Get_F_Function_Meyer( tau, epsilon, Z1,Z2,mass1,mass2);
  
  return 0;
}




void GFunctions(double* g1,double *g2, const double tau)// PROVIDE VALUES OF G1 and G2 in function of TAU
{


//Diese Routine gibt in Abhaengigkeit von der reduzierten Dicke 'tau'
//Funktionswerte fuer g1 und g2 zurueck. g1 und g2 sind dabei die von
//Meyer angegebenen tabellierten Funktionen fuer die Berechnung von Halbwerts-
//breiten von Streuwinkelverteilungen. (L.Meyer, phys.stat.sol. (b) 44, 253
//(1971))


  double help;

  int i;


  double tau_[] = {0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0,
	  2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 7.0, 8.0, 9.0,
	  10.0, 12.0, 14.0, 16.0, 18.0, 20.0 };
  
  double g1_[]  = {0.050,0.115,0.183,0.245,0.305,0.363,0.419,0.473,0.525,0.575,
	  0.689,0.799,0.905,1.010,1.100,1.190,1.370,1.540,1.700,1.850,
	  1.990,2.270,2.540,2.800,3.050,3.290 };
  
  double g2_[]  = {0.00,1.25,0.91,0.79,0.73,0.69,0.65,0.63,0.61,0.59,
	  0.56,0.53,0.50,0.47,0.45,0.43,0.40,0.37,0.34,0.32,
	  0.30,0.26,0.22,0.18,0.15,0.13 };
  
  
  if (tau<tau_[1])
    {
      std::cout<<"SUBROUTINE G_Functions:"<<std::endl;
      std::cout<<"  Fehler bei Berechnung der g-Funktionen fuer Winkelaufstreuung:"<<std::endl;
      std::cout<<"  aktuelles tau ist kleiner als kleinster Tabellenwert:"<<std::endl;
      std::cout<<"  tau     = "<< tau<<std::endl;
      std::cout<<"  tau_(1) = "<< tau_[1]<<std::endl;
      return;
    }
  i = 1;
  
  do
    {
      i = i + 1;
      if (i==27)
	{
	  std::cout<<"SUBROUTINE G_Functions:"<<std::endl;
	  std::cout<<"  Fehler bei Berechnung der g-Funktionen fuer Winkelaufstreuung:"<<std::endl;
	  std::cout<<"  aktuelles tau ist groesser als groesster Tabellenwert:"<<std::endl;
	  std::cout<<"  tau      = "<< tau <<std::endl;
	  std::cout<<"  tau_[26] = "<< tau_[26] <<std::endl;
	  break;
	} 
    }while(tau>tau_[i]);
      

//lineare Interpolation zwischen Tabellenwerten:

  help = (tau-tau_[i-1])/(tau_[i]-tau_[i-1]);
  std::cout<<"help: "<<help<<std::endl;

  *g1 = g1_[i-1] + help*(g1_[i]-g1_[i-1]);

  *g2 = g2_[i-1] + help*(g2_[i]-g2_[i-1]);

  std::cout<<"g1: "<<*g1<<std::endl;
  std::cout<<"g2: "<<*g2<<std::endl;

}

