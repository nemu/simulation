#ifndef meyer_h
#define meyer_h 1

#include <iomanip>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <ios>


class meyer
{
 public:
  meyer();
 ~meyer();


 void GFunctions(double*, double*, double);
 void Get_F_Function_Meyer(double tau, double Ekin, double Z1, double Z2, double m1, double m2);
 void F_Functions_Meyer( double tau,double thetaSchlange,double *f1,double *f2);


};

#endif
