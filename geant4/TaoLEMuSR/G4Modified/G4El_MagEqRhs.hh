#ifndef G4EL_MAGEQRHS
#define G4EL_MAGEQRHS

#include "G4Types.hh"
#include "G4Mag_EqRhs.hh"
#include "G4El_UsualEqRhs.hh"
#include "G4ios.hh"
#include "G4EquationOfMotion.hh"


class G4MagneticField;
class G4ElectricField;
class G4Mag_EqRhs;
class G4El_UsualEqRhs;

class G4El_MagEqRhs : public G4EquationOfMotion
{
public:  // with description
  
  G4El_MagEqRhs( G4Mag_EqRhs* ,  G4El_EqRhs*,G4Field* field );
  ~G4El_MagEqRhs();
  // Constructor and destructor. No actions.
  
  void RightHandSide( const  G4double y[],  G4double dydx[]   ) const;
  
  virtual void SetChargeMomentumMass( G4double particleCharge, // in e+ units
				      G4double MomentumXc,
				      G4double mass);

   void EvaluateRhsGivenB( const  G4double y[],
				  const  G4double E[3],
			   G4double dydx[] ) const;
   
private:
  
  G4Mag_EqRhs *fMagEq;
  G4El_EqRhs *fElEq;
  
  
};

#endif /* G4EL_MAGEQRHS */
