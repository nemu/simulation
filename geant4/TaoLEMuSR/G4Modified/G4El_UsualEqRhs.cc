//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
// $Id: G4El_UsualEqRhs.cc,v 1.10 2003/11/05 17:31:31 japost Exp $
// GEANT4 tag $Name: geant4-06-00-patch-01 $
//
//
//  This is the 'standard' right-hand side for the equation of motion
//  of a charged particle in a magnetic field.
//
//  Initial version: J. Apostolakis, January 13th, 1997
//
// --------------------------------------------------------------------
#include "G4UnitsTable.hh"
#include "G4El_UsualEqRhs.hh"
#include "G4ElectricField.hh"
#include "G4ios.hh"

G4El_UsualEqRhs::G4El_UsualEqRhs( G4ElectricField* ElField )
  : G4El_EqRhs( ElField ) {}

G4El_UsualEqRhs::~G4El_UsualEqRhs() {}

void
G4El_UsualEqRhs::EvaluateRhsGivenB( const G4double y[],
			             const G4double E[3],
				           G4double dydx[] ) const
{
   G4double momentum_square = y[3]*y[3] + y[4]*y[4] + y[5]*y[5];
   G4double inv_momentum_magnitude = 1.0 / sqrt( momentum_square );

   G4double cof = cst*inv_momentum_magnitude;

   dydx[0] = y[3]*inv_momentum_magnitude;       //  (d/ds)x = Vx/V
   dydx[1] = y[4]*inv_momentum_magnitude;       //  (d/ds)y = Vy/V
   dydx[2] = y[5]*inv_momentum_magnitude;       //  (d/ds)z = Vz/V

   dydx[3] = cof*(E[0]) ;   // Ax = a*(Ex)
   dydx[4] = cof*(E[1]) ;   // Ay = a*(Ey)
   dydx[5] = cof*(E[2]) ;   // Az = a*(Ez)
 
#ifdef DEBUG_FIELD
   G4cout<<"LEMuSREl_UsualEqRhs :: posmomE \n"  
   	 << y[0]/100 <<"    " << y[1]/100 <<"    "<< y[2]/100+5.67 <<"    "<<"\n"
   	 << y[3] <<"    " << y[4] <<"    "<< y[5] <<"    "<<"\n"
   	 << E[0]/volt*meter <<"    " << E[1]/volt*meter <<"    "<< E[2]/volt*meter <<"    "<<"\n"
	 <<G4endl;
   G4cout<<"LEMuSREl_UsualEqRhs :: dydx \n"  
   	 << dydx[0] <<"    " << dydx[1] <<"    "<< dydx[2] <<"    "<<"\n"
   	 << dydx[3] <<"    " << dydx[4] <<"    "<< dydx[5] <<"    "<<"\n"
   	 << dydx[6] <<"    " << dydx[7] <<"    "<< dydx[8] <<"    "<<"\n"
   	 << dydx[9] <<"    " << dydx[10] <<"    "<< dydx[11] <<"     "<<"\n"
	 <<G4endl;
   //   getchar();
#endif

   return ;
} 

void
G4El_UsualEqRhs:: SetChargeMomentumMass( G4double particleCharge, // in e+ units
			                 G4double MomentumXc,
			                 G4double mass )//mass

{
  fInvCurrentMomentumXc= 1.0 / MomentumXc;
   cst = particleCharge*eplus*mass;//*c_light;
}

void G4El_UsualEqRhs::RightHandSide( const  G4double y[],
				   G4double dydx[]   ) const
{
     G4double Field[3];   
     G4double  PositionAndTime[4];

     // Position
     PositionAndTime[0] = y[0];
     PositionAndTime[1] = y[1];
     PositionAndTime[2] = y[2];
     // Global Time
     PositionAndTime[3] = y[7];  // See G4FieldTrack::LoadFromArray

#ifdef DEBUG_FIELD
     G4cout  <<"EL_USUALEQ RIGHT HAND SIDE!"<<G4endl;
#endif

     GetFieldValue(PositionAndTime, Field) ;
     EvaluateRhsGivenB( y, Field, dydx );
}

