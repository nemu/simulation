//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
// $Id: G4El_EqRhs.cc,v 1.10 2003/11/05 16:33:55 japost Exp $
// GEANT4 tag $Name: geant4-06-00-patch-01 $
//
//  This is the standard right-hand side for equation of motion  
//    in a pure Electric Field .
//
//   Other that might be required are:
//     i) is when using a moving reference frame ... or
//    ii) extending for other forces, eg an electric field
//
//            J. Apostolakis, January 13th, 1997
// 
// --------------------------------------------------------------------

#include "G4ElectricField.hh"
#include "G4El_EqRhs.hh"
#include "globals.hh"

//const G4double G4El_EqRhs::fUnitConstant = 0.299792458 * (GeV/(tesla*m)); 

// Constructor Implementation
//
G4El_EqRhs::G4El_EqRhs( G4ElectricField *elField ) 
   : G4EquationOfMotion(elField)
{ 
}


G4El_EqRhs::~G4El_EqRhs() { }

 void G4El_EqRhs::RightHandSide( const  G4double y[],
				       G4double dydx[]   ) const
{
  G4double Field[3];   
  G4double  PositionAndTime[4];

  // Position
  PositionAndTime[0] = y[0];
  PositionAndTime[1] = y[1];
  PositionAndTime[2] = y[2];
  // Global Time
  PositionAndTime[3] = y[7];  // See G4FieldTrack::LoadFromArray
  
  G4cout  <<"EL_EQ RIGHT HAND SIDE!"<<G4endl;
  GetFieldValue(PositionAndTime, Field) ;
  EvaluateRhsGivenB( y, Field, dydx );
}


void  
G4El_EqRhs::SetChargeMomentumMass( G4double particleCharge, // e+ units
			            G4double ,                // MomentumXc
                                    G4double )               // particleMass
{
  fCof_val = particleCharge*eplus*c_light ; 
 
}
