

#include "G4El_MagEqRhs.hh"
#include "G4Mag_SpinEqRhs.hh"
#include "G4MagneticField.hh"
#include "G4ThreeVector.hh"
#include "G4El_UsualEqRhs.hh"
#include "G4ElectricField.hh"
#include "G4ios.hh"



G4El_MagEqRhs::G4El_MagEqRhs(  G4Mag_EqRhs *Meq,  G4El_EqRhs *Eeq,G4Field* field)
  :G4EquationOfMotion(field)
{
  fMagEq=Meq;
  fElEq=Eeq;
}


G4El_MagEqRhs::~G4El_MagEqRhs()
{;}

void G4El_MagEqRhs::RightHandSide( const G4double y[],
			 G4double dydx[]   )const
{

    G4double MagField[3];   
    G4double ElField[3];   
 
    G4double dydx1[12];
    G4double dydx2[12];

     G4double  PositionAndTime[4];

     // Position
     PositionAndTime[0] = y[0];
     PositionAndTime[1] = y[1];
     PositionAndTime[2] = y[2];
     // Global Time
     PositionAndTime[3] = y[7]; 


     // Get Respective Field Values
     fMagEq->GetFieldValue(PositionAndTime, MagField) ;
     fElEq->GetFieldValue(PositionAndTime, ElField) ;


     fMagEq->EvaluateRhsGivenB( y, MagField, &dydx1[0] );

     fElEq->EvaluateRhsGivenB( y, ElField, &dydx2[0] );
     
     G4int i;
     i=0;
     for(i=0;i==18;i++)
       {
	 dydx[i] = dydx1[i]+dydx2[i];
       }

}

void G4El_MagEqRhs::SetChargeMomentumMass( G4double particleCharge, // e+ units
			            G4double MomentumXc,                // MomentumXc
                                    G4double mass)               // particleMass
{

  fMagEq->SetChargeMomentumMass( particleCharge,  MomentumXc ,  mass);

  fElEq->SetChargeMomentumMass( particleCharge,MomentumXc ,  mass);
}


void G4El_MagEqRhs::EvaluateRhsGivenB( const G4double y[],
			            const G4double B[3],
				    G4double dydx[] ) const
{}
