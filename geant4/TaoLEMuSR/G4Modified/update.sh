

# In order to handle the LEMuSR new types of field
cp G4ChordFinder.cc    $G4INSTALL/source/geometry/magneticfield/src/
cp G4ChordFinder.hh    $G4INSTALL/source/geometry/magneticfield/include/

# For the FieldHasMagComponent boolean variable
cp G4FieldManager.cc     $G4INSTALL/source/geometry/magneticfield/src/
cp G4FieldManager.hh    $G4INSTALL/source/geometry/magneticfield/include/
cp G4FieldManager.icc    $G4INSTALL/source/geometry/magneticfield/include/

# Better equations of motions including time update
cp G4El_EqRhs.cc    $G4INSTALL/source/geometry/magneticfield/src/
cp G4El_EqRhs.hh     $G4INSTALL/source/geometry/magneticfield/include/

cp G4El_UsualEqRhs.cc    $G4INSTALL/source/geometry/magneticfield/src/
cp G4El_UsualEqRhs.hh     $G4INSTALL/source/geometry/magneticfield/include/

# Enable the muon decay channel for "Mu" particle
# Set parent polarization
cp G4VDecayChannel.cc    $G4INSTALL/source/particles/management/src/
cp G4VDecayChannel.hh    $G4INSTALL/source/particles/management/include/
cp G4MuonDecayChannel.cc    $G4INSTALL/source/particles/management/src/
cp G4MuonDecayChannel.hh    $G4INSTALL/source/particles/management/include/


#cd $G4INSTALL/source/geometry/magneticfield/
#gmake clean
#gmake


#cd $G4INSTALL/source/particles/management/
#gmake clean
#gmake

cd $G4INSTALL/source
gmake

cd $G4WORKDIR



