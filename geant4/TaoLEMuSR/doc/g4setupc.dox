/*! @page  g4setupc Geant4 Installation

<BR>
<B> Previous:</B> @ref ssg4setup
<B> Up:</B> @ref g4setup
<B> Next:</B>  @ref lemusetup
<BR>

<HR>
 
@section ssg4setup2 Geant4 Compilation

<UL>
<LI><A><A HREF="#g4safe">Safest way</A>
<LI><A HREF="#g4manual">Manual way</A>
<LI><A HREF="#g4compile">Compile Geant4:</A>
</UL>


\anchor g4safe
<H4><A>Safest way:</A></H4>
The safest way is to execute the automatic installation by using the configure shell script. One should go to the geant4.version/ directory and first build the libraries. Type
<PRE>
./Configure -build
</PRE>
and follow the instructions. Each question must be answered <B><I>very carefully</I></B> in order to get a good configuration<A HREF="http://hep.ucsb.edu/people/joel/geant4.html"><SUP>tip</SUP></A>.

Before going any further, make sure that the @ref clheplibs "CLHEP environment variables" are properly set. Remember that the values of the library directory and name can be obtained by entering <tt>./clheplib</tt> in <tt>CLHEP/bin</tt>.
The environment variables for the visualization drivers shall also be set at this point.
Finally, it is recommended to run the script <tt>geant4.version/.config/bin/Linux-g++/env.sh</tt> as a confirmation (it should have been ran automatically after the configuration script of the previous command).
Then execute
<PRE>
./Configure -install
</PRE> 
And compile the source code
<PRE>
cd geant4.version/source
gmake
</PRE>
The \gf code is now ready to use and one can compile an example to check that it runs correctly.

It is usefull to launch the \gf environment variables script <tt>geant4.version/.config/bin/Linux-g++/env.sh</tt> via the SetEnv.sh script.


\anchor g4manual
<H4><A>Manual way:</A></H4>
First, some environement variables must be set (for example):
<PRE>
export G4INSTALL=/home/user/geant4.version   
export G4SYSTEM=Linux-g++ (for example)
export CLHEP\_BASE\_DIR=/home/user/CLHEP
</PRE>
Then the other variables should be set executing the $G4INSTALL/Configure file. 

<P>
Changes of the configuration can be done directly, editing the config.sh file which is located in the $G4INSTALL/.config/bin/Linux-g++ directory.
After modification, this executed in order to prepare the compilation.


\anchor g4compile
<H4><A>Compile Geant4:</A></H4>
The compilation is launched by the command $G4INSTALL/source/gmake.



*/