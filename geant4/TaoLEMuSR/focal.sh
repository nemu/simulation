export ASYM_USE_LEMU=1

export LEMU_TEST_FOCAL_LENGTH=1
unset LEMU_TEST_ASYM
unset LEMU_TEST_FIELD


rm ~/geant4/tmp/Linux-g++/LEMuSR/exe/*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRDetectorConstruction.*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRPrimaryGeneratorAction.*

