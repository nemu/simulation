//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION   
//  ID    : LEMuSR.cc , v 1.2    
//  AUTHOR: Taofiq PARAISO    
//  DATE  : 2004-06-24 09:57               
         
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//   
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &   
//
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//     
 
                       
        
// I G4 CLASSES
#include "G4RunManager.hh"
#include "G4UImanager.hh" 
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4ios.hh"
#include <iomanip> 

// II LEMuSR CLASSES
// a_ Mandatory Classes
#include "LEMuSRDetectorConstruction.hh"
#include "LEMuSRPhysicsList.hh"
#include "PhysicsList.hh"
#include "LEMuSRPrimaryGeneratorAction.hh"

// b_ Optionnal Classes
#include "LEMuSRRunAction.hh"
#include "LEMuSREventAction.hh"
#include "LEMuSRSteppingAction.hh"
#include "LEMuSRStackingAction.hh"
#include "LEMuSRTrackingAction.hh"

// III TEST STEPPING ACTIONS CLASSES
#include "AsymCheck.hh"
#include "FieldCheck.hh"
#include "TDCheck.hh"
#include "FocalLengthTest.hh"


// IV LEMuSR VISUALIZATION CLASS 
#include "LEMuSRVisManager.hh"

// V Interactive root xwindow
#ifdef G4UI_USE_ROOT
#include "G4UIRoot.hh"
#endif


int main() //argc:: defines the user interface
{
 // random numbers
  time_t myseed;
  time(&myseed);
  RanecuEngine *theRanGenerator = new RanecuEngine;
  theRanGenerator->setSeed(myseed);
  HepRandom::setTheEngine(theRanGenerator);
  

  //! 1  The run manager construction
  G4RunManager* runManager = new G4RunManager;
  
  //! 2  The three mandatory classes
   
     //! 2.1  LEMuSR Initialization classes
  LEMuSRDetectorConstruction*    lemuDetector    = new  LEMuSRDetectorConstruction(); 
  
  LEMuSRPhysicsList*      lemuPhysicsList        = new  LEMuSRPhysicsList();
      
 
     //! 2.2  Setting the mandatory Initialization classes
  runManager ->SetUserInitialization( lemuDetector );

  runManager ->SetUserInitialization( lemuPhysicsList );
 

    //! 2.3  LEMuSR Action class
  LEMuSRPrimaryGeneratorAction* lemuPGA          = new  LEMuSRPrimaryGeneratorAction();
  
  
    
     //! 2.4  Setting the mandatory Action class
  runManager ->SetUserAction( lemuPGA );
  
  //! 3  The optionnal classes
  runManager ->SetUserAction( new  LEMuSRRunAction());
  runManager ->SetUserAction( new  LEMuSREventAction());// scintillators, sensitive detectors
  runManager ->SetUserAction( new  LEMuSRTrackingAction());
   runManager ->SetUserAction( new  LEMuSRStackingAction());
 

       
  //! Optionnal stepping action: enable one at once only 
     
#if defined LEMU_TEST_ASYM
  runManager ->SetUserAction( new  AsymCheck());  //! To test the asymmetry
#elif defined LEMU_TEST_FIELD  
    runManager ->SetUserAction( new  FieldCheck()); //! To test the EM fields
#elif defined LEMU_TEST_CFOIL   
    runManager ->SetUserAction( new  TDCheck()); //! To test the trigger foil
#elif defined LEMU_TEST_FOCAL_LENGTH    
    runManager ->SetUserAction( new  FocalLengthTest()); //! To test the focal length of the einzel lens
#else
    runManager ->SetUserAction( new  LEMuSRSteppingAction()); //! For a normal run
#endif    
  
     
         
   //! 4  The visualization manager construction and initialization. It will be initialize only if the env variable G4VIS_USE=1  !
#ifdef G4VIS_USE
  LEMuSRVisManager* lemuVisManager = new  LEMuSRVisManager;
  lemuVisManager -> Initialize();
#endif 

  //! 5  Initialize G4 kernel
  runManager -> Initialize();
 
  
  //! 6  Configuration of the User Interface manager   
  G4UImanager* UI = G4UImanager::GetUIpointer();
   
  G4UIsession* session = 0;

      
   
#if defined G4UI_USE_ROOT 
  // G4UIRoot is a ROOT based GUI 
      session = new G4UIRoot( argc, argv);
#elif defined G4UI_USE_TCSH
      session = new G4UIterminal(new G4UItcsh);      
  G4cout<<"\n G4UI_USE_TCSH! \n"<<G4endl;
#else
  session = new G4UIterminal();
#endif 
 
  UI->ApplyCommand("/vis/sceneHandler/create");
  UI->ApplyCommand("/control/verbose 1");
  UI->ApplyCommand("/run/verbose 1");

#if defined LEMU_TEST_ASYM  
  UI->ApplyCommand("/Detector/MagneticField 50");
  UI->ApplyCommand("/Detector/AsymCheck on");
  UI->ApplyCommand("/lemuGun/gunPosition 0 0 0");
  UI->ApplyCommand("/lemuGun/energy/defined 0");
  UI->ApplyCommand("/lemuGun/energy/offset 0");
  G4cout<<"\n READY TO TEST ASYMETRY! ";
#endif
 
  //! JOB START
 
  session->SessionStart();
  
  //! JOB TERMINATION
 
  delete session;
   
#if defined G4VIS_USE
  delete lemuVisManager;
#endif
  

  delete runManager;
  return 0;
} 

     
