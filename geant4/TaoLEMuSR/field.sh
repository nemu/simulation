export ASYM_USE_LEMU=1
export LEMU_TEST_FIELD=1
unset LEMU_TEST_CFOIL
unset LEMU_TEST_FOCAL_LENGTH
unset LEMU_TEST_ASYM

rm ~/geant4/tmp/Linux-g++/LEMuSR/exe/*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRDetectorConstruction.*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRPrimaryGeneratorAction.*

