export ASYM_USE_LEMU=1

unset LEMU_TEST_FIELD
unset LEMU_TEST_CFOIL
unset LEMU_TEST_FOCAL_LENGTH
export LEMU_TEST_ASYM=1
 

rm ~/geant4/tmp/Linux-g++/LEMuSR/exe/*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRDetectorConstruction.*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRPrimaryGeneratorAction.*
rm ~/geant4/tmp/Linux-g++/LEMuSR/LEMuSRMuonPhysics.*

