/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/


void QmuSRSim::fileNew()
{

}


void QmuSRSim::fileOpen()
{
    my $file=Qt::FileDialog::getOpenFileName(
	    ".",
	    "Initialization file (.* *.*)",
	    this,
	    "open file dialog",
	    "Choose an initialization file");
    
# If the user gave a valid filename try to read it
    if ($file ne "") {
	open (INF,q{<},"$file" );
	my @lines = <INF>;
	close(INF);
	my $Attrib="";
	foreach my $line (@lines) {
# Remove white spaces
	    $line =~ s/\s+//g;
	    my @InitPar = split (/=/,$line);
# Check it is not empty or title line
	    if ($InitPar[0] ne "" && $InitPar[1] ne "") {
		$Attrib = child($InitPar[0]);
		$Attrib->setText($InitPar[1]);
	    }
	}
    }
}


void QmuSRSim::fileSave()
{
    my %All=CreateAllInput();
    my $InitFile="";
    for my $key (keys %All) {
	$InitFile=$InitFile.$key."=".$All{$key}."\n";
    }

# Save to default file name "musrSim.cfg"
     my $file = "musrSim.cfg";
     open (OUTF,q{>},"$file" );
     print OUTF (" $InitFile");
     close(OUTF);
}


void QmuSRSim::fileSaveAs()
{
    my %All=CreateAllInput();
    my $InitFile="";
    for my $key (keys %All) {
	$InitFile=$InitFile.$key."=".$All{$key}."\n";
    }

    my $file=Qt::FileDialog::getSaveFileName(
            "musrSim cfg",
            "musrSim GUI Initialization file (*.cfg)",
            this,
            "save file dialog",
            "Choose a filename to save under");
# if the user gave a filename then sanesave  copy to it
    if ($file ne "") {
	open (OUTF,q{>},"$file" );
	print OUTF (" $InitFile");
	close(OUTF);
    }
}


void QmuSRSim::filePrint()
{

}


void QmuSRSim::fileExit()
{
    my $Ans = Qt::MessageBox::question( this, "Quit?","Are you sure you want to quit?","&Yes","&No","",0,1);
    if ($Ans==0) {
# Then quit
	Qt::Application::exit( 0 );
  }
# Otherwize go back
}


void QmuSRSim::editUndo()
{

}


void QmuSRSim::editRedo()
{

}


void QmuSRSim::editCut()
{

}


void QmuSRSim::editCopy()
{

}


void QmuSRSim::editPaste()
{

}


void QmuSRSim::editFind()
{

}


void QmuSRSim::helpIndex()
{

}


void QmuSRSim::helpContents()
{

}


void QmuSRSim::helpAbout()
{
    my $AboutText="
		  This is a GUI that uses the muSRSim binary, to simulate the LEM muon
		  beam properties. 
		  
		  This software is free software: you can redistribute it and/or modify
		  it under the terms of the GNU General Public License as published by
		  the Free Software Foundation, either version 3 of the License, or
		  (at your option) any later version.
		  
		  This software is distributed in the hope that it will be useful,
		  but WITHOUT ANY WARRANTY; without even the implied warranty of
		  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		  GNU General Public License for more details.
		  
		  You should have received a copy of the GNU General Public License
		  along with MuSRFitGUI.  If not, see <http://www.gnu.org/licenses/>.
		  
		  Copyright 2011 by Zaher Salman and the LEM Group.
		  <zaher.salman\@psi.ch>
		  ";
    my $AboutWindow = Qt::MessageBox::information( this, "About QmuSRSim GUI",$AboutText);

}

void QmuSRSim::CreateAllInput()
{
    my %All=();
    my @AllParams= ("L1","L3","TL","BFIELD","SAMHV","RA_T","RA_B","RA_R","RA_L","SR_B","SR_E","ENERGY","NMuon",
		    "L1_2","L2_2","L3_2","BFIELD_2","RA_T_2","RA_B_2","RA_R_2","RA_L_2","ENERGY_2",
		    "MAC","musrSim","Geant4","ROOTSYS","TEMPL");
    foreach my $Param (@AllParams) {
	my $Child = child($Param);
	my $Value = $Child->text;
	$All{$Param}=$Value;
#	print "$Param=".$All{$Param}."\n";
    }
    
# Convert SR B field from current to field
# SR B Field 8A=0.032238 T
    $All{"SR_B"}=$All{"SR_B"}*0.032238/8;
# SR plate voltage to difference between plates
    $All{"SR_E"}=$All{"SR_E"}*2;
    
# Dont forget mirror, depending on muon energy
    $All{"MIRRFIELD"}=$All{"ENERGY"}/20;
    $All{"MIRRFIELD_2"}=$All{"ENERGY_2"}/20;
    
# Check if ROOTSYS option is set
    if ($All{"ROOTSYS"} eq "") {
	$All{"ROOTSYS"}=$ENV{"ROOTSYS"};
	ROOTSYS->setText($All{"ROOTSYS"});
    }
    
# Return Hash with all important values
    return %All;  
}



void QmuSRSim::StartCalc()
{
    my %All=CreateAllInput();
    
# Check if the simulation can run
    my $answ = CheckSim();
    print "Answer=$answ\n";
    
    if ($answ == 1) {
	my $Page = Tabs->currentPageIndex;
	my %ToReplace=();
	my @SpaceHolder=();
	my $FName= $All{"TEMPL"};
	
# 0 for spin rotator and 1 for current.
	if ($Page == 0) {
	    %ToReplace= (
		    "L1"=>"L1VOLTAGE",
		    "L3"=>"L3VOLTAGE",
		    "TL"=>"TLVOLTAGE",
		    "BFIELD"=>"SAM_BFIELD",
		    "SAMHV"=>"SAM_HV",
		    "RA_T"=>"RA_TVOL",
		    "RA_B"=>"RA_BVOL",
		    "RA_R"=>"RA_RVOL",
		    "RA_L"=>"RA_LVOL",
		    "SR_B"=>"SR_BFIELD",
		    "SR_E"=>"SR_EFIELD",
		    "NMuon"=>"NMuon",
		    "ENERGY"=>"ENERGY",
		    "MIRRFIELD"=>"MIRRFIELD"
			     );
# Use the default template
	    if ($FName eq "") {$FName="SRBeamLine_Bend.mac";}
	} elsif ($Page == 1) {
	%ToReplace= (
		"L1_2"=>"L1VOLTAGE",
		"L2_2"=>"L2VOLTAGE",
		"L3_2"=>"L3VOLTAGE",
		"BFIELD_2"=>"SAM_BFIELD",
		"RA_T_2"=>"RA_TVOL",
		"RA_B_2"=>"RA_BVOL",
		"RA_R_2"=>"RA_RVOL",
		"RA_L_2"=>"RA_LVOL",
		"NMuon"=>"NMuon",
		"ENERGY_2"=>"ENERGY",
		"MIRRFIELD_2"=>"MIRRFIELD"
			   );
# Use the default template
	    if ($FName eq "") {$FName="CBeamLine.mac";}
    }
	
	open(MACF,"$FName");
	my @lines=<MACF>;
	close(MACF);
	
	for my $key (keys %ToReplace) { 
# Need to check values of replace parameters...
	    map(s/$ToReplace{$key}/$All{$key}/, @lines);
	    print "Replacing \t".$ToReplace{$key}."\t by \t".$All{$key}."\n";
	}
	
	my $NewFile=$All{"MAC"}.".mac";
	open(NEWMACF,">$NewFile");
	print NEWMACF @lines;
	close(NEWMACF);
	print "Created mac file $NewFile\n";
	
	my $DIR=`pwd`;
	chomp $DIR;
	my $musrSim=$All{"musrSim"};
	my $Geant4=$All{"Geant4"};
	my $SYSROOT=$All{"SYSROOT"};
	my $cmd="cd $musrSim; . $Geant4/env.sh; export SYSROOT=$SYSROOT; cd run; cp $DIR/$NewFile $NewFile;$musrSim/bin/Linux-g++/musrSim $NewFile > $NewFile.out ";
	system("$cmd");
    }
}



void QmuSRSim::GetPATH()
{
# Which button was clicked?
    my $sender = sender();
# Then find the name of the sender button and convert it to the name of the appropriate lineEdit 
    my $name = $sender->name();
    $name =~ s/Button//g;
    
   my $RETVAL = "";
   if ($name eq "TEMPL") {
# Get template file name
	$RETVAL = Qt::FileDialog::getOpenFileName(
	    "./",
	    "MAC file (*.mac)",
	    this,
	    "Select template file dialog",
	    "Choose a template file");
    } else {
# Get the requested path
	$RETVAL = Qt::FileDialog::getExistingDirectory("./",this,"get existing directory","Choose a directory",1);
    }
# Now submit the path or file name into the lineEdit
    if ($RETVAL ne "") {
	child($name)->setText($RETVAL);
    }
}

void QmuSRSim::CheckSim()
{
# Returns 1 if the simulation can run and 0 if not
    my $answ=1;
    my $answtext="";
    
    my $musrSimPATH= musrSim->text();
    my $Geant4PATH= Geant4->text();
    my $ROOTSYSPATH=ROOTSYS->text();
    my $MAC=MAC->text();
    
# Check if any of them is empty or does not exist
    unless (-d $musrSimPATH) {
	$answ=0;
	$answtext=$answtext."musrSim Path \" $musrSimPATH\" is wrong!\n";
    } else {
	unless (-e "$Geant4PATH/env.sh") {
	    	$answ=0;
		$answtext=$answtext."Geant4 'env.sh' file does not exist!\n";
                }
    }
    unless (-d $ROOTSYSPATH) {
	$answ=0;
	$answtext=$answtext."ROOTSYS Path is wrong!\n";
    }

#    my $NewFile="$musrSim/run/$MAC.mac";
#    unless (-e $NewFile) {
#      $answ=0;
#      $answtext=}answtext."Macro file $NewFile exists!\n";
#    }
    
    if ($answ==0) {
	print STDERR $answtext;
	my $HelpWindow = Qt::MessageBox::information( this, "Error!",$answtext);  
    }
    return $answ;
}


void QmuSRSim::plotBeamSpot()
{
    my $musrSimPATH= musrSim->text();
    my $file=Qt::FileDialog::getOpenFileName(
	    "$musrSimPATH/run/data",
	    "musrSim root file (*.root)",
	    this,
	    "open root file dialog",
	    "Choose a root data file");
        
    if ($file ne "" ) {
#	my $TrimPath = $ENV{'PERLLIB'};	
# Now that we have the file name send it to root macro for plotting.
	my $cmd='root -n -l plotBeamSpot.C\(\"'.$file.'\"\)&';
	print $cmd."\n";
	my $pid=system($cmd);	
    }


}
