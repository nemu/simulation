//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#ifndef lem4TabulatedElementField2Df_h
#define lem4TabulatedElementField2Df_h 1

#include "F04ElementField.hh"
#include "F04GlobalField.hh"

#include "globals.hh"
#include "G4ios.hh"

#include <fstream>
#include <vector>
#include <cmath>


class lem4TabulatedElementField2Df : public F04ElementField
{
public: // with description
  // Class constructor for r-axis FOLDED 2D axial field map (r, z, EMr, EMz) - with EM = E or B; r - radial, z - longit.
  lem4TabulatedElementField2Df(const char* filename, const char fieldType, G4double fieldValue, G4LogicalVolume* logVolume, G4ThreeVector positionOfTheCenter);
  //
  //  "fieldType"          is the type of EM field: electric - E, or magnetic - B
  //  "fieldValue"         is the field to be applied (in T, or in kV/mm). The normalised field
  //                       map values are multiplied by this value. The field-map itself has no units!
  //  "lenUnit"            is the unit in which the grid coordinates of the field-map are specified
  //  "fieldNormalisation" is the normalisation factor that once applied to the tabulated field values
  //                       satisfies the condition: (max. field value)*fieldNormalisation = 1
  //                       To revert field direction, change its sign to negative.

  // Virtual destructor
  virtual ~lem4TabulatedElementField2Df() {}

  // addFieldValue() adds the field for THIS particular map into field[].
  // point[] is expressed in GLOBAL coordinates.
  void  addFieldValue( const  G4double Point[4], G4double* field) const;

  // Usual Set and Get functions
  G4double GetNominalFieldValue();
  void SetNominalFieldValue(G4double newFieldValue);

  //  getWidth(), getHeight(), getLength(),  return the dimensions of the field
  // (used to define the boundary of the field)
  virtual G4double getWidth()  { return 2*dr; }   // x coordinate
  virtual G4double getHeight() { return 2*dr; }   // y coordinate
  virtual G4double getLength() { return 2*dz; }   // z coordinate


private:
  // Storage space for the 2D table
  std::vector< std::vector< double > > rField;
  std::vector< std::vector< double > > zField;
  // The field-map dimensions
  int nr, nz;
  // The field map Length unit (string and number)
  G4String lUnit;
  double lenUnit;
  // The DEFAULT user-defined field units for E and B (kilovolt/mm and tesla)
  G4String fUnit;
  double fieUnit;
  // The field-map Field normalisation factor
  double fieldNormalisation;
  // The physical limits of the defined region
  double minimumr, maximumr, minimumz, maximumz;
  // The physical extent of the defined region
  double dr, dz;
  // See the description under the class constructor
  char   fldType;
  double ffieldValue;

  void   Invert(const char* indexToInvert);

};

#endif
