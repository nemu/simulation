//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef lem4ShieldHit_h
#define lem4ShieldHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4MagneticField.hh"
#include "globals.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class lem4ShieldHit : public G4VHit
{
  public:

      lem4ShieldHit();
     ~lem4ShieldHit();
      lem4ShieldHit(const lem4ShieldHit&);
      const lem4ShieldHit& operator=(const lem4ShieldHit&);
      G4int operator==(const lem4ShieldHit&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Draw();
      void Print();

  public:


      void SetTrackID  (G4int track)      { trackID = track; };
  void SetParticleName (G4String name) {particle_name = name; };
      void SetEdep     (G4double de)      { edep = de; };
      void SetPos      (G4ThreeVector xyz){ pos = xyz; };
      void SetPol      (G4ThreeVector ijk){pol = ijk;};
  void SetLogVolName   (G4String logivol) {logicalvolume = logivol;};
      

      G4int GetTrackID()    { return trackID; };
  G4String GetParticleName() {return particle_name; };
      G4double GetEdep()    { return edep; };      
      G4ThreeVector GetPos(){ return pos; };
      G4ThreeVector GetPol(){ return pol; };
  G4String GetLogVolName() { return logicalvolume; };
      G4double point[4];
      G4double B[6];
      const G4Field *mfield;


  private:


      G4int         trackID;
  G4String          particle_name;
      G4double      edep;
      G4ThreeVector pos;
      G4ThreeVector pol;
  G4String          logicalvolume;
  static G4int eventIDoldB;
  static G4int Nb;
  static G4int runIDoldB;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<lem4ShieldHit> lem4ShieldHitsCollection;

extern G4Allocator<lem4ShieldHit> lem4ShieldHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* lem4ShieldHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) lem4ShieldHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void lem4ShieldHit::operator delete(void *aHit)
{
  lem4ShieldHitAllocator.FreeSingle((lem4ShieldHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
