//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef lem4ScintSD_h
#define lem4ScintSD_h 1

#include "G4VSensitiveDetector.hh"
#include "lem4ScintHit.hh"
#include "lem4RootOutput.hh"

//cksdel extern lem4RootOutput* myRootOutput;

class G4Step;
class G4HCofThisEvent;
//class lem4ScintHit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class lem4ScintSD : public G4VSensitiveDetector
{
  public:
      lem4ScintSD(G4String);
     ~lem4ScintSD();

      void Initialize(G4HCofThisEvent*);
      G4bool ProcessHits(G4Step*, G4TouchableHistory*);
      void EndOfEvent(G4HCofThisEvent*);
  //      bool timeOrdering (lem4ScintHit* hit1, lem4ScintHit* hit2);
  //     bool myREMOVEfunction (int i,int j) { return (i<j); }

  private:
      lem4ScintHitsCollection* scintCollection;
      G4bool   myStoreOnlyEventsWithHits;
      G4double mySignalSeparationTime;
      G4bool   myStoreOnlyTheFirstTimeHit;
  //      G4bool Positron_momentum_already_stored;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

