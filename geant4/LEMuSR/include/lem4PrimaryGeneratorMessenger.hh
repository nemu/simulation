//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#ifndef lem4PrimaryGeneratorMessenger_h
#define lem4PrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class lem4PrimaryGeneratorAction;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWith3Vector;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class lem4PrimaryGeneratorMessenger: public G4UImessenger
{
  public:
    lem4PrimaryGeneratorMessenger(lem4PrimaryGeneratorAction* lem4Gun);
   ~lem4PrimaryGeneratorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String newValue);
    
  private:
    lem4PrimaryGeneratorAction* lem4Action;
    G4UIcmdWith3VectorAndUnit* setvertexCmd;
    G4UIcmdWith3VectorAndUnit* setvertexSigmaCmd;
    G4UIcmdWith3VectorAndUnit* setvertexBoundaryCmd;
    G4UIcmdWithADoubleAndUnit* setEnergyCmd;
    G4UIcmdWithADoubleAndUnit* setMomentumCmd;
    G4UIcmdWithADoubleAndUnit* setMomentumSmearingCmd;
    G4UIcmdWith3VectorAndUnit* setMomentumBoundaryCmd;
    G4UIcmdWith3VectorAndUnit* setTiltAngleCmd;
    G4UIcmdWith3VectorAndUnit* setSigmaTiltAngleCmd;
    G4UIcmdWithADoubleAndUnit* setPitchCmd;
    G4UIcmdWith3Vector*        setMuonPolarizCmd;
    G4UIcmdWith3VectorAndUnit* setMuonDecayTimeCmd;
    G4UIcmdWithAString*        setTurtleCmd;
};
#endif
