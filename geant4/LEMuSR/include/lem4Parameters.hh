//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#ifndef lem4Parameters_h
#define lem4Parameters_h 1

#include "globals.hh"

class lem4Parameters {
  public:
    ///lem4Parameters();
    lem4Parameters(G4String steeringFileName); // ADDED by TS - overloaded constructor
    ~lem4Parameters();

    static lem4Parameters* GetInstance();

    void     SetMyTypeOfProcesses(G4String string) {typeOfProcesses=string;};
    G4String GetMyTypeOfProcesses() {return typeOfProcesses;};

    static G4bool storeOnlyEventsWithHits;   // Variable specifying whether to store interesting 
                                             // or all events into the ROOT tree. (default = true)
    static G4double signalSeparationTime;    // Minimim time separation between two subsequent signal 
    static G4bool storeOnlyTheFirstTimeHit;  // If true, only the hit that happened first will be
                                             // stored, anything else will be ignored 
                                             // (useful in some special cases, not for a routine simulation)
    static G4bool includeMuoniumProcesses;   // If true, includes Muonium formation and all
                                             // other Mu-related processes in the simulation
    //ADDED by TS
    
    static G4bool boolG4GeneralParticleSource;//If true, G4GeneralParticleSource will be initialised 
                                              //instead of G4ParticleGun - for simulating radioactive sources
    
    static G4bool field_DecayWithSpin;       // If true, then the routine for calculating the magnetic 
                                             // field will use a more precise argument.  
					     // This variable is set to "true" by the SteppinAction 
					     // and reset to "false" in the GetFieldValue.
                                             // It is being changed on step by step basis.

    static G4int nrOfEventsToBeGenerated;    // Nr of events to be simulated in this run (set by /run/beamOn command)

  private:
    static lem4Parameters* pointerToParameters;
    G4String typeOfProcesses;  // Variable defining what kind of Physics processes to call:
                               // - Standard EM
                               // - LowEnergy (default)
                               // - Penelope
			       // - Coulomb
};

#endif
