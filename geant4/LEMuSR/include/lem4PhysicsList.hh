//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef lem4PhysicsList_h
#define lem4PhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"
//cks Added to have Geant default muon decay with spin
#include "G4DecayWithSpin.hh"
//#include "lem4DecayWithSpin.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class lem4PhysicsList: public G4VUserPhysicsList
{
  public:
    lem4PhysicsList();
   ~lem4PhysicsList();

  protected:
    // Construct particle and physics
    void ConstructParticle();
    void ConstructProcess();
 
    void SetCuts();

   
  protected:
    // these methods Construct particles 
    void ConstructBosons();
    void ConstructLeptons();
    void ConstructMesons();
    void ConstructBaryons();

  protected:
  // these methods Construct physics processes and register them
    void ConstructGeneral();
    void ConstructEM();
  
  //  private:
  //    char myProcesses[100];
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

 
