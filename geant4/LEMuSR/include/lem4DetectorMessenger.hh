//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef lem4DetectorMessenger_h
#define lem4DetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class lem4DetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;
class G4UIcmdWith3Vector;
class G4UIcmdWith3VectorAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class lem4DetectorMessenger: public G4UImessenger
{
  public:
    lem4DetectorMessenger(lem4DetectorConstruction*);
   ~lem4DetectorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    lem4DetectorConstruction* myDetector;
    
    G4UIdirectory*             lem4Dir;
    G4UIdirectory*             detDir;
    G4UIdirectory*             runDir;
    G4UIcmdWithAString*        CommandCmd;
    G4UIcmdWithAnInteger*      RunIDSetCmd;
    G4UIcmdWithAnInteger*      RandomOptionCmd;
    G4UIcmdWithAnInteger*      HowOftenToPrintEventCmd;
  //    G4UIcmdWithAString*        WhichProcessesCmd;
    G4UIcmdWithoutParameter*   UpdateCmd;
  //    G4UIcmdWithADoubleAndUnit* FieldCmd;
    G4UIcmdWithADoubleAndUnit* UFieldCmd;
///    G4UIcmdWithADoubleAndUnit* UEFieldCmd; //Uniform Electric Field. TS
    G4UIcmdWith3Vector*        GFieldCmd;


  public:
  //  cks:  The two variables used for the random number initialisation each event (if required)
  //    long myEventNr;
  //    static G4bool setRandomNrSeedAccordingEventNr;
  //    void IncrementMyEventNr() {myEventNr++};
  //    long GetMyEventNr() {return myEventNr};
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

