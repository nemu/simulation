//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#ifndef lem4TabulatedElementField2D_h
#define lem4TabulatedElementField2D_h 1

#include "globals.hh"
////#include "G4MagneticField.hh"
#include "F04ElementField.hh"
#include "F04GlobalField.hh"
#include "G4ios.hh"
#include <fstream>
#include <vector>
#include <cmath>


class lem4TabulatedElementField2D : public F04ElementField
{
public:
  lem4TabulatedElementField2D(const char* filename, G4double fieldValue, G4double lenUnit, G4double fieldNormalisation, G4LogicalVolume* logVolume, G4ThreeVector positionOfTheCenter);
  //  "lenUnit" is the unit in which the grid coordinates are specified in the table
  //  "fieldNormalisation" is the normalisation that has to be applied on the field values in the table
  //                       such that the values correspond do 1T nominal value
  //  "fieldValue" is the field value (in T) that is required (i.e. values normalised to 1T will be
  //                       multiplied by this value).

  ///  Destructor.
  virtual ~lem4TabulatedElementField2D() {}

  ///  addFieldValue() adds the field for this solenoid into field[].
  ///  point[] is in global coordinates.
  void  addFieldValue( const  G4double Point[4], G4double* field) const;

  G4double GetNominalFieldValue();
  void SetNominalFieldValue(G4double newFieldValue);
  
  //  getWidth(), getHeight(), getLength(),  return the dimensions of the field 
  // (used to define the boundary of the field)
  virtual G4double getWidth()  { return 2*dx; }   // x coordinate
  virtual G4double getHeight() { return 2*dx; }   // y coordinate
  virtual G4double getLength() { return 2*dz; }   // z coordinate


private:
  // Storage space for the table
  std::vector< std::vector< double > > xField;
  std::vector< std::vector< double > > zField;
  // The dimensions of the table
  int nx,nz; 
  // The physical limits of the defined region
  double minimumx, maximumx, minimumz, maximumz;
  // The physical extent of the defined region
  double dx, dz;
  double ffieldValue;

  void   Invert(const char* indexToInvert);

};

#endif
