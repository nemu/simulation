//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "globals.hh"
#include "G4MagneticField.hh"
#include "G4ios.hh"
//cks For the special case of muon decay:
//#include "G4EventManager.hh"
//#include "G4RunManagerKernel.hh"
//csk

#include <fstream>
#include <vector>
#include <cmath>


class lem4TabulatedField2D
#ifndef STANDALONE
 : public G4MagneticField
#endif
{
  
  // Storage space for the table
  std::vector< std::vector< double > > xField;
  std::vector< std::vector< double > > zField;
  // The dimensions of the table
  int nx,nz; 
  // The physical limits of the defined region
  double minx, maxx, minz, maxz;
  // The physical extent of the defined region
  double dx, dz;
  double ffieldValue;
  bool invertX, invertZ;
  double positionOffset[4];

  static lem4TabulatedField2D* pointerToTabulatedField2D;

public:

  static lem4TabulatedField2D* GetInstance();

  lem4TabulatedField2D(const char* filename, double fieldValue, double lenUnit, double fieldNormalisation );
  //  "lenUnit" is the unit in which the grid coordinates are specified in the table
  //  "fieldNormalisation" is the normalisation that has to be applied on the field values in the table
  //                       such that the values correspond do 1T nominal value
  //  "fieldValue" is the field value (in T) that is required (i.e. values normalised to 1T will be
  //                       multiplied by this value).
  void  GetFieldValue( const  double Point[4],
		       double *Bfield          ) const;
  G4double GetFieldSetValue();
  void SetFieldValue(double newFieldValue);
  void SetPositionOffset(double offset[4]) { positionOffset[0]=offset[0]; positionOffset[1]=offset[1];
    positionOffset[2]=offset[2]; positionOffset[3]=offset[3];}
};

