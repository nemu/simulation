//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef lem4ScintHit_h
#define lem4ScintHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4MagneticField.hh"
#include "globals.hh"
#include "G4ios.hh"
//  ROOT
#include "TFile.h"
#include "TTree.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class lem4ScintHit : public G4VHit
{
  public:

      lem4ScintHit();
     ~lem4ScintHit();
      lem4ScintHit(const lem4ScintHit&);
      const lem4ScintHit& operator=(const lem4ScintHit&);
      G4int operator==(const lem4ScintHit&) const;
  //      bool operator() (lem4ScintHit hit1, lem4ScintHit hit2) { return (hit1.globalTime<hit2.globalTime);}
  
      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Draw();
      void Print();

  public:

      void SetParticleName (G4String name) {particleName = name; };
      void SetParticleID (G4int id) {particleID = id; };
      void SetTrackID  (G4int track)      { trackID = track; };
      void SetEdep     (G4double de)      { edep = de; };
      void SetPrePos      (G4ThreeVector xyz){ pre_pos = xyz; };
      void SetPostPos      (G4ThreeVector xyz){ post_pos = xyz; };
      void SetPol      (G4ThreeVector ijk){pol = ijk;};
      void SetLogVolName   (G4String logivol) {logicalVolume = logivol;};
      void SetGlobTime     (G4double gt)      { globalTime = gt;};
      void SetFirstStepInVolumeFlag (G4bool flag) { firstStepInVolume=flag;};
      void SetLastStepInVolumeFlag (G4bool flag)  { lastStepInVolume=flag;};
      void SetKineticEnergy     (G4double en)     { kineticEnergy = en;};
      void SetStepLength     (G4double length)    { stepLength = length;};
  //      void SetBField (G4double f[6]) {BF[0]=f[0]; BF[1]=f[1]; BF[2]=f[2]; BF[3]=f[3]; BF[4]=f[4]; BF[5]=f[5];};
      void SetRunID(G4int i) {runID=i;};
      void SetEventID(G4int i) {eventID=i;};
  //      void SetVerboseLevel   (G4int n) {  G4int lem4ScintHit::verboseLevel=n;};

      G4String GetParticleName() {return particleName; };
      G4int GetParticleID() {return particleID; };
      G4int GetTrackID()    { return trackID; };
      G4double GetEdep()    { return edep; };      
      G4ThreeVector GetPrePos(){ return pre_pos; };
      G4ThreeVector GetPostPos(){ return post_pos; };
      G4ThreeVector GetPol(){ return pol; };
      G4String GetLogVolName() { return logicalVolume; };
      G4double GetGlobalTime() { return globalTime; };
      G4bool GetFirstStepInVolumeFlag() {return firstStepInVolume;};
      G4bool GetLastStepInVolumeFlag() {return lastStepInVolume;};
      G4double GetKineticEnergy() { return kineticEnergy; };
      G4double GetStepLength() {return stepLength; };
      G4double* GetBField () {return BF;};
      G4int GetRunID() {return runID;};
      G4int GetEventID() {return eventID;};
      G4double point[4];
      G4double B[6];
      const G4Field *mfield;

  private:

      G4String      particleName;
      G4int         particleID;
      G4int         trackID;
      G4double      edep;
      G4ThreeVector pre_pos;
      G4ThreeVector post_pos;
      G4ThreeVector pol;
      G4String      logicalVolume;
      G4double      globalTime;
      G4bool        firstStepInVolume;
      G4bool        lastStepInVolume;
      G4double      kineticEnergy;
      G4double      stepLength;
      G4int         eventID;
      G4int         runID;
      G4double      BF[6];

      static G4int ScintMultihit;
      static G4int runIDoldScint;
      static G4int eventIDoldScint;
      static G4int NIS;
      static G4int ScintChamberNbold;

      static G4int verboseLevel;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<lem4ScintHit> lem4ScintHitsCollection;

extern G4Allocator<lem4ScintHit> lem4ScintHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* lem4ScintHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) lem4ScintHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void lem4ScintHit::operator delete(void *aHit)
{
  lem4ScintHitAllocator.FreeSingle((lem4ScintHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
