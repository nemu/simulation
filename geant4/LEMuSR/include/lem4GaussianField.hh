//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "globals.hh"
#include "G4MagneticField.hh"

class lem4GaussianField
#ifndef STANDALONE
 : public G4MagneticField
#endif
{
private:
  G4double ffieldValue;
  G4double fieldMagnitude,RMS;
public:
  lem4GaussianField(double fieldValue, double sigma);
  void  GetFieldValue( const  double point[4], double *Bfield ) const;
  G4double GetFieldSetValue();
};
