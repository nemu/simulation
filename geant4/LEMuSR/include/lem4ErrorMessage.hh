//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#ifndef lem4ErrorMessage_h
#define lem4ErrorMessage_h 1

#include <map>
#include "globals.hh"
enum SEVERITY {INFO, WARNING, SERIOUS, FATAL};

typedef struct 
{
    SEVERITY mesSeverity;
    int nTimes;
} ErrorStruct;

class lem4ErrorMessage {
  public:
  //    enum SEVERITY {INFO, WARNING, SERIOUS, FATAL};
    lem4ErrorMessage();
   ~lem4ErrorMessage();
    static lem4ErrorMessage* GetInstance();
    void lem4Error(SEVERITY severity, G4String message, G4bool silent);
    void PrintErrorSummary();

  private:
    static lem4ErrorMessage* pointerToErrors;
    G4int nErrors;
  //    std::map<G4String,int> ErrorMapping;
    std::map<G4String,ErrorStruct> ErrorMapping;

    std::map<SEVERITY,G4String> severityWord;
};
#endif
