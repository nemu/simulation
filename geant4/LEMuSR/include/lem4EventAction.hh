//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
#ifndef lem4EventAction_h
#define lem4EventAction_h 1

#include "globals.hh"
#include "G4UserEventAction.hh"
#include <vector>
using namespace std;
class G4Timer;
class G4Event;
class lem4MagneticField;
class G4ElectricField; // Unif. el. field. TS
class lem4TabulatedField3D;
class lem4TabulatedField2D;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class lem4EventAction : public G4UserEventAction
{
  public:
    lem4EventAction();
   ~lem4EventAction();

  public:
    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);
    static lem4EventAction* GetInstance();

  private:
  //  pointer to this class
    static lem4EventAction* pointer;
  //
    G4Timer* timer;
    static vector<int> * pointerToSeedVector;
  //    Variables for the time-dependent magnetic field
    G4bool   timeDependentField;
    G4double fieldValueStart, fieldValueEnd, fieldStep, lastFieldValue;
    G4int    fieldNrOfSteps;
    G4int    maxEventNr;
    lem4MagneticField*     pointerToMusrUniformField;
    G4ElectricField*       pointerToMusrUniformEField; // Pointer to uniform electric field. TS
    lem4TabulatedField3D*  pointerToTabulatedField3D;
    lem4TabulatedField2D*  pointerToTabulatedField2D;
    G4int    latestEventNr;

  public:
    static G4bool setRandomNrSeedAccordingEventNr;
    static G4bool setRandomNrSeedFromFile;
    static G4int  nHowOftenToPrintEvent;
    static vector<int> * GetPointerToSeedVector();
  //  void setMyEventNr(long n) {myEventNr=n;};
    void SetTimeDependentField(G4bool setFieldToBeTimeDependend, G4double initialField,
			       G4double finalField, G4int nrOfSteps);
    void SetNumberOfEventsInThisRun(G4int nrOfEventsInThisRun) {maxEventNr=nrOfEventsInThisRun;};
    void SetPointerToMusrUniformField(lem4MagneticField* pointer) {pointerToMusrUniformField=pointer;};
    void SetPointerToMusrUniformEField(G4ElectricField* pointer) {pointerToMusrUniformEField=pointer;}; // Unif. El. field. TS
    void SetPointerToTabulatedField3D(lem4TabulatedField3D* pointer) {pointerToTabulatedField3D=pointer;};
    void SetPointerToTabulatedField2D(lem4TabulatedField2D* pointer) {pointerToTabulatedField2D=pointer;};
    G4int GetLatestEventNr(){return latestEventNr;};
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
