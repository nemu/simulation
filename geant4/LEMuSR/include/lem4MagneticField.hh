//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef lem4MagneticField_H
#define lem4MagneticField_H

#include "G4UniformMagField.hh"

class G4FieldManager;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


class lem4MagneticField: public G4UniformMagField
{
  public:
  
   lem4MagneticField(G4ThreeVector);  //  The value of the field
   lem4MagneticField();               //  A zero field
  ~lem4MagneticField();  
      
   //Set the field (fieldValue,0,0)
   void SetFieldValue(G4double fieldValue);
   void SetFieldValue(G4ThreeVector fieldVector);
      
   G4ThreeVector GetConstantFieldValue();

  protected:

      // Find the global Field Manager
      G4FieldManager* GetGlobalFieldManager();   // static 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


G4bool CheckOverlaps(G4int res=1000, G4double tol=0., G4bool verbose=true);
