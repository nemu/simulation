//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "lem4DetectorConstruction.hh"
#include "lem4PhysicsList.hh"
#include "lem4PrimaryGeneratorAction.hh"
#include "lem4RunAction.hh"
#include "lem4EventAction.hh"
#include "lem4SteppingAction.hh"
#include "lem4SteppingVerbose.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

#include "Randomize.hh"

#include <X11/Xlib.h>

#ifdef G4VIS_USE
  //#include "lem4VisManager.hh"
  #include "G4VisExecutive.hh"  // TS Trajectory drawing by ID or charge
  #include "G4TrajectoryDrawByCharge.hh"
#endif

#include "lem4RootOutput.hh"
#include "lem4Parameters.hh"
#include "lem4ErrorMessage.hh"

int main(int argc,char** argv) {

  XInitThreads();

  // choose the Random engine
  //  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);   // the /lem4/run/randomOption 2 does not work with RanecuEngine
  CLHEP::HepRandom::setTheEngine(new CLHEP::HepJamesRandom);

  //my Verbose output class
  G4VSteppingVerbose::SetInstance(new lem4SteppingVerbose);
  
  // Run manager
  G4RunManager * runManager = new G4RunManager;
 
 // Create class "myParameters", which is a collection of many different parameters
  G4String steeringFileName=argv[1];
  lem4Parameters* myParameters = new lem4Parameters(steeringFileName);
//  lem4Parameters* myParameters = new lem4Parameters();

  // Create class "lem4ErrorMessage"
  lem4ErrorMessage* myErrorMessage = new lem4ErrorMessage();

  // Create Root class for storing the output of the Geant simulation
  lem4RootOutput* myRootOutput = new lem4RootOutput();

  // UserInitialization classes (mandatory)
  lem4DetectorConstruction* lem4detector = new lem4DetectorConstruction;
  //  if(argc>2) { lem4detector->SetInputParameterFileName(argv[2]); }
  //  else 
  if (argc>1) {
    G4int myRunNr=atoi(argv[1]);       // Get the run number from the name of the 
                                       // parameter file, if it starts with a number.
    if (myRunNr>0)  {runManager->SetRunIDCounter(myRunNr);}
    lem4detector->SetInputParameterFileName(argv[1]);
  }
  runManager->SetUserInitialization(lem4detector);
  runManager->SetUserInitialization(new lem4PhysicsList);
  
#ifdef G4VIS_USE
  // Visualization, if you choose to have it!
  G4VisManager* visManager = new G4VisExecutive; // lem4VisManager; TS Trajectory drawing by ID or charge
  visManager->Initialize();
#endif
   
  // UserAction classes
  runManager->SetUserAction(new lem4PrimaryGeneratorAction(lem4detector));
  runManager->SetUserAction(new lem4RunAction);  
  runManager->SetUserAction(new lem4EventAction);
  runManager->SetUserAction(new lem4SteppingAction);

  //Initialize G4 kernel
  runManager->Initialize();
      
  //get the pointer to the User Interface manager 
  G4UImanager * UI = G4UImanager::GetUIpointer();  

  if(argc==1)
  // Define (G)UI terminal for interactive mode  
  { 
    // G4UIterminal is a (dumb) terminal.
    G4UIsession * session = 0;
#ifdef G4UI_USE_TCSH
      session = new G4UIterminal(new G4UItcsh);      
#else
      session = new G4UIterminal();
#endif    

    UI->ApplyCommand("/control/execute vis.mac");    
    session->SessionStart();
    delete session;
  }
  else
  // Batch mode
  { 
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UI->ApplyCommand(command+fileName);
    if (argc>2) {
      G4String SecondArgument = argv[2];
      if (SecondArgument=="idle") {
	G4UIsession * session = 0;
#ifdef G4UI_USE_TCSH
	session = new G4UIterminal(new G4UItcsh);      
#else
	session = new G4UIterminal();
#endif    
	session->SessionStart();
	delete session;
      }
    }
  }

#ifdef G4VIS_USE
  delete visManager;
#endif
  delete myRootOutput;
  delete myErrorMessage;
  delete myParameters;
  delete runManager;

  return 0;
}



