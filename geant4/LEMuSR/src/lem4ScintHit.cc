//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "lem4ScintHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"
#include "G4MagneticField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4ProcessManager.hh"
#include "G4Track.hh"
#include "G4ThreeVector.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include <fstream>
#include <iostream>
#include <iomanip>

G4Allocator<lem4ScintHit> lem4ScintHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4ScintHit::lem4ScintHit() {
}

G4int lem4ScintHit::ScintMultihit=0;
G4int lem4ScintHit::runIDoldScint=-1;
G4int lem4ScintHit::eventIDoldScint=-1;
G4int lem4ScintHit::NIS=0;
G4int lem4ScintHit::ScintChamberNbold=-1;
G4int lem4ScintHit::verboseLevel=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4ScintHit::~lem4ScintHit() {
  //save the Tree header. The file will be automatically closed
  //when going out of the function scope
  //  rootTree->Write();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4ScintHit::lem4ScintHit(const lem4ScintHit& right)
  : G4VHit()
{
  particleName = right.particleName;
  trackID   = right.trackID;
  edep      = right.edep;
  pre_pos       = right.pre_pos;
  pol       = right.pol;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const lem4ScintHit& lem4ScintHit::operator=(const lem4ScintHit& right)
{
  particleName = right.particleName;
  trackID   = right.trackID;
  edep      = right.edep;
  pre_pos       = right.pre_pos;
  pol       = right.pol;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int lem4ScintHit::operator==(const lem4ScintHit& right) const
{
  return (this==&right) ? 1 : 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4ScintHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(pre_pos);
    circle.SetScreenSize(0.04);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,0.,0.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4ScintHit::Print()
{
  if (verboseLevel>2) G4cout<<"VERBOSE 3: Kamil:  lem4ScintHit::Print()"<<G4endl;
  
  G4RunManager* fRunManager = G4RunManager::GetRunManager();
  eventID = fRunManager->GetCurrentEvent()->GetEventID();
  runID   = fRunManager->GetCurrentRun()->GetRunID();
  G4int ScintMultihitSwitch=0;
  
  if (runID != runIDoldScint)  {
      NIS=0;
      ScintMultihit = 0;
      eventIDoldScint = -1;
      ScintChamberNbold = -1;
    }
  
  //cks  if (particleName== "e+" and (eventIDoldScint != eventID or ScintChamberNbold != IBchamberNb)) {
  if (particleName== "e+") {
    if (eventIDoldScint == eventID) {
      ScintMultihit++;
      ScintMultihitSwitch=1;
    }
    NIS++;

    G4FieldManager *fMgr=G4TransportationManager::GetTransportationManager()->GetFieldManager();
    point[0]=0.;
    point[1]=0.;
    point[2]=0.;
    B[2]=0.0;
    if(!fMgr->DoesFieldChangeEnergy())  {          //then we have a magnetic field
      mfield = fMgr->GetDetectorField();
      mfield->GetFieldValue(point,B);
      B[0]=B[0]/tesla;
      B[1]=B[1]/tesla;
      B[2]=B[2]/tesla;
    }
    //      G4cout << "  Segment: " << IBchamberNb << G4endl;
    //      G4cout <<"Position " << pos.x()/cm<<" "<<pos.y()/cm <<" "<< pos.z()/cm <<G4endl;
    //	 G4cout << "Field is "<< B[2]<<G4endl;
    
    std::ofstream posfile1;
    posfile1.open ("scint.dat", std::ios::out | std::ios::app);
    posfile1 << runID << " " << eventID 
	   << " " << logicalVolume
	   << " " << ScintMultihitSwitch 
	   <<" " << edep
	   << " " << fabs(B[2]) 
           <<" "<< pre_pos.x()/cm<<" "<<pre_pos.y()/cm <<" "<< pre_pos.z()/cm  
	   << " " << globalTime/s 
        // << " " << IBchamberNb 
      //	     << " first=" << firstStepInVolume << " last=" << lastStepInVolume
	   << G4endl;
    posfile1.close();
  }
  eventIDoldScint=eventID;
  runIDoldScint = runID;
  //  ScintChamberNbold = IBchamberNb; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

