//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "lem4TabulatedField2D.hh"
//#include "lem4EventAction.hh"    // cks: just to get the event nr. if needed.
#include "lem4Parameters.hh"


lem4TabulatedField2D* lem4TabulatedField2D::pointerToTabulatedField2D=NULL;
lem4TabulatedField2D* lem4TabulatedField2D::GetInstance() {
  return pointerToTabulatedField2D;
}

lem4TabulatedField2D::lem4TabulatedField2D( const char* filename, double fieldValue, double lenUnit, double fieldNormalisation ) 
  :ffieldValue(fieldValue),invertX(false),invertZ(false)
{    
  pointerToTabulatedField2D=this;
  G4cout << "pointerToTabulatedField2D="<<pointerToTabulatedField2D<<G4endl;
  //  double lenUnit= meter;
  //  double fieldUnit= tesla; 
  G4cout << "\n-----------------------------------------------------------"
	 << "\n      Magnetic field"
	 << "\n-----------------------------------------------------------"
         << G4endl;
  G4cout << "  tesla="<<tesla<<G4endl;
  G4cout << " Field set to "<< fieldValue/tesla << " T"<< G4endl;  
  G4cout << "\n ---> " "Reading the field grid from " << filename << " ... " << G4endl; 
  positionOffset[0]=0;
  positionOffset[1]=0;
  positionOffset[2]=0;
  positionOffset[3]=0;
  std::ifstream file( filename ); // Open the file for reading.
  
  // Ignore first blank line
  char buffer[256];
  //  file.getline(buffer,256);
  
  // Read table dimensions 
  int nDummy;
  file >> nx >> nDummy >> nz; // Note dodgy order

  G4cout << "  [ Number of values x,z: " 
	 << nx << " " << nz << " ] "
	 << G4endl;

  // Set up storage space for table
  xField.resize( nx );
  zField.resize( nx );
  int ix, iz;
  for (ix=0; ix<nx; ix++) {
    xField[ix].resize(nz);
    zField[ix].resize(nz);
  }
  
  // Ignore other header information    
  // The first line whose second character is '0' is considered to
  // be the last line of the header.
  do {
    file.getline(buffer,256);
  } while ( buffer[1]!='0');
  
  // Read in the data
  double xval,yval,zval,bx,bz;
  double permeability; // Not used in this example.
  for (ix=0; ix<nx; ix++) {
    for (iz=0; iz<nz; iz++) {
      file >> xval >> yval >> zval >> bx >> bz >> permeability;
      if ( ix==0 && iz==0 ) {
	minx = xval * lenUnit;
	minz = zval * lenUnit;
      }
      //xField[ix][iy][iz] = bx * fieldUnit * fieldValue;
      //zField[ix][iy][iz] = bz * fieldUnit * fieldValue;
      //        xField[ix][iy][iz] = bx * fieldValue;
      //        zField[ix][iy][iz] = bz * fieldValue;
      xField[ix][iz] = bx*fieldNormalisation;
      zField[ix][iz] = bz*fieldNormalisation;
    }
  }
  file.close();

  maxx = xval * lenUnit;
  maxz = zval * lenUnit;

  G4cout << "\n ---> ... done reading " << G4endl;

  // G4cout << " Read values of field from file " << filename << G4endl; 
  G4cout << " ---> assumed the order:  x, y, z, Bx, Bz "
	 << "\n ---> Min values x,y,z: " 
	 << minx/cm << " " << minz/cm << " cm "
	 << "\n ---> Max values x,y,z: " 
	 << maxx/cm << " " << maxz/cm << " cm " << G4endl;


  // Should really check that the limits are not the wrong way around.
  if (maxx < minx) {std::swap(maxx,minx); invertX = true;} 
  if (maxz < minz) {std::swap(maxz,minz); invertZ = true;} 
  G4cout << "\nAfter reordering if neccesary"  
	 << "\n ---> Min values x,y,z: " 
	 << minx/cm << " " << minz/cm << " cm "
	 << " \n ---> Max values x,y,z: " 
	 << maxx/cm << " " << maxz/cm << " cm ";

  dx = maxx - minx;
  dz = maxz - minz;
  G4cout << "\n ---> Dif values x,z (range): " 
	 << dx/cm << " " << dz/cm << " cm in z "
	 << "\n-----------------------------------------------------------" << G4endl;
}

void lem4TabulatedField2D::GetFieldValue(const double point[4],
				      double *Bfield ) const
{
  //  G4cout<<"Tabulated: Field requested at point ("<<point[0]<<","<<point[1]<<","<<point[2]<<")"<<G4endl;
  //  lem4EventAction* myEventAction= lem4EventAction::GetInstance();
  //  G4int evNr=myEventAction->GetLatestEventNr();
  //  G4int evNrKriz=6795;  //457;  //14250
  //  if (evNr==evNrKriz) {
  //    std::cout<<"evNr="<<evNr<<std::endl;
  //    printf ("for point= %f %f %f   B= %10.10f %10.10f %10.10f \n", 
  //    	    point[0],point[1],point[2],Bfield[0]/tesla,Bfield[1]/tesla,Bfield[2]/tesla);
  //  }
  Bfield[0]=0.; Bfield[1]=0.; Bfield[2]=0.; 

  double pppoint[4];
  pppoint[0]=point[0]+positionOffset[0];
  pppoint[1]=point[1]+positionOffset[1];
  pppoint[2]=point[2]+positionOffset[2];
  pppoint[3]=point[3]+positionOffset[3];
  double x = sqrt(pppoint[0]*pppoint[0]+pppoint[1]*pppoint[1]);
  double z = fabs(pppoint[2]);
  double z_sign = (pppoint[2]>0) ? 1.:-1.;
  //if (evNr==evNrKriz) std::cout<<"x ="<<x<<"   maxx="<<maxx<<std::endl;

  // Check that the point is within the defined region 
  if ( x<maxx && z<maxz ) {
    // if (evNr>evNrKriz) std::cout<<"bol som tu"<<std::endl;
    
    // Position of given point within region, normalized to the range
    // [0,1]
    double xfraction = (x - minx) / dx;
    double zfraction = (z - minz) / dz;

    if (invertX) { xfraction = 1 - xfraction;}
    if (invertZ) { zfraction = 1 - zfraction;}
    // Need addresses of these to pass to modf below.
    // modf uses its second argument as an OUTPUT argument.
    double xdindex, zdindex;
    
    // Position of the point within the cuboid defined by the
    // nearest surrounding tabulated points
    double xlocal = ( modf(xfraction*(nx-1), &xdindex));
    double zlocal = ( modf(zfraction*(nz-1), &zdindex));
    
    // The indices of the nearest tabulated point whose coordinates
    // are all less than those of the given point
    int xindex = static_cast<int>(xdindex);
    int zindex = static_cast<int>(zdindex);

    //cks  The following check is necessary - even though xindex and zindex should never be out of range,
    //     it may happen (due to some rounding error ?).  It is better to leave the check here.
    if ((xindex<0)||(xindex>(nx-2))) {
      std::cout<<"SERIOUS PROBLEM:  xindex out of range!  xindex="<<xindex<<"   x="<<x<<"  xfraction="<<xfraction<<std::endl;
      if (xindex<0) xindex=0;
      else xindex=nx-2;
    }
    if ((zindex<0)||(zindex>(nz-2))) {
      std::cout<<"SERIOUS PROBLEM:  zindex out of range!  zindex="<<zindex<<"   z="<<z<<"  zfraction="<<zfraction<<std::endl;
      if (zindex<0) zindex=0;
      else zindex=nz-2;
    }

    // Find out whether the muon is decaying.  If yes, calculate the field in
    // more detail (more precisely).
    //         The following commented piece of code was not reliable (was indicating the
    //         proccess "DecayWithSpin" even if there was none).
    //    G4String processName = G4RunManagerKernel::GetRunManagerKernel()->
    //   //    G4String processName = G4EventManager::GetEventManager()->
    //                      GetTrackingManager()->GetTrack()->GetStep()->
    //                      GetPostStepPoint()->GetProcessDefinedStep()->
    //                      GetProcessName();
    //         The following seems to be OK:
    //    if (lem4Parameters::field_DecayWithSpin) {
    //      lem4Parameters::field_DecayWithSpin=false;
    //
    //    }
    //    else {  // Muon is not decaying, use a fast method of interpolation.
      // Interpolate between the neighbouring points
      double Bfield_R =
	xField[xindex  ][zindex  ] * (1-xlocal) * (1-zlocal) +
	xField[xindex  ][zindex+1] * (1-xlocal) *    zlocal  +
	xField[xindex+1][zindex  ] *    xlocal  * (1-zlocal) +
	xField[xindex+1][zindex+1] *    xlocal  *    zlocal  ;
      Bfield[0] = (x>0) ? Bfield_R * (pppoint[0]/x) : 0.;
      Bfield[1] = (x>0) ? Bfield_R * (pppoint[1]/x) : 0.;
      Bfield[2] =
	zField[xindex  ][zindex  ] * (1-xlocal) * (1-zlocal) +
	zField[xindex  ][zindex+1] * (1-xlocal) *    zlocal  +
	zField[xindex+1][zindex  ] *    xlocal  * (1-zlocal) +
	zField[xindex+1][zindex+1] *    xlocal  *    zlocal  ;
      
      Bfield[0] = Bfield[0] * ffieldValue * z_sign;
      Bfield[1] = Bfield[1] * ffieldValue * z_sign;
      Bfield[2] = Bfield[2] * ffieldValue;
      //    } 
  }
  
  // Set some small field if field is almost zero (to avoid internal problems of Geant).
  if (sqrt(Bfield[0]*Bfield[0]+Bfield[1]*Bfield[1]+Bfield[2]*Bfield[2])<0.00001*tesla) {
    //    if (evNr>evNrKriz) std::cout<<"bol som tuna"<<std::endl;
    //    Bfield[0] = 0.0;
    //    Bfield[1] = 0.0;
    //    Bfield[2] = Bfield[2] + 0.00001*tesla;
    Bfield[2] = 0.00001*tesla;
  }
  //  Print the field (for debugging)
  //  if ((point[2]>-0.1*mm)&&(point[2]<0.1*mm)) {
  //    printf ("for point= %f %f %f   B= %10.10f %10.10f %10.10f \n", 
  //    	    point[0],point[1],point[2],Bfield[0]/tesla,Bfield[1]/tesla,Bfield[2]/tesla);
  //  }
  //  if (evNr>evNrKriz) std::cout<<"this is the end"<<std::endl;
}

G4double lem4TabulatedField2D::GetFieldSetValue() {
  return ffieldValue;
}

void lem4TabulatedField2D::SetFieldValue(double newFieldValue) {
  //  // Rescale the magnetic field for a new value of the magnetic field
  ffieldValue=newFieldValue;
  G4cout<<"lem4TabulatedField2D.cc:   ffieldValue changed to="<< ffieldValue/tesla<<"T "<<G4endl;
}
