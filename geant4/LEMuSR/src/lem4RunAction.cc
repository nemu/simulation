//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// Make G4Timer appear first!
#include "G4Timer.hh"
#include "lem4RunAction.hh"
#include "lem4EventAction.hh"
#include "G4Run.hh"
#include "lem4ErrorMessage.hh"
#include "F04GlobalField.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4RunAction::lem4RunAction() {
  timer = new G4Timer;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4RunAction::~lem4RunAction() {
  delete timer;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4RunAction::BeginOfRunAction(const G4Run* aRun)  {
  timer->Start();
  G4int run_id= aRun->GetRunID();
  //  if (run_id%100 == 0) {
  G4cout << "### Run " << run_id << G4endl;
  //  }
  // Set nr. of events that will be processed in this run to lem4EventAction class, as it will
  // be needed for the time dependent magnetic field;
  lem4EventAction* pointerToEventAction = lem4EventAction::GetInstance();
  pointerToEventAction->SetNumberOfEventsInThisRun(aRun->GetNumberOfEventToBeProcessed());
  //
  lem4RootOutput::GetRootInstance()->BeginOfRunAction();
  //
  // Initiate global electromagnetic field (if it exists):
  if (F04GlobalField::Exists()) {
    FieldList* fields = F04GlobalField::getObject()->getFields();

    if (fields) {
      if (fields->size()>0) {
        G4int jjj=0;
	G4cout<<"\n------------ The following fields were defined:  ----------------\n"<<G4endl;
	FieldList::iterator i;
	for (i=fields->begin(); i!=fields->end(); ++i) { 
	(*i)->construct();
	// Get the nominal field value for the given field and store it in the Root output
	G4double nomFieldValue = (*i)->GetNominalFieldValue();
	lem4RootOutput::GetRootInstance()->SetFieldNomVal(jjj,nomFieldValue);
	jjj++;
	}
	G4cout<<"-----------------------------------------------------------------"<<G4endl;
      }
    }  
    
    // Print out the field values at the points user requested to be printed out:
    F04GlobalField::getObject()->PrintFieldAtRequestedPoints();
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4RunAction::EndOfRunAction(const G4Run* aRun)  { 
  lem4RootOutput::GetRootInstance()->EndOfRunAction();
  lem4ErrorMessage::GetInstance()->PrintErrorSummary();
  timer->Stop();
  G4cout << "lem4RunAction::EndOfRunAction:"<<G4endl;
  G4cout << "    Number of events    = " << aRun->GetNumberOfEvent()<<G4endl;
  //         << " " << *timer << G4endl;
  G4cout << "    User elapsed time   = "<<timer->GetUserElapsed()/3600<<"h   = "
	 <<timer->GetUserElapsed()/60<<"min   = "<<timer->GetUserElapsed()<<"s."<<G4endl;
  G4cout << "    Real elapsed time   = "<<timer->GetRealElapsed()/3600<<"h   = "
	 <<timer->GetRealElapsed()/60<<"min   = "<<timer->GetRealElapsed()<<"s."<<G4endl;
  G4cout << "    System elapsed time = "<<timer->GetSystemElapsed()/3600<<"h   = "
	 <<timer->GetSystemElapsed()/60<<"min   = "<<timer->GetSystemElapsed()<<"s."<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



