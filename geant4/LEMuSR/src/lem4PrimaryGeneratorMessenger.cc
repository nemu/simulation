//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "lem4PrimaryGeneratorMessenger.hh"
#include "lem4PrimaryGeneratorAction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

lem4PrimaryGeneratorMessenger::lem4PrimaryGeneratorMessenger(lem4PrimaryGeneratorAction* lem4Gun)
:lem4Action(lem4Gun)
{ 
  setvertexCmd = new G4UIcmdWith3VectorAndUnit("/gun/vertex",this);
  setvertexCmd->SetGuidance(" Set x0, y0, z0 of the generated muons (with unit)");
  setvertexCmd->SetParameterName("mes_x0","mes_y0","mes_z0",true,true);
  setvertexCmd->SetDefaultUnit("mm");

  setvertexSigmaCmd = new G4UIcmdWith3VectorAndUnit("/gun/vertexsigma",this);
  setvertexSigmaCmd->SetGuidance(" Set xSigma, ySigma, ySigma of the generated muons (with unit)");
  setvertexSigmaCmd->SetParameterName("mes_xSigma","mes_ySigma","mes_zSigma",true,true);
  setvertexSigmaCmd->SetDefaultUnit("mm");

  setvertexBoundaryCmd = new G4UIcmdWith3VectorAndUnit("/gun/vertexboundary",this);
  setvertexBoundaryCmd->SetGuidance(" Set maximum allowed radius, zmin, zmax of the generated vertex (with unit)");
  setvertexBoundaryCmd->SetParameterName("mes_rMaxAllowed","mes_zMinAllowed","mes_zMaxAllowed",true,true);
  setvertexBoundaryCmd->SetDefaultUnit("mm");
  
  setEnergyCmd = new G4UIcmdWithADoubleAndUnit("/gun/kenergy",this);
  setEnergyCmd->SetGuidance(" Set kinetic energy of the generated muons (with unit)");
  setEnergyCmd->SetParameterName("mes_E0",true);
  setEnergyCmd->SetDefaultUnit("keV");
//setEnergyCmd->SetUnitCandidates("eV keV MeV GeV");
  
  setMomentumCmd = new G4UIcmdWithADoubleAndUnit("/gun/momentum",this);
  setMomentumCmd->SetGuidance(" Set mean momentum of the generated muons (with unit)");
  setMomentumCmd->SetParameterName("mes_p0",true);
  setMomentumCmd->SetDefaultUnit("MeV");

  setMomentumSmearingCmd = new G4UIcmdWithADoubleAndUnit("/gun/momentumsmearing",this);
  setMomentumSmearingCmd->SetGuidance(" Set sigma of the momentum of the generated muons (with unit)");
  setMomentumSmearingCmd->SetParameterName("mes_pSigma",true);
  setMomentumSmearingCmd->SetDefaultUnit("MeV");

  setMomentumBoundaryCmd = new G4UIcmdWith3VectorAndUnit("/gun/momentumboundary",this);
  setMomentumBoundaryCmd->SetGuidance(" Set minimum and maximum momentum allowed (with unit, z component ignored)");
  setMomentumBoundaryCmd->SetParameterName("mes_pMinAllowed","mes_pMaxAllowed","mes_dummy",true,true);
  setMomentumBoundaryCmd->SetDefaultUnit("MeV");

  setTiltAngleCmd = new G4UIcmdWith3VectorAndUnit("/gun/tilt",this);
  setTiltAngleCmd->SetGuidance(" Set tilt angle of the generated muons (with unit, z component ignored)");
  setTiltAngleCmd->SetParameterName("mes_xangle","mes_yangle","dummy",true,true);
  setTiltAngleCmd->SetDefaultUnit("deg");

  setSigmaTiltAngleCmd = new G4UIcmdWith3VectorAndUnit("/gun/tiltsigma",this);
  setSigmaTiltAngleCmd->SetGuidance(" Set sigma of the tilt angle (with unit, z component ignored)");
  setSigmaTiltAngleCmd->SetParameterName("mes_xangleSigma","mes_yangleSigma","mes_zangleSigma",true,true);
  setSigmaTiltAngleCmd->SetDefaultUnit("deg");

  setPitchCmd = new G4UIcmdWithADoubleAndUnit("/gun/pitch",this);
  setPitchCmd->SetGuidance(" Set pitch angle of the generated muons (with unit)");
  setPitchCmd->SetParameterName("mes_pitch",true);
  setPitchCmd->SetDefaultUnit("deg");

  //  setMuonPolarizCmd = new G4UIcmdWithAnInteger("/gun/muonpolarization",this);
  //  setMuonPolarizCmd->SetGuidance(" Set initial mu polariz: 0=transverse, 1=longitudinal ");
  //  setMuonPolarizCmd->SetParameterName("IniPol",true);
  //  setMuonPolarizCmd->SetDefaultValue(0) ;

  setMuonPolarizCmd = new G4UIcmdWith3Vector("/gun/muonPolarizVector",this);
  setMuonPolarizCmd->SetGuidance("Set initial mu polarisation as a vector (without unit)");
  setMuonPolarizCmd->SetGuidance("    The vector does not have to be normalised to 1");
  setMuonPolarizCmd->SetParameterName("mes_polarisX","mes_polarisY","mes_polarisZ",true,true);

  setMuonDecayTimeCmd = new G4UIcmdWith3VectorAndUnit("/gun/decaytimelimits",this);
  setMuonDecayTimeCmd->SetGuidance(" Set minimum and maximum decay time and the muon mean life ");
  setMuonDecayTimeCmd->SetParameterName("decayMin","decayMax","decayTime",true,true);
  setMuonDecayTimeCmd->SetDefaultUnit("ns");

  setTurtleCmd = new G4UIcmdWithAString("/gun/turtlefilename",this);
  setTurtleCmd->SetGuidance("Set the filename of the TURTLE input file.");
  setTurtleCmd->SetGuidance("If this varialble is set, TURTLE input will be used. for initial muons");
  setTurtleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

lem4PrimaryGeneratorMessenger::~lem4PrimaryGeneratorMessenger()
{
  delete setvertexCmd;
  delete setvertexSigmaCmd;
  delete setvertexBoundaryCmd;
  delete setEnergyCmd;
  delete setMomentumCmd;
  delete setMomentumSmearingCmd;
  delete setMomentumBoundaryCmd;
  delete setTiltAngleCmd;
  delete setSigmaTiltAngleCmd;
  delete setPitchCmd;
  delete setMuonPolarizCmd;
  delete setMuonDecayTimeCmd;
  delete setTurtleCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void lem4PrimaryGeneratorMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{ 
  if( command == setvertexCmd)
   { lem4Action->Setvertex(setvertexCmd->GetNew3VectorValue(newValue));}
  else if( command == setvertexSigmaCmd)
   { lem4Action->SetvertexSigma(setvertexSigmaCmd->GetNew3VectorValue(newValue));}
  else if( command == setvertexBoundaryCmd)
   { lem4Action->SetvertexBoundary(setvertexBoundaryCmd->GetNew3VectorValue(newValue));}
  else if( command == setEnergyCmd)
   { lem4Action->SetEnergy(setEnergyCmd->GetNewDoubleValue(newValue));}
  else if( command == setMomentumCmd)
   { lem4Action->SetMomentum(setMomentumCmd->GetNewDoubleValue(newValue));}
  else if( command == setMomentumSmearingCmd)
    { lem4Action->SetMomentumSmearing(setMomentumSmearingCmd->GetNewDoubleValue(newValue));}
  else if( command == setMomentumBoundaryCmd)
   { lem4Action->SetMomentumBoundary(setMomentumBoundaryCmd->GetNew3VectorValue(newValue));}
  else if( command == setTiltAngleCmd)
   { lem4Action->SetTilt(setTiltAngleCmd->GetNew3VectorValue(newValue));}
  else if( command == setSigmaTiltAngleCmd)
   { lem4Action->SetSigmaTilt(setSigmaTiltAngleCmd->GetNew3VectorValue(newValue));}
  else if( command == setPitchCmd)
    { lem4Action->SetPitch(setPitchCmd->GetNewDoubleValue(newValue));}
  else if( command == setMuonPolarizCmd)
    { lem4Action->SetInitialMuonPolariz(setMuonPolarizCmd->GetNew3VectorValue(newValue));}
  else if( command == setMuonDecayTimeCmd)
    { lem4Action->SetMuonDecayTimeLimits(setMuonDecayTimeCmd->GetNew3VectorValue(newValue)); }
  else if ( command == setTurtleCmd)
    { lem4Action->SetTurtleInput(newValue); }
}
