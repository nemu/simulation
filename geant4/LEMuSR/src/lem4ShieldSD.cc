//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "lem4ShieldSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  ///
  /// The Shield sensitive detector. This object collects the
  /// information of a particle's interaction with this part
  /// of the geometry
  ///

lem4ShieldSD::lem4ShieldSD(G4String name)
:G4VSensitiveDetector(name)
{
  G4String HCname;
  collectionName.insert(HCname="shieldCollection");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4ShieldSD::~lem4ShieldSD(){ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4ShieldSD::Initialize(G4HCofThisEvent* HCE)
{
  shieldCollection = new lem4ShieldHitsCollection
                          (SensitiveDetectorName,collectionName[0]); 
  static G4int HCID = -1;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection( HCID, shieldCollection ); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool lem4ShieldSD::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{
  ///
  /// Collect the information regarding the interaction of a particle
  /// and the sensitive detector.
  ///
  G4double edep = aStep->GetTotalEnergyDeposit();

  if(edep==0.) return false;

  lem4ShieldHit* newHit = new lem4ShieldHit();
  newHit->SetParticleName (aStep->GetTrack()->GetDefinition()->GetParticleName());
  newHit->SetTrackID  (aStep->GetTrack()->GetTrackID());
  newHit->SetEdep     (edep);
  newHit->SetPos      (aStep->GetPostStepPoint()->GetPosition());
  newHit->SetPol      (aStep->GetTrack()->GetPolarization());
  newHit->SetLogVolName (aStep->GetTrack()->GetVolume()->GetLogicalVolume()->GetName());
  shieldCollection->insert( newHit );
  
  newHit->Print();
  newHit->Draw();



  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4ShieldSD::EndOfEvent(G4HCofThisEvent*)
{
  if (verboseLevel>0) { 
     G4int NbHits = shieldCollection->entries();
     G4cout << "\n-------->Hits Collection: in this event they are " << NbHits 
            << " hits: " << G4endl;
     for (G4int i=0;i<NbHits;i++) (*shieldCollection)[i]->Print();
    } 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

