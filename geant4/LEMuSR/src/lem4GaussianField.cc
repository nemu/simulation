//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "lem4GaussianField.hh"


lem4GaussianField::lem4GaussianField(double fieldValue, double sigma):ffieldValue(fieldValue) {
  fieldMagnitude=fieldValue;
  RMS=sigma;
  G4cout << "\n-----------------------------------------------------------"
	 << "\n         Magnetic field ON"
	 << "\n-----------------------------------------------------------"
         << G4endl;
  //  G4cout << " Gaussian magnetic field set to "<< fieldValue*1000 << " T,  with sigma in R="<< sigma<<" mm"<< G4endl;  
  G4cout << "Gaussian magnetic field set to "<< fieldValue/tesla << " Tesla, with a radial sigma "<< sigma/mm<<" mm.\n"<< G4endl;  
}

void lem4GaussianField::GetFieldValue(const double point[4], double *Bfield ) const {
  double x = point[0];
  double y = point[1];
  //  double z = point[2];
  double r = sqrt(x*x+y*y);
  Bfield[0] = 0;
  Bfield[1] = 0;
  Bfield[2] = fieldMagnitude*exp(-r*r/(2*RMS*RMS));
  //  G4cout<<"z="<<z<<"  r="<<r<<"  B[2]="<<Bfield[2]<<G4endl;
}

G4double lem4GaussianField::GetFieldSetValue() {
  return ffieldValue;
}
