//  Geant4 simulation for MuSR
//  AUTHOR: Toni SHIROKA, Paul Scherrer Institut, PSI
//  DATE  : 2008-05
//

#include "lem4DetectorMessenger.hh"
#include "lem4DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"

#include "G4RunManager.hh"   //cks  included in order to be able to change run ID
#include "Randomize.hh"      //cks  included in order to initialise the random nr. generator by time
#include <time.h>            //cks   -----------------------------||-------------------------------
#include "lem4EventAction.hh" // cks needed for the initialisation of the random nr. generator by event nr.
//#include <fstream>
//#include <vector>
#include "globals.hh"
//#include "lem4Parameters.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


lem4DetectorMessenger::lem4DetectorMessenger(lem4DetectorConstruction* myDet)
  :myDetector(myDet)
{ 
  lem4Dir = new G4UIdirectory("/lem4/");
  lem4Dir->SetGuidance("UI commands specific to this example.");

  CommandCmd = new G4UIcmdWithAString("/lem4/command",this);
  CommandCmd->SetGuidance("This command will be used for the detector construction (and ignored by the default messenger.");
  //  CommandCmd->SetParameterName("choice",false);
  CommandCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  runDir = new G4UIdirectory("/lem4/run/");
  runDir->SetGuidance("lem4 run control");

  RunIDSetCmd = new G4UIcmdWithAnInteger("/lem4/run/runID",this);
  RunIDSetCmd->SetGuidance("Set the run number");
  RunIDSetCmd->SetParameterName("something",false);
  RunIDSetCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  RandomOptionCmd = new G4UIcmdWithAnInteger("/lem4/run/randomOption",this);
  RandomOptionCmd->SetGuidance("Specify the random number generator initialisation");
  RandomOptionCmd->SetGuidance("   0 ... no initialisation (default)");
  RandomOptionCmd->SetGuidance("   1 ... use actual computer time to initialise now");
  RandomOptionCmd->SetGuidance("   2 ... use event number to initialise at the beginning of each event");
  RandomOptionCmd->SetGuidance("   3 ... read in the random no. initial values for each event from a file");
  RandomOptionCmd->SetParameterName("randomOpt",false);
  RandomOptionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  HowOftenToPrintEventCmd = new G4UIcmdWithAnInteger("/lem4/run/howOftenToPrintEvent",this);
  HowOftenToPrintEventCmd->SetGuidance("Each n-th event will be notified.  Set _n_ by this command.");
  HowOftenToPrintEventCmd->SetParameterName("howOftenToPrintEv",false);
  HowOftenToPrintEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  detDir = new G4UIdirectory("/lem4/det/");
  detDir->SetGuidance("detector control.");

  //  WhichProcessesCmd = new G4UIcmdWithAString("/lem4/det/processes",this);
  //  WhichProcessesCmd ->SetGuidance("Select Standard, Low Energy or Penelope processes");
  //  WhichProcessesCmd ->SetParameterName("mes_processes",false);
  //  WhichProcessesCmd ->AvailableForStates(G4State_PreInit,G4State_Idle);

  UpdateCmd = new G4UIcmdWithoutParameter("/lem4/det/update",this);
  UpdateCmd->SetGuidance("Update calorimeter geometry.");
  UpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
  UpdateCmd->SetGuidance("if you changed geometrical value(s).");
  UpdateCmd->AvailableForStates(G4State_Idle);
 
  //  FieldCmd = new G4UIcmdWithADoubleAndUnit("/lem4/det/setField",this);  
  //  FieldCmd->SetGuidance("Define magnetic field.");
  //  FieldCmd->SetGuidance("Magnetic field will be in Z direction.");
  //  FieldCmd->SetParameterName("Bz",false);
  //  FieldCmd->SetUnitCategory("Magnetic flux density");
  //  FieldCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  UFieldCmd = new G4UIcmdWithADoubleAndUnit("/lem4/det/setUniformField",this);  
  UFieldCmd->SetGuidance("Define uniform magnetic field.");
  UFieldCmd->SetGuidance("Magnetic field will be in Z direction.");
  UFieldCmd->SetParameterName("Bz",false);
  UFieldCmd->SetUnitCategory("Magnetic flux density");
  UFieldCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  //  GFieldCmd = new G4UIcmdWithADoubleAndUnit("/lem4/det/setGaussianField",this);  
  //  GFieldCmd = new G4UIcmdWith3VectorAndUnit("/lem4/det/setGaussianField",this);
  GFieldCmd = new G4UIcmdWith3Vector("/lem4/det/setGaussianField",this);
  GFieldCmd->SetGuidance("Define gaussian magnetic field: intensity in T, sigma in mm, and a dummy variable.");
  GFieldCmd->SetGuidance("Magnetic field will be in Z direction.");
  GFieldCmd->SetParameterName("Bz","sigmaBz","dummy",true,false);
  //  GFieldCmd->SetUnitCategory("Magnetic flux density");
  GFieldCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

///  UEFieldCmd = new G4UIcmdWithADoubleAndUnit("/lem4/det/setUnifEField",this);  
///  UEFieldCmd->SetGuidance("Define uniform electric field.");
///  UEFieldCmd->SetGuidance("Electric field will be in Z direction.");
///  UEFieldCmd->SetParameterName("Ez",false);
///  UEFieldCmd->SetUnitCategory("Electric field");
///  UEFieldCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

lem4DetectorMessenger::~lem4DetectorMessenger()
{
  //  delete WhichProcessesCmd;
  delete UpdateCmd;
  delete detDir;
  delete lem4Dir;
  delete CommandCmd;
  //  delete FieldCmd;
  delete UFieldCmd;
  delete GFieldCmd;
///  delete UEFieldCmd;
  delete RunIDSetCmd;
  delete RandomOptionCmd;
  delete HowOftenToPrintEventCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void lem4DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)  { 
  
  //  if( command == WhichProcessesCmd )
  //    { lem4Parameters* myParameters = lem4Parameters::GetInstance();
  //      lem4Parameters	-> SetMyProcesses(newValue); }
    //    lem4Parameters* myParameters = lem4Parameters::GetInstance();
    //    myParameters -> strcnpy(lem4Parameters::myProcesses,newValue,99);
    //    lem4Parameters::myProcesses[99]='\0';

  if( command == UpdateCmd )
    { myDetector->UpdateGeometry(); }

  //  if( command == FieldCmd )
  //    { myDetector->SetMagField("fieldprofiles/input.dat",FieldCmd->GetNewDoubleValue(newValue));}
  
  if( command == UFieldCmd )
    { myDetector->SetUniformMagField(UFieldCmd->GetNewDoubleValue(newValue));}

  if( command == GFieldCmd )
    { myDetector->SetGaussianMagField(GFieldCmd->GetNew3VectorValue(newValue));}
  
///  if( command == UEFieldCmd )
///    { myDetector->SetUniformElField(UEFieldCmd->GetNewDoubleValue(newValue));}

  if( command == RunIDSetCmd )
    { (G4RunManager::GetRunManager())->SetRunIDCounter(RunIDSetCmd->GetNewIntValue(newValue));}
  
  if( command == RandomOptionCmd )
    { 
      G4int RandomOption=RandomOptionCmd->GetNewIntValue(newValue);
      if (RandomOption == 1) {
	//	G4long seed=time(0); //returns time in seconds as an integer
	//	HepRandom::setTheSeed(seed);//changes the seed of the random engine
	G4cout << "******************************************" << G4endl;
	G4cout << "*** Random Seed set by the system time ***" << G4endl;
	G4cout << "******************************************" << G4endl;
	long seeds[2];
	time_t systime = time(NULL);
	seeds[0] = (long) systime;
	seeds[1] = (long) (systime*G4UniformRand());
	G4cout << "seed1: " << seeds[0] << "; seed2: " << seeds[1] << G4endl;
	CLHEP::HepRandom::setTheSeeds(seeds);
	CLHEP::HepRandom::showEngineStatus();
      }
      else if (RandomOption == 2) {
	G4cout << "*******************************************" << G4endl;
	G4cout << "*** Random Seed set by the event number ***" << G4endl;
	G4cout << "*******************************************" << G4endl;
	lem4EventAction::setRandomNrSeedAccordingEventNr=1;
	//	lem4EventAction::setMyEventNr(70);
      }
      else if (RandomOption == 3) {
	G4cout << "*******************************************" << G4endl;
	G4cout << "*** Random Seed set from external file  ***" << G4endl;
	G4cout << "*******************************************" << G4endl;
	lem4EventAction::setRandomNrSeedFromFile=1;

	std::ifstream indata;
	int num;

	indata.open("randomNum.dat"); // opens the file
	if(!indata) { // file couldn't be opened
	  G4cout << "Error: file could not be opened" << G4endl;
	  exit(1);
	}
	//	vector<int> * seedVector = new vector<int>;
	vector<int> * seedVector = lem4EventAction::GetPointerToSeedVector();
	indata >> num;
	while ( !indata.eof() ) { // keep reading until end-of-file
	  G4cout << "The next number is " << num << G4endl;
	  seedVector->push_back(num);
	  indata >> num; // sets EOF flag if no value found
	  
	  //	  lem4EventAction::RandomNrInitialisers->push_back(num);
	}
	indata.close();
	G4cout << "End-of-file reached.." << seedVector->size()<<G4endl;
	//	lem4EventAction::RandomNrInitialisers=seedVector;
      }
    }
  if ( command == HowOftenToPrintEventCmd ) 
    {
      G4int n = HowOftenToPrintEventCmd->GetNewIntValue(newValue);
      lem4EventAction::nHowOftenToPrintEvent=n;
    }
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
