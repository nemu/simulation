#include "sr1Parameters.hh"

///sr1Parameters::sr1Parameters() 
///  :typeOfProcesses("standard")
///{
///  pointerToParameters=this;
///}

sr1Parameters::sr1Parameters(G4String steeringFileName) 
  :typeOfProcesses("coulombAndMultiple") //lowenergy")
{
  pointerToParameters=this;

  // Read in the parameters, which have to be known before the detector construction is run
  // (and therefore the parameters can not be read in in the sr1DetectorConstruction.cc class).

  FILE *fSteeringFile=fopen(steeringFileName.c_str(),"r");
  if (fSteeringFile==NULL) {
    G4cout<<"sr1Parameters::sr1Parameters:  steeringFileName=\""<<steeringFileName
	  <<"\" not opened for some reason."<<G4endl;
  } 
  G4cout<<"sr1Parameters::sr1Parameters:  steeringFileName=\""<<steeringFileName<<"\" opened."<<G4endl;
  char  line[501];

  while (!feof(fSteeringFile)) {
    fgets(line,500,fSteeringFile);
    if ((line[0]!='#')&&(line[0]!='\n')&&(line[0]!='\r')) {
      char tmpString0[100]="Unset";
      sscanf(&line[0],"%s",tmpString0);
     
      // First find out how many events will be generated 
      // (should be known at an early stage, if the field is to be set-up in steps):
      if (strcmp(tmpString0,"/run/beamOn")==0) {
	int nev;
	sscanf(&line[0],"%*s %d", &nev);
	sr1Parameters::nrOfEventsToBeGenerated = nev;
      }
      
      // Now find out about some other private parameters that also need to be initialised at an early stage 
      if (strcmp(tmpString0,"/sr1/command")!=0) {continue; }///{ break; }

      char tmpString1[100]="Unset", tmpString2[100]="Unset";
      sscanf(&line[0],"%*s %s %s",tmpString1,tmpString2);
      
//! First commented by TS. Currently not in use! or not strictly necessary.
      if (strcmp(tmpString1,"G4GeneralParticleSource")==0){
      	if (strcmp(tmpString2,"true")==0){ sr1Parameters::boolG4GeneralParticleSource = true; }
      }

      
    }
  }
  fclose(fSteeringFile);
}


sr1Parameters::~sr1Parameters() {}

sr1Parameters* sr1Parameters::pointerToParameters=NULL;
sr1Parameters* sr1Parameters::GetInstance() {
return pointerToParameters;
}

G4bool   sr1Parameters::storeOnlyEventsWithHits =true;
G4double sr1Parameters::signalSeparationTime=100*nanosecond;
G4bool   sr1Parameters::storeOnlyTheFirstTimeHit=false;
G4bool   sr1Parameters::includeMuoniumProcesses =true;
G4bool   sr1Parameters::field_DecayWithSpin=false; //Added by TS to confrom with the new StepAction ...
G4bool   sr1Parameters::boolG4GeneralParticleSource=false; ///! This is not strictly necessary! TS
G4int    sr1Parameters::nrOfEventsToBeGenerated=0;

