//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//  Muonium "Scattering"
//  Id    : sr1MuScatter.cc, v 1.4
//  Author: Taofiq PARAISO, T. Shiroka
//  Date  : 2007-12
//  Notes : Simplified model for Mu scattering. Spin effects have been excluded.
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#include "sr1MuScatter.hh"

using namespace std;

sr1MuScatter::sr1MuScatter(const G4String& name,
			     G4ProcessType  aType)
               : G4VDiscreteProcess(name, aType){}

sr1MuScatter:: ~sr1MuScatter(){}

/*! - At the end of the step, the current volume is checked and if Muonium is in a solid 
  material (except for the carbon foil where it is generated), it is stopped immediately. */
G4VParticleChange* sr1MuScatter::PostStepDoIt(const G4Track& trackData,
                                               const G4Step& aStep)
{
  fParticleChange.Initialize(trackData);
  
  //! Tao - Get time information */
  itime = trackData.GetProperTime();
  gtime = trackData.GetGlobalTime();
  ftime = trackData.GetDynamicParticle()->GetPreAssignedDecayProperTime(); 
  
  deltatime = ftime - itime;
  fParticleChange.ProposeGlobalTime(deltatime + itime -gtime);
  
  /*! - Set position, momentum, energy and time of the particle change. */
  fParticleChange.ProposePosition(trackData.GetPosition());
  fParticleChange.ProposeMomentumDirection(trackData.GetMomentumDirection());
  fParticleChange.ProposeEnergy(trackData.GetKineticEnergy());
  fParticleChange.ProposeGlobalTime(gtime);
  fParticleChange.ProposeProperTime(itime);
  fParticleChange.ProposeTrackStatus(trackData.GetTrackStatus()) ;
  
  /*! - Verify the condition of applying the process: if Mu is in a material 
        different than vacuum and carbon foil, then stop it directly. */
  if( CheckCondition(aStep))
    {
      fParticleChange.ProposePosition(trackData.GetStep()->GetPreStepPoint()->GetPosition());
      fParticleChange.ProposeTrackStatus(fStopButAlive) ;
    }
  
  /*! - Return the changed particle object. */
  return &fParticleChange;
}


/*! - Muonium will be stopped as soon as it enters a material different than vacuum or C foil. */
G4bool sr1MuScatter::CheckCondition(const G4Step& aStep)
{
  G4bool condition = false;
  p_name = aStep.GetTrack()->GetDefinition()->GetParticleName(); // particle name  
  if(p_name == "Mu" && aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName()!="log_CFoil" &&
  aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetMaterial()->GetName()!="G4_Galactic")
    {
      condition=true;
    }
  return condition;
}  


G4double sr1MuScatter::GetMeanFreePath(const G4Track&,
					G4double,
					G4ForceCondition* condition)
{
  *condition = Forced;
   return DBL_MAX;
}


void sr1MuScatter::PrepareSecondary(const G4Track& track)
{
  aSecondary = new G4Track(DP,track.GetDynamicParticle()->GetPreAssignedDecayProperTime(),track.GetPosition());
}
