#include "globals.hh"

#include "G4GeometryManager.hh"

//#include "F04GlobalField.hh"
#include "sr1UniformField.hh"
///#include "sr1Parameters.hh" ///Is this really needed. TS ???


///sr1UniformField::sr1UniformField(G4ThreeVector (Bfieldx,Bfieldy,Bfieldz), G4LogicalVolume* lv, G4ThreeVector c)
sr1UniformField::sr1UniformField(G4double EMF[6], G4LogicalVolume* lv, G4ThreeVector c)
///sr1UniformField::sr1UniformField(G4double Bz, G4LogicalVolume* lv, G4ThreeVector c)
                : F04ElementField(c,lv) ///, EMfield(EMF) /// ??? Why Bz is not enough??
{
  for (int i = 0; i < 6; i++){
    EMfield[i] = EMF[i];
  }
  
///  Bfield  = Bz; ///G4ThreeVector finalField(B[0],B[1],B[2]); G4ThreeVector B(0.0,0.0,Bfield);
  ///G4ThreeVector BField(Bfieldx,Bfieldy,Bfieldz);
  
  fieldLength = 2.*((G4Box*)lvolume->GetSolid())->GetZHalfLength();
  fieldWidth  = 2.*((G4Box*)lvolume->GetSolid())->GetXHalfLength();
  fieldHeight = 2.*((G4Box*)lvolume->GetSolid())->GetYHalfLength();

  G4cout << "\n-----------------------------------------------------------"
         << "\n      Uniform electromagnetic field"
         << "\n-----------------------------------------------------------"
         << G4endl;
   
  G4String volName = lv->GetName().substr(4);
  G4cout << "\n ---> EM field in volume " << volName << " set to:" << G4endl;
  printf   ("      B = (%0.3g, %0.3g, %0.3g) T,  E = (%0.3g, %0.3g, %0.3g) kV/mm\n",
  EMF[0]/tesla,         EMF[1]/tesla,         EMF[2]/tesla, 
  EMF[3]/(kilovolt/mm), EMF[4]/(kilovolt/mm), EMF[5]/(kilovolt/mm));
}



void sr1UniformField::addFieldValue(const G4double point[4],
                                           G4double field[6]) const
{
   G4ThreeVector global(point[0],point[1],point[2]);
   G4ThreeVector local;
   
   local = global2local.TransformPoint(global);
   
   if (isOutside(local)) return;

   G4ThreeVector B(EMfield[0],EMfield[1],EMfield[2]);
   G4ThreeVector E(EMfield[3],EMfield[4],EMfield[5]);
   
   B = global2local.Inverse().TransformAxis(B);
   E = global2local.Inverse().TransformAxis(E);

   field[0] += B[0];
   field[1] += B[1];
   field[2] += B[2];
   
   field[3] += E[0];
   field[4] += E[1];
   field[5] += E[2];
      
   //printf ("   EM field components:  B = (%0.3g, %0.3g, %0.3g) T,  E = (%0.3g, %0.3g, %0.3g) kV/mm\n",
   //field[0]/tesla,         field[1]/tesla,         field[2]/tesla,
   //field[3]/(kilovolt/mm), field[4]/(kilovolt/mm), field[5]/(kilovolt/mm));
}



G4double sr1UniformField::GetNominalFieldValue() {
  ///G4double EMfield[6] = {0,0,0,0,0,0};
  ///for (int i = 0; i < 6; i++){
    //return EMfield[i];
    return 0;
    ///G4double nomFieldValue = (*i)->GetNominalFieldValue();
    ///EMfield[i] = Bfield[i];
  //}
   ///return Bfield; ///ffieldValue;
}

void sr1UniformField::SetNominalFieldValue(G4double newFieldValue){ ///[6]) {
   ///Bfield = newFieldValue; ///ffieldValue=newFieldValue;
  ///for (int i = 0; i < 6; i++){
    ///EMfield[i] = newFieldValue[i];
  ///}
  G4cout<<"sr1UniformField.cc:  SetNominalFieldValue method is NOT defined in this case!\n Dummy field value "<< newFieldValue << G4endl;
  //G4cout<<"sr1UnifromField.cc:   ffieldValue changed to="<< Bfield/tesla<<"T "<<G4endl;
}



G4bool sr1UniformField::isOutside(G4ThreeVector& local) const
{
  return (std::fabs(local.z()) > fieldLength/2.0 || std::fabs(local.x()) > fieldWidth/2.0 || std::fabs(local.y()) > fieldHeight/2.0);
}

G4bool sr1UniformField::isWithin(G4ThreeVector& local) const
{
  return (std::fabs(local.z()) < fieldLength/2.0 && std::fabs(local.x()) < fieldWidth/2.0 && std::fabs(local.y()) < fieldHeight/2.0);
}
