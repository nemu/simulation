//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "sr1ShieldSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  ///
  /// The Shield sensitive detector. This object collects the
  /// information of a particle's interaction with this part
  /// of the geometry
  ///

sr1ShieldSD::sr1ShieldSD(G4String name)
:G4VSensitiveDetector(name)
{
  G4String HCname;
  collectionName.insert(HCname="shieldCollection");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

sr1ShieldSD::~sr1ShieldSD(){ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void sr1ShieldSD::Initialize(G4HCofThisEvent* HCE)
{
  shieldCollection = new sr1ShieldHitsCollection
                          (SensitiveDetectorName,collectionName[0]); 
  static G4int HCID = -1;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection( HCID, shieldCollection ); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool sr1ShieldSD::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{
  ///
  /// Collect the information regarding the interaction of a particle
  /// and the sensitive detector.
  ///
  G4double edep = aStep->GetTotalEnergyDeposit();

  if(edep==0.) return false;

  sr1ShieldHit* newHit = new sr1ShieldHit();
  newHit->SetParticleName (aStep->GetTrack()->GetDefinition()->GetParticleName());
  newHit->SetTrackID  (aStep->GetTrack()->GetTrackID());
  newHit->SetEdep     (edep);
  newHit->SetPos      (aStep->GetPostStepPoint()->GetPosition());
  newHit->SetPol      (aStep->GetTrack()->GetPolarization());
  newHit->SetLogVolName (aStep->GetTrack()->GetVolume()->GetLogicalVolume()->GetName());
  shieldCollection->insert( newHit );
  
  newHit->Print();
  newHit->Draw();



  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void sr1ShieldSD::EndOfEvent(G4HCofThisEvent*)
{
  if (verboseLevel>0) { 
     G4int NbHits = shieldCollection->entries();
     G4cout << "\n-------->Hits Collection: in this event they are " << NbHits 
            << " hits: " << G4endl;
     for (G4int i=0;i<NbHits;i++) (*shieldCollection)[i]->Print();
    } 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

