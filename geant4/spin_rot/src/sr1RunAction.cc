//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// Make G4Timer appear first!
#include "G4Timer.hh"
#include "sr1RunAction.hh"
#include "sr1EventAction.hh"
#include "G4Run.hh"
#include "sr1ErrorMessage.hh"
#include "F04GlobalField.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

sr1RunAction::sr1RunAction() {
  timer = new G4Timer;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

sr1RunAction::~sr1RunAction() {
  delete timer;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void sr1RunAction::BeginOfRunAction(const G4Run* aRun)  {
  timer->Start();
  G4int run_id= aRun->GetRunID();
  //  if (run_id%100 == 0) {
  G4cout << "### Run " << run_id << G4endl;
  //  }
  // Set nr. of events that will be processed in this run to sr1EventAction class, as it will
  // be needed for the time dependent magnetic field;
  sr1EventAction* pointerToEventAction = sr1EventAction::GetInstance();
  pointerToEventAction->SetNumberOfEventsInThisRun(aRun->GetNumberOfEventToBeProcessed());
  //
  sr1RootOutput::GetRootInstance()->BeginOfRunAction();
  //
  // Initiate global electromagnetic field (if it exists):
  if (F04GlobalField::Exists()) {
    FieldList* fields = F04GlobalField::getObject()->getFields();

    if (fields) {
      if (fields->size()>0) {
        G4int jjj=0;
	G4cout<<"\n------------ The following fields were defined:  ----------------\n"<<G4endl;
	FieldList::iterator i;
	for (i=fields->begin(); i!=fields->end(); ++i) { 
	(*i)->construct();
	// Get the nominal field value for the given field and store it in the Root output
	G4double nomFieldValue = (*i)->GetNominalFieldValue();
	sr1RootOutput::GetRootInstance()->SetFieldNomVal(jjj,nomFieldValue);
	jjj++;
	}
	G4cout<<"-----------------------------------------------------------------"<<G4endl;
      }
    }  
    
    // Print out the field values at the points user requested to be printed out:
    F04GlobalField::getObject()->PrintFieldAtRequestedPoints();
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void sr1RunAction::EndOfRunAction(const G4Run* aRun)  { 
  sr1RootOutput::GetRootInstance()->EndOfRunAction();
  sr1ErrorMessage::GetInstance()->PrintErrorSummary();
  timer->Stop();
  G4cout << "sr1RunAction::EndOfRunAction:"<<G4endl;
  G4cout << "    Number of events    = " << aRun->GetNumberOfEvent()<<G4endl;
  //         << " " << *timer << G4endl;
  G4cout << "    User elapsed time   = "<<timer->GetUserElapsed()/3600<<"h   = "
	 <<timer->GetUserElapsed()/60<<"min   = "<<timer->GetUserElapsed()<<"s."<<G4endl;
  G4cout << "    Real elapsed time   = "<<timer->GetRealElapsed()/3600<<"h   = "
	 <<timer->GetRealElapsed()/60<<"min   = "<<timer->GetRealElapsed()<<"s."<<G4endl;
  G4cout << "    System elapsed time = "<<timer->GetSystemElapsed()/3600<<"h   = "
	 <<timer->GetSystemElapsed()/60<<"min   = "<<timer->GetSystemElapsed()<<"s."<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



