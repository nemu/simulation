#include "sr1ErrorMessage.hh"

sr1ErrorMessage::sr1ErrorMessage():nErrors(1)
{
  pointerToErrors=this;
  severityWord[INFO]="INFO";
  severityWord[WARNING]="WARNING";
  severityWord[SERIOUS]="SERIOUS";
  severityWord[FATAL]="FATAL";
}

sr1ErrorMessage::~sr1ErrorMessage() {}

sr1ErrorMessage* sr1ErrorMessage::pointerToErrors=NULL;
sr1ErrorMessage* sr1ErrorMessage::GetInstance() {
  return pointerToErrors;
}

void sr1ErrorMessage::sr1Error(SEVERITY severity, G4String message, G4bool silent) {
  std::map<G4String,ErrorStruct>::iterator it;
  it = ErrorMapping.find(message);
  if (it == ErrorMapping.end()) {      // The error message is called for the first time
    ErrorStruct actualErrorMessage;
    actualErrorMessage.mesSeverity = severity;
    actualErrorMessage.nTimes = 1;
    ErrorMapping[message]=actualErrorMessage;
    G4cout<<"!!!"<<severityWord[severity]<<"!!! "<<message<<"  (First time occurrence)"<<G4endl;
    nErrors++;
  }
  else {                                // The error message is called for more than the first time
    (*it).second.nTimes++;
  }

  // Print out the error message if required
  if ((!silent)||(severity==FATAL)) { 
    if ((*it).second.nTimes>1) {
      G4cout<<"!!!"<<severityWord[severity]<<"!!! "<<message
	    <<" ("<<(*it).second.nTimes<<" occurences)"<<G4endl;
    }
  }
  
  if (severity==FATAL) {
    G4cout<<"S T O P     F O R C E D!"<<G4endl;
    exit(1);
  }
}



void sr1ErrorMessage::PrintErrorSummary() {
  std::map<G4String,ErrorStruct>::iterator it;
  G4cout<<"------   ERROR SUMMARY:  -------------------------------------------------------"<<G4endl;
  for (G4int i=0; i<4; i++) {
    for ( it=ErrorMapping.begin() ; it != ErrorMapping.end(); it++ ) {
      if ((*it).second.mesSeverity==i) {
	G4cout<<severityWord[(*it).second.mesSeverity]<<" ("<<(*it).second.nTimes<<" times):"
	      << (*it).first <<G4endl;
      }
    }
  }
G4cout<<"--------------------------------------------------------------------------------"<<G4endl;
}
