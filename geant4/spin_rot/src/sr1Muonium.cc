//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: sr1Muonium.cc,v 1.13 2007/03/15 06:53:58 kurasige Exp $
// GEANT4 tag $Name: geant4-09-00 $
//
// 
// ----------------------------------------------------------------------
//      GEANT 4 class implementation file
//
//      History: first implementation, based on object model of
//      4th April 1996, G. Cosmo
// **********************************************************************
// New implementation as an utility class  M. Asai, 26 July 2004
// ----------------------------------------------------------------------

#include "sr1Muonium.hh"
#include "G4ParticleTable.hh"

#include "MuDecayChannel.hh"
#include "G4DecayTable.hh"

// ######################################################################
// ###                          MUONIUM                               ###
// ######################################################################
sr1Muonium* sr1Muonium::theInstance = 0;

sr1Muonium* sr1Muonium::Definition()
{
  if (theInstance !=0) return theInstance;
  const G4String name = "Mu";
  // search in particle table]
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance ==0)
  {
  // create particle
  //
  //    Arguments for constructor are as follows
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table
  //             shortlived      subType    anti_encoding
  anInstance = new G4ParticleDefinition(
                 name,   0.1056584*GeV, 2.99591e-16*MeV,   0.*eplus, 
		    1,               0,             0,          
		    0,               0,             0,             
	     "lepton",              -1,             0,         -1313,
		false,      2197.03*ns,          NULL,
             false,           "mu"
              );
   // Bohr magnetron of Muonium - T. Shiroka
   // The magnetic moment of Mu is the sum of those of mu+ and e- with
   // the respective gyromagnetic ratio anomalies as coefficients
   
   G4double muBmu =  0.5*eplus*hbar_Planck/(0.10565840*GeV/c_squared);
   G4double muBel = -0.5*eplus*hbar_Planck/(0.51099906*MeV/c_squared);
   G4double muB   =  1.0011659208*muBmu + 1.0011596521859*muBel;
   
   anInstance->SetPDGMagneticMoment( muB );

  //create Decay Table 
  G4DecayTable* table = new G4DecayTable();
  // create a decay channel
  G4VDecayChannel* mode = new MuDecayChannel("Mu",1.00);
  table->Insert(mode);
  anInstance->SetDecayTable(table);
  }
  theInstance = reinterpret_cast<sr1Muonium*>(anInstance);
  return theInstance;
}

sr1Muonium*  sr1Muonium::MuoniumDefinition()
{
  return Definition();
}

sr1Muonium*  sr1Muonium::Muonium()
{
  return Definition();
}

