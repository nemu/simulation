#include "sr1DetectorConstruction.hh"
#include "sr1PhysicsList.hh"
#include "sr1PrimaryGeneratorAction.hh"
#include "sr1RunAction.hh"
#include "sr1EventAction.hh"
#include "sr1SteppingAction.hh"
#include "sr1SteppingVerbose.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

#include "Randomize.hh"

#include <X11/Xlib.h>

#ifdef G4VIS_USE
  //#include "sr1VisManager.hh"
  #include "G4VisExecutive.hh"  // TS Trajectory drawing by ID or charge
  #include "G4TrajectoryDrawByCharge.hh"
#endif

#include "sr1RootOutput.hh"
#include "sr1Parameters.hh"
#include "sr1ErrorMessage.hh"

int main(int argc,char** argv) {

  XInitThreads();

  // choose the Random engine
  //  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);   // the /sr1/run/randomOption 2 does not work with RanecuEngine
  CLHEP::HepRandom::setTheEngine(new CLHEP::HepJamesRandom);

  //my Verbose output class
  G4VSteppingVerbose::SetInstance(new sr1SteppingVerbose);
  
  // Run manager
  G4RunManager * runManager = new G4RunManager;
 
 // Create class "myParameters", which is a collection of many different parameters
  G4String steeringFileName=argv[1];
  sr1Parameters* myParameters = new sr1Parameters(steeringFileName);
//  sr1Parameters* myParameters = new sr1Parameters();

  // Create class "sr1ErrorMessage"
  sr1ErrorMessage* myErrorMessage = new sr1ErrorMessage();

  // Create Root class for storing the output of the Geant simulation
  sr1RootOutput* myRootOutput = new sr1RootOutput();

  // UserInitialization classes (mandatory)
  sr1DetectorConstruction* sr1detector = new sr1DetectorConstruction;
  //  if(argc>2) { sr1detector->SetInputParameterFileName(argv[2]); }
  //  else 
  if (argc>1) {
    G4int myRunNr=atoi(argv[1]);       // Get the run number from the name of the 
                                       // parameter file, if it starts with a number.
    if (myRunNr>0)  {runManager->SetRunIDCounter(myRunNr);}
    sr1detector->SetInputParameterFileName(argv[1]);
  }
  runManager->SetUserInitialization(sr1detector);
  runManager->SetUserInitialization(new sr1PhysicsList);
  
#ifdef G4VIS_USE
  // Visualization, if you choose to have it!
  G4VisManager* visManager = new G4VisExecutive; // sr1VisManager; TS Trajectory drawing by ID or charge
  visManager->Initialize();
#endif
   
  // UserAction classes
  runManager->SetUserAction(new sr1PrimaryGeneratorAction(sr1detector));
  runManager->SetUserAction(new sr1RunAction);  
  runManager->SetUserAction(new sr1EventAction);
  runManager->SetUserAction(new sr1SteppingAction);

  //Initialize G4 kernel
  runManager->Initialize();
      
  //get the pointer to the User Interface manager 
  G4UImanager * UI = G4UImanager::GetUIpointer();  

  if(argc==1)
  // Define (G)UI terminal for interactive mode  
  { 
    // G4UIterminal is a (dumb) terminal.
    G4UIsession * session = 0;
#ifdef G4UI_USE_TCSH
      session = new G4UIterminal(new G4UItcsh);      
#else
      session = new G4UIterminal();
#endif    

    UI->ApplyCommand("/control/execute vis.mac");    
    session->SessionStart();
    delete session;
  }
  else
  // Batch mode
  { 
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UI->ApplyCommand(command+fileName);
    if (argc>2) {
      G4String SecondArgument = argv[2];
      if (SecondArgument=="idle") {
	G4UIsession * session = 0;
#ifdef G4UI_USE_TCSH
	session = new G4UIterminal(new G4UItcsh);      
#else
	session = new G4UIterminal();
#endif    
	session->SessionStart();
	delete session;
      }
    }
  }

#ifdef G4VIS_USE
  delete visManager;
#endif
  delete myRootOutput;
  delete myErrorMessage;
  delete myParameters;
  delete runManager;

  return 0;
}



