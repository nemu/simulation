//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
#ifndef sr1EventAction_h
#define sr1EventAction_h 1

#include "globals.hh"
#include "G4UserEventAction.hh"
#include <vector>
using namespace std;
class G4Timer;
class G4Event;
class sr1MagneticField;
class G4ElectricField; // Unif. el. field. TS
class sr1TabulatedField3D;
class sr1TabulatedField2D;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class sr1EventAction : public G4UserEventAction
{
  public:
    sr1EventAction();
   ~sr1EventAction();

  public:
    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);
    static sr1EventAction* GetInstance();

  private:
  //  pointer to this class
    static sr1EventAction* pointer;
  //
    G4Timer* timer;
    static vector<int> * pointerToSeedVector;
  //    Variables for the time-dependent magnetic field
    G4bool   timeDependentField;
    G4double fieldValueStart, fieldValueEnd, fieldStep, lastFieldValue;
    G4int    fieldNrOfSteps;
    G4int    maxEventNr;
    sr1MagneticField*     pointerToMusrUniformField;
    G4ElectricField*       pointerToMusrUniformEField; // Pointer to uniform electric field. TS
    sr1TabulatedField3D*  pointerToTabulatedField3D;
    sr1TabulatedField2D*  pointerToTabulatedField2D;
    G4int    latestEventNr;

  public:
    static G4bool setRandomNrSeedAccordingEventNr;
    static G4bool setRandomNrSeedFromFile;
    static G4int  nHowOftenToPrintEvent;
    static vector<int> * GetPointerToSeedVector();
  //  void setMyEventNr(long n) {myEventNr=n;};
    void SetTimeDependentField(G4bool setFieldToBeTimeDependend, G4double initialField,
			       G4double finalField, G4int nrOfSteps);
    void SetNumberOfEventsInThisRun(G4int nrOfEventsInThisRun) {maxEventNr=nrOfEventsInThisRun;};
    void SetPointerToMusrUniformField(sr1MagneticField* pointer) {pointerToMusrUniformField=pointer;};
    void SetPointerToMusrUniformEField(G4ElectricField* pointer) {pointerToMusrUniformEField=pointer;}; // Unif. El. field. TS
    void SetPointerToTabulatedField3D(sr1TabulatedField3D* pointer) {pointerToTabulatedField3D=pointer;};
    void SetPointerToTabulatedField2D(sr1TabulatedField2D* pointer) {pointerToTabulatedField2D=pointer;};
    G4int GetLatestEventNr(){return latestEventNr;};
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
