#ifndef sr1DetectorConstruction_h
#define sr1DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4FieldManager.hh"
#include <map>

class G4Tubs;
class G4Box;
class G4Cons;
class G4Trd;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class sr1DetectorMessenger;
//class sr1TrackerSD;
//class sr1ShieldSD;
//class sr1IForwardSD;
//class sr1IBackwardSD;
class sr1ScintSD;


//class sr1UniformField; // Added by TS to implement uniform box field according to Gumplinger

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class sr1DetectorConstruction : public G4VUserDetectorConstruction
{
public:
  
  sr1DetectorConstruction();
  ~sr1DetectorConstruction();

public:
  
  G4VPhysicalVolume* Construct();


  void UpdateGeometry();
  ///ALL THESE ARE OLD FIELD SETTING METHODS. USE THAT OF GUMPLINGER INSTEAD! TS.
  void SetMagField(const char* fieldTableType, const char* inputFileName, G4double fieldValue);
  void SetUniformMagField(G4double);
  void SetGaussianMagField(G4ThreeVector);
  ///void SetUniformElField(G4double); // Uniform Electric Field. TS

  void SetInputParameterFileName(G4String fileName) {parameterFileName=fileName;};
  void ReportGeometryProblem(char myString[501]);
  void ReportProblemInStearingFile(char* myString); //!Changed -added
  G4Material* CharToMaterial(char myString[100]);
  G4LogicalVolume* FindLogicalVolume(G4String LogicalVolumeName);
  void SetColourOfLogicalVolume(G4LogicalVolume* pLogVol,char* colour);
  G4bool CheckIfStringContainsSubstring(char* string, char* substring); //!Changed -added

private:
  G4String     parameterFileName;     // name of the file with the geometry parameters
  G4bool       checkOverlap;          // parameter to check ovelaping volumes
  G4double     largestAcceptableStep; // parameter defining largest step in the magnetic field


  //G4Material*  TargetMater;// pointer to the target  material
  //G4Material*  VacMater;   // pointer to the target  material
  //G4Material*  DetMater;   // pointer to the target  material
  //G4Material*  Al;         // pointer to the target  material
  //G4Material*  Pb;         // pointer to the target  material
  //G4Material*  AirMater;   // pointer to the target  material
  //G4Material*  Vacuum;     // pointer to vacuum material

  sr1ScintSD* aScintSD;  
  sr1DetectorMessenger* detectorMessenger;  // pointer to the Messenger

  std::map<std::string,G4RotationMatrix*> pointerToRotationMatrix;
  std::map<std::string,G4FieldManager*> pointerToField;
  
private:
  void DefineMaterials(); 
};

#endif
