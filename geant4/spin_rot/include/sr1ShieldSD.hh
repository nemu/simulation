//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef sr1ShieldSD_h
#define sr1ShieldSD_h 1

#include "G4VSensitiveDetector.hh"
#include "sr1ShieldHit.hh"

class G4Step;
class G4HCofThisEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class sr1ShieldSD : public G4VSensitiveDetector
{
  public:
      sr1ShieldSD(G4String);
     ~sr1ShieldSD();

      void Initialize(G4HCofThisEvent*);
      G4bool ProcessHits(G4Step*, G4TouchableHistory*);
      void EndOfEvent(G4HCofThisEvent*);

  private:
      sr1ShieldHitsCollection* shieldCollection;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

