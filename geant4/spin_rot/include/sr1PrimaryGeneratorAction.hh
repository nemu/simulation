#ifndef sr1PrimaryGeneratorAction_h
#define sr1PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "Randomize.hh"
#include "G4ThreeVector.hh"
#include <stdio.h>
//cksdel #include "sr1ParticleGun.hh"

class G4ParticleGun;
class G4Event;
class sr1DetectorConstruction;
class sr1PrimaryGeneratorMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class sr1PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    sr1PrimaryGeneratorAction(sr1DetectorConstruction*);   
   ~sr1PrimaryGeneratorAction();

  public:
    void GeneratePrimaries(G4Event*);
    void SetRndmFlag(G4String val)          {rndmFlag = val;}
    void Setvertex(G4ThreeVector v)         {x0=v[0]; y0=v[1]; z0=v[2];}
    void SetvertexSigma(G4ThreeVector v)    {xSigma=v[0]; ySigma=v[1]; zSigma=v[2];}
    void SetvertexBoundary(G4ThreeVector v) {rMaxAllowed=v[0]; zMinAllowed=v[1]; zMaxAllowed=v[2];}
    void SetEnergy(G4double val)            {p0=std::sqrt(val*val + 2*mu_mass*val);}   // {E0=val;}
    void SetMomentum(G4double val)          {p0=val;}
    void SetMomentumSmearing(G4double val)  {pSigma=val;}
    void SetMomentumBoundary(G4ThreeVector v){pMinAllowed=v[0]; pMaxAllowed=v[1];}
    void SetTilt(G4ThreeVector v)           {xangle0=v[0]; yangle0=v[1];}
    void SetSigmaTilt(G4ThreeVector v)      {xangleSigma=v[0]; yangleSigma=v[1];}
    void SetPitch(G4double val)             {pitch=val;}
    void SetInitialMuonPolariz(G4ThreeVector vIniPol);
    void SetMuonDecayTimeLimits(G4ThreeVector decayTimeLimits);
    void SetTurtleInput(G4String turtleFileName);

    static G4String GetPrimaryName();  

  private:
    G4ParticleGun*                 particleGun;  // Pointer a to G4 service class
    sr1DetectorConstruction*      sr1Detector; // Pointer to the geometry
    sr1PrimaryGeneratorMessenger* gunMessenger; // Messenger of this class
    G4String                       rndmFlag;     // Flag for a random impact point       
    
    G4double x0, y0, z0, xSigma, ySigma, zSigma, rMaxAllowed, zMinAllowed, zMaxAllowed;
    G4double E0, p0, pSigma, pMinAllowed, pMaxAllowed;
    G4double xangle0, yangle0, xangleSigma, yangleSigma, pitch;
    G4bool   UnpolarisedMuonBeam;
    G4double xPolarisIni, yPolarisIni, zPolarisIni;
    G4double muonDecayTimeMin;
    G4double muonDecayTimeMax;
    G4double muonMeanLife;
    G4double mu_mass;
    
    // For Turtle input
    FILE*    fTurtleFile;
    G4bool   takeMuonsFromTurtleFile;
    
    static G4String thePrimaryParticleName;
  
  public: 
    G4double decaytime;
};

#endif
