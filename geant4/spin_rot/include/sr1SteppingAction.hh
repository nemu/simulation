//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef sr1SteppingAction_h
#define sr1SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "G4ProcessManager.hh"
#include "globals.hh"
#include <fstream>

class G4Timer;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class sr1SteppingAction : public G4UserSteppingAction
{
  public:

    static sr1SteppingAction* GetInstance();
    sr1SteppingAction();
    ~sr1SteppingAction();

    void UserSteppingAction(const G4Step *theStep);
    void DoAtTheBeginningOfEvent();
    void SetLogicalVolumeAsSpecialSaveVolume(G4String logicName, G4int volumeID);

  private:
    G4Timer* timer;
    static sr1SteppingAction* pointer;
  //    G4int oldEventID;
    G4ProcessManager* muPlusProcessManager;
    G4bool multipleToCoulombScatteringIsPossible;
    G4bool coulombScatteringIsActive;
    size_t multipleScatteringIndex;
    size_t coulombScatteringIndex;
    G4bool muAlreadyWasInTargetInThisEvent;
    G4bool radioactiveElectronAlreadySavedInThisEvent;
    G4bool boolIsAnySpecialSaveVolumeDefined;
    std::map<G4String,G4int>  saveVolumeMapping;
    G4String lastActualVolume;
  //    G4int debugOldEventID;
  //    G4int nrOfSteps;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
