#include "globals.hh"
#include "G4MagneticField.hh"
#include "G4ios.hh"

#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

class sr1TabulatedField3D
#ifndef STANDALONE
 : public G4MagneticField
#endif
{
  
  // Storage space for the table
  vector< vector< vector< double > > > xField;
  vector< vector< vector< double > > > yField;
  vector< vector< vector< double > > > zField;
  // The dimensions of the table
  int nx,ny,nz; 
  // The physical limits of the defined region
  double minx, maxx, miny, maxy, minz, maxz;
  // The physical extent of the defined region
  double dx, dy, dz;
  double ffieldValue;
  bool invertX, invertY, invertZ;
  double positionOffset[4];

  static sr1TabulatedField3D* pointerToTabulatedField3D;

public:

  static sr1TabulatedField3D* GetInstance();

  sr1TabulatedField3D(const char* filename, double fieldValue );
  void  GetFieldValue( const  double Point[4],
		       double *Bfield          ) const;
  G4double GetFieldSetValue();
  void SetFieldValue(double newFieldValue);
  void SetPositionOffset(double offset[4]) { positionOffset[0]=offset[0]; positionOffset[1]=offset[1];
    positionOffset[2]=offset[2]; positionOffset[3]=offset[3];}

};

