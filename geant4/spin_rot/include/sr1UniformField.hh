#ifndef UNIFORM_BFIELD_HH
#define UNIFORM_BFIELD_HH

#include "G4LogicalVolume.hh"
#include "G4Box.hh"

#include "F04ElementField.hh"
#include "F04GlobalField.hh"

// UniformField implements a constant electromagnetic field in any direction. TS

class sr1UniformField : public F04ElementField
{
  public:

    sr1UniformField(G4double EMF[6], G4LogicalVolume*, G4ThreeVector);

    virtual ~sr1UniformField() {}

    // TS: Define the two newly added VIRTUAL functions of F04ElementField
    G4double GetNominalFieldValue();
    void SetNominalFieldValue(G4double newFieldValue);
    
    virtual G4double getLength() { return fieldLength; }
    virtual G4double getWidth()  { return fieldWidth; }
    virtual G4double getHeight() { return fieldHeight; }

    G4bool isOutside(G4ThreeVector& local) const;
    G4bool isWithin (G4ThreeVector& local) const;

    void addFieldValue(const G4double point[4], G4double field[6]) const;

  private:

    G4double EMfield[6];

    G4double fieldLength;
    G4double fieldWidth;
    G4double fieldHeight;

};

#endif
