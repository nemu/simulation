//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef sr1ScintSD_h
#define sr1ScintSD_h 1

#include "G4VSensitiveDetector.hh"
#include "sr1ScintHit.hh"
#include "sr1RootOutput.hh"

//cksdel extern sr1RootOutput* myRootOutput;

class G4Step;
class G4HCofThisEvent;
//class sr1ScintHit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class sr1ScintSD : public G4VSensitiveDetector
{
  public:
      sr1ScintSD(G4String);
     ~sr1ScintSD();

      void Initialize(G4HCofThisEvent*);
      G4bool ProcessHits(G4Step*, G4TouchableHistory*);
      void EndOfEvent(G4HCofThisEvent*);
  //      bool timeOrdering (sr1ScintHit* hit1, sr1ScintHit* hit2);
  //     bool myREMOVEfunction (int i,int j) { return (i<j); }

  private:
      sr1ScintHitsCollection* scintCollection;
      G4bool   myStoreOnlyEventsWithHits;
      G4double mySignalSeparationTime;
      G4bool   myStoreOnlyTheFirstTimeHit;
  //      G4bool Positron_momentum_already_stored;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

