#ifndef sr1PrimaryGeneratorMessenger_h
#define sr1PrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class sr1PrimaryGeneratorAction;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWith3Vector;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class sr1PrimaryGeneratorMessenger: public G4UImessenger
{
  public:
    sr1PrimaryGeneratorMessenger(sr1PrimaryGeneratorAction* sr1Gun);
   ~sr1PrimaryGeneratorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String newValue);
    
  private:
    sr1PrimaryGeneratorAction* sr1Action;
    G4UIcmdWith3VectorAndUnit* setvertexCmd;
    G4UIcmdWith3VectorAndUnit* setvertexSigmaCmd;
    G4UIcmdWith3VectorAndUnit* setvertexBoundaryCmd;
    G4UIcmdWithADoubleAndUnit* setEnergyCmd;
    G4UIcmdWithADoubleAndUnit* setMomentumCmd;
    G4UIcmdWithADoubleAndUnit* setMomentumSmearingCmd;
    G4UIcmdWith3VectorAndUnit* setMomentumBoundaryCmd;
    G4UIcmdWith3VectorAndUnit* setTiltAngleCmd;
    G4UIcmdWith3VectorAndUnit* setSigmaTiltAngleCmd;
    G4UIcmdWithADoubleAndUnit* setPitchCmd;
    G4UIcmdWith3Vector*        setMuonPolarizCmd;
    G4UIcmdWith3VectorAndUnit* setMuonDecayTimeCmd;
    G4UIcmdWithAString*        setTurtleCmd;
};
#endif
