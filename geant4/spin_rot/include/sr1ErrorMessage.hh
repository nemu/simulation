#ifndef sr1ErrorMessage_h
#define sr1ErrorMessage_h 1

#include <map>
#include "globals.hh"
enum SEVERITY {INFO, WARNING, SERIOUS, FATAL};

typedef struct 
{
    SEVERITY mesSeverity;
    int nTimes;
} ErrorStruct;

class sr1ErrorMessage {
  public:
  //    enum SEVERITY {INFO, WARNING, SERIOUS, FATAL};
    sr1ErrorMessage();
   ~sr1ErrorMessage();
    static sr1ErrorMessage* GetInstance();
    void sr1Error(SEVERITY severity, G4String message, G4bool silent);
    void PrintErrorSummary();

  private:
    static sr1ErrorMessage* pointerToErrors;
    G4int nErrors;
  //    std::map<G4String,int> ErrorMapping;
    std::map<G4String,ErrorStruct> ErrorMapping;

    std::map<SEVERITY,G4String> severityWord;
};
#endif
