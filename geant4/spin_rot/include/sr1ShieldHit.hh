//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef sr1ShieldHit_h
#define sr1ShieldHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4MagneticField.hh"
#include "globals.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class sr1ShieldHit : public G4VHit
{
  public:

      sr1ShieldHit();
     ~sr1ShieldHit();
      sr1ShieldHit(const sr1ShieldHit&);
      const sr1ShieldHit& operator=(const sr1ShieldHit&);
      G4int operator==(const sr1ShieldHit&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Draw();
      void Print();

  public:


      void SetTrackID  (G4int track)      { trackID = track; };
  void SetParticleName (G4String name) {particle_name = name; };
      void SetEdep     (G4double de)      { edep = de; };
      void SetPos      (G4ThreeVector xyz){ pos = xyz; };
      void SetPol      (G4ThreeVector ijk){pol = ijk;};
  void SetLogVolName   (G4String logivol) {logicalvolume = logivol;};
      

      G4int GetTrackID()    { return trackID; };
  G4String GetParticleName() {return particle_name; };
      G4double GetEdep()    { return edep; };      
      G4ThreeVector GetPos(){ return pos; };
      G4ThreeVector GetPol(){ return pol; };
  G4String GetLogVolName() { return logicalvolume; };
      G4double point[4];
      G4double B[6];
      const G4Field *mfield;


  private:


      G4int         trackID;
  G4String          particle_name;
      G4double      edep;
      G4ThreeVector pos;
      G4ThreeVector pol;
  G4String          logicalvolume;
  static G4int eventIDoldB;
  static G4int Nb;
  static G4int runIDoldB;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<sr1ShieldHit> sr1ShieldHitsCollection;

extern G4Allocator<sr1ShieldHit> sr1ShieldHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* sr1ShieldHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) sr1ShieldHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void sr1ShieldHit::operator delete(void *aHit)
{
  sr1ShieldHitAllocator.FreeSingle((sr1ShieldHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
