//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef sr1MagneticField_H
#define sr1MagneticField_H

#include "G4UniformMagField.hh"

class G4FieldManager;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


class sr1MagneticField: public G4UniformMagField
{
  public:
  
   sr1MagneticField(G4ThreeVector);  //  The value of the field
   sr1MagneticField();               //  A zero field
  ~sr1MagneticField();  
      
   //Set the field (fieldValue,0,0)
   void SetFieldValue(G4double fieldValue);
   void SetFieldValue(G4ThreeVector fieldVector);
      
   G4ThreeVector GetConstantFieldValue();

  protected:

      // Find the global Field Manager
      G4FieldManager* GetGlobalFieldManager();   // static 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


G4bool CheckOverlaps(G4int res=1000, G4double tol=0., G4bool verbose=true);
