#include "globals.hh"
#include "G4MagneticField.hh"

class sr1GaussianField
#ifndef STANDALONE
 : public G4MagneticField
#endif
{
private:
  G4double ffieldValue;
  G4double fieldMagnitude,RMS;
public:
  sr1GaussianField(double fieldValue, double sigma);
  void  GetFieldValue( const  double point[4], double *Bfield ) const;
  G4double GetFieldSetValue();
};
