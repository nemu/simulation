//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//*
//             LOW ENERGY MUON SPIN RELAXATION, ROTATION, RADIATION   Geant4 SIMULATION
//  ID    : MEYER.hh , v 1.0
//  AUTHOR: Taofiq PARAISO
//  DATE  : 2005-04
//                   
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
//
//  &           &&&&&&&&&&                            &&&&&&&         &&&&&&&&
//  &           &                                   &&      &&       &       &&
//  &           &                   &      &       &                &       &&
//  &           &&&&&&&            &      &         &&&&&&         &&&&&&&&  
//  &           &                 &      &&                &      &      &&
//  &           &                &&     &  &     &&      &&      &        &
//  &&&&&&&&&&  &&&&&&&&&&      &  &&&&&    &&    &&&&&&&       &        &&   
//                             &
//                            &
//                           &
//                         &  
//                             MEYER
/*
  fIRST IMPLEMENTATION BY ANLSEM,H. IN FORTRAN
  C++ CONVERSION T.K.PARAISO 04-2005

  !!! IMPORTANT  !!!
 
  Notice: 
  Tables definition changes between FORTRAN and C++:
  1/ Fortran indices start at 1 and C++ indices start at 0
  2/ Tables are defined as table[column][row] in Fortran
  table[row][column] in c++

  usefull reference 
  http://gershwin.ens.fr/vdaniel/Doc-Locale/Langages-Program-Scientific/Fortran/Tutorial/arrays.htm

*/
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$// 


#ifndef meyer_h
#define meyer_h 1

#include <iomanip>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <ios>
#include "globals.hh"

class meyer
{
 public:
  meyer();
 ~meyer();

 void GFunctions(double*, double*, double);
 void Get_F_Function_Meyer(double tau, double Ekin, double Z1, double Z2, double m1, double m2);
 void F_Functions_Meyer( double tau,double thetaSchlange,double *f1,double *f2);
 void Get_F_Function(double tau,double theta, double Ekin, double Z1, double Z2, double m1, double m2, double* F);

};

#endif
