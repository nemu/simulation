% Electric field

% Read file (check the number of header lines) and assing variables.
filename = 'E3D_cap_symm.map';
[x y z Ex Ey Ez] = textread(filename,'%f %f %f %f %f %f','headerlines',8);

z_ind = find (x==0 & y==0); % Determine indices of the points along z axis.
z_p = z(z_ind);   % z values along z axis
Ex_p = Ex(z_ind); % Ex field profile along z axis

plot(z_p,-Ex_p*0.8e-8,'m-');

%Calculate effective field length l_eff = Int(Ex dz) / max(Ex).

z_step = max(diff(z));
i_center = find (x==0 & y==0 & z==0); % Field center index
if Ex(i_center) < 0, % If the field at center is negative use min, otherwise max.
   l_effE = sum(Ex_p)*z_step/min(Ex_p);
else
   l_effE = sum(Ex_p)*z_step/max(Ex_p);
end
   
ylim = get(gca,'YLim');
hl1 = line([-l_effE/2 -l_effE/2],[ylim(1) ylim(2)]);
hl2 = line([ l_effE/2  l_effE/2],[ylim(1) ylim(2)]);
set(hl1,'Color','m'); set(hl2,'Color','m'); 
text(0.75,0.8,['Eff. length E = ',num2str(l_effE,3), ' cm'],'Units','Normal','Color','m')

xlabel('z coordinate (cm)','FontSize',14)
ylabel('EM Field (arb. unit)','FontSize',14)
title('Field profile along z axis','FontSize',16)

hold on

% Magnetic field
filename = 'B3D_iron1.map';
[x y z Bx By Bz] = textread(filename,'%f %f %f %f %f %f','headerlines',5);

z_ind = find (x==0 & y==0); % Determine indices of the points along z axis.
z_p = z(z_ind);   % z values along z axis
By_p = By(z_ind); % By field profile along z axis

plot(z_p,-By_p,'b-');

%Calculate effective field length l_eff = Int(By dz) / max(By).

z_step = max(diff(z));
i_center = find (x==0 & y==0 & z==0); % Field center index
if By(i_center) < 0, % If the field at center is negative use min, otherwise max.
   l_effB = sum(By_p)*z_step/min(By_p);
else
   l_effB = sum(By_p)*z_step/max(By_p);
end
   
ylim = get(gca,'YLim');
hl3 = line([-l_effB/2 -l_effB/2],[ylim(1) ylim(2)]);
hl4 = line([ l_effB/2  l_effB/2],[ylim(1) ylim(2)]);
text(0.75,0.75,['Eff. length B = ',num2str(l_effB,3), ' cm'],'Units','Normal','Color','b')
text(0.03,0.8,{'C-magnet 20x40 cm poles','Symm. capacitor voltages'},'Units','Normal','FontSize',12)
set(gca,'FontSize',14)
axis([-50 50 0 8e-7])
