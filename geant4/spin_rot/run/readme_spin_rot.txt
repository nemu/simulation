How to carry out a simulation with the spin rotator suite sr1:

1. Export the original Opera magnetic field maps (first octant) to an 
   extended field map (all octants), suitable for use with Geant4.
   - Launch Matlab and then run field_import.
   - Use help field_import for usage hints.

2. Copy the extended field map in the directory spin_rot/run

3. If needed, edit one of the 11xx.mac files and change the line:
   ... 2DE  L1_Erz.map  log_L1ENV  8. 
   so that the L1 voltage coincides with the file name (e.g. 8 is 1108.mac).
   
   Check also that you are using the correct magnetic field map (line above):
   ... 3DB  sep_1nl_ext.map  log_BField  0.0312
   Run it once with: sr1 1108.mac > data/1108.txt and, if everything is 
   correct, proceed to the next step.
   
4. Save the current macro and make as many copies of it as are the L1 
   voltages to explore. Edit each of them (e.g. with vim) and modify 
   the L1ENV  8 line to coincide with the respective file name.
   
5. Run a BATCH simulation using the command (edit if necessary):
   run_jobs &
   Do not forget to issue the g49 command BEFORE launching the job.

6. Check the simulation progress by using: ls -trl data.
   NOTE: To stop a running job, bring it to foreground first with 
   e.g. fg run_jobs and then stop it by Ctrl+C (or kill %1).
   To suspend a job temporarily just type Ctrl+Z (refer to ref_card).
   
7. Analyse the generated root and txt files. If only the total number 
   of events is needed, then use the sr_transm.C script inside root:
   
   - Enter root with: root -l
   - Load the script with: .L sr_transm.C
   - Execute the script:   sr_transm("data/sr1_1108.root")
   - Quit root using:      .q
   
8. The root script generates both a screen output as well as an .eps file.
   One can view it with, e.g.: gv data/sr_bspot_8.eps, get the needed 
   information from the statistics and copy the Entries value by hand in 
   the final matlab file (sr_transm_L1.m).
   For a batch processing use the following fast grep command:
   less data/sr_bspot_*.eps | grep bspot_histEntries

9. If not needed, clear the root, txt and eps simulation data:
   rm data/sr1_11*.root, etc.

  