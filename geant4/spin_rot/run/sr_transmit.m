% Transmission of Spin Rotator as a function of angular dispersion of incoming muons.
% Settings B = 360 G, E = 21 kV.
d = [...
0.0   89.0 
0.2   88.8
0.4   86.8
0.7   81.0
1.0   74.1
1.5   62.6
2.0   51.7
2.5   45.6
3.0   42.0
3.5   37.71
4.0   34.03
];

d1 = [...
0.0   92.99
0.2   92.58
0.4   91.80
0.7   93.08
1.0   92.85
1.5   87.82
2.0   75.97
2.5   62.93
3.0   49.73
3.5   40.12
4.0   34.66
];

ang_sigma = d(:,1);  sr_trans = d(:,2);
ang_sigm1 = d1(:,1); sr_trans1 = d1(:,2);
hp = plot(ang_sigma, sr_trans,'ro-');
hold on
hp1= plot(ang_sigm1, sr_trans1,'bo-');
set(hp, 'MarkerFaceColor','w','MarkerSize',8)
set(hp1,'MarkerFaceColor','w','MarkerSize',8)
legend('SR ON','SR OFF',3)
axis([0 4 0 100])
grid on
xlabel('Muon beam angular dispersion \sigma (deg.)','FontSize',14)
ylabel('Spin Rotator transmission (%)','FontSize',14)
text(2.5, 85, [{'E = 21 kV, B = 360 G'},{'10 mm symm. capacitor'},{'p rejection: total'}],'FontSize',14)
set(gca,'FontSize',14)