% Test of Spin Rotator transmittance as a function of Lens L1 voltage
% Simulation performed with 1e4 events (9908), B = 360 G, E = 21 kV/m
% To get count number use, e.g.: less sr_bspot_*.eps | grep bspot_histEntries 

d0 = [ ... % Parallel beam = > alpha = 0
% L1 (kV)  Counts
 0.0       8684
 1.0       8702
 2.0       8755
 3.0       8780
 4.0       8847
 5.0       8927
 6.0       8983
 7.0       8985
 8.0       8982
 9.0       8979
 9.5       8976
10.0       8964
10.5       8938
11.0       8869
11.3       8758
12.0       8395
13.0       7522 
14.0       6345
15.0       5305
];

da = [... % alpha = 1 deg., alpha = 2 deg., third column SR OFF, alpha = 1 deg.
 0.0       5777     3688     8794        
 1.0       5687     3781     8791 % 9005 for alpha = 0
 2.0       5780     3790     8804
 3.0       5892     3782     8825
 4.0       5919     3846     8853
 5.0       5986     3881     8898
 6.0       6200     3925     8944 % 8991 for alpha = 0 
 7.0       6453     4192     8959
 8.0       6724     4340     8975
 9.0       7058     4542     8981
10.0       7337     4889     8981
11.0       7452     5291     8964  % 8972 for alpha = 0
12.0       7322     5663     8881
13.0       6896     5835     8593
14.0       6057     5497     7975
15.0       5082     4827     6911  % 7034 for alpha = 0
];

L1_V0 = d0(:,1); counts0 = d0(:,2); % alpha = 0 deg.
L1_V1 = da(:,1); counts1 = da(:,2); % alpha = 1 deg.
L1_V2 = da(:,1); counts2 = da(:,3); % alpha = 2 deg.
L1_V3 = da(:,1); counts3 = da(:,4); % SR OFF! alpha = 1 deg.
counts0 = counts0/100; counts1 = counts1/100;
counts2 = counts2/100; counts3 = counts3/100;

hp = plot(L1_V0, counts0,'bo-', L1_V1, counts1,'mo-', L1_V2, counts2,'ro-', L1_V3, counts3,'ko-');
set(hp, 'MarkerFaceColor','w','MarkerSize',8)
legend('Ang. spread = 0 deg.','Ang. spread = 1 deg.','Ang. spread = 2 deg.',' SR off, Ang. = 0 deg.',4)
axis([0 15 0 100])
grid on
xlabel('Lens L1 voltage (kV)','FontSize',14)
ylabel('Spin Rotator transmission (%)','FontSize',14)
text(2, 15, [{'E = 21 kV, B = 360 G'},{'10 cm symm. capacitor'},{'E_0 = 20 keV'}],'FontSize',14)
set(gca,'FontSize',14)





% Simulations as a function of L1 voltage for E0 = 15 keV and 20 keV,
% using alpha = 1.4 deg. from previous G3 simulations.

de = [...%E0 = 15, E0 = 20 keV, alpha = 1.4 deg. fixed. Last col. SR OFF at 15 keV.
 0.0     4990    5065     7635
 1.0     4974    5063     7665
 2.0     5056    5048     7722
 3.0     5023    5122     7840
 4.0     5229    5190     8009
 5.0     5457    5269     8253
 6.0     5774    5415     8470
 7.0     6174    5607     8639
 8.0     6716    5784     8714
 9.0     6964    6107     8642
10.0     6733    6462     8198
11.0     5628    6790     7043
12.0     4510    6923     5233
13.0     3347    6745     3542
14.0     2431    6077     2175
15.0     1568    5355     1268
];


L1_V = de(:,1); % L1 Voltage values (alpha = 1.4 deg.)
counts1 = de(:,2); counts4 = de(:,4); % E0 = 15 keV, SR ON/OFF
counts2 = de(:,3); % E0 = 20 keV, SR ON 
% For SR OFF at 20 keV and alpha = 1 deg. see the variable counts3 above
counts1 = counts1/100; counts2 = counts2/100; counts4 = counts4/100;

figure(2)
hp = plot(L1_V, counts1,'bv-', L1_V, counts2,'mv-', L1_V, counts4,'ro-', L1_V3, counts3,'ko-');
set(hp, 'MarkerFaceColor','w','MarkerSize',8)
legend('E_0 = 15 keV','E_0 = 20 keV','SR off (15 keV)','SR off (20 keV)',4)
axis([0 15 0 100])
grid on

% E_0 = 20 keV, E = 21 kV, B = 360 G
% E_0 = 15 keV, E = 18.1865 kV, B = 311.7691 G

xlabel('Lens L1 voltage (kV)','FontSize',14)
ylabel('Spin Rotator transmission (%)','FontSize',14)
text(1, 15, [{'E_0 = 20 keV, E = 21 kV, B = 360 G'},...
{'E_0 = 15 keV, E = 16.3 kV, B = 312 G'},...
{'10 cm symm. capacitor'},{'Ang. spread = 1.4 deg.'}],'FontSize',14)
set(gca,'FontSize',14)




% Simulations as a function of L1 voltage for E0 = 15 keV and 20 keV,
% using alpha = 1.4 deg. and CONSTANT E and B fields.

dc = [...%E0 = 15: a) EB const; b) only E const. (19.3 kV). c) E0 = 20 keV, EB const, 
% d) E0 = 15 keV using E and B MAPS with improved E field map (E3D_cap_symm_edge.map instead of E3D_cap_symm.map)
% e) E0 = 15 keV using E and B MAPS with improved E field map (E3D_edge_rod.map)
% f) E0 = 15 keV using E and B MAPS with improved E field map (E3D_edge_3rods.map)  <= BEST E-FIELD MAP!
%         a        b       c       d       e       f
 0.0     8065    7192    8082     5772    5669    7250
 1.0     8102    7244    8197     5847    5612    7278
 2.0     8190    7272    8243     5847    5646    7293
 3.0     8257    7365    8190     5915    5825    7374
 4.0     8379    7540    8301     6082    5906    7495
 5.0     8527    7753    8445     6357    6219    7709
 6.0     8659    8016    8541     6673    6515    7965
 7.0     8762    8299    8669     7114    6956    8290
 8.0     8783    8503    8874     7578    7411    8527 
 9.0     8747    8497    8792     7738    7653    8577
10.0     8525    8134    8920     7392    7243    8319
11.0     7790    7194    8945     6466    6291    7553
12.0     6301    5714    8889     5215    5057    6225
13.0     4443    4090    8762     4109    3650    4646
14.0     2901    2641    8326     2874    2377    3127
15.0     1674    1607    7605     1834    1485    1982
];

L1_V = dc(:,1); % L1 Voltage values (alpha = 1.4 deg.)
counts1 = dc(:,2); counts2 = dc(:,3); % E0 = 15 keV, EB const, E const only
counts3 = dc(:,4); % E0 = 20 keV, EB const
counts4 = dc(:,5); % E0 = 15 keV, using E and B maps with improved E field map (E3D_cap_symm_edge.map)
counts7 = dc(:,6); % E0 = 15 keV, using E and B maps with improved E field map (E3D_edge_rod.map)
counts8 = dc(:,7); % E0 = 15 keV, using E and B maps with improved E field map (E3D_edge_3rods.map) <- BEST!
% For SR OFF at 20 keV and alpha = 1 deg. see the variable counts3 above
counts1 = counts1/100; counts2 = counts2/100; counts3 = counts3/100; 
counts4 = counts4/100; counts5 = de(:,2)/100; counts6 = de(:,4)/100;
counts7 = counts7/100; counts8 = counts8/100;

figure(3)
hp3 = plot(L1_V, counts1,'bv-', L1_V, counts2,'mv-', L1_V,  counts4,'yv-', ... 
           L1_V, counts5,'gv-', L1_V, counts6,'ro-', L1_V3, counts3,'kd-');
set(hp3, 'MarkerFaceColor','w','MarkerSize',8)                            
legend('Const. E, B (15 keV)','Const. E (15 keV)','Map1 E, B (15 keV)','Map0 E, B (15 keV)','SR off (15 keV)','Const. E, B (20 keV)',3)
axis([0 15 0 100])
grid on

% E_0 = 20 keV, E = 21 kV, B = 360 G
% E_0 = 15 keV, E = 18.1865 kV, B = 311.7691 G

xlabel('Lens L1 voltage (kV)','FontSize',14)
ylabel('Spin Rotator transmission (%)','FontSize',14)
title('Spin Rot. transmission - Constant field vs. Field maps at 15 keV','FontSize',16)
text(1, 15, [{'E_0 = 20 keV, E = 21 kV, B = 360 G'},...
{'E_0 = 15 keV, E = 16.6 kV, B = 312 G'},...
{'12 cm symm. capacitor'},{'Ang. spread = 1.4 deg.'}],'FontSize',14)
set(gca,'FontSize',14)



figure(4)
hp4 = plot(L1_V, counts1,'bv-', L1_V, counts2, 'mv-', L1_V, counts8,'kv-', ...
           L1_V, counts7,'rv-', L1_V, counts4,'yv-',  L1_V, counts5,'gv-');
set(hp4, 'MarkerFaceColor','w','MarkerSize',8)                            
legend('Const. E, B fields','Const. E field, B map','Map3 E "3 rods"','Map2 E "1 rod"','Map1 E "12 cm plates"','Map0 E "10 cm plates"',3)
axis([0 15 0 100])
grid on

xlabel('Lens L1 voltage (kV)','FontSize',14)
ylabel('Spin Rotator transmission (%)','FontSize',14)
title('Spin Rot. transmission - E-field const. vs. maps at 15 keV','FontSize',16)
text(6.8, 12, [{'E_0 = 15 keV, E = 15.8 kV, B = 312 G'},...
{'12 cm symm. capacitor'},{'Ang. spread = 1.4 deg.'}],'FontSize',14)
set(gca,'FontSize',14)




return

% SR TOT transm = SR_transm * L1 transm. (SR OFF) => SR_transm. = SR TOT transm / L1 transm. (SR OFF)
hold on
hp1 = plot(L1_V, counts2./counts4*100,'go-');
set(hp1, 'MarkerFaceColor','w','MarkerSize',8)
hp2 = plot(L1_V, da(:,2)./counts4,'yo-');
set(hp2, 'MarkerFaceColor','w','MarkerSize',8)

text(1,82,'L1 transm.','FontSize',12)
text(1,68,'SR transm.','FontSize',12)
text(1,53,'Tot. transm.','FontSize',12)