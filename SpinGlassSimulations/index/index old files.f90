!
!	Program to index old files
!
	character*4   file_index
	character*80  calculation, dummy
	character*48  g_t
	character*32  his
	integer       ifile, i,j
!
	ifile = 0
!
1	CONTINUE
200	write(6,201)
201	format(' '/' Give name of the calculation     > ', \)
	read(5,'(a60)') calculation
	IF ( calculation(1:1) .EQ. ' ' ) STOP ' Stopped by operator '
    l_calc = index( calculation, ' ') - 1
!
	open(1,file=calculation(1:l_calc)//'.inp',status='old',action='read',err=200 )
	open(3,file=calculation(1:l_calc)//'.g_t',status='old',action='read')
	open(4,file=calculation(1:l_calc)//'.his',status='old',action='read')
!
	read(1,'(a80)') comment
	read(4,'(a80)') dummy
!
300	read(1,'(a80)',end=900) dummy
	ifile = ifile + 1							  ! increase outputfile number
	write(file_index,'(''_'',i3)') ifile		  ! generate file_name
	DO j = 2, 4 
	  IF (file_index(j:j) .EQ. ' ' ) file_index(j:j) = '0'
	  END DO
!
	write(6,*) ' file index = ', file_index
!
	open(8,file=calculation(1:l_calc)//file_index//'.g_t',status='unknown',action='write')
	open(9,file=calculation(1:l_calc)//file_index//'.his',status='unknown',action='write')
!
	DO j = 1, 1000
	  read(3,'(a48)',end=305) g_t
	  write(8,'(a48)') g_t
	  END DO
305	close(8)
!
310	read(4,'(a32)',end=390) his
	IF (his(2:2) .EQ. '-') goto 390
	write(9,'(a32)') his
	GOTO 310
!
390	close(9)
	GOTO 300
!
!	finish
!
900	close(1)
	close(3)
	close(4)
	Write(6,*)   ' end of *.inp file '
	Write(6,*) ' '
	GOTO 200
	END