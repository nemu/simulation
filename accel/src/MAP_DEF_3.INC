
c===============================================================================
c                          MAP_DEF_3.INC
c===============================================================================

c in diesem File sind die Dimensionen des Potentialmappen-Speichers fuer Mappe 3
c des Programms 'ACCEL' niedergelegt:

c Position der Folie relativ zur Kryoachse:

	real    xFoil
	common /xFoil/ xFoil


c the grid characteristics:

	real	Dx_,Dy_,Dz_
c	integer imax_,jmax_,kmax_

	parameter ( Dx_  = .125, Dy_  = .125, Dz_  = .500)
c	parameter ( imax_=   60, jmax_=  160, kmax_=   40)

	real	xmin,xmax
	integer imax,jmax,kmax
	common /map_3/ xmin,xmax, imax,jmax,kmax


c der Beginn des Uebergabebereichs zur naechsten Mappe:

	real xStartUeberUpp,xStartUeberLow
	common /map_3/ xStartUeberUpp,xStartUeberLow


c die naeherungsweise Obergrenze fuer die einzelnen Schrittlaengen:

	real dl_max
	parameter (dl_max = .2)


c the map:

	INCLUDE 'accel$sourcedirectory:MAPMAP.INC'

