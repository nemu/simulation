
c===============================================================================
c			   CALC_3D-FIELD_2
c===============================================================================

c Dieses Include-file wird in Unterprogramme 'EFELD_mappenname(x,E)' fuer die
c Berechnung von elektrischen Feldstaerken aus dreidimensionalen Potential-
c mappen eingebunden.

c Zusaetzlich zu diesem Includefile wird das Includefile 'CALC_3D-FIELD_1.INC'
c benoetigt.

c...............................................................................

c Teste, ob Raumpunkt innerhalb der Potentialmappe liegt:

	if (real_j.GT.jmax .OR. real_k.GT.kmax) then
	    returnCode_EFeld = 2
	    RETURN 1
	elseif (real_i.GT.imax) then
	    if (real_i.LT.real(imax)+1.e-5) then
		real_i = real(imax)
	    else
		returnCode_EFeld = 1
		RETURN 1
	    endif
	elseif (real_i.LT.0.) then
	    if (real_i.GE.-.1e-4) then
		real_i = 0.
	    else
c		write(*,*)'x = ',x
c		write(*,*)'real_i = ',real_i
c		write(*,*)'xmin,xmax = ',xmin,xmax
c		write(*,*)'dx_ = ',dx_
		returnCode_EFeld = 3
		RETURN 1
	    endif
	endif


c Bestimme naechstgelegene Stuetzstellen (stuetzstelle_q(n)) und die
c Komponenten des Abstands-Gittervektors zur allernaechsten Stuetzstelle
c (Abstand_q) sowie deren Betraege:

	stuetzstelle_i(1) = nint(real_i)
        Abstand_i = real_i - stuetzstelle_i(1) ! Abstand zur naeheren Stuetzstelle
	Abstand_i_Betrag = abs(Abstand_i)
	if (Abstand_i.gt.0.) then
            stuetzstelle_i(2) = stuetzstelle_i(1) + 1
	elseif (Abstand_i.lt.0.) then
            stuetzstelle_i(2) = stuetzstelle_i(1) - 1
        else
            stuetzstelle_i(2) = stuetzstelle_i(1)
        endif

        stuetzstelle_j(1) = nint(real_j)
        Abstand_j = real_j - stuetzstelle_j(1)
	Abstand_j_Betrag = abs(Abstand_j)
        if (Abstand_j.gt.0.) then
            stuetzstelle_j(2) = stuetzstelle_j(1) + 1
	elseif (Abstand_j.lt.0.) then
            stuetzstelle_j(2) = stuetzstelle_j(1) - 1
        else
            stuetzstelle_j(2) = stuetzstelle_j(1)
        endif

        stuetzstelle_k(1) = nint(real_k)
        Abstand_k = real_k - stuetzstelle_k(1)
	Abstand_k_Betrag = abs(Abstand_k)
        if (Abstand_k.gt.0.) then
            stuetzstelle_k(2) = stuetzstelle_k(1) + 1
	elseif (Abstand_k.lt.0.) then
            stuetzstelle_k(2) = stuetzstelle_k(1) - 1
        else
            stuetzstelle_k(2) = stuetzstelle_k(1)
        endif


c...............................................................................
c Berechnen des elektrischen Feldes:
c ----------------------------------
c
c In dieser Version wird nicht mehr vorausgesetzt, dass das Potential auf den
c Mappenraendern Null ist!
c Bei der Berechnung der Feldstaerke ist angenommen, dass die xy-Ebene (k==0)
c und die xz-Ebene (j==0) Symmetrieebenen sind:
c 
c 		map(i,-j,k) == map(i,j,k).
c 		map(i,j,-k) == map(i,j,k).
c
c Entlang j=0 ist also E(2)=0, entlang k=0 ist E(3)=0.
c
c (In der vorliegenden Version ist map(i,j,k) durch 
c	map(  k*(jmax+1)*(imax+1) + j*(imax+1) + i)  =
c	map( (k*(jmax+1) + j)*(imax+1) + i)
c zu ersetzen!)
c (i,j,k laufen von 0 weg, ebenso wie die Indizierung von 'map')
c...............................................................................

c Berechne in den beiden naechstgelegenen k-Ebenen die x-Komponente der Feld-
c staerke. Danach berechne tatsaechlichen Wert aus linearer Interpolation. Um
c die Feldstaerken in den einzelnen k-Ebenen zu bekommen, interpoliere jeweils
c linear zwischen den Werten auf den beiden naechstgelegenen j-Ketten der
c jeweiligen k-Ebene:

	i = stuetzstelle_i(1)

	do m = 1, 2
	    k = stuetzstelle_k(m)
	    do n = 1, 2
		j = stuetzstelle_j(n)
		ihelp = (k*(jmax+1)+ j)*(imax+1) + i
		if (i.EQ.imax) then
c		    E__(n) =  map(imax-1,j,k) - map(imax,j,k)
		    E__(n) =  map(ihelp-1)     - map(ihelp  )
		elseif (i.GT.0) then
c		    E__(n) = (-0.5+Abstand_i)*(map(i,j,k)-map(i-1,j,k))
c     +			   + ( 0.5+Abstand_i)*(map(i,j,k)-map(i+1,j,k))
		    E__(n) = (-0.5+Abstand_i)*(map(ihelp)-map(ihelp-1))
     +			   + ( 0.5+Abstand_i)*(map(ihelp)-map(ihelp+1))
		else
c		    E__(n) =  map(0,j,k) - map(1,j,k)
		    E__(n) =  map(ihelp) - map(ihelp+1)
		endif
	    enddo
	    E_(m) = E__(1) + Abstand_j_Betrag*(E__(2)-E__(1))
	enddo
	E(1) = E_(1) + Abstand_k_Betrag*(E_(2)-E_(1))

	E(1) = E(1) / Dx_	! Reskalierung entsprechend x-Gitterkonstanten


c Berechne die y-Komponente der Feldstaerke:

	j = stuetzstelle_j(1)

	do m = 1, 2
	    k = stuetzstelle_k(m)
	    do n = 1, 2
		i = stuetzstelle_i(n)
		ihelp = (k*(jmax+1)+ j)*(imax+1) + i
		if (j.EQ.jmax) then
c		    E__(n) =  map(i,jmax-1,k) - map(i,jmax,k)
		    E__(n) =  map(ihelp-(imax+1)) - map(ihelp)
		elseif (j.GT.0) then
c		    E__(n) = (-0.5+Abstand_j)*(map(i,j,k)-map(i,j-1,k))
c     +			   + ( 0.5+Abstand_j)*(map(i,j,k)-map(i,j+1,k))
		    E__(n) = (-0.5+Abstand_j)*(map(ihelp)-map(ihelp-(imax+1)))
     +			   + ( 0.5+Abstand_j)*(map(ihelp)-map(ihelp+(imax+1)))
		else   ! j=0 -> map(i,j-1,k) = map(i,j+1,k) == map(i,1,k)
c		    E__(n) = 2.0*Abstand_j*(map(i,0,k)-map(i,1,k))
		    E__(n) = 2.0*Abstand_j*(map(ihelp)-map(ihelp+(imax+1)))
		endif
	    enddo
	    E_(m) = E__(1) + Abstand_i_Betrag*(E__(2)-E__(1))
	enddo
	E(2) = E_(1) + Abstand_k_Betrag*(E_(2)-E_(1))

	E(2) = E(2) / Dy_	! Reskalierung entsprechend y-Gitterkonstanten
	if (x(2).LT.0) E(2) = -E(2)


c Berechne die z-Komponente der Feldstaerke:

	k = stuetzstelle_k(1)

	do m = 1, 2
	    j = stuetzstelle_j(m)
	    do n = 1, 2
		i = stuetzstelle_i(n)
		ihelp = (k*(jmax+1)+ j)*(imax+1) + i
		if (k.EQ.kmax) then
c		    E__(n)=  map(i,j,kmax-1) - map(i,j,kmax)
		    E__(n) =  map(ihelp-(jmax+1)*(imax+1)) - map(ihelp)
		elseif (k.GT.0) then
c		    E__(n) = (-0.5+Abstand_k)*(map(i,j,k)-map(i,j,k-1))
c     +			   + ( 0.5+Abstand_k)*(map(i,j,k)-map(i,j,k+1))
		    E__(n) = (-0.5+Abstand_k)*(map(ihelp)-map(ihelp-(jmax+1)*(imax+1)))
     +			   + ( 0.5+Abstand_k)*(map(ihelp)-map(ihelp+(jmax+1)*(imax+1)))
		else   ! k=0 -> map(i,j,k-1) = map(i,j,k+1) == map(i,j,1)
c		    E__(n) = 2.0*Abstand_k*(map(i,j,0)-map(i,j,1))
		    E__(n) = 2.0*Abstand_k*(map(ihelp)-map(ihelp+(jmax+1)*(imax+1)))
		endif
	    enddo
	    E_(m) = E__(1) + Abstand_i_Betrag*(E__(2)-E__(1))
	enddo
	E(3) = E_(1) + Abstand_j_Betrag*(E_(2)-E_(1))

	E(3) = E(3) / Dz_	! Reskalierung entsprechend z-Gitterkonstanten
	if (x(3).LT.0) E(3) = -E(3)

cd	write(18,*)'x,E = ',x,E

	RETURN

