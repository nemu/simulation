
c===============================================================================
c			        COM_ACCEL.INC   
c===============================================================================
c
c===============================================================================
c			I. Konstanten
c===============================================================================

c-------------------------------------------------------------------------------
c		Die Versionsnummer
c-------------------------------------------------------------------------------

	character*(*) version

	parameter ( version = '2.0.0' )


c 1.2.1: Variablen 'dy_TgtHolder','dz_TgtHolder' durch 'outerDy_TgtHolder' und
c	 'outerDz_TgtHolder' ersetzt. Variablen 'innerDij_TgtHolder' (i=y,z;
c	 j=1,2) neu eingefuehrt (fuer Targetgeometrie bei Run10).
c	 Die neuen Schleifenparmeter B_TD und B_Helm fuer die Magnetfelder von
c	 TD-Spule und Progen-Helmholtzspule neu eingefuehrt, jedoch noch keine
c	 Berechnungen implementiert.
c 1.2.2: 26-jan-1998: AH: implementation of 'adjustSigmaE0'
c 1.2.3: 02-feb-1998: AH: implementation of 'E0InterFromFile'
c 2.0.0: 22-jan-1998: AH: as Verwion 1.2.3. Renamed to 2.0.0 to flag that
c			  this is the last version maintained by A.H.

c-------------------------------------------------------------------------------
c		Die Ausgabekanaele
c-------------------------------------------------------------------------------

	integer lunREAD, lunScreen, lunLOG, lunNTP, lunPHYSICA, lunTMP
	integer lunMESSAGE
	integer lunINFO,lunStart,lunDecay,lunZwi1,lunZwi2


	parameter ( lunScreen   =  6 )
	parameter ( lunTMP      = 16 )
	parameter ( lunREAD     = 17 )
	parameter ( lunLOG      = 18 )
	parameter ( lunNTP      = 19 )
	parameter ( lunPHYSICA  = 20 )
	parameter ( lunMESSAGE  = 14 )

	parameter ( lunINFO     = 40 )
	parameter ( lunStart    = 41 )
	parameter ( lunDecay    = 42 )
	parameter ( lunZwi1     = 43 )
	parameter ( lunZwi2     = 44 )


c Die Tabellenfiles werden entsprechend ihrer Nummer den Ausgabeeinheiten
c (lunPHYSICA + 1) bis (lunPHYSICA + stat_Anzahl) zugeordnet.


c die id des Ausgabe-NTupels:

	integer idNTP
	parameter (idNTP = 5)


c-------------------------------------------------------------------------------
c		Zuteilung der GebietsNummern k (0 <= k <= Gebiete_Anzahl)
c		zu den Gebieten
c
c (Zuteilung muss mit Definition bei 'MUTRACK' uebereinstimmen (ansonsten waere
c in MUTRACK eine Uebersetzung notwendig)
c-------------------------------------------------------------------------------

	integer	target,upToGrid1,upToGrid2,upToHeShield,upToLNShield

c		    GEBIET	  'k'
	parameter ( target	=  0 )	! <- zaehlt nicht fuer 'Gebiete_Anzahl'!
	parameter ( upToGrid1	=  1 )
	parameter ( upToGrid2	=  2 )
	parameter ( upToHeShield=  3 )
	parameter ( upToLNShield=  4 )

	integer     Gebiete_Anzahl
	parameter ( Gebiete_Anzahl=4 )	! <- Startpkt 'Target' zaehlt nicht !! 

	character Gebiet_Text(Gebiete_Anzahl)*40
	COMMON	  Gebiet_Text


c-------------------------------------------------------------------------------
c	Zuteilungen der Schleifenparameter zu den Feldelemenkten k
c       (1 <= k <= par_Anzahl) von par(i,k), n_par(k), parWert(k), par_text(k)
c-------------------------------------------------------------------------------

	integer UTarget,UGuard, UGi1, BTD,BHelm, mass,charge,
     +		ener,yPos,zPos,thetAng,phiAng

c		  PARAMETER   'k'

	parameter ( UTarget =  1 )
	parameter ( UGuard  =  2 )
	parameter ( UGi1    =  3 )

	parameter ( BTD     =  4 )
	parameter ( BHelm   =  5 )

	parameter ( mass    =  6 )
	parameter ( charge  =  7 )

	parameter ( ener    =  8 )
	parameter ( yPos    =  9 )
	parameter ( zPos    = 10 )
	parameter ( thetAng = 11 )
	parameter ( phiAng  = 12 )

	integer     par_Anzahl
	parameter ( par_Anzahl=12)  ! <- 'Zufalls-Schleife' zu k=0 zaehlt nicht!


c-------------------------------------------------------------------------------
c		Code-Nummern fuer das Schicksal des Teilchens
c-------------------------------------------------------------------------------

	integer code_vor_Mappe,code_neben_Mappe
	integer code_hit_grid2,code_hit_grid1,code_hit_TgtHolder
	integer code_ok
	integer smallest_code_Nr

	integer code_decay,code_reflektiert,
     +		code_wand,code_lost,code_dtsmall

c		    SCHICKSAL		'code'

	parameter ( smallest_code_Nr    = -5 )

	parameter ( code_vor_Mappe      = -5 )
	parameter ( code_neben_Mappe    = -4 )
	parameter ( code_hit_grid2      = -3 )
	parameter ( code_hit_grid1      = -2 )
	parameter ( code_hit_TgtHolder  = -1 )
	parameter ( code_ok 	        =  0 )

	parameter ( code_decay		=  1 )
	parameter ( code_reflektiert    =  2 )
	parameter ( code_wand		=  3 )
	parameter ( code_lost		=  4 )
	parameter ( code_dtsmall	=  5 )

	integer     highest_code_Nr
	parameter ( highest_code_Nr	=  5 )

	character code_Text(smallest_code_Nr:highest_code_Nr)*27
	COMMON	  code_text


c-------------------------------------------------------------------------------
c	Zuteilung der Statistiken zu den Feldelementen k ( 1<= k <= stat_Anzahl)
c	von statInMemory(k),createTabelle(k),statNeeded(k),statMem(i,k)
c-------------------------------------------------------------------------------

	integer Nr_S1M2

c		    STATISTIK     'k'	
	parameter ( Nr_S1M2	=  1 )

	integer Stat_Anzahl
	parameter (Stat_Anzahl = 1)


c===============================================================================
c			II. Variablen in Commonbloecken
c===============================================================================

c-------------------------------------------------------------------------------
c		die Gebietsnummer
c-------------------------------------------------------------------------------

        integer	Gebiet0	     ! GebietsNummer beim Start
        integer	Gebiet	     ! aktuelle GebietsNummer

	integer StartFlaeche

	COMMON Gebiet0, StartFlaeche


c-------------------------------------------------------------------------------
c		zufallsverteilte Startparameter
c-------------------------------------------------------------------------------

c Energie:

	logical random_E0	  ! Zufallsverteilung fuer Startenergie?
	integer random_energy	  ! welche Verteilung fuer Startenergie?
	logical random_E0_equal   ! gleichverteilte Startenergie
	real	lowerE0,upperE0	  ! Grenzen fuer Zufallsverteilung
	logical random_E0_gauss   ! gaussverteilte Startenergie
	real	sigmaE0		  ! Breite der Gaussverteilung
	logical adjustSigmaE0 /.false./

	logical e0InterFromFile /.FALSE./
	real	E0Low(101)
	integer nE0Inter

c Position:

	logical random_pos	  ! Zufallsverteilung fuer Startposition?
	integer random_position   ! welche Verteilung fuer Startposition?
	logical random_y0z0_equal ! gleichverteilt auf Viereckflaeche
	logical random_r0_equal	  ! gleichverteilt auf Kreisflaeche
	logical random_y0z0_Gauss ! Gauss-verteilt auf Viereckflaeche
	logical random_r0_Gauss   ! Gauss-verteilt auf Kreisflaeche
	real	StartBreite,StartHoehe,StartRadius, sigmaPosition

c Winkel:

	logical random_angle	  ! Zufallsverteilung fuer Startwinkel?
	integer random_winkel     ! welche Verteilung fuer Startwinkel?
	logical random_lambert    ! Lambert-Verteilung
	logical random_gauss	  ! Gauss-Verteilung
	real	StartLambertOrd 
	real	SigmaWinkel	  ! Breite der Gaussverteilung

	logical	ener_offset,pos_offset,angle_offset ! Falls Zufallsverteilung
				  ! mit durch Startparameter vorgegebenen
				  ! Offsets

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON random_E0,random_energy,random_E0_equal,lowerE0,upperE0
	COMMON random_E0_gauss,sigmaE0,adjustSigmaE0
	COMMON random_pos,random_position,random_y0z0_equal,
     +		random_r0_equal,random_y0z0_Gauss,random_r0_Gauss,
     +		StartBreite,StartHoehe,StartRadius,sigmaPosition
	COMMON e0InterFromFile,nE0Inter,E0Low 

	COMMON random_angle,random_winkel,random_lambert,random_gauss
	COMMON StartLambertOrd,sigmaWinkel

	COMMON ener_offset,pos_offset,angle_offset


c-------------------------------------------------------------------------------
c		Schleifen-Parameter
c-------------------------------------------------------------------------------
c (par(n,0) wird fuer die 'Zufallsschleife' verwendet)

        real       par(3,0:par_Anzahl)  ! min, max und step der ParameterSchleifen
        integer    n_par(0:par_Anzahl)  ! die Anzahl unterschiedl. Werte je Schleife
	real	   parWert(par_Anzahl)  ! der aktuelle Wert der Schleifenvariablen
        character  par_text(par_Anzahl)*22 ! Beschreibung jeder Variablen fuer Ausgabezwecke

	integer	   reihenFolge(par_Anzahl)	! Enthaelt die Reihenfolge der
						! Abarbeitung der Schleifenparameter

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

        COMMON	  par, parWert, n_par, par_text, reihenfolge


c-------------------------------------------------------------------------------
c		die Teilchenarten (artList)
c-------------------------------------------------------------------------------
 
	integer	    Arten_Zahl		    ! Anzahl der bekannten Arten
	parameter  (Arten_Zahl = 36)

	character*4 art_Name(Arten_Zahl)    ! Bezeichnungen der bekannten Arten
	real	    art_Masse(Arten_Zahl)   ! Massen der bekannten Arten
	real	    art_Ladung(Arten_Zahl)  ! Ladungen der bekannten Arten

	character   artList*50		    ! Liste zu startender Teilchen
	logical	    artList_defined	    ! wurde 'artList' gesetzt?
	logical	    mu_flag		    ! signalisiert, ob Myon-Teilchen erkannt wurde

	integer	    artenMax		    ! Maximalzahl in 'artList'
	parameter  (artenMax = 9)	    !     akzeptierter Arten
	integer	    art_Nr(artenMax)	    ! die in artList enthaltenen Arten
	integer	    artNr		    ! die Nummer der aktuellen Art

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON art_Name,art_Masse,art_Ladung
	COMMON artList,artList_defined,mu_flag
	COMMON art_Nr,artNr


c-------------------------------------------------------------------------------
c		Programmsteuerung
c-------------------------------------------------------------------------------

	real	scaleFactor	! Skalierungsfaktor fuer die Beschleuniger-
				! geometrie

	logical UseDecay	! MYONEN-Zerfall beruecksichtigen?
	logical UseDecay_	! MYONEN-Zerfall beruecksichtigen und Art ist myon?

	logical DEBUG		! DEBUG-Ausgabe?
	integer DEBUG_Anzahl	! fuer wieviele Starts je Schleife sollen
				! (so ueberhaupt) DEBUG-Informationen ausgegeben
				! werden? (in COMMON /output/)
	logical DEBUG_		! DEBUG .AND. startNr.LE.DEBUG_Anzahl

	logical notLastLoop	! aktuelle Schleife ist nicht letzte Schleife

	logical BATCH_MODE	! -> keine Graphikausgabe auf Schirm; keine
				! Ausgabe der Prozentzahl schon gerechneter
				! Trajektorien
	logical INPUT_LIST	! spezielle Version eines Batch-Runs
	integer ListLength	!
	logical gotFileNr	!
	character inputListName*20
	logical HVs_from_map	! sollen die Mappen-intrinsischen HVs verwendet werden?
				! (bei Mappen, die fuer feste HVs gerechnet wurden)
	logical TestRun		! 'true' -> RunNummern zwischen 9900 und 9999
        logical log_confine     ! Begrenze Schrittweite in Integrationsgebieten
                                !    -> 'dl_max_...'

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON /scaleFactor/ scaleFactor
	COMMON UseDecay,UseDecay_
	COMMON DEBUG,DEBUG_
	COMMON notLastLoop
	COMMON BATCH_MODE,INPUT_LIST,ListLength,gotFileNr,inputListName
	COMMON HVs_from_map,TestRun,log_confine


c-------------------------------------------------------------------------------
c		Graphikausgabe
c-------------------------------------------------------------------------------

	logical graphics	! graphische Ausgabe?
	integer	graphics_Anzahl ! fuer wieviele Starts je Schleife?
	logical graphics_	! GRAPHICS .AND. startNr.LE.GRAPHICS_Anzahl

	integer n_postSkript    ! PostSkript-files erstellen?

	integer iMonitor	! Abtastfrequenz fuer Graphik und Debug (jeder
				! wievielte Schritt wird ausgegeben)


c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON  graphics,graphics_Anzahl,graphics_
	COMMON  n_postSkript, iMonitor


c-------------------------------------------------------------------------------
c		FileName
c-------------------------------------------------------------------------------

	character filename*20	! Name der Ausgabe-Dateien
	character mappenName*25 ! Namenskern der Potentialmappen
	integer   nameLength    ! reale Laenge von 'mappenName'

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON filename
	COMMON /mappenName/ mappenName, NameLength


c-------------------------------------------------------------------------------
c		Vorgaben fuer das Summary (.LOG-file)
c-------------------------------------------------------------------------------

	integer n_outWhere	  ! LogFile auf welche Ausgabekanaele geben?

	logical LogFile     	  ! Logfile erstellen?
	logical smallLogFile	  ! minimalversion des Logfiles erstellen?

	logical statsInSummary	  ! Statistiken in das Summary?
	logical statInSummary(Stat_Anzahl) ! welche Statistiken in das Summary?

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON n_outWhere
	COMMON LogFile,smallLogFile
	COMMON statsInSummary,statInSummary


c-------------------------------------------------------------------------------
c		WELCHE FILES sollen erzeugt werden?
c-------------------------------------------------------------------------------

	logical createPhysTab	  ! PAW-Gesamt-Tabelle (.PAW) erzeugen?
	logical NTP_Misc          ! SchleifenNr,StartNr,Mappe,Steps ins NTupel?
	logical NTP_start    	  ! Die Startgroessen ...?
	logical NTP_stop     	  ! Die Stopgroessen ...?
	logical NTP_40mm     	  ! Die auf x = 40 mm extrapolierte Ort ...?

	logical createTabellen			! Tabellen-files erzeugen?
	logical createTabelle(Stat_Anzahl)	! welche Tabellen-files erzeugen?

	character statName(stat_Anzahl)*9	! Tabellenfile-Ueberschriften
	character TabExt(stat_Anzahl)*9		! Extensions der Tabellenfiles

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON createPhysTab
	COMMON NTP_Misc,NTP_start,NTP_stop,NTP_40mm
	COMMON createTabellen,createTabelle,statName,TabExt

c-------------------------------------------------------------------------------
c		Fehlerkontrolle
c-------------------------------------------------------------------------------

	real	eps_x		! Fehlertoleranz bei Ortsberechnung
	real	eps_v		! Fehlertoleranz bei Geschw.Berechnung
	logical log_relativ	! relative Fehlerbetrachtung?

	integer maxStep		! maximale Anzahl an Integrationsschritten

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON eps_x,eps_v,log_relativ
	COMMON maxStep

c-------------------------------------------------------------------------------
c		haeufig benutzte Faktoren
c-------------------------------------------------------------------------------

	real Energie_Faktor  ! Faktor bei Berechn. der Energie aus der Geschw.
	real Beschl_Faktor   ! Faktor bei Berechn. der Beschleunigung im EFeld

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON Energie_Faktor
	COMMON /Beschl_Faktor/ Beschl_Faktor


c-------------------------------------------------------------------------------
c		Programmablauf
c-------------------------------------------------------------------------------

	real	x0(3),v0(3),E0  ! Startort, -geschwindigkeit und -energie
	real	lifetime	! individuelle Myon-Lebensdauer [ns]
	real	theta0		! 3D-Startwinkel gegen x_Achse
	real	phi0		! azimuthaler Startwinkel (y-Achse:0, z-Achse:90)

	real	x(3),t,v(3)	! Ort, Zeit, Geschwindigkeit
	integer destiny		! die Codezahl fuer das Schicksal des Teilchens
	integer lastMap		! die Nummer der letzten Potentialmappe fuer
				! individuelle Teilchen

        integer start_nr	! Startnummer des Teilchen (je Schleife)
	integer GesamtZahl	! Gesamtzahl der Teilchen (ueber alle Schleifen)
	integer SchleifenZahl	! Anzahl durchzufuehrender Schleifen
	integer SchleifenNr     ! Nummer der aktuellen Schleife
        integer	Steps		! Nummer des Integrationssteps (je Teilchen)

	integer seed		! fuer Zufallsgenerator

	real	dtsmall		! kleinster Zeitschritt fuer Integrationen
	integer maxBelowDtSmall ! max. tolerierte Anzahl an Unterschreitungen von
				! dtsmall
	integer n_dtSmall	! wie oft hat einzelnes Teilchen dtSmall unterschritten
	integer n_dtsmall_Max	! groesste aufgetretene Anzahl an Unterschreitungen
				! (ueber alle Schleifen)
	integer dtsmall_counter	! wieviele Teilchen haben dtsmall unterschritten
	integer Lost_counter	! wieviele Teilchen wurden wegen steps>maxSteps
				!	verlorengegebe
	logical OneLoop		! genau eine Schleife
	logical	OneStartPerLoop ! Zufallsschleife macht genau einen Durchlauf

	logical reachedEndOfMap, backOneMap
	logical log_percent

	logical freeGuard	! Spannung am Guardring separat?

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

c Die benannten Common-Bloecke sind teilweise fuer die NTupelausgabe noetig!

c	COMMON /STARTPARAMETER/ x0,v0,E0,theta0,phi0
	COMMON /x0/ x0
	COMMON /v0/ v0
	COMMON /E0/ E0
	COMMON /angle0/ theta0,phi0
	COMMON /lifeTime/       lifetime
	COMMON /TRAJEKTORIE/    t,x,v
	COMMON /basics/		SchleifenNr,start_Nr,lastMap,steps
	COMMON /gebiet/ gebiet,destiny
	COMMON GesamtZahl,SchleifenZahl
	COMMON dtsmall, maxBelowDtSmall, n_dtsmall, n_dtsmall_Max, dtsmall_counter
	COMMON Lost_counter
	COMMON OneLoop, OneStartPerLoop
	COMMON /seed/ seed	! COMMON /seed/ ist auch in manchen Subroutinen
				! explizit gesetzt! -> wird benoetigt!
	COMMON reachedEndOfMap,backOneMap
	COMMON log_percent
	COMMON freeGuard


c-------------------------------------------------------------------------------
c		Statistik
c-------------------------------------------------------------------------------

	real	Koord_NTP(8,0:Gebiete_Anzahl) ! Koordinatenspeicher fuer NTP-Ausgabe

	integer statDestiny(smallest_code_Nr:Gebiete_Anzahl*highest_code_Nr) 
					 ! Statistik der Teilchenschicksale

	real	statMem(9,stat_Anzahl)   ! Statistiken von Flugzeiten ext.
	logical statNeeded(stat_Anzahl)	 ! welche Statistiken muessen fuer die
					 ! geforderten Informationen gefuehrt werden?

c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	COMMON /KOORD_NTP/ Koord_NTP

	COMMON statDestiny
	COMMON statMem,statNeeded


c-------------------------------------------------------------------------------
c		Datenausgabe
c-------------------------------------------------------------------------------

	integer  lun(2), indx
	integer  indx1, indx2

        COMMON /OUTPUT/  lun, indx1,indx2,indx, DEBUG_Anzahl

