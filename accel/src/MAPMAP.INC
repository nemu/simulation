
c===============================================================================
c			MAPMAP.INC
c===============================================================================
c Dieser Includefile stellt den Speicherplatz fuer die Potentialmappen bereit.
c Die einzelnen Mappen werden nacheinander fuer die jeweiligen Integrations-
c abschnitte eingelesen.

	integer maxmaxmem
	parameter (maxmaxmem = 4e6)

	real	map(0:maxmaxmem)
	COMMON /map/ map

