c===============================================================================
c			   CALC_3D-FIELD_1
c===============================================================================

c Dieses Include-file wird in Unterprogramme 'EFELD_mappenname(x,E)' fuer die
c Berechnung von elektrischen Feldstaerken aus dreidimensionalen Potential-
c mappen eingebunden.

c Zusaetzlich zu diesem Includefile wird das Includefile 'CALC_3D-FIELD_2.INC'
c benoetigt.


	real real_i,real_j,real_k    ! x,y,z im Mappensystem in Gittereinheiten

	integer stuetzstelle_i(2)    ! naechste Stuetzstellen in x-,
	integer stuetzstelle_j(2)    !				 y- und
	integer stuetzstelle_k(2)    !				 z- Richtung

	real	Abstand_i,Abstand_i_Betrag ! Entfernung zur naechsten Stuetzstelle
	real	Abstand_j,Abstand_j_Betrag !     (in Gittereinheiten!)
	real	Abstand_k,Abstand_k_Betrag

	integer	i,j,k, n,m, ihelp

        real x(3),E(3)		     ! Ort und Feldstaerke
	real E_(2),E__(2)	     ! Hilfsspeicher fuer Feldberechnung


c Falls Testort ausserhalb der Mappe liegt oder Anode getroffen hat:

	integer returnCode_EFeld
	COMMON /returnCode_EFeld/ returnCode_EFeld
		!   1: Testort hinter der Mappe
		!   2: Testort neben der Mappe
		!   3: Testort vor der Mappe
		! 101: .... getroffene Elektroden


