$!******************************************************************************
$! DIESE KOMMANDOPROZEDUR DEFINIERT LOGICALS UND SYMBOLS FUER DIE PROGRAMMIER-
$! ARBEIT, DAS KOMPILIEREN UND LINKEN VON ACCEL (INTERAKTIV)
$! SIE WIRD VON LOGIN.COM AUS AUFGERUFEN.
$!******************************************************************************
$ define accel$OBJdirectory     "accelSRC$directory:[EXE]"
$ OLDAC        :== "define accel$SOURCEdirectory UD1:[SIMULA.ACCEL.OLD_SOURCE]"
$ NEWAC        :== "define accel$SOURCEdirectory UD1:[SIMULA.ACCEL.SOURCE]"
$ NEWAC
$!------------------------------------------------------------------------------
$ ACCOM        :== "SET DEF UD1:[SIMULA.ACCEL.COM]"
$ ACSOURCE     :== "SET DEF accel$SOURCEdirectory"
$ ACMAP        :== "SET DEF accel$MAPPENdirectory"
$ FORAC        :== "@mutrack$COMdirectory:compile.com ACCEL _AC "
$ LINKAC       :== "@accel$COMdirectory:linkac.com"
$ LINKACV      :== "@accel$COMdirectory:linkacv.com"
$ LINKACD      :== "@accel$COMdirectory:linkacd.com"
$ LINKACVD     :== "@accel$COMdirectory:linkacvd.com"
$!==============================================================================
