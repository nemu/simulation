$ set verify
$ link -
	accel$directory:[exe]ACCEL, -
	accel$directory:[exe]SUB_ARTLIST, - 
	accel$directory:[exe]SUB_INTEGR_1, -
	accel$directory:[exe]SUB_INTEGR_2, -
	accel$directory:[exe]SUB_INTEGR_3, -
	accel$directory:[exe]SUB_INTEGR_4, -
	accel$directory:[exe]SUB_INTEGR_5, -
	accel$directory:[exe]SUB_INTEGR_6, -
	accel$directory:[exe]SUB_INPUT, -
	accel$directory:[exe]SUB_PICTURE, -
	accel$directory:[exe]SUB_OUTPUT,- 
	'cernlibs' /debug /exe=accel$directory:[exe]accel
$ purge /log accel$directory:[exe]
$ set noverify
