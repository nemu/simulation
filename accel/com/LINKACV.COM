$ set noverify
$!==============================================================================
$! Author: Anselm Hofer
$!
$! Commandoprozedur fuer das Compilieren und Linken des kompletten ACCEL-
$! Quelltextes. Aufzurufen mittels '$ LINKACV'. ('V' steht fuer 'Vollstaendig').
$!==============================================================================
$ set noon
$!==============================================================================
$ prog= "accel"
$ ext = "_AC"
$!==============================================================================
$ sourceDir =  "''prog'$SOURCEdirectory"
$ objectDir =  "''prog'$OBJdirectory"
$ executeDir = "''prog'$EXEdirectory"
$!==============================================================================
$ options  = "/fast /nolist"
$! options = "/fast /nolist /warn=nogeneral"
$!==============================================================================
$ archi = F$GETSYI("ARCH_NAME")    ! Host OS either "VAX" or "Alpha"
$ ext = "''ext'_''archi'"
$ if archi .EQS. "VAX" then options = ""
$ if P1 .NES. "" then options = "''options' ''P1'"
$
$ file = "ACCEL"
$ CALL compile
$ file = "SUB_ARTLIST
$ CALL compile
$ file = "SUB_INTEGR_1
$ CALL compile
$ file = "SUB_INTEGR_2
$ CALL compile
$ file = "SUB_INTEGR_3
$ CALL compile
$ file = "SUB_INTEGR_4
$ CALL compile
$ file = "SUB_INTEGR_5
$ CALL compile
$ file = "SUB_INTEGR_6
$ CALL compile
$ file = "SUB_INPUT
$ CALL compile
$ file = "SUB_PICTURE
$ CALL compile
$ file = "SUB_OUTPUT
$ CALL compile
$!==============================================================================
$ set verify
$ purge 'objectDir':*.OBJ
$ link -
	'objectDir':ACCEL'ext', -
	'objectDir':SUB_ARTLIST'ext', -
	'objectDir':SUB_INTEGR_1'ext', -
	'objectDir':SUB_INTEGR_2'ext', -
	'objectDir':SUB_INTEGR_3'ext', -
	'objectDir':SUB_INTEGR_4'ext', -
	'objectDir':SUB_INTEGR_5'ext', -
	'objectDir':SUB_INTEGR_6'ext', -
	'objectDir':SUB_INPUT'ext', -
	'objectDir':SUB_PICTURE'ext', -
	'objectDir':SUB_OUTPUT'ext',-
	'cernlibs' /exe='executeDir':ACCEL_'archi'
$ purge 'executeDir':*.EXE
$ set on
$ set noverify
$ EXIT
$
$!==============================================================================
$
$ COMPILE: SUBROUTINE
$ comp = "fortran ''sourceDir':''file' ''options' /object=''objectDir':''file'''ext'"
$ write sys$output "=============================================================================="
$ write sys$output "''COMP'"
$ comp
$ ENDSUBROUTINE
$!==============================================================================
