#include <cstdlib>
#include <cstring>
#include <cmath>

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#define PI_SQUARE 9.86960440109
#define GAMMA_MU  851.615503527 // Mc/T
#define PHI_0     2067.833758   // T m^2 / 10^{-18}, 10^{-18} originates form the fact that lambda is given in (nm)!

//--------------------------------------------------------------
typedef struct {
  double deadLayer;             ///< dead layer in (nm)
  vector<double> filmThickness; ///< film thickness in (nm)
  vector<double> lambda;        ///< lambda London in (nm)
  vector<double> applField;     ///< applied field in (T)
  string rgeFln;                ///< rge file name template
  vector<double> implEnergy;    ///< implantation energy in (eV)
} inputParam;

//--------------------------------------------------------------
class PMuonStop
{
  public:
    PMuonStop();
    PMuonStop(const double implEnergy);
    virtual ~PMuonStop() {}

    virtual void SetEnergy(const double energy) { fImplEnergy = energy; }
    virtual void SetNofZ(const vector<double> z, const vector<double> n);
    virtual double GetEnergy() { return fImplEnergy; }
    virtual double GetNofZ(double z);

  private:
    double fImplEnergy;
    vector<double> fZ;
    vector<double> fN;
};

PMuonStop::PMuonStop() : fImplEnergy(0)
{
}

PMuonStop::PMuonStop(const double implEnergy) : fImplEnergy(implEnergy)
{
}

void PMuonStop::SetNofZ(const vector<double> z, const vector<double> n)
{
  fN = n;
  fZ = z;

  // normalize n(z)
  double dz = fZ[1]-fZ[0];
  double sum = 0.0;
  for (unsigned int i=0; i<fN.size(); i++)
    sum += fN[i];
  for (unsigned int i=0; i<fN.size(); i++)
    fN[i] /= (sum*dz);
}

double PMuonStop::GetNofZ(double z)
{
  // find closest z value
  int idx=-1;
  for (unsigned int i=0; i<fZ.size(); i++) {
    if (z <= fZ[i]) {
      idx = (int) i;
      break;
    }
  }

  // nothing found
  if (idx == -1) {
    return 0.0;
  }

  // linear interpolation
  double n=0.0;

  if (idx == fZ.size()-1) // last entry
    n = fN[idx];
  else // somewhere inbetween where the linear interpolation works
    n = fN[idx] + (fN[idx+1] - fN[idx]) * (z-fZ[idx])/(fZ[idx+1]-fZ[idx]);

  return n;
}

//--------------------------------------------------------------
void syntax()
{
  cout << "usage: lth2m <paramFln> <outputFln>" << endl;
  cout << "         <paramFln>  input parameter file name" << endl;
  cout << "         <outputFln> output file name" << endl;
}

//--------------------------------------------------------------
double get_number(const string str)
{
  double result=-1.0;

  size_t pos=0;
  string nstr=str;

  pos = nstr.find_first_of(":");
  if (pos == string::npos) {
    cerr << endl << "**ERROR** '" << nstr << "' is not a proper paramter string." << endl;
    return result;
  }
  nstr.erase(0, pos+1);

  result = strtod(nstr.c_str(), (char **)NULL);

  return result;
}

//--------------------------------------------------------------
int read_parameter_file(const char *fln, inputParam &param)
{
  ifstream fin(fln, ifstream::in);

  if (!fin.is_open()) {
    cerr << endl << "**ERROR** couldn't open input file '" << fln << "'." << endl;
    return 1;
  }

  char line[256];
  string str("");
  double dval=0.0;
  unsigned int error=0;

  param.deadLayer = 0.0;

  while (fin.good() && (error==0)) {
    fin.getline(line, 256);
    if ((line[0] == '%') || (line[0] == '#'))
      continue;
    str = line;

    if (str.find("dead layer") != string::npos) {
      dval = get_number(str);
      if (dval != -1.0)
        param.deadLayer = dval;
      else
        error=2;
    } else if (str.find("film thickness") != string::npos) {
      dval = get_number(str);
      if (dval != -1.0)
        param.filmThickness.push_back(dval);
      else
        error=2;
    } else if (str.find("lambda") != string::npos) {
      dval = get_number(str);
      if (dval != -1.0)
        param.lambda.push_back(dval);
      else
        error=2;
    } else if (str.find("applied field") != string::npos) {
      dval = get_number(str);
      if (dval != -1.0)
        param.applField.push_back(dval);
      else
        error=2;
    } else if (str.find("rge file name") != string::npos) {
      param.rgeFln = str;
      size_t pos=param.rgeFln.find_first_of(":");
      if (pos == string::npos) {
        cerr << endl << "**ERROR** '" << str << "' has wrong syntax." << endl;
        error=3;
      } else {
        param.rgeFln.erase(0, pos+2);
      }
    } else if (str.find("impl energy") != string::npos) {
      dval = get_number(str);
      if (dval != -1.0)
        param.implEnergy.push_back(dval);
      else
        error=2;
    } else {
      if (str.find_last_of(" ") != str.length()-1) {
        cerr << endl << "**ERROR** found unrecognized string: '" << str << "'" << endl;
        error=4;
      }
    }
  }

  fin.close();

  for (unsigned int i=0; i<param.filmThickness.size(); i++) {
    if (2.0*param.deadLayer >= param.filmThickness[i]) {
      cerr << endl << "**ERROR** 2*deadLayer (" << 2.0*param.deadLayer << " nm) >= filmThickness (" << param.filmThickness[i] << " nm). This doesn't make any sense." << endl;
      error=5;
      break;
    }
  }

  return error;
}

//--------------------------------------------------------------
double calc_2nd_moment(const double filmThickness, const double deadLayer, const double lambda, const double field, PMuonStop *nz, const double dz)
{
  cout << "++++++++++++" << endl;
  cout << "info> filmThickness=" << filmThickness << " nm, deadLayer=" << deadLayer << " nm, lambda=" << lambda << " nm, field=" << field << " T, energy=" << nz->GetEnergy() << " keV" << endl;

  double z=0.0, zz=0.0;
  double bb2, bb, bz, bz2, dd=filmThickness-2.0*deadLayer; // dd is the superconducting thickness
  double lam2 = lambda*lambda;
  double kPre = 8.0 * PI_SQUARE / sqrt(3.0) * field * lam2 / PHI_0;
  double factor = 2.0;

  double sigma=0.0;
  double nzVal=0.0;
  vector<double> nzVec;
  bool leakIntoSubstrate = false;

  // in the dead layer
  z = 0.0;
  if (deadLayer > 0.0)
    cout << "info> in the dead layer ..." << endl;
  while (z < deadLayer) {
    nzVal = nz->GetNofZ(z);
    nzVec.push_back(nzVal);

    zz = z - filmThickness/2.0; // z: [-t/2 ... -d/2]

    for (unsigned int m=0; m<500; m++) {
      for (unsigned int n=0; n<500; n++) {
        if ((m==0) && (n==0))
          continue;

        if (m==0) {
          // 0+ = 0- -> factor 2
          bb2 = 1.0 + kPre * n*n;
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(zz+dd/2.0))-exp(bb/lambda*(zz-dd/2.0)));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
        } else if (n==0) {
          // +0 = -0 -> factor 2
          bb2 = 1.0 + kPre * m*m;
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(zz+dd/2.0))-exp(bb/lambda*(zz-dd/2.0)));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
        } else { // m!=0 && n!=0
          // ++ = --
          bb2 = 1.0 + kPre * (m*m - m*n + n*n);
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(zz+dd/2.0))-exp(bb/lambda*(zz-dd/2.0)));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
          // +- = -+
          bb2 = 1.0 + kPre * (m*m + m*n + n*n);
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(zz+dd/2.0))-exp(bb/lambda*(zz-dd/2.0)));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
        }
      }
    }
    z += dz;
  }

  double zOffset = z;

  // within the film
  cout << "info> in the sc part of the film ..." << endl;
  for (unsigned i=0; ; i++) {
    z = zOffset + i*dz;
    if (z > filmThickness-deadLayer) {
      leakIntoSubstrate = true;
      break;
    }
    nzVal = nz->GetNofZ(z);
    z = filmThickness/2.0 - z; // z for the calcuation has the zero in the middle of the film!
    if (nzVal == 0.0) {
      break;
    }
    nzVec.push_back(nzVal);
    for (unsigned int m=0; m<500; m++) {
      for (unsigned int n=0; n<500; n++) {
        if ((m==0) && (n==0))
          continue;

        if (m==0) {
          // 0+ = 0- -> factor 2
          bb2 = 1.0 + kPre * n*n;
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * (1.0 - 0.5*(exp(bb/lambda*(z-dd/2.0))+exp(-bb/lambda*(z+dd/2.0))));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
        } else if (n==0) {
          // +0 = -0 -> factor 2
          bb2 = 1.0 + kPre * m*m;
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * (1.0 - 0.5*(exp(bb/lambda*(z-dd/2.0))+exp(-bb/lambda*(z+dd/2.0))));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
        } else { // m!=0 && n!=0
          // ++ = --
          bb2 = 1.0 + kPre * (m*m - m*n + n*n);
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * (1.0 - 0.5*(exp(bb/lambda*(z-dd/2.0))+exp(-bb/lambda*(z+dd/2.0))));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
          // +- = -+
          bb2 = 1.0 + kPre * (m*m + m*n + n*n);
          bb = sqrt(bb2);
          bz = GAMMA_MU * field / bb2 * (1.0 - 0.5*(exp(bb/lambda*(z-dd/2.0))+exp(-bb/lambda*(z+dd/2.0))));
          bz2 = bz*bz;
          sigma += factor*bz2*nzVal;
        }
      }
    }
  }

  // in the substrate
  if (leakIntoSubstrate) {
    if (deadLayer > 0.0)
      cout << "info> in the dead layer/substrate ..." << endl;
    else
      cout << "info> in the substrate ..." << endl;
    for (unsigned int i=0; ; i++) {
      z = dd + dz*i; // z coordinate for n(z)
      nzVal = nz->GetNofZ(z);
      if (nzVal == 0.0) {
        break;
      }
      nzVec.push_back(nzVal);

      z = -dd/2.0 - dz*i; // z coordinate for B_z

      for (unsigned int m=0; m<500; m++) {
        for (unsigned int n=0; n<500; n++) {
          if ((m==0) && (n==0))
            continue;

          if (m==0) {
            // 0+ = 0- -> factor 2
            bb2 = 1.0 + kPre * n*n;
            bb = sqrt(bb2);
            bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(z+dd/2.0))-exp(bb/lambda*(z-dd/2.0)));
            bz2 = bz*bz;
            sigma += factor*bz2*nzVal;
          } else if (n==0) {
            // +0 = -0 -> factor 2
            bb2 = 1.0 + kPre * m*m;
            bb = sqrt(bb2);
            bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(z+dd/2.0))-exp(bb/lambda*(z-dd/2.0)));
            bz2 = bz*bz;
            sigma += factor*bz2*nzVal;
          } else { // m!=0 && n!=0
            // ++ = --
            bb2 = 1.0 + kPre * (m*m - m*n + n*n);
            bb = sqrt(bb2);
            bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(z+dd/2.0))-exp(bb/lambda*(z-dd/2.0)));
            bz2 = bz*bz;
            sigma += factor*bz2*nzVal;
            // +- = -+
            bb2 = 1.0 + kPre * (m*m + m*n + n*n);
            bb = sqrt(bb2);
            bz = GAMMA_MU * field / bb2 * 0.5*(exp(bb/lambda*(z+dd/2.0))-exp(bb/lambda*(z-dd/2.0)));
            bz2 = bz*bz;
            sigma += factor*bz2*nzVal;
          }
        }
      }
    }
  }

  double sum=0.0;
  for (unsigned int i=0; i<nzVec.size(); i++)
    sum += nzVec[i];

  cout << "info> sigma=" << sqrt(sigma/sum) << " (1/us)" << endl;

  return sqrt(sigma/sum);
}

//--------------------------------------------------------------
void calc_all_2nd_moment(const inputParam param, const vector<PMuonStop*> &nz, vector<double> &secMom)
{
  double dval = -1.0, dz=0.5;
  unsigned int idx=0;
  for (unsigned int i=0; i<param.filmThickness.size(); i++) {
    for (unsigned int j=0; j<param.lambda.size(); j++) {
      for (unsigned int k=0; k<param.applField.size(); k++) {
        for (unsigned int h=0; h<param.implEnergy.size(); h++) {
          // get index for the proper n(z)
          for (unsigned int m=0; m<nz.size(); m++) {
            if (nz[m]->GetEnergy() == param.implEnergy[h]) {
              idx = m;
              break;
            }
          }
          if (param.implEnergy[h] == -11.0) {
            if (param.filmThickness[i] > 125.0)
              dz = param.filmThickness[i]/250.0;
          }
          dval = calc_2nd_moment(param.filmThickness[i], param.deadLayer, param.lambda[j], param.applField[k], nz[idx], dz);
          secMom.push_back(dval);
        }
      }
    }
  }
}

//--------------------------------------------------------------
int write_result_file(const char *fln, const inputParam param, const vector<double> secMom)
{
  ofstream fout(fln, ofstream::out);

  fout << "% London Thin Film Second Moment Results:" << endl;
  fout << "%" << endl;
  fout << "% film thickness (nm), dead layer (nm), lambda (nm), applied field (T), impl. energy (eV), 2nd moment (1/us)" << endl;
  unsigned int idx=0;
  for (unsigned int i=0; i<param.filmThickness.size(); i++) {
    for (unsigned int j=0; j<param.lambda.size(); j++) {
      for (unsigned int k=0; k<param.applField.size(); k++) {
        for (unsigned int h=0; h<param.implEnergy.size(); h++) {
          fout << param.filmThickness[i] << ", " << param.deadLayer << ", " << param.lambda[j] << ", " << param.applField[k] << ", " << param.implEnergy[h] << ", ";
          if (idx < secMom.size())
            fout << secMom[idx++] << endl;
          else
            fout << "??" << endl;
        }
      }
    }
  }

  fout.close();
}

//--------------------------------------------------------------
int read_muon_stopping_distro(const unsigned int idx, const inputParam param, vector<double> &zz, vector<double> &nn)
{
  // make sure that zz and nn are empty
  zz.clear();
  nn.clear();

  if (param.implEnergy[idx] == -11) { // fake implantation energy
    double tt = param.filmThickness[0];
    // will calculate a Gaussian profile positioned around the center of the film
    // sigma will be 8.0/tt. The number of points will be <= 250
    unsigned int noOfPoints = 250;
    double step = tt/250;
    if (tt < 125.0) {
      step = 0.5;
      noOfPoints = (unsigned int)(tt/step);
    }
    cout << "info> noOfPoints=" << noOfPoints << ", step=" << step << endl;
    double z, n;
    double sigma = 8.0/tt;
    for (unsigned int i=0; i<noOfPoints; i++) {
      z=i*step;
      n=exp(-0.5*pow(sigma*(tt/2.0-z),2.0));
      zz.push_back(z);
      nn.push_back(n);
    }
  } else { // normal trimsp rge-files

    string fln("");
    char num[256];
    snprintf(num, sizeof(num), "%d", (int)param.implEnergy[idx]);
    fln = param.rgeFln + num + ".rge";

    ifstream fin(fln.c_str(), ifstream::in);
    if (!fin.is_open()) {
      cerr << endl << "**ERROR** couldn't open file: '" << fln << "'" << endl;
      return 1;
    }

    char line[256];
    double z, n;
    bool data=false;
    while (fin.good()) {
      fin.getline(line, 256);
      if (strstr(line, "DEPTH")) {
        data = true;
        continue;
      }
      if (data && (strlen(line) == 0))
        continue;
      if (data) {
        sscanf(line, "%lf %lf", &z, &n);
        zz.push_back(z/10.0); // A -> nm
        nn.push_back(n);
      }
    }

    fin.close();
  }

  return 0;
}

//--------------------------------------------------------------
int main(int argc, char *argv[])
{
  if (argc != 3) {
    syntax();
    return 1;
  }

  int status = 0;
  inputParam param;
  status = read_parameter_file(argv[1], param);
  if (status != 0)
    return 2;

  vector<double> zz, nn;
  vector<PMuonStop*> nz;
  unsigned int idx=0;
  for (unsigned int i=0; i<param.implEnergy.size(); i++) {
    status = read_muon_stopping_distro(i, param, zz, nn);
    if (status == 0) {
      nz.push_back(new PMuonStop(param.implEnergy[i]));
      nz[idx]->SetNofZ(zz,nn);
      idx++;
    } else {
      return 3;
    }
  }

  vector<double> secondMoment;
  calc_all_2nd_moment(param, nz, secondMoment);

  write_result_file(argv[2], param, secondMoment);

  return 0;
}
