c	read_mcv3k_out.for
c
	program read_mcv3k_out
c
c	program to read the unformatted output file of the MCV3K_TEST program
c	at the moment the "format" is
c	12 entries per record
c
c	1.entry	      number of particle    I*4
c	2.entry       number of layer       I*4
c	3.-5.entry    x,y,z                 R*4
c	6.-8.entry    px,py,pz              R*4
c	9.entry       kinetic energy        R*4
c	10.entry      theta                 R*4
c	11.entry      phi                   R*4
c	12.entry      total time            R*4
c
c	later these data should be written to a NTUPLE to be analysed using PAW
c
c	TP, PSI, 2-jun-1995
c	
c-----------------------------------------------------------------------------------------------
c
	implicit none				 ! inquire variable definition
c
	integer*4   lun				 ! logical unit for data file
	integer*4   id, il                       ! particle and layer number
	real*4      x,y,z,px,py,pz,ekin          ! location, momentum and energy of particle
	real*4      theta, phi,time              ! direction angulars and total time of
c						 ! flight
c
c-----------------------------------------------------------------------------------------------	
c
	write(6,*) 'Enter the LUN of the MCV3K output file : '
	read(5,*) lun
	do while ( .true. ) 
	  read(lun,err=11,end=10) id,il,x,y,z,px,py,pz,ekin,theta,phi,time
	  write(6,'(I6,2x,I3,10f10.3)') id,il,x,y,z,px,py,pz,
     1                                    ekin,theta,phi,time

c
	enddo
10	write(6,'('' Succesfully read data from LUN '',i)')  lun
	call exit
11	write(6,'('' Error during reading...'')')
	call exit
	end
c
c------------------------------------------------------------------------------------------------
c
c 	EOF READ_MCV3K_OUT.FOR
